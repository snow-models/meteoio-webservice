# SPDX-License-Identifier: AGPL-3.0-or-later
import re
from typing import Optional

import oic.exception
from oic import rndstr
from oic.oic import Client
from oic.oic.message import RegistrationResponse, AuthorizationResponse, IdToken
from oic.utils.authn.client import CLIENT_AUTHN_METHOD
from pydantic import SecretStr
from pydantic.dataclasses import dataclass

from .models.errors import AuthException, UserReportableException


@dataclass(frozen=True)
class LoginSession:
    state: str
    auth_request_json: str
    login_url: str
    final_redirect_url: str


@dataclass(frozen=True)
class VerifiedSubject:
    sub: str
    full_name: Optional[str] = None
    """Some attribute that can be used as full, display name."""


@dataclass(frozen=True)
class OicConnectionSetting:
    client_id: str
    client_secret: SecretStr
    issuer: str
    """URL of the issuer, like https://sandbox.orcid.org/ """
    login_button_label: str


OIC_CONFIG_KEY_RE = re.compile('^[a-zA-Z0-9]{1,6}$')


@dataclass(frozen=True)
class Connection:
    client_id: str
    client_secret: str
    redirect_uri: str = "https://developers.google.com/oauthplayground"
    issuer: str = "https://sandbox.orcid.org/"

    def _get_client(self) -> Client:
        client = Client(client_authn_method=CLIENT_AUTHN_METHOD)
        client.store_registration_info(RegistrationResponse(
            client_id=self.client_id,
            client_secret=self.client_secret,
            redirect_uris=[self.redirect_uri],
        ))
        # client.allow['issuer_mismatch'] = True
        # The above line would be a workaround for possible errors raising from the following line
        try:
            client.provider_config(self.issuer)
        except oic.exception.PyoidcError as err:
            if 'provider info issuer mismatch' in str(err):
                # This is possible with Microsoft: login.microsoftonline.com -> sts.windows.net
                raise UserReportableException('OpenID Connect - provider info issuer mismatch.')
            else:
                raise
        client.redirect_uris = [self.redirect_uri]
        return client

    def start_login(self, final_redirect_url: str) -> LoginSession:
        client = self._get_client()
        state = rndstr(32)
        args = {
            "client_id": client.client_id,
            "response_type": "code",
            "scope": ["openid"],
            "nonce": rndstr(32),
            "state": state,
            "redirect_uri": [self.redirect_uri],
        }
        auth_req = client.construct_AuthorizationRequest(request_args=args)
        login_url = auth_req.request(client.authorization_endpoint)
        return LoginSession(
            state=state,
            auth_request_json=auth_req.to_json(),
            login_url=login_url,
            final_redirect_url=final_redirect_url,
        )

    def verify_login(self, session: LoginSession, query_string: str) -> VerifiedSubject:
        client = self._get_client()
        auth_resp = client.parse_response(AuthorizationResponse, info=query_string, sformat="urlencoded")

        if not auth_resp["state"] == session.state:
            raise AuthException

        atr = client.do_access_token_request(
            state=auth_resp["state"],
            request_args={"code": auth_resp["code"], "scope": ["openid"]},
            authn_method="client_secret_basic",
            skew=3600 * 3,  # trying to patch IATError (id_token "issued in the future").
        )
        # NOTE: the above call was observed doing some verification of an id_token (JWT).
        # NOTE: the above call stores the access grant internally for the following request.
        # userinfo = client.do_user_info_request(state=auth_resp["state"])

        id_token: IdToken = atr['id_token']
        id_token.verify()

        return VerifiedSubject(
            # sub=userinfo["sub"]
            id_token['sub'],
            full_name=_get_full_name(id_token)
        )

# TODO?: maybe get rid of pyoic and just run the http requests (it could also be async)


def _get_full_name(id_token: IdToken) -> Optional[str]:
    """
    The only mandatory claim is 'sub'.
    The presence of some full name in the ID token depends on the provider.
    """
    if 'name' in id_token:
        # Microsoft
        return id_token['name']
    if 'given_name' in id_token and 'family_name' in id_token:
        # ORCID iD
        return id_token['given_name'] + ' ' + id_token['family_name']

    return None
