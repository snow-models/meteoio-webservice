# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import random
import time

from apscheduler.schedulers.blocking import BlockingScheduler

from ..app.entity_manager import get_entity_manager

LOGGER = logging.getLogger(__name__)


def main():
    # noinspection PyBroadException
    try:
        entity_manager = get_entity_manager()

        scheduler = entity_manager.datasets.scheduler
        assert isinstance(scheduler, BlockingScheduler), 'It must be a scheduler that runs in a separate thread.'

        _is_started = False

        for _ in range(6):
            LOGGER.info(f'Refreshing the scheduler...')
            entity_manager.datasets.update_scheduling_all()

            _max_time = 600 + random.uniform(-120, 0)
            LOGGER.info(
                f'{"Resume" if _is_started else "Started"} the datasets cron scheduler (will refresh in {round(_max_time / 60):d} minutes)...')
            if not _is_started:
                scheduler.start()
                _is_started = True
            else:
                scheduler.resume()

            time.sleep(_max_time)

            scheduler.pause()

        LOGGER.info(f'Stopping the datasets cron scheduler for a clean restart...')
        scheduler.shutdown(wait=True)
        time.sleep(5)

        LOGGER.info(f'Letting this process die for a clean restart...')
    except:
        LOGGER.exception('Unexpected failure, will restart')
        exit(-1)