# SPDX-License-Identifier: AGPL-3.0-or-later
import base64
from functools import cache
from pathlib import Path
from typing_extensions import TypedDict

from pydantic import BaseModel, Field, field_validator


@cache
def get_default_logo() -> str:
    try:
        with open(Path(__file__).parent / 'default_logo.png', "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read()).decode('utf-8')
            data_url = f"data:image/png;base64,{encoded_string}"
        return data_url
    except:
        return ''


BrandColors = TypedDict('BrandColors', {
    "10": str,
    "20": str,
    "30": str,
    "40": str,
    "50": str,
    "60": str,
    "70": str,
    "80": str,
    "90": str,
    "100": str,
    "110": str,
    "120": str,
    "130": str,
    "140": str,
    "150": str,
    "160": str,
})

class AppCustomizationSettingsInstance(BaseModel):
    app_short_title: str = Field(default="MeteoIO Web Service")

    app_long_title: str = Field(default="METEOROLOGICAL DATA OWNERS PLATFORM")
    app_subtitle: str = Field(default="SUPPORTING TIME SERIES PROCESSING WITH METEOIO")

    app_logo_url: str = Field(default_factory=get_default_logo)

    welcome_guests: str = Field(default=(
        "Authorized users can create their online workspace, upload their datasets and MeteoIO configurations, "
        "keep a revisions log and schedule cron jobs. "
        "\n\n"
        "As a guest, you will be able to view published datasets or, after ORCID Login, submit jobs to process data "
        "with MeteoIO and have the result temporarily available for you to download. "
        "\n\n"
        "As a new user, you are definitely welcomed to have a look at the [documentation](docs/)!"
    ))

    welcome_restricted_internal: str = Field(default=(
        "Authorized users can create their online workspace, upload their datasets and MeteoIO configurations, "
        "keep a revisions log and schedule cron jobs. "
        "\n\n"
        "With your account, you can submit jobs to process data with MeteoIO and have the result temporarily available "
        "for you to download."
    ))

    welcome_data_owners: str = Field(default=(
        "Create your online workspace, upload your datasets and MeteoIO configurations, keep a revisions log and "
        "schedule cron jobs. "
        "\n\n"
        "You can also submit guest jobs to process data with MeteoIO and have the result temporarily available for you "
        "to download."
    ))

    brand_colors: BrandColors = Field(default_factory=lambda: {
        # See BrandVariants from npm package "@fluentui/react-theme" v9.1.9
        "10": "#0e334a",
        "20": "#113c58",
        "30": "#134565",
        "40": "#05adcb",  # colorBrandForegroundLinkPressed, dataset Handle
        "50": "#175277",
        "60": "#18577e",
        "70": "#195b85",  # TopBar text, Home page title, subtle buttons text
        "80": "#22638c",  # primary Buttons, control labels
        "90": "#2a6b93",
        "100": "#4480a5",
        "110": "#5d94b6",
        "120": "#81adc8",
        "130": "#a5c6da",
        "140": "#b9d3e3",  # secondary button border (and spinner background track)
        "150": "#cde0eb",
        "160": "#e0ecf3",
    })

    @field_validator('brand_colors')
    def _validate_brand_colors(cls, value: dict):
        for code in ["10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "110", "120", "130", "140", "150", "160"]:
            assert code in value, f'`{code}` must be in brand_colors'
        return value
