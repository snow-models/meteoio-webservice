# SPDX-License-Identifier: AGPL-3.0-or-later
from pathlib import Path

import anyio
from pydantic.dataclasses import dataclass

from .instance import AppCustomizationSettingsInstance


@dataclass(frozen=True)
class GeneralAppSettingsManager:
    file_path: Path

    def read(self) -> AppCustomizationSettingsInstance:
        if self.file_path.exists():
            return AppCustomizationSettingsInstance.model_validate_json(self.file_path.read_text(encoding='utf-8'), strict=False)
        else:
            return AppCustomizationSettingsInstance()

    async def read_async(self) -> AppCustomizationSettingsInstance:
        _path = anyio.Path(self.file_path)
        if await _path.exists():
            return AppCustomizationSettingsInstance.model_validate_json(await _path.read_text(encoding='utf-8'), strict=False)
        else:
            return AppCustomizationSettingsInstance()

    def write(self, settings: AppCustomizationSettingsInstance) -> None:
        settings = AppCustomizationSettingsInstance.model_validate(settings, strict=True)
        json = settings.model_dump_json(indent=2)
        self.file_path.write_text(json, encoding='utf-8')

    # NOTE: the following method is not used -- there's a caching mechanism from the web framework
    # def read_with_cache(self, *, max_age_seconds: float = 5) -> AppCustomizationSettingsInstance:
    #     # NOTE: due to multiprocessing, it's not useful to update the in-process cache in the write method.
    #     now = datetime.datetime.now(tz=datetime.timezone.utc)
    #     if hasattr(self, '_cached_at') and hasattr(self, '_cached_instance'):
    #         cached_at: datetime.datetime = getattr(self, '_cached_at')
    #         if (now - cached_at).total_seconds() < max_age_seconds:
    #             return getattr(self, '_cached_instance')
    #     _instance = self.read()
    #     setattr(self, '_cached_at', now)
    #     setattr(self, '_cached_instance', _instance)
    #     return _instance
