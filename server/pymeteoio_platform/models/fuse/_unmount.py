# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import subprocess
from pathlib import Path
from typing import Union

from .exceptions import FuseUnmountException

logger = logging.getLogger(__name__)


def unmount(local_mount_point: Union[Path, str]):
    local_mount_point = Path(local_mount_point)
    try:
        if not local_mount_point.is_mount():
            raise FuseUnmountException('Path is not a mount')
    except OSError:
        logger.exception('Could not check is_mount before unmount. Will try to continue...')
        pass
    try:
        # Attempt to unmount using fusermount or umount
        subprocess.run(
            ['fusermount', '-u', str(local_mount_point.absolute())],
            check=True, text=True, capture_output=True
        )
        logger.debug(f"Unmounted {local_mount_point} successfully.")
    except subprocess.CalledProcessError as e:
        # Capture the return code and stderr to raise a detailed exception
        raise FuseUnmountException(f"Unmounting failed with return code {e.returncode}: {e.stderr.strip()}") from e
    except FileNotFoundError as e:
        raise FuseUnmountException(f"fusermount command not found: {e.filename}. Ensure FUSE is installed.") from e
