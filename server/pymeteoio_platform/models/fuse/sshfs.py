# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import subprocess
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Literal

from pydantic import Field, SecretStr, FilePath

from ._abs import AbstractFuseMountConfig, AbstractFuseMountRuntime
from ._check import check_fuse_mount
from ._unmount import unmount
from .exceptions import FuseMountException, FuseUnmountException

logger = logging.getLogger(__name__)


class SSHFSMountConfig(AbstractFuseMountConfig):
    driver: Literal['sshfs']
    remote_host: str = Field()
    remote_port: int = Field(default=22)
    remote_path: str = Field(default='~')
    username: str = Field()
    password: Optional[SecretStr] = Field(default=None)
    identity_file_path: Optional[FilePath] = Field(default=None)

    def mount(self, local_mount_root: Path) -> 'SSHFSMountRuntime':
        local_mount_point = self._mk_local_mount_point(local_mount_root)

        try:

            # Building the sshfs command
            sshfs_command = [
                "sshfs",
                f"{self.username}@{self.remote_host}:{self.remote_path}",
                str(local_mount_point),
                "-p", str(self.remote_port),  # Specify the port to use
                '-o', "allow_other",  # Allow non-privileged users to access the mount
                '-o', "reconnect",
                '-o', "ServerAliveInterval=5", '-o', "ServerAliveCountMax=6",  # 5 * 6 = 30 seconds total timeout
                '-o', "StrictHostKeyChecking=no",  # Automatically accept host key
                '-o', "UserKnownHostsFile=/dev/null",  # Don't store host key
                "-o", "umask=0000",
                # "-v"
            ]

            # If a path to private key in container is provided, use it
            if self.identity_file_path:
                sshfs_command = [
                    *sshfs_command,
                    '-o', f"IdentityFile={self.identity_file_path}",
                ]

            # # If a password is provided, use 'sshpass' to pass the password to sshfs
            # if self.password:
            #     sshfs_command = [
            #         "sshpass",
            #         "-e",  # take password from env
            #         *sshfs_command,
            #     ]
            #     env['SSHPASS'] = self.password.get_secret_value()

            # If a password is provided, give it to stdin
            if self.password:
                sshfs_command = [
                    *sshfs_command,
                    '-o', 'password_stdin'
                ]

            # logger.info(' '.join(sshfs_command))

            # Running the sshfs command and capturing stdout and stderr
            pipe = subprocess.Popen(
                sshfs_command,
                env=dict(),
                stdin=subprocess.PIPE,
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
                text=True,
            )
            logger.debug(f"Mounting {self.remote_path} from {self.remote_host} to {str(local_mount_point)} ...")

            if self.password:
                try:
                    pipe.communicate(input=self.password.get_secret_value(), timeout=1)
                except subprocess.TimeoutExpired:
                    pass
            else:
                try:
                    pipe.communicate(timeout=1)
                except subprocess.TimeoutExpired:
                    pass

            check_fuse_mount(local_mount_point=local_mount_point)

            return SSHFSMountRuntime(
                pipe=pipe,
                mount_dir=local_mount_point,
            )

        except subprocess.TimeoutExpired as e:
            raise FuseMountException(f"Timeout while trying to mount from {self.remote_host}") from e
        except subprocess.CalledProcessError as e:
            # logger.error(f"Command '{e.cmd}' returned non-zero exit status {e.returncode}.")
            # logger.error(f"Standard output: {e.stdout}")
            # logger.error(f"Standard error: {e.stderr}")

            # if e.stderr and e.returncode == 3:
            #     raise FuseMountException(f"Error: {e.stderr}")

            # Handling specific sshfs or sshpass exit codes TODO: review return codes
            # if e.returncode == 1:
            #     raise FuseMountException("General error in file operations.") from e
            # elif e.returncode == 2:
            #     raise FuseMountException("SSH connection failure. Please check your credentials and network.") from e
            # elif e.returncode == 4:
            #     raise FuseMountException("Unable to mount. Check mount options or permissions.") from e
            # elif e.returncode == 8:
            #     raise FuseMountException(
            #         "Fuse error. Ensure FUSE is installed and you have the necessary permissions.") from e
            # elif e.returncode == 255:
            #     raise FuseMountException(
            #         "SSH error. Check SSH connection, host key verification, or authentication.") from e
            # else:
            raise FuseMountException(f"An error occurred with exit code {e.returncode}: {e.stderr}") from e

        except FileNotFoundError as e:
            raise FuseMountException(f"Command not found: {e.filename}. Is sshfs or sshpass installed?") from e

        except PermissionError as e:
            raise FuseMountException(f"Permission denied: {e}. Are you running with sufficient privileges?") from e


@dataclass(frozen=True)
class SSHFSMountRuntime(AbstractFuseMountRuntime):
    pipe: subprocess.Popen
    mount_dir: Path

    def unmount(self) -> None:
        unmount(self.mount_dir)
        for _ in range(3):
            try:
                self.pipe.wait(5)
            except subprocess.TimeoutExpired:
                self.pipe.terminate()
            else:
                return
        raise FuseUnmountException('Unable to terminate fuse session.')
