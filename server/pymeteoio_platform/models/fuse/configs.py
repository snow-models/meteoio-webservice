# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
from dataclasses import dataclass
from pathlib import Path
from typing import List, Union, Dict

from pydantic import BaseModel, Field

from ._abs import AbstractFuseMountRuntime
from .sshfs import SSHFSMountConfig
from .exceptions import FuseUnmountException, FuseMountException

_LOGGER = logging.getLogger(__name__)

FuseMountConfigUnion = Union[
    SSHFSMountConfig,
]


class FuseMountListConfig(BaseModel):
    mounts: Dict[str, FuseMountConfigUnion] = Field(default_factory=dict, discriminator='driver')

    def mount(self, local_mount_root: Path) -> 'FuseMountListRuntime':
        if len(self.mounts) <= 0:
            _LOGGER.info(f'No fuse configurations to be mounted.')
            return FuseMountListRuntime(mount_runtimes=[])

        mount_runtimes: List[AbstractFuseMountRuntime] = list()

        _LOGGER.debug(f'Mounting {len(self.mounts)} fuse configurations...')
        try:
            for mount in self.mounts.values():
                mount_runtimes.append(mount.mount(local_mount_root))

        except FuseMountException as ex:
            _LOGGER.exception('Fuse mount failed. Unmounting before re-raising the exception...')
            _so_far = FuseMountListRuntime(mount_runtimes=mount_runtimes)
            _so_far.unmount()  # Compensate before bailing out previous mount exception
            raise ex

        _LOGGER.info(f'Successfully mounted {len(self.mounts)} fuse configurations.')
        return FuseMountListRuntime(mount_runtimes=mount_runtimes)


@dataclass(frozen=True)
class FuseMountListRuntime(AbstractFuseMountRuntime):
    mount_runtimes: List[AbstractFuseMountRuntime]

    def unmount(self) -> None:
        if len(self.mount_runtimes) <= 0:
            _LOGGER.info(f'No mount points to be unmounted.')
            return

        _exs: List[FuseUnmountException] = list()
        _ok: int = 0
        for mp in self.mount_runtimes:
            try:
                mp.unmount()
            except FuseUnmountException as ex:
                _LOGGER.exception('Fuse unmount failed')
                _exs.append(ex)
            else:
                _ok += 1

        _LOGGER.info(f'Unmounted {_ok} out of {len(self.mount_runtimes)} fuse configurations.')
        if len(_exs) > 0:
            raise FuseUnmountException(_exs)
