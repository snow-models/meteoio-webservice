# SPDX-License-Identifier: AGPL-3.0-or-later

class FuseException(RuntimeError):
    pass


class FuseMountException(FuseException):
    pass


class FuseUnmountException(FuseException):
    pass
