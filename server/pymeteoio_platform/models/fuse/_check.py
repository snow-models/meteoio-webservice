import logging
import subprocess
import time
from pathlib import Path
from typing import Union

_LOGGER = logging.getLogger(__name__)


def check_fuse_mount(
    *,
    local_mount_point: Union[Path, str],
    raise_on_exceptions: bool = False,
    n_attempts: int = 20,
    timeout: float = 30.
) -> None:
    """
    Run fusermount -q , which returns 0 iff it finds the FUSE mount point.
    Retries to allow mounting to take place in a matter of seconds in a concurrent process.
    """
    _start_time = time.monotonic()
    for attempts_left in reversed(range(n_attempts)):
        try:
            _elapsed = time.monotonic() - _start_time
            subprocess.check_output(
                ['fusermount', '-q', str(local_mount_point)],
                timeout=max(timeout - _elapsed, 1.),
            )
        except (subprocess.TimeoutExpired, subprocess.CalledProcessError, FileNotFoundError, PermissionError):
            _elapsed = time.monotonic() - _start_time
            if attempts_left > 0 and _elapsed < timeout:
                time.sleep(1.)
            else:
                if raise_on_exceptions:
                    raise
                else:
                    _LOGGER.exception(f"Unable to check mount at {str(local_mount_point)}")
        else:
            break
