# SPDX-License-Identifier: AGPL-3.0-or-later
from dataclasses import dataclass
from pathlib import Path

from pydantic import BaseModel, Field, field_validator


class AbstractFuseMountConfig(BaseModel):
    driver: str
    local_mount_sub_path: str = Field(default='mnt')

    # noinspection PyNestedDecorators
    @field_validator('local_mount_sub_path')
    @classmethod
    def __ensure_local_mount_sub_path_is_not_absolute(cls, v: str):
        if v.startswith('/'):
            raise ValueError('local_mount_sub_path must not be absolute')
        return v

    def _mk_local_mount_point(self, local_mount_root: Path) -> Path:
        local_mount_point = local_mount_root / self.local_mount_sub_path
        assert local_mount_point.is_relative_to(local_mount_root), \
            'Config validation must ensure local_mount_sub_path is relative.'

        # Ensure the local mount point exists
        local_mount_point.mkdir(exist_ok=True, parents=True)

        return local_mount_point.absolute()

    def mount(self, local_mount_root: Path) -> 'AbstractFuseMountRuntime':
        """
        :param local_mount_root: base for the mount point, where local_mount_sub_path is relative.
        :return: The runtime object that allows un-mount
        """
        raise NotImplementedError


@dataclass(frozen=True)
class AbstractFuseMountRuntime:

    def unmount(self) -> None:
        raise NotImplementedError