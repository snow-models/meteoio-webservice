import logging
import re
import uuid
from typing import Optional, Iterable

import ldap3
import ldap3.core.exceptions
from pydantic import BaseModel, Field

from .config import LdapConfig
from ..errors import AuthException, UserReportableException


class LdapAuthException(AuthException):
    pass


class LdapSearchException(UserReportableException):
    pass


_SEARCH_OBJ_CLASS_RE = re.compile(r'[a-zA-Z0-9_.]+')
_SEARCH_QUERY_RE = re.compile(r'[a-zA-Z0-9_.]+')

_logger = logging.getLogger(__name__)


class SearchResultEntry(BaseModel):
    unique_id: uuid.UUID = Field(description="The objectGUID or entryUUID of the LDAP entry")
    display_name: Optional[str] = Field(None, description="The display name of the LDAP entry")

class LdapLoginForm(BaseModel):
    username: str
    password: str
    user_domain: Optional[str] = None


class AuthServer(BaseModel):
    config: LdapConfig = Field(description="Configuration for the LDAP server")

    def _get_server(self) -> ldap3.Server:
        server = ldap3.Server(self.config.url, get_info=ldap3.NONE)
        return server

    def _get_connection(self, user_dn: str, password: str) -> ldap3.Connection:
        server = self._get_server()
        return ldap3.Connection(
            server,
            user=user_dn,
            password=password,
            raise_exceptions=True
        )

    def authenticate(self, data: LdapLoginForm) -> SearchResultEntry:
        username = data.username
        password = data.password
        user_domain = data.user_domain
        if user_domain is None:
            user_domain = self.config.default_user_domain
            if user_domain is None:
                _logger.warning("No user domain provided and no default user domain configured.")
                raise LdapAuthException("No user domain provided and no default user domain configured.")
        _user_dn = f"cn={username},ou={user_domain},{self.config.base_dn}"
        _logger.info(f"Attempting to authenticate user {_user_dn}.")
        conn = self._get_connection(_user_dn, password)
        try:
            if not conn.bind():
                raise LdapAuthException("Failed LDAP bind for user authentication.")
        except ldap3.core.exceptions.LDAPException as e:
            raise LdapAuthException("Failed LDAP bind for user authentication.") from e
        else:
            _logger.info(f"Authentication success for user {_user_dn}. Going to search for user information...")
            try:
                if self.config.use_search_user_after_login:
                    conn.unbind()
                    conn = self._get_connection(
                        self.config.search_user, self.config.search_user_password.get_secret_value())
                    if not conn.bind():
                        raise LdapAuthException("Failed LDAP bind for service user after login.")
                conn.search(
                    search_base=f"ou={user_domain},{self.config.base_dn}",
                    search_filter=f"(cn={username})",
                    # search_filter=f"(&(cn={username})(ou={user_domain}))",
                    search_scope=ldap3.SUBTREE,
                    attributes=ldap3.ALL_ATTRIBUTES,  # NOTE: all because of errors if a given attribute is not recognized.
                )
                for entry in conn.entries:
                    _logger.info(f"Got information for user {_user_dn}.")
                    return self._format_entry(entry)
                raise LdapSearchException("LDAP search gave no results.")
            except ldap3.core.exceptions.LDAPException as e:
                raise LdapSearchException("Failed LDAP search for user info after login.") from e
        finally:
            conn.unbind()

    def search(self, query: str, object_class: Optional[str] = None) -> Iterable[SearchResultEntry]:
        assert object_class is None or _SEARCH_OBJ_CLASS_RE.match(object_class), \
            "The object class can only contain alphanumeric characters and underscores, if not None"
        assert _SEARCH_QUERY_RE.match(query), \
            "The query can only contain alphanumeric characters and underscores"
        assert len(query) >= 3, "The query must contain at least 3 characters"

        assert self.config.search_user and self.config.search_user_password, \
            "The service user and password must be set in the configuration for LDAP searches."

        try:
            conn = self._get_connection(self.config.search_user, self.config.search_user_password.get_secret_value())
            if not conn.bind():
                raise LdapAuthException("Failed LDAP bind for service user for search.")

            search_filter = f"(|(cn=*{query}*)(displayName=*{query}*))"
            if object_class:
                search_filter = f"(&(objectClass={object_class}){search_filter})"

            conn.search(
                search_base=self.config.base_dn,
                search_filter=search_filter,
                search_scope=ldap3.SUBTREE,
                attributes=ldap3.ALL_ATTRIBUTES,  # NOTE: all because of errors if a given attribute is not recognized.
            )

            for entry in conn.entries:
                yield self._format_entry(entry)

        except ldap3.core.exceptions.LDAPException as e:
            raise LdapSearchException(e)

    def _format_entry(self, entry: ldap3.Entry) -> 'SearchResultEntry':
        # Check for objectGUID (AD)
        if 'objectGUID' in entry:
            unique_id = uuid.UUID(bytes_le=entry.objectGUID.raw_values[0])

        # Check for entryUUID (OpenLDAP, 389 Directory Server, etc.)
        elif 'entryUUID' in entry:
            unique_id = uuid.UUID(hex=entry.entryUUID.value)

        # Handle case where neither attribute is available
        else:
            raise LdapSearchException('No unique identifier found in LDAP entry.')

        # Check for display name attribute options
        for _attr in self.config.display_name_attribute:
            if _attr in entry:
                display_name = entry[_attr].value
                break
        else:
            _logger.warning(f"No configured display name attribute found in LDAP entry.")
            display_name = None

        return SearchResultEntry(
            unique_id=unique_id,
            display_name=display_name,
        )
