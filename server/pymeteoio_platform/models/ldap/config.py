from typing import Optional, List

from pydantic import Field, BaseModel, SecretStr, field_validator, model_validator


class LdapConfig(BaseModel):
    url: str = Field(description="The URL of the LDAP server")
    base_dn: str = Field(description="Base DN for LDAP users login and searches")
    search_user: Optional[str] = Field(None, description="Service account dn for LDAP user information search")
    search_user_password: Optional[SecretStr] = Field(None, description="Service account password")
    use_search_user_after_login: bool = Field(False, description=(
        "If True, user information will be searched using the service account; "
        "if False, user information will be searched using the login user itself."))
    default_user_domain: Optional[str] = Field(None, description="Default user domain (organization unit)")
    display_name_attribute: List[str] = Field(
        default_factory=lambda: ['displayName', 'name', 'cn', 'sAMAccountName'],
        description="The LDAP attributes used for display name, in order of preference")

    # noinspection PyNestedDecorators
    @field_validator('display_name_attribute')
    @classmethod
    def _validate_display_name_attribute(cls, v: List[str]) -> List[str]:
        if len(v) <= 0:
            raise ValueError("display_name_attribute must not be empty")
        return v

    @model_validator(mode='after')
    def _validate_use_search_user_after_login(self):
        if self.use_search_user_after_login:
            if not (self.search_user and self.search_user_password):
                raise ValueError("The service user and password must be set in the configuration for LDAP searches.")
        return self
