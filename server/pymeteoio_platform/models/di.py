# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Some models may depend on a global entity manager for simplicity.
We support this possibility via the usual notion of Dependency Injection.
"""
from threading import Lock
from typing import Optional

from . import EntityManager

__global_entity_manager: Optional[EntityManager] = None
__global_entity_manager_lock: Lock = Lock()


def get_global_entity_manager_lock() -> Lock:
    return __global_entity_manager_lock


def set_global_entity_manager(em: EntityManager):
    global __global_entity_manager
    if __global_entity_manager:
        raise RuntimeError('EntityManager dependency has already been injected.')
    __global_entity_manager = em


def has_global_entity_manager() -> bool:
    return __global_entity_manager is not None


def get_global_entity_manager() -> EntityManager:
    if not __global_entity_manager:
        raise RuntimeError('EntityManager dependency has not been injected.')
    return __global_entity_manager
