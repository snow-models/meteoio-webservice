# SPDX-License-Identifier: AGPL-3.0-or-later
from contextlib import contextmanager
from functools import cached_property
from typing import ContextManager

from pydantic.dataclasses import dataclass
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from .entities import tables_registry


@dataclass(frozen=True)
class UsersStore:
    """
    Examples:::
        us = UsersStore("sqlite:///")

        with us.transaction() as tx:
            u = User()
            tx.add(u)

        with us.transaction() as tx:
            u = tx.get(User, _id)
            u.full_name = "Example User"

    Also take a look at:
    https://docs.sqlalchemy.org/en/20/orm/session_state_management.html

    Also consider `from sqlalchemy.exc import InvalidRequestError, IntegrityError`
    """

    url: str

    @cached_property
    def _session_maker(self) -> sessionmaker:
        e = create_engine(url=self.url)
        tables_registry.metadata.create_all(e)
        return sessionmaker(e, autoflush=False, expire_on_commit=True)

    @contextmanager
    def transaction(self) -> ContextManager[Session]:
        s = self._session_maker()
        with s.begin():
            yield s

    @cached_property
    def cli(self):
        """Helper property that simplifies instantiation of UsersCLI from CLI tool."""
        from .cli import UsersCLI  # NOTE: using local import to avoid circular dependency.
        return UsersCLI(self)
