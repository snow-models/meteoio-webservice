# SPDX-License-Identifier: AGPL-3.0-or-later
from dataclasses import field
from typing import Optional, List, Annotated

from pydantic import SecretStr, StringConstraints
from pydantic.dataclasses import dataclass

EmailStr = StringConstraints(strip_whitespace=True, to_lower=True, min_length=3, max_length=200)


@dataclass(frozen=True)
class AskPasswordResetDto:
    """A user may ask to start a password reset procedure.
        What they need is their email only."""
    email: Annotated[str, EmailStr]


@dataclass(frozen=True)
class DoPasswordResetDto:
    """A user may do reset their password after asking for a secret verification code
        which they should have received via email or combined channels."""
    email: Annotated[str, EmailStr]
    secret_verification_code: Annotated[str, SecretStr]
    new_secret_password: Annotated[str, SecretStr]


@dataclass(frozen=True)
class EmailPasswordLoginDto:
    """A user may wish to log into the platform."""
    email: Annotated[str, EmailStr]
    secret_password: Annotated[str, SecretStr]


@dataclass(frozen=True)
class UserProfileInputDto:
    """Some information about a user may be editable."""
    full_name: Annotated[str, StringConstraints(strip_whitespace=True, max_length=1000)]


@dataclass(frozen=True)
class UserOidReflection:
    issuer: str
    subject: str


@dataclass(frozen=True)
class UserReflection:
    id: Optional[str] = None
    email: Optional[str] = None
    full_name: Optional[str] = None
    has_password: bool = False
    is_logged_in: bool = False
    openid: List[UserOidReflection] = field(default_factory=list)
    can_create_datasets: bool = False
    is_admin: bool = False


@dataclass(frozen=True)
class SSHAuthorizedKey:
    public_key: str
    public_title: Optional[str] = ''
    entry_id: Optional[str] = None
    insert_date: Optional[str] = None
    fingerprint: Optional[str] = None
