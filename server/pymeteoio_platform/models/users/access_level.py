import enum
from typing import Optional

from pymeteoio_platform.models.users import User


class AccessLevel(enum.Enum):
    """Derived, run-time description of the access level of the User in the platform."""

    GUEST = 'guest'
    AUTHENTICATED = 'authenticated'
    DATA_OWNER = 'data_owner'

    @staticmethod
    def for_user(user: Optional[User]) -> 'AccessLevel':
        level = AccessLevel.GUEST
        if user is not None and isinstance(user, User):
            level = AccessLevel.AUTHENTICATED
            if user.can_create_datasets:
                level = AccessLevel.DATA_OWNER
        return level