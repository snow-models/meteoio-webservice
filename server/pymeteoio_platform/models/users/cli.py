# SPDX-License-Identifier: AGPL-3.0-or-later
from dataclasses import dataclass, field
from typing import Iterable, Optional, List

from sqlalchemy import select

try:
    from rich.console import Console
    from rich.table import Table
except ImportError:
    Console = None
    Table = None

from . import UsersStore
from .entities import User


@dataclass(frozen=True)
class UsersCLI:
    store: UsersStore
    console: Optional[Console] = field(default_factory=(lambda: None) if Console is None else Console)

    def _print_users(self, users: Iterable[User], title: Optional[str] = None) -> None:
        if self.console and self.console.is_terminal:
            table = Table(title=title)
            table.add_column("ID", no_wrap=True)
            table.add_column("Email")
            table.add_column("N. OpenID identifiers")
            for user in users:
                table.add_row(user.id, user.email if user.has_email else "",
                              str(len(list(user.openid_identifiers or []))))
            self.console.print(table)
        else:
            if title:
                print(title)
            for user in users:
                print(user.id)

    def add_with_email(self, email: str) -> str:
        with self.store.transaction() as tx:
            user = User(email=email)
            tx.add(user)
            tx.flush()
            tx.refresh(user)
            self._print_users([user], 'Created user')
            return user.id

    def find_by_email(self, email: str):
        with self.store.transaction() as tx:
            count: int = 0
            users: List[User] = []
            for user, in tx.execute(select(User).filter(User.email == email)).unique():
                users += [user]
                count += 1
            self._print_users(users, f'Found {count} users with given email.')

    def find_admins(self):
        with self.store.transaction() as tx:
            count: int = 0
            users: List[User] = []
            for user, in tx.execute(select(User).filter(User.attributes.contains('is_admin'))).unique():
                if user.is_admin:
                    users += [user]
                    count += 1
            self._print_users(users, f'Found {count} admin users.')

    def set_password(self, user_id: str, clear_text_password: str):
        with self.store.transaction() as tx:
            user: User = tx.get(User, user_id)
            user.password_hash = user.make_new_hash(clear_text_password)
            print(f'Password set successfully.')

    def set_is_admin(self, user_id: str, is_admin: bool):
        with self.store.transaction() as tx:
            user: User = tx.get(User, user_id)
            user.set_is_admin(is_admin)
            print(f'User admin access updated.')
