# SPDX-License-Identifier: AGPL-3.0-or-later
import datetime
import json
from typing import Optional, List, Dict, Union

import uuid6
from passlib.hash import bcrypt
from pydantic.json import pydantic_encoder
from pydantic.tools import parse_obj_as
from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, registry, mapped_column, relationship

from .dtos import SSHAuthorizedKey
from ..errors import AuthenticationFailure

tables_registry = registry()

AttributeValueElemT = Union[str, float, bool, None]
AttributeValueT = Union[AttributeValueElemT, List[AttributeValueElemT], Dict[str, AttributeValueElemT]]
AttributesT = Dict[str, AttributeValueT]


@tables_registry.mapped_as_dataclass
class User:
    __tablename__ = "users"

    id: Mapped[str] = mapped_column(
        primary_key=True, default_factory=lambda: str(uuid6.uuid7()))

    email: Mapped[Optional[str]] = mapped_column(
        nullable=False, unique=True, default_factory=lambda: f"null-{str(uuid6.uuid7())}@null")

    full_name: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    created_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default_factory=lambda: datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc))

    password_hash: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    reset_asked_at: Mapped[Optional[datetime.datetime]] = mapped_column(
        nullable=True, default=None)

    reset_verification_hash: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    openid_identifiers: Mapped[List['OpenIDIdentifier']] = relationship(
        back_populates='user', default_factory=list, lazy='joined', cascade='all')

    attributes: Mapped[str] = mapped_column(
        nullable=False, default='{}')

    @property
    def _secrets_hashing(self) -> bcrypt:
        return bcrypt.using(truncate_error=True, rounds=13)

    def _verify_secret_hash(self, /, *, _hash: str, secret: str) -> None:
        """Does not return anything. Raises exception if verification fails"""
        if not _hash or not secret:
            raise ValueError  # hash and secret must never be empty during verification
        _result = self._secrets_hashing.verify(hash=_hash, secret=secret)
        if not _result:
            raise AuthenticationFailure

    def make_new_hash(self, secret: str) -> str:
        """This is for both login password and reset verification"""
        return self._secrets_hashing.hash(secret)

    def verify_password(self, secret_password: str) -> None:
        """Does not return anything. Raises exception if verification fails"""
        self._verify_secret_hash(_hash=self.password_hash, secret=secret_password)

    def verify_reset(self, secret_verification: str) -> None:
        """Does not return anything. Raises exception if verification fails"""
        self._verify_secret_hash(_hash=self.reset_verification_hash, secret=secret_verification)

    @property
    def has_email(self) -> bool:
        return self.email and not (self.email.startswith('null') and self.email.endswith('@null'))

    def get_attributes(self) -> AttributesT:
        return json.loads(self.attributes)

    def _set_attribute(self, key: str, value: AttributeValueT):
        kv = self.get_attributes()
        kv.update({key: value})
        # noinspection PyTypeChecker
        self.attributes = json.dumps(kv, sort_keys=True)

    def set_attribute(self, key: str, value: AttributeValueT) -> None:
        assert key != 'is_admin'
        self._set_attribute(key, value)

    def set_is_admin(self, is_admin: bool):
        self._set_attribute('is_admin', is_admin)

    @property
    def is_admin(self) -> bool:
        return self.get_attributes().get('is_admin', False) is True

    @property
    def can_create_datasets(self) -> bool:
        return self.get_attributes().get('can_create_datasets', False) is True

    @can_create_datasets.setter
    def can_create_datasets(self, value: bool) -> None:
        self.set_attribute('can_create_datasets', bool(value))

    @property
    def ssh_authorized_keys(self) -> List[SSHAuthorizedKey]:
        _obj_entries = self.get_attributes().get('ssh_authorized_keys', [])
        _pyd_entries: List[SSHAuthorizedKey] = []
        for _obj in _obj_entries:
            _pyd_entries.append(parse_obj_as(SSHAuthorizedKey, _obj))
        return _pyd_entries

    @ssh_authorized_keys.setter
    def ssh_authorized_keys(self, entries: List[SSHAuthorizedKey]) -> None:
        _obj = json.loads(json.dumps(entries, default=pydantic_encoder))
        self.set_attribute('ssh_authorized_keys', _obj)



@tables_registry.mapped_as_dataclass
class OpenIDIdentifier:
    __tablename__ = "openid_identifiers"
    __table_args__ = (
        UniqueConstraint("issuer", "subject"),
    )

    id: Mapped[int] = mapped_column(
        primary_key=True, autoincrement=True)

    user_id: Mapped[str] = mapped_column(
        ForeignKey('users.id'))

    issuer: Mapped[str] = mapped_column(
        nullable=False)

    subject: Mapped[str] = mapped_column(
        nullable=False)

    user: Mapped[User] = relationship(
        back_populates='openid_identifiers', lazy='joined')
