# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
from functools import cached_property
from pathlib import Path
from typing import Optional

from pydantic import DirectoryPath, validator
from pydantic.dataclasses import dataclass

from .ldap.config import LdapConfig

_LOGGER = logging.getLogger(__name__)

@dataclass(frozen=True)
class EntityManager:
    meteoio_timeseries_executable: Path  # TODO?: refactor: JobCommand instantiation from higher-level abstraction?
    external_base_url: str
    do_collect_job_strace: Optional[bool] = False

    job_dispatch_type: str = ''  # 'remote'
    job_dispatcher_host: Optional[str] = None
    job_dispatcher_port: Optional[int] = None

    # TODO?: separate the above properties so we don't rely on EntityManager and we can remove optionality below?

    data_root_directory: Optional[DirectoryPath] = None

    ldap: Optional[LdapConfig] = None

    # noinspection PyMethodParameters
    @validator('meteoio_timeseries_executable')
    def validate_meteoio_timeseries_executable(cls, value: Path):
        return value.resolve().absolute()

    @cached_property
    def dir(self) -> Path:
        if self.data_root_directory is None:
            raise RuntimeError('data_root_directory is not configured')
        return self.data_root_directory

    @cached_property
    def version_file(self):
        from .datasets.config_file import TextConfigFile
        return TextConfigFile(parent=self.dir, name='version')

    @cached_property
    def _datasets_path(self) -> Path:
        return self.dir / 'datasets'

    @cached_property
    def _public_datasets_path(self) -> Path:
        return self.dir / 'public_datasets'

    @cached_property
    def _datasets_meta_path(self) -> Path:
        return self.dir / 'datasets_meta'

    @cached_property
    def _guest_jobs_path(self) -> Path:
        return self.dir / 'guest_jobs'

    @cached_property
    def sessions_path(self) -> Path:
        return self.dir / 'sessions'

    @cached_property
    def pywps_path(self) -> Path:
        return self.dir / 'pywps'

    def get_version(self) -> str:
        return self.version_file.read().strip()

    def have_to_migrate(self) -> bool:
        return self.get_version() != '4'

    def migrate(self):
        _LOGGER.info('Running migrations...')

        if not self.version_file.path.exists():
            from .migrations import init_to_v0001
            init_to_v0001.up(self)
            self.version_file.create('1')

        if self.get_version() == '1':
            from .migrations import v0001_to_v0002
            v0001_to_v0002.up(self)
            self.version_file.update('2')

        if self.get_version() == '2':
            from .migrations import v0002_to_v0003
            v0002_to_v0003.up(self)
            self.version_file.update('3')

        if self.get_version() == '3':
            from .migrations import v0003_to_v0004
            v0003_to_v0004.up(self)
            self.version_file.update('4')

        if not self.have_to_migrate():
            _LOGGER.info('Migrated successfully.')
            return  # It's the current version!

        raise ValueError(f'Unexpected version number: `{self.get_version()}`')

    @cached_property
    def users(self):
        from .users import UsersStore
        return UsersStore(
            url=f"sqlite:///{(self.dir / 'users.sqlite').resolve().absolute()}"
        )

    @cached_property
    def datasets(self):
        # NOTE: this could be a field, but we are relying on local imports
        from .datasets import DatasetsManager
        from .guest_directories import GuestDirectoriesRoot

        return DatasetsManager(
            dir_ctrl=GuestDirectoriesRoot(root=self._datasets_path),
            public_dir=self._public_datasets_path,
            meta_dir=self._datasets_meta_path,
        )

    @cached_property
    def guest_jobs(self):
        # NOTE: this could be a field, but we are relying on local imports
        from .jobs import JobsQuotaPolicy
        from .jobs_manager import JobsManager
        from .guest_directories import GuestDirectoriesRoot

        return JobsManager(
            dir_ctrl=GuestDirectoriesRoot(root=self._guest_jobs_path),
            concurrency=1,
            quota_policy=JobsQuotaPolicy(),
            index_url=f"sqlite:///{(self.dir / 'guest_jobs.sqlite').resolve().absolute()}",
            job_dispatch_type=self.job_dispatch_type,
            job_dispatcher_host=self.job_dispatcher_host,
            job_dispatcher_port=self.job_dispatcher_port,
            do_collect_job_strace=self.do_collect_job_strace,
        )

    @cached_property
    def general_settings(self):
        from .general_settings.manager import GeneralAppSettingsManager
        return GeneralAppSettingsManager(file_path=self.dir / 'general_settings.json')

    @cached_property
    def ldap_server(self):
        from .ldap.srv import AuthServer
        if self.ldap is None:
            raise RuntimeError('LDAP is not configured')
        return AuthServer(config=self.ldap)
