# SPDX-License-Identifier: AGPL-3.0-or-later
from pathlib import Path
from typing import Optional, List

from pydantic import root_validator, model_validator
from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class Range:
    begin: Optional[str] = None
    end: Optional[str] = None  # Union[Literal['NOW'], datetime]  # Type unions may not contain more than one str-like..
    duration_days: Optional[int] = None

    # noinspection PyMethodParameters
    @model_validator(mode="before")
    def check_interval(cls, values):
        begin, end, duration = values.get('begin'), values.get('end'), values.get('duration')
        if begin is not None and end is not None and duration is not None:
            raise ValueError('begin, end, duration given; you can provide two of them.')
        return values


@dataclass(frozen=True)
class TimeSeriesJobParams:
    range: Range
    ini: str
    resolution_minutes: Optional[int] = None

    def get_command_args(self, /, *, inis_dir: Path) -> List[str]:
        ini_path = (inis_dir / self.ini).resolve()
        if not ini_path.is_relative_to(inis_dir):
            raise ValueError('Invalid ini file path')
        if not ini_path.is_file():
            raise ValueError('Specified ini path is not a file')
        ini_path = ini_path.relative_to(inis_dir)
        return [
            *(['-b', self.range.begin] if self.range.begin else []),
            *(['-e', self.range.end] if self.range.end else []),
            *(['-d', str(self.range.duration_days)] if self.range.duration_days else []),
            '-c', str(ini_path),
            *(['-s', str(self.resolution_minutes)] if self.resolution_minutes else []),
        ]
