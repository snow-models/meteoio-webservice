# SPDX-License-Identifier: AGPL-3.0-or-later
import asyncio
import functools
import json
import logging
import os
import signal
import subprocess
import time
from concurrent.futures import Executor
from datetime import datetime, timezone
from enum import Enum
from pathlib import Path
from subprocess import Popen
from threading import Thread
from time import sleep
from typing import Callable, List, Optional, AsyncIterable

from pydantic import confloat, conint, TypeAdapter
from pydantic.dataclasses import dataclass
from pydantic.json import pydantic_encoder

from .di import get_global_entity_manager, has_global_entity_manager
from .fuse.configs import FuseMountListConfig, FuseMountListRuntime
from .guest_directories import GuestDirectory

_logger = logging.getLogger(__name__)


class IllegalJobStateError(RuntimeError):
    pass


class JobFailure(RuntimeError):
    pass


class JobProcessPriority(str, Enum):
    below_normal = 'below_normal'
    idle = 'idle'


class JobEvent(str, Enum):
    created = 'created'
    queued = 'queued'
    started = 'started'
    finished = 'finished'


@dataclass(frozen=True)
class JobCommand:
    executable: Path
    args: List[str]
    priority: JobProcessPriority = JobProcessPriority.idle


@dataclass(frozen=True)
class JobStatus:
    is_created: bool
    is_queued: bool
    is_started: bool
    is_running: bool
    is_broken: bool
    is_finished: bool
    is_returned: bool


@dataclass(frozen=True)
class JobStats:
    status: JobStatus
    created: Optional[datetime]
    preparation_seconds: Optional[float]
    wait_seconds: Optional[float]
    processing_seconds: Optional[float]
    return_code: Optional[int]
    disk_usage_bytes: Optional[int]

    def get_age_seconds(self) -> Optional[float]:
        """The age is always defined as the creation time is always set,
            unless we are hitting a race condition, o there's an error (the creation timestamp was not stored).
            In case of missing creation timestamp, the job shall be considered as very old.
            Race conditions can be excluded by the caller with a second, delayed call to this function. """
        if self.created is None:
            return None
        return (datetime.utcnow().replace(tzinfo=timezone.utc) - self.created).total_seconds()


@dataclass(frozen=True)
class JobsQuotaPolicy:
    max_job_disk_usage_bytes: conint(ge=1024) = 100 * 1024 * 1024
    max_age_seconds: confloat(allow_inf_nan=False, ge=60.) = 3600. * 24 * 7
    max_processing_seconds: confloat(allow_inf_nan=False, ge=5.) = 300.

    def should_terminate(self, job_stats: JobStats) -> bool:
        """Assumes that race conditions during job creation have been already excluded or waited out."""
        if job_stats.status.is_running:
            _age = job_stats.get_age_seconds()
            if _age is None:
                return True
            _processing = _age - job_stats.preparation_seconds - job_stats.wait_seconds
            if _processing > self.max_processing_seconds:
                return True
        return False

    def should_remove(self, job_stats: JobStats) -> bool:
        """Assumes that race conditions during job creation have been already excluded or waited out."""
        if job_stats.disk_usage_bytes > self.max_job_disk_usage_bytes:
            return True
        if job_stats.get_age_seconds() is None:
            return True
        if job_stats.get_age_seconds() > self.max_age_seconds:
            return True
        return False


@dataclass(frozen=True)
class JobListEntryDTO:
    id: str
    created_at: Optional[datetime] = None


JobTarget = Callable[['Job', JobCommand], None]


def get_current_utc_timestamp_string() -> str:
    return datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ")


def serialize_utc_timestamp_string(t: datetime) -> str:
    assert t.tzinfo == timezone.utc, 'The give datetime must be aware of UTC timezone.'
    return t.strftime("%Y-%m-%dT%H:%M:%S.%fZ")


def parse_utc_timestamp_string(t: str) -> datetime:
    return datetime.strptime(t, "%Y-%m-%dT%H:%M:%S.%fZ").replace(tzinfo=timezone.utc)


@dataclass(frozen=True)
class Job:
    dir: GuestDirectory
    do_collect_job_strace: Optional[bool] = False

    @functools.cached_property
    def id(self):
        return self.dir.id

    @functools.cached_property
    def meta_path(self) -> Path:
        return self.dir.root_path / 'job_meta'

    @functools.cached_property
    def created_path(self) -> Path:
        return self.meta_path / 'created'

    @functools.cached_property
    def command_path(self) -> Path:
        return self.meta_path / 'command'

    @functools.cached_property
    def queued_path(self) -> Path:
        return self.meta_path / 'queued'

    @functools.cached_property
    def started_path(self) -> Path:
        """Process start time as reported by the application that starts it"""
        return self.meta_path / 'started'

    @functools.cached_property
    def process_create_time_path(self) -> Path:
        """Process creation time as reported by the operating system
            This is used to reliably identify a process in combination with the PID."""
        return self.meta_path / 'process_create_time'

    @functools.cached_property
    def pid_path(self) -> Path:
        return self.meta_path / 'pid'

    @functools.cached_property
    def stdout_path(self) -> Path:
        return self.meta_path / 'stdout'

    @functools.cached_property
    def stderr_path(self) -> Path:
        return self.meta_path / 'stderr'

    @functools.cached_property
    def strace_log_path(self) -> Path:
        return self.meta_path / 'strace_log'

    @functools.cached_property
    def finished_path(self) -> Path:
        return self.meta_path / 'finished'

    @functools.cached_property
    def return_code_path(self) -> Path:
        return self.meta_path / 'return_code'

    # @functools.cached_property
    # def fuse_configs_path(self) -> Path:
    #     return self.meta_path / 'fuse_configs'
    # TODO?: FUSE mounts: this is a possible location where path to a json config file for fuse mounts could be added.

    def create(self) -> None:
        self.dir.create()
        self.meta_path.mkdir()
        self.created_path.write_text(get_current_utc_timestamp_string())

    def _subprocess_pipe_target(self) -> None:
        fuse_runtime: Optional[FuseMountListRuntime] = None

        stdout = None
        # stderr = None

        # noinspection PyBroadException
        try:
            # noinspection PyTypeChecker
            cmd: JobCommand = TypeAdapter(JobCommand).validate_json(self.command_path.read_text())

            creation_flags = 0
            preexec_fn = None
            if os.name == 'nt':
                if cmd.priority == 'below_normal':
                    creation_flags = subprocess.BELOW_NORMAL_PRIORITY_CLASS
                if cmd.priority == 'idle':
                    creation_flags = subprocess.IDLE_PRIORITY_CLASS
            if os.name == 'posix':
                if cmd.priority == 'below_normal':
                    preexec_fn = lambda: os.nice(5)
                if cmd.priority == 'idle':
                    preexec_fn = lambda: os.nice(20)

            strace_args = []
            if os.name == 'posix' and (
                    self.do_collect_job_strace if self.do_collect_job_strace is not None else (
                            has_global_entity_manager() and get_global_entity_manager().do_collect_job_strace)):
                strace_args = ['strace', '-ttt', '-xx', '-o', str(self.strace_log_path.absolute())]
                # TODO?: maybe add -DDD ?

            # if self.fuse_configs_path.exists():
            #     # noinspection PyTypeChecker
            #     _fuse_cfg: FuseMountListConfig = TypeAdapter(FuseMountListConfig).validate_json(self.fuse_configs_path.read_text())
            if hasattr(self, 'fuse_mount_list_config'):
                # Let's exploit the dynamic nature of Python -- Strongly typed languages would just require more code.
                _fuse_cfg: Optional[FuseMountListConfig] = getattr(self, 'fuse_mount_list_config')
                if _fuse_cfg:
                    fuse_runtime = _fuse_cfg.mount(self.dir.user_path)
            if fuse_runtime is None:
                _logger.info('No FUSE mounts configuration.')

            stdout = os.open(self.stdout_path, os.O_RDWR | os.O_CREAT | os.O_EXCL)
            # stderr = os.open(self.stderr_path, os.O_RDWR | os.O_CREAT | os.O_EXCL)

            _started = get_current_utc_timestamp_string()

            pipe = Popen(
                args=[*strace_args, cmd.executable, *cmd.args],
                cwd=self.dir.user_path,
                # stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,  # NOTE: merging stderr into stdout for better and simpler processing.
                stdout=stdout,
                # stderr=stderr,
                start_new_session=True,
                close_fds=True,
                creationflags=creation_flags,
                preexec_fn=preexec_fn,
                env=dict()  # TODO: pass env for user profile
            )

            self.pid_path.write_text(str(pipe.pid))
            self.started_path.write_text(_started)

            try:
                # noinspection PyUnresolvedReferences
                import psutil
            except ImportError:
                pass
            else:
                try:
                    proc = psutil.Process(pid=pipe.pid)
                    self.process_create_time_path.write_text(str(proc.create_time()))
                except psutil.NoSuchProcess:
                    pass

            # TODO?: Improve management of stdout/stderr lines with timestamping via AnyIO
            #           https://anyio.readthedocs.io/en/stable/subprocesses.html#working-with-processes
            #           https://anyio.readthedocs.io/en/stable/api.html#anyio.streams.text.TextReceiveStream

            # NOTE: The following is not working. Selectors instead of async may fix it, but not on Windows.
            # combined_line_index = 0
            #
            # async def _stream_proc_one(reading_f_, writing_path: Path):
            #     nonlocal combined_line_index
            #     reading_f = anyio.wrap_file(reading_f_)
            #     with os.open(writing_path, os.O_RDWR | os.O_CREAT | os.O_EXCL) as writing_f_:
            #         writing_f = anyio.wrap_file(writing_f_)
            #
            #         while True:
            #             line = await reading_f.readline()
            #             li = combined_line_index
            #             combined_line_index += 1
            #             ts = datetime.utcnow().isoformat(timespec='microseconds')
            #             await writing_f.writelines([json.dumps({'li': li, 'ts': ts, 'line': line})])
            #
            # async def _stream_proc():
            #     try:
            #         async with anyio.create_task_group() as tg:
            #             tg.start_soon(_stream_proc_one, pipe.stdout, self.stdout_path)
            #             tg.start_soon(_stream_proc_one, pipe.stderr, self.stderr_path)
            #     except Exception:
            #         _logger.exception('Job stream tracking failed with exception.')
            #
            # anyio.run(_stream_proc)
            pipe.wait()

            self.return_code_path.write_text(str(pipe.returncode))

        except Exception:
            _logger.exception('Job execution failed with exception.')
            self.return_code_path.write_text(str(10200410))

        finally:
            try:
                if stdout:
                    os.close(stdout)
                # if stderr:
                #     os.close(stderr)
                self.pid_path.unlink(missing_ok=True)
                self.finished_path.write_text(get_current_utc_timestamp_string())
            finally:
                # NOTE: It's very important to unmount any fuse mounts before returning, in all cases.
                #       Otherwise, a huge zip of results may be attempted.
                #       So, in case of exceptions during unmount,
                #           it's better to crash the job runner instead of trying to handle exceptions.
                #                   (maybe TODO: improve code with context manager for safer and automatic unmount)
                if fuse_runtime:
                    fuse_runtime.unmount()

    def set_command(self, cmd: JobCommand):
        assert self.created_path.is_file()
        assert not self.queued_path.exists()
        assert not self.pid_path.exists()
        assert not self.finished_path.exists()
        self.command_path.write_text(json.dumps(cmd, default=pydantic_encoder))

    # def set_fuse_configs(self, cfg: FuseMountListConfig):
    #     assert self.created_path.is_file()
    #     # assert not self.queued_path.exists()
    #     assert not self.pid_path.exists()
    #     assert not self.finished_path.exists()
    #     self.fuse_configs_path.write_text(json.dumps(cfg, default=pydantic_encoder))
    # NOTE: this is a possible approach that would allow to bring the config from the main process.

    def set_queued(self) -> None:
        if not self.queued_path.exists():
            self.queued_path.write_text(get_current_utc_timestamp_string())

    def start(self, /, *, executor: Optional[Executor] = None, block: Optional[bool] = None) -> None:
        # TODO: fix signaling of queued time.
        assert self.created_path.is_file()
        assert not self.pid_path.exists()
        assert not self.finished_path.exists()
        assert self.command_path.exists()

        self.set_queued()

        if executor:
            assert block is None, 'Could not guarantee blocking on provided executor.'
            executor.submit(self._subprocess_pipe_target)
        else:
            if block:
                self._subprocess_pipe_target()
            else:
                Thread(target=self._subprocess_pipe_target, daemon=True).start()

    # TODO?: maybe remove self.start and self._subprocess_pipe_target and just have some self.run and self.set_queued

    def _find_process(self, timeout: float = 0) -> Optional['psutil.Process']:
        """Waits for PID determination, checks against PID recycling, returns the PID and Process."""
        import psutil
        if not self.queued_path.exists():
            raise IllegalJobStateError
        if self.finished_path.exists():
            return
        if timeout > 0:
            _poll_seconds = 0.05
            for _ in range(int(timeout / _poll_seconds)):
                if self.pid_path.exists() and self.started_path.exists() and self.process_create_time_path.exists():
                    break
                else:
                    sleep(_poll_seconds)
                    if self.finished_path.exists():
                        return
            else:
                raise TimeoutError

        try:
            pid = int(self.pid_path.read_text())
        except FileNotFoundError:
            return  # it has been deleted

        try:
            proc = psutil.Process(pid)
        except psutil.NoSuchProcess:
            return  # it finished
        else:
            try:
                job_proc_create_time = float(self.process_create_time_path.read_text())
            except FileNotFoundError:
                return  # Could not reliably identify the process without its create_time
            proc_create_time = float(proc.create_time())
            diff = job_proc_create_time - proc_create_time
            if diff > 1:
                return  # The process creation time does not match -> the PID has been recycled for another process.
        return proc

    def wait(self, timeout: float):
        start = time.monotonic()

        # Wait process termination
        proc = self._find_process(timeout)
        if proc is not None:
            elapsed = time.monotonic() - start
            proc.wait(timeout=timeout - elapsed)
        # else: process already finished.

        # Wait finish file
        elapsed = time.monotonic() - start
        _poll_seconds = 0.01
        for _ in range(int((timeout - elapsed) / _poll_seconds)):
            if self.finished_path.exists():
                break
            else:
                sleep(_poll_seconds)

    def get_status(self) -> JobStatus:
        _is_created = self.created_path.exists()
        _is_queued = _is_created and self.queued_path.exists()
        _is_started = _is_queued and self.started_path.exists()
        _pid_path_exists = _is_started and self.pid_path.exists()
        _is_finished = _is_started and self.finished_path.exists()
        _is_returned = _is_finished and self.return_code_path.exists()
        if _pid_path_exists and not _is_finished:
            # _proc = self._find_process()
            _pid_running = True  # _proc is not None
        else:
            _pid_running = False

        return JobStatus(
            is_created=_is_created,
            is_queued=_is_queued,
            is_started=_is_started,
            is_running=_pid_path_exists and _pid_running,
            is_broken=_pid_path_exists and not _pid_running and not _is_finished,  # NOTE: race conditions may give True
            is_finished=_is_finished,
            is_returned=_is_returned,
        )

    def get_stats(self, /, *, always_get_disk_usage: bool = False) -> JobStats:
        _status = self.get_status()
        self.get_status()
        _created: Optional[datetime] = None
        _preparation_seconds = None
        _wait_seconds = None
        _processing_seconds = None
        _return_code = None
        _queued = None
        _disk_usage_bytes = None
        try:
            if _status.is_created:
                _created = parse_utc_timestamp_string(self.created_path.read_text())
                if _status.is_queued:
                    _queued = parse_utc_timestamp_string(self.queued_path.read_text())
                    _preparation_seconds = (_queued - _created).total_seconds()
                    if _status.is_started:
                        _started = parse_utc_timestamp_string(self.started_path.read_text())
                        # NOTE: process start time may be off by a fraction of a second.
                        _wait_seconds = max(0.0, (_started - _queued).total_seconds())
                        if not _status.is_running:
                            _disk_usage_bytes = self.dir.get_total_bytes()  # see also same line below
                        if _status.is_finished:
                            _finished = parse_utc_timestamp_string(self.finished_path.read_text())
                            _processing_seconds = (_finished - _started).total_seconds()
                            _return_code = int(self.return_code_path.read_text())
            if _disk_usage_bytes is None and always_get_disk_usage:
                _disk_usage_bytes = self.dir.get_total_bytes()  # see also same line above
        except (ValueError, OSError):
            pass
        return JobStats(
            status=_status,
            created=_created.replace(microsecond=0) if _created else None,  # Hide precise system clock
            preparation_seconds=_preparation_seconds,
            wait_seconds=_wait_seconds,
            processing_seconds=_processing_seconds,
            return_code=_return_code,
            disk_usage_bytes=_disk_usage_bytes,
        )

    def kill(self, sig: int = signal.SIGTERM) -> None:
        _proc = self._find_process(60)
        if _proc is None:
            return  # already finished or dead
        os.kill(_proc.pid, sig)

    # async def tail(self, file: Optional[Literal['stdout', 'stderr']] = None):
    #     if file is None:
    #         file = self.stdout_path
    #     assert file in ['stdout', 'stderr']
    #     file = {'stdout': self.stdout_path, 'stderr': self.stderr_path}[file]
    #     import aiofile
    #     async with aiofile.async_open(file, mode='r') as f:
    #         async for line in f:
    #             yield line

    def get_strace(self):
        from .strace import StraceModel
        return StraceModel(self.strace_log_path.read_text())

    async def aiter_events(self) -> AsyncIterable[JobEvent]:
        _events_seq = [
            (self.created_path, JobEvent.created),
            (self.queued_path, JobEvent.queued),
            (self.started_path, JobEvent.started),
            (self.finished_path, JobEvent.finished),
        ]
        for path, event in _events_seq:
            _got = False
            while not _got:
                if path.exists():
                    yield event
                    _got = True
                else:
                    await asyncio.sleep(0.1)
