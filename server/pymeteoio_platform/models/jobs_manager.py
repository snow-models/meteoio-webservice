# SPDX-License-Identifier: AGPL-3.0-or-later
import functools
import time
import warnings
from concurrent.futures import Executor
from datetime import datetime, timezone
from threading import Thread
from typing import Iterable, Optional

from pydantic.dataclasses import dataclass

from .guest_directories import GuestDirectoriesRoot
from .jobs import JobsQuotaPolicy, Job, _logger
from .jobs_dispatcher import RemoteJobDispatcher
from .jobs_index import JobsIndexStore, JobRelation


@dataclass(frozen=True)
class JobsManager:
    dir_ctrl: GuestDirectoriesRoot
    quota_policy: JobsQuotaPolicy
    index_url: str  # = 'sqlite://'  # use this for in-memory db
    concurrency: int = 1
    job_dispatch_type: str = ''
    job_dispatcher_host: Optional[str] = None
    job_dispatcher_port: Optional[int] = None
    do_collect_job_strace: Optional[bool] = None

    # NOTE: this manager makes Job's with no "job_class" attribute, but no Job subclassing should ever be required.

    @functools.cached_property
    def index(self):
        assert self.index_url, 'The index db url must be configured if the index is used.'
        return JobsIndexStore(url=self.index_url)

    @functools.cached_property
    def executor(self) -> Executor:
        from concurrent.futures import ThreadPoolExecutor
        tpe = ThreadPoolExecutor(max_workers=self.concurrency, thread_name_prefix='JobsManager')
        return tpe

    @functools.cached_property
    def remote_job_dispatcher(self) -> RemoteJobDispatcher:
        assert self.job_dispatch_type == 'remote'
        return RemoteJobDispatcher(host=self.job_dispatcher_host, port=self.job_dispatcher_port)

    def make_job(self, /, *, create: bool = False, user_id: Optional[str] = None) -> Job:
        """
        :param create must be True
        :param user_id optional identifier of the user that submitted this job
        """
        assert create, 'Making of a job instance must also create its support files'
        _dir = self.dir_ctrl.make_dir()
        job = Job(dir=_dir, do_collect_job_strace=self.do_collect_job_strace)
        job.create()
        with self.index.transaction() as tx:
            tx.add(JobRelation(
                job_id=job.id,
                created_at=datetime.utcnow().replace(tzinfo=timezone.utc, microsecond=0),
                submitted_by_user_id=user_id,
            ))
        return job

    def get_job(self, _id: str, /, *, must_exist: bool = False) -> Job:
        _dir = self.dir_ctrl.get_dir(_id)
        _job = Job(dir=_dir, do_collect_job_strace=self.do_collect_job_strace)
        if must_exist:
            assert _job.created_path.exists()
        return _job

    def start(self, job: Job, /, *, blocking: bool = False, retry_remote: bool = True) -> None:
        job.set_queued()
        if self.job_dispatch_type == 'remote':
            if blocking:
                _logger.info('Running job via RemoteJobDispatcher')
                self.remote_job_dispatcher.run(job, retry=retry_remote)
            else:
                Thread(target=self.remote_job_dispatcher.run, args=(job,), name=f'Job-{job.id}').start()
                # self.executor.submit(self.remote_job_dispatcher.run, args=(job,))
                _logger.info('Submitted to RemoteJobDispatcher')
        elif self.job_dispatch_type == 'local':
            warnings.warn('Using local job executor')
            job.start(executor=self.executor)
        else:
            raise ValueError('Unexpected value for `job_dispatch_type`')

    def iter_jobs(self) -> Iterable[Job]:
        for _dir in self.dir_ctrl.list_dirs():
            yield Job(dir=_dir, do_collect_job_strace=self.do_collect_job_strace)

    def enforce_quota_policy(self) -> None:
        """Garbage collector: cleanup jobs that shall be removed"""
        # A background service shall use this method.
        # We do not enforce the same quota for released dataset runs.

        from .di import get_global_entity_manager
        from .datasets.runs_index import RunRelation
        entity_manager = get_global_entity_manager()
        runs_index = entity_manager.datasets.runs_index

        for job in self.iter_jobs():
            _stats = job.get_stats(always_get_disk_usage=True)
            if not _stats.created:
                time.sleep(0.1)  # wait out race conditions during creation of the job
                _stats = job.get_stats(always_get_disk_usage=True)

            with runs_index.transaction() as tx:
                _run_rel = tx.get(RunRelation, job.id)
                if _run_rel is not None:
                    if _run_rel.released:
                        continue  # skip! no policy is enforced for released runs.

            if self.quota_policy.should_terminate(_stats):
                _logger.info(f'Terminating job {job.id} due to quota policy')
                job.kill()  # TODO: this is likely a no-op at the moment
            if self.quota_policy.should_remove(_stats):
                # TODO?: check it's not running before trying to remove? who fails in case it's still running?
                _logger.info(f'Removing job {job.id} due to quota policy')
                job.dir.remove(confirm=job.id, recursive=True)
                self.remove_lost_job_from_indexes(job)

    def remove_lost_job_from_indexes(self, job: Job):
        assert not job.dir.root_path.exists(), 'The job directory must already be deleted'

        from .di import get_global_entity_manager
        from .datasets.runs_index import RunRelation
        entity_manager = get_global_entity_manager()
        runs_index = entity_manager.datasets.runs_index

        # Remove indexed job if it's still there
        with self.index.transaction() as tx:
            job_rel: Optional[JobRelation] = tx.get(JobRelation, job.id)
            if job_rel:
                tx.delete(job_rel)

        # Remove indexed ds run if it's still there
        with runs_index.transaction() as tx:
            run_rel: Optional[RunRelation] = tx.get(RunRelation, job.id)
            if run_rel:
                tx.delete(run_rel)
