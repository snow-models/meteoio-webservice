# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .. import EntityManager


# noinspection PyProtectedMember
def up(em: 'EntityManager'):
    em._datasets_path.mkdir()
    em._public_datasets_path.mkdir()
    em._datasets_meta_path.mkdir()
    em._guest_jobs_path.mkdir()
    em.sessions_path.mkdir()
