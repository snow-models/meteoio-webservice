# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .. import EntityManager


def up(em: 'EntityManager'):
    em.pywps_path.mkdir(exist_ok=True, parents=True)
