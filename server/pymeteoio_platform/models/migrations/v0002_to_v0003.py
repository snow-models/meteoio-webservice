# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import TYPE_CHECKING

from sqlalchemy import DDL
from sqlalchemy.exc import OperationalError

if TYPE_CHECKING:
    from .. import EntityManager


def up(em: 'EntityManager'):
    try:
        with em.datasets.runs_index.transaction() as tx:
            tx.execute(DDL('ALTER TABLE runs ADD COLUMN ini_name TEXT DEFAULT NULL'))
    except OperationalError:
        pass  # The column already exists