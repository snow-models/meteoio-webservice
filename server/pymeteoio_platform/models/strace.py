# SPDX-License-Identifier: AGPL-3.0-or-later
"""
Utility for parsing the output of `strace`,
the linux tool that traces the syscalls made by a process.

The data collected by `strace` could be used to structurally know:
- which files are used, with which access (read, write, append, truncate)
- the last syscalls before an error occurred
- timestamps of all syscalls, including prints to stdout and stderr
- something about network activity
- exit value of the process

A small subset of the above is actually parsed with some regular expressions:
- which files are used via `openat`, with which flags (readonly or not).
"""

import ast
import re
from datetime import datetime, timezone
from functools import cached_property
from pathlib import Path
from typing import Iterable, Set, List, Optional

from pydantic.dataclasses import dataclass

gre_syscall = re.compile(r'^(?P<timestamp>[0-9.]+) (?P<syscall_name>[a-zA-Z0-9-_]+)' +
                         r'\((?P<syscall_args>.*?)\)[ ]+=[ ]+(?P<syscall_result>.*)')
gre_num = re.compile(r'[0-9.]+')
gre_const = re.compile(r'[a-zA-Z0-9|]+')
gre_string = re.compile(r'"(\\x[0-9a-f]{2})*"')


@dataclass(frozen=True, repr=False)
class StraceOpenFileSyscallModel:
    path: Path
    flags: Set[str]

    @property
    def is_readonly(self) -> bool:
        return 'O_RDONLY' in self.flags


@dataclass(frozen=True, repr=False)
class StraceSyscallLineModel:
    timestamp: float
    syscall_name: str
    syscall_args: str
    syscall_result: str

    def __repr__(self):
        return f"<{self.datetime.isoformat()} {self.syscall_name}({', '.join(repr(x) for x in self.args)})>"

    @property
    def datetime(self) -> datetime:
        return datetime.utcfromtimestamp(self.timestamp).replace(tzinfo=timezone.utc)

    @cached_property
    def args(self) -> List[str]:
        _args = []
        for _arg in self.syscall_args.split(','):
            _arg = _arg.strip()
            if gre_num.match(_arg):
                try:
                    _args.append(ast.literal_eval(_arg))
                except SyntaxError:
                    _args.append(None)
            elif gre_const.match(_arg):
                _args.append(_arg)
            elif gre_string.match(_arg):
                try:
                    _args.append(ast.literal_eval(_arg))
                except SyntaxError:
                    _args.append(None)
            else:
                _args.append(None)
        return _args

    @property
    def is_open_file(self) -> bool:
        """https://man7.org/linux/man-pages/man2/openat.2.html"""
        return self.syscall_name in ['open', 'creat', 'openat', 'openat2']

    @cached_property
    def open_file(self) -> StraceOpenFileSyscallModel:
        """https://man7.org/linux/man-pages/man2/openat.2.html"""
        assert self.is_open_file

        if self.syscall_name == 'open':
            assert len(self.args) >= 2, "At least 2 args are expected for open syscall."
            return StraceOpenFileSyscallModel(
                path=Path(self.args[0]), flags={flag.strip() for flag in self.args[1].split('|')})

        if self.syscall_name == 'creat':
            assert len(self.args) >= 2, "At least 2 args are expected for creat syscall."
            return StraceOpenFileSyscallModel(
                path=Path(self.args[0]), flags=set())

        if self.syscall_name == 'openat':
            assert len(self.args) >= 3, "At least 3 args are expected for openat syscall."
            return StraceOpenFileSyscallModel(
                path=Path(self.args[1]), flags={flag.strip() for flag in self.args[2].split('|')})

        if self.syscall_name == 'openat2':
            assert len(self.args) >= 4, "At least 4 args are expected for openat2 syscall."
            return StraceOpenFileSyscallModel(
                path=Path(self.args[1]), flags=set())

        raise AssertionError


@dataclass(frozen=True, repr=False)
class StraceDto:
    offset_seconds: float
    syscall: str
    open_file_path: Optional[str] = None
    open_file_flags: Optional[List[str]] = None
    current_file_size: Optional[int] = None


@dataclass(frozen=True, repr=False)
class StraceModel:
    text: str

    # TODO: add cwd path here

    def iter_syscalls(self) -> Iterable[StraceSyscallLineModel]:
        # from strace_parser import json_transformer
        # from strace_parser import parser
        # _parser = parser.get_parser()
        # for line in self.text.splitlines(keepends=True):
        #     try:
        #         for json in json_transformer.to_json(_parser.parse(line)):
        #             yield StraceLineModel(**json)
        #     except (TypeError, ValueError) as ex:
        #         yield StraceLineModel(parse_error=ex)
        # NOTE: the above full parser based on Lark is sloooooow.

        for line in self.text.splitlines(keepends=True):
            m = gre_syscall.match(line)
            if m:
                yield StraceSyscallLineModel(**m.groupdict())

    # def get_opened_files(self) -> Set[Path]:
    #     return {line.open_file.path for line in self.iter_syscalls() if line.is_open_file}
    #
    # def get_written_files(self) -> Set[Path]:
    #     return {line.open_file.path for line in self.iter_syscalls() if line.is_open_file
    #             and not line.open_file.is_readonly}

    def iter_dtos(self, /, *, open_files_only: bool = False, output_only: bool = False, cwd: Optional[Path] = None) -> \
    Iterable[StraceDto]:
        _start = None  # We need to find out the first timestamp in order to compute the time offset
        for sc in self.iter_syscalls():
            if _start is None:
                _start = sc.datetime

            # Let's get a value for open_file_* fields only in case of system call about files
            _open_file_path = None
            _open_file_flags = None
            _current_file_size = None
            if sc.is_open_file:
                if output_only and sc.open_file.is_readonly:
                    continue

                _open_file_flags = sc.open_file.flags
                if not sc.open_file.path.is_absolute():
                    # NOTE: assuming we don't want to display data about paths outside the working directory
                    # TODO: better filter of paths under cwd
                    _open_file_path = str(sc.open_file.path)

                    if cwd:
                        _local_path = cwd / sc.open_file.path
                        if _local_path.is_relative_to(cwd):
                            # noinspection PyBroadException
                            try:
                                if _local_path.exists():
                                    _current_file_size = _local_path.stat().st_size
                            except Exception:
                                pass

            elif open_files_only:
                continue

            yield StraceDto(
                offset_seconds=(sc.datetime - _start).total_seconds(),
                syscall=sc.syscall_name,
                open_file_path=_open_file_path,
                open_file_flags=_open_file_flags,
                current_file_size=_current_file_size,
            )

    def iter_unique_files(self, /, *, output_only: bool = False, cwd: Optional[Path] = None) -> Iterable[StraceDto]:
        _paths: Set[str] = set()
        for dto in self.iter_dtos(output_only=output_only, cwd=cwd):
            if dto.open_file_path:
                if dto.open_file_path not in _paths:
                    yield dto
                    _paths.add(dto.open_file_path)
