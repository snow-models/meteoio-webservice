# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import socket
from time import sleep

from pydantic.dataclasses import dataclass

from .jobs import Job, get_current_utc_timestamp_string
from .zip import export_zip_stream, import_zip_stream

_LOGGER = logging.getLogger(__name__)


@dataclass(frozen=True)
class RemoteJobDispatcher:
    host: str
    port: int

    # NOTE: Add method to wait for job finish?
    #       We probably don't need it: we can poll for status update.
    #       Also, the cron scheduler works better with a blocking call and
    #       a number of scheduler processes equal to the number of job runners

    def run(self, job: Job, /, *, retry: bool = True):
        _LOGGER.info(f'Starting in RemoteJobDispatcher... ({job.id=})')
        attempts = 3 if retry else 1
        for attempt_i in range(attempts):
            # noinspection PyBroadException
            try:
                self._run_1(job)
            except:
                _LOGGER.exception(f'Failure in RemoteJobDispatcher attempt {attempt_i + 1}/{attempts}: ({job.id=})')
                sleep(1 + 4 * attempt_i)
            else:
                break
        else:
            _LOGGER.warning(f'Too many failures in RemoteJobDispatcher. ({job.id=})')
            job.started_path.write_text(get_current_utc_timestamp_string())
            job.finished_path.write_text(get_current_utc_timestamp_string())
            job.return_code_path.write_text(str(10200400))

    def _run_1(self, job: Job):
        _LOGGER.info(f'Connecting dispatcher... ({job.id=})')
        with socket.create_connection((self.host, self.port), timeout=20) as sock:
            sock.send(b'j')
            _LOGGER.info(f'Sending job to dispatcher... ({job.id=})')
            for _chunk in export_zip_stream(root=job.dir.root_path, compression_level=2):
                sock.sendall(_chunk)
            sock.shutdown(socket.SHUT_WR)

            _LOGGER.info(f'Job sent. Waiting for start signal... ({job.id=})')

            sock.settimeout(120)

            __started_signal = sock.recv(1)
            if not __started_signal:
                raise RuntimeError('Connection closed without starting.')
            assert __started_signal == b's', f'Expecting started signal, got {repr(__started_signal)}'

            job.started_path.write_text(get_current_utc_timestamp_string())
            job.pid_path.write_text("-1")
            job.stdout_path.write_text("")
            job.strace_log_path.write_text("")

            # NOTE: If we signal the start, then we need consistency with the started state

            _LOGGER.info(f'Processing started. Waiting result... ({job.id=})')

            sock.settimeout(120)

            # TODO?: keep alive messages while waiting for result? Maybe no because the runner would have to be MT

            def _mk_zipped_chunks():
                _first = True
                while __chunk := sock.recv(1 if _first else 4096):
                    yield __chunk
                    if _first:
                        _first = False
                        _LOGGER.info(f'Receiving result... ({job.id=})')
                        sock.settimeout(20)

            import_zip_stream(root=job.dir.root_path, zipped_chunks=_mk_zipped_chunks())

            _LOGGER.info(f'Job result received. ({job.id=})')
