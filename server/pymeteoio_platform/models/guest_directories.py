# SPDX-License-Identifier: AGPL-3.0-or-later
import functools
import shutil
import stat
from datetime import datetime, timezone
from pathlib import Path
from typing import BinaryIO, Iterable, Annotated

import uuid6
from pydantic import DirectoryPath, StringConstraints
from pydantic.dataclasses import dataclass

from .errors import NotFound, UserReportableBadRequestException
from .files import FsEntry, FsEntryType, PathLike


@dataclass(frozen=True)
class GuestDirectory:
    id: Annotated[str, StringConstraints(
        strip_whitespace=True, to_lower=True, strict=True, min_length=36, max_length=36, pattern="^[0-9a-f-]{36}$")]
    parent: DirectoryPath

    @functools.cached_property
    def _path(self) -> Path:
        return self.parent.resolve() / self.id  # NOTE: TransactionalDirectory breaks without parent.resolve

    @functools.cached_property
    def root_path(self) -> Path:
        """Structured directory.
            The programmer must know the structure."""
        return self._path

    @functools.cached_property
    def user_path(self) -> Path:
        """User content directory, unstructured."""
        return self._path / 'user'

    # @functools.cached_property
    # def files_dir_path(self) -> Path:
    #     return self.path / 'user'
    #
    # @functools.cached_property
    # def meta_dir_path(self) -> Path:
    #     return self.path / 'sys'

    def create(self) -> None:
        # TODO?: mkdir mode=0o777 is ok?
        self._path.mkdir(parents=False, exist_ok=False)
        self.user_path.mkdir(parents=False, exist_ok=False)

    def _ensure_user_path_from_relative(self, path: PathLike) -> Path:
        _path = self.user_path / ('./' + str(path))
        _path = _path.resolve()  # NOTE: this is required to let the following raise in case of paths outside the root.
        _path.relative_to(self.user_path)  # Let it raise ValueError
        if not self.user_path.is_dir():
            raise NotFound
        return _path

    def mkdir(self, /, *, path: PathLike) -> None:
        file_path = self._ensure_user_path_from_relative(path)
        if not file_path.exists():
            file_path.mkdir(parents=True, exist_ok=True)

    def write(self, /, *, path: PathLike, file: BinaryIO) -> None:
        file_path = self._ensure_user_path_from_relative(path)
        if not file_path.exists():
            file_path.parent.mkdir(parents=True, exist_ok=True)
        with file_path.open('wb') as f:
            while True:
                block = file.read(1024)
                if not block:
                    break
                f.write(block)

    def rm_file(self, /, *, path: PathLike, missing_ok: bool = False) -> None:
        file_path = self._ensure_user_path_from_relative(path)
        if file_path.exists() and file_path.is_dir():
            try:
                file_path.rmdir()
            except OSError as err:
                if err.errno == 39:  # Directory not empty
                    raise UserReportableBadRequestException("Directory not empty")
                else:
                    raise
        else:
            file_path.unlink(missing_ok=missing_ok)

    def rename_file(self, /, *, path: PathLike, new_name: str) -> Path:
        """Can only rename without changing location. Returns new path."""
        file_path = self._ensure_user_path_from_relative(path)
        try:
            target = file_path.with_name(new_name)
        except ValueError as err:
            raise UserReportableBadRequestException(err)
        else:
            file_path.rename(target)
        return target

    def check_user_path(self, path: PathLike) -> PathLike:
        self._ensure_user_path_from_relative(path)
        return path

    def get_full_user_path(self, path: PathLike) -> Path:
        return self._ensure_user_path_from_relative(path)

    def remove(self, /, *, confirm: str, recursive: bool) -> None:
        assert confirm == self.id
        assert recursive, 'recursive remove is the only available option.'
        shutil.rmtree(self._path)

    def iter_recursive_user_paths(self, /, *, files_only: bool = True) -> Iterable[Path]:
        for f in self.user_path.glob('**/*'):
            if not files_only or f.is_file():
                yield f

    def get_total_bytes(self) -> int:
        return sum(f.stat().st_size for f in self._path.glob('**/*') if f.is_file())
        # TODO?: this function is underestimating the overhead space occupied by small files and directories themselves.
        #        https://stackoverflow.com/a/55648984 (equivalent of `du` linux tool).

    def list(self, path: PathLike) -> Iterable[FsEntry]:
        _path = self._ensure_user_path_from_relative(path)
        for entry in _path.iterdir():
            _stat = entry.stat()
            yield FsEntry(
                path=entry.name,
                type=FsEntryType.folder if stat.S_ISDIR(_stat.st_mode) else FsEntryType.file,
                size=_stat.st_size,
                date=datetime.utcfromtimestamp(_stat.st_mtime).replace(tzinfo=timezone.utc),
            )


@dataclass(frozen=True)
class GuestDirectoriesRoot:
    root: DirectoryPath

    def make_dir(self, /, *, create: bool = False) -> GuestDirectory:
        _id = str(uuid6.uuid7())
        _dir = GuestDirectory(id=_id, parent=self.root)
        if create:
            _dir.create()
        return _dir

    def get_dir(self, _id: str, /, *, must_exist: bool = False) -> GuestDirectory:
        _dir = GuestDirectory(id=_id, parent=self.root)
        if must_exist and not _dir.root_path.exists():
            raise NotFound()
        return _dir

    def list_dirs(self) -> Iterable[GuestDirectory]:
        for child in self.root.iterdir():
            if child.is_dir():
                yield GuestDirectory(id=child.parts[-1], parent=self.root)


__all__ = [GuestDirectory, GuestDirectoriesRoot]
