# SPDX-License-Identifier: AGPL-3.0-or-later
"""
    Secondary storage for dataset runs indexing
"""
import datetime
from contextlib import contextmanager
from functools import cached_property
from typing import Optional, Union, Dict, ContextManager

from pydantic.dataclasses import dataclass
from sqlalchemy import create_engine
from sqlalchemy.orm import Mapped, registry, mapped_column, sessionmaker, Session

from .runs_dtos import DsRunDto

tables_registry = registry()

AttributeValueT = Union[str, float, bool, 'AttributesT', None]  # TypeAlias requires python 3.10 ?
AttributesT = Dict[str, AttributeValueT]


@tables_registry.mapped_as_dataclass
class RunRelation:
    __tablename__ = "runs"

    job_id: Mapped[str] = mapped_column(
        primary_key=True, nullable=False)

    dataset_id: Mapped[str] = mapped_column(
        nullable=False)

    created_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False, default_factory=lambda: datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc))

    cron_id: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    released: Mapped[bool] = mapped_column(
        nullable=False, default=False)

    ini_name: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    # NOTE: ini name does not refer to a specific revision, this is a known behaviour.

    def get_dto(self) -> DsRunDto:
        return DsRunDto(
            job_id=self.job_id,
            dataset_id=self.dataset_id,
            created_at=self.created_at,
            released=self.released,
            ini_name=self.ini_name,
            cron_id=self.cron_id,
        )


@dataclass(frozen=True)
class RunsIndexStore:
    url: str

    @cached_property
    def _session_maker(self) -> sessionmaker:
        e = create_engine(url=self.url)
        tables_registry.metadata.create_all(e)
        return sessionmaker(e, autoflush=False, expire_on_commit=True)

    @contextmanager
    def transaction(self) -> ContextManager[Session]:
        # TODO?: get global lock?
        s = self._session_maker()
        with s.begin():
            yield s