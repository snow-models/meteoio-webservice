# SPDX-License-Identifier: AGPL-3.0-or-later
import datetime
from dataclasses import field
from enum import Enum
from typing import List, Optional

from pydantic.dataclasses import dataclass


class Access(str, Enum):
    private = 'private'
    public = 'public'


class ActorType(str, Enum):
    person = 'person'


@dataclass()
class Actor:
    full_name: str
    email: Optional[str] = None
    type: Optional[ActorType] = None


@dataclass()
class Location:
    coordSys: Optional[str] = None
    latitude: Optional[float] = None
    longitude: Optional[float] = None
    elevation: Optional[int] = None


@dataclass()
class License:
    name: str


@dataclass()
class DatasetHeading:
    title: Optional[str] = None
    description: Optional[str] = None
    creators: List[Actor] = field(default_factory=list)
    locations: List[Location] = field(default_factory=list)
    licenses: List[License] = field(default_factory=list)


@dataclass()
class DatasetListEntry:
    id: str
    owner_user_id: str
    access: Access
    heading: Optional[DatasetHeading] = None


@dataclass()
class DatasetRevisionsListEntry:
    id: str
    created_at: Optional[datetime.datetime]
    updated_at: Optional[datetime.datetime]
    confirmed_at: Optional[datetime.datetime]
    emptied_at: Optional[datetime.datetime]
    created_from_id: Optional[str]
    number: Optional[int]
    title: Optional[str]
    message: Optional[str]


@dataclass()
class DatasetRevisionMsg:
    title: Optional[str]
    message: Optional[str]


class FsEntryType(str, Enum):
    file = 'file'
    folder = 'folder'


@dataclass()
class FsEntry:
    path: str
    date: datetime.datetime
    size: int
    type: FsEntryType