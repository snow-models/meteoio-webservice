# SPDX-License-Identifier: AGPL-3.0-or-later
import contextlib
import errno
import shutil
import warnings
from pathlib import Path
from typing import Optional, ContextManager, Iterable, Literal, Union

import uuid6
from pydantic import DirectoryPath
from pydantic.dataclasses import dataclass

# from .config_dir import ConfigDirectory

TxId = Union[Literal['head'], str]


# TxWrappedT: TypeAlias = ConfigDirectory  # NOTE: can't use TypeVar: https://github.com/pydantic/pydantic/issues/5803


@dataclass(frozen=True)
class TransactionalDirectory:
    """
    ACID-like transactional operations on a directory of files.
    This is based on:
    - copying the whole directory with an efficient copy method to implement isolation (MVCC),
    - symlink to the head state to implement atomic transaction commit,
    - assumption that file write operations do not break consistency (of a version),
    - assumption that file write operations are synced for durability,
    - other small details to prevent some anomalies.

    Read-only transactions are not enforced as such (you may write),
    but the programmer has to explicitly ask for read-only access while the default is read-write with MVCC.
    """

    head_path: Path
    transactions_dir: DirectoryPath

    # transaction_wrapper: Callable[[Path], TxWrappedT] = field(default=lambda path: path)

    @staticmethod
    def _make_tx_id() -> TxId:
        return str(uuid6.uuid7())

    @contextlib.contextmanager
    def init(self) -> ContextManager[Path]:
        tx_id = self._make_tx_id()  # TODO: use 'init' as init transaction id (it will be renamed)
        _dir = self.transactions_dir / tx_id
        _dir.mkdir(mode=0o700, parents=False, exist_ok=False)
        assert not self.head_path.exists(), 'Transaction log head must not already exist when initializing'
        assert _dir.is_dir(), 'Transaction directory must have been created at this point'
        did_error = False
        try:
            yield _dir
            did_error = False
        finally:
            if not did_error:
                self._relink_head(tx_id)
            else:
                self._forget_tx(tx_id)

    def _get_head_tx(self) -> TxId:
        assert self.head_path.is_symlink(), 'Transactions log head must always be a symlink'
        _path = self.head_path.resolve()
        assert not _path.is_symlink(), 'Transactions log head symlink must not resolve to a symlink'
        assert _path != self.head_path, 'Transactions log head symlink must not resolve to itself'
        assert _path.relative_to(self.transactions_dir), 'Transaction head must have relative to transactions dir'
        return _path.name

    @property
    def head_tx(self) -> TxId:
        return self._get_head_tx()

    def _ensure_dir(self, tx_id: Optional[TxId] = None) -> Path:
        if tx_id is None or tx_id == 'head':
            _dir = self.transactions_dir / self._get_head_tx()
            if not _dir.exists():
                raise RuntimeError('Transactions log not initialized')
        else:
            _dir = self.transactions_dir / tx_id
            if not _dir.exists():
                raise RuntimeError('Transaction not found, possibly forgotten')
        if _dir.parent != self.transactions_dir:
            raise ValueError('Invalid transaction id')
        if not _dir.is_dir():
            raise RuntimeError('Invalid transaction (not a dir)')
        return _dir

    def begin_read(self, /, *, from_id: Optional[TxId] = None) -> TxId:
        """Note: this gives access to the current head data, with no protection against write operations"""
        return self._ensure_dir(from_id).name

    def begin_write(self, /, *, from_id: Optional[TxId] = None) -> TxId:
        _from_dir = self._ensure_dir(from_id)
        tx_id = self._make_tx_id()  # TODO: use a shorted random dir name
        _new_dir = self.transactions_dir / tx_id
        shutil.copytree(_from_dir, _new_dir, dirs_exist_ok=False)
        return tx_id

    def apply(self, tx_id: TxId) -> TxId:
        """Atomically update the head of the transactional directory."""
        old_head_id = self._get_head_tx()
        _dir = self._ensure_dir(tx_id)
        if old_head_id == _dir.name:
            raise RuntimeError('Already applied')

        new_head_id = self._make_tx_id()
        _dir_new = self.transactions_dir / new_head_id
        _dir.rename(_dir_new)  # NOTE: changing name to prevent further writes

        _did_error = True
        try:
            self._relink_head(new_head_id)
            _did_error = False
        finally:
            if _did_error:
                _dir_new.rename(_dir)  # NOTE: roll back to old name to allow further writes

        # self._forget_tx(old_head_id)  # NOTE: removal of old state is delayed to allow working from old txs (rollback)
        # ? maybe implement extendability entry point to allow subclass to skip deletion of old state and keep versions.

        return new_head_id

    def _forget_tx(self, tx_id: TxId):
        assert tx_id, 'Transaction to be removed must be well specified'
        assert tx_id != self._get_head_tx(), 'Transaction log head must not be forgotten'
        _dir = self.transactions_dir / tx_id

        # NOTE: rename tx to be forgotten to prevent access while deleting files
        _dir_new = self.transactions_dir / self._make_tx_id()
        _dir.rename(_dir_new)

        shutil.rmtree(_dir_new)

    def _relink_head(self, tx_id: TxId):
        _path = self.transactions_dir / tx_id

        assert _path.parent == self.transactions_dir, 'Transaction log head can only be linked to a child of the log'
        assert _path.is_dir(), 'Transaction log head can only be linked to a directory'
        assert not self.head_path.exists() or self.head_path.is_symlink(), \
            'Transaction log head can only be relinked from a consistent state'

        # TODO?: maybe use _path = _path.relative_to(self.head_path.parent) ?

        try:
            self.head_path.symlink_to(_path, target_is_directory=True)
        except OSError as e:
            if e.errno == errno.EEXIST:
                import warnings
                warnings.warn('symlink replace is unsupported: deleting and recreating it without strong atomicity.')
                self.head_path.unlink()
                self.head_path.symlink_to(_path, target_is_directory=True)
            else:
                raise

        self._get_head_tx()

    @contextlib.contextmanager
    def transaction(self, /, *,
                    from_id: Optional[TxId] = None,
                    read_only: Optional[bool] = False,
                    ) -> ContextManager[Path]:
        if read_only:
            yield self.transactions_dir / self.begin_read(from_id=from_id)
            return

        warnings.warn('Usage of this method for write transactions will apply without revision history entry.')

        tx_id = self.begin_write(from_id=from_id)
        did_error = True
        try:
            yield self.transactions_dir / tx_id
            did_error = False
        finally:
            if not did_error:
                self.apply(tx_id)
            else:
                self._forget_tx(tx_id)

    def continuation(self, /, *,
                     from_id: Optional[TxId] = None,
                     read_only: Optional[bool] = False,
                     ) -> Path:
        _dir = self._ensure_dir(from_id)
        if not read_only and _dir.name == self._get_head_tx():
            raise RuntimeError('Trying write continuation on head transaction')
        return _dir

    def get_pending_transaction_ids(self) -> Iterable[TxId]:
        head_tx_id = self._get_head_tx()
        for path in self.transactions_dir.iterdir():
            if path.is_dir() and path.name != head_tx_id:
                yield path.name

    def delete_pending(self, tx_id: TxId) -> None:
        self._ensure_dir(tx_id)
        if self._get_head_tx() == tx_id:
            raise RuntimeError('The transaction is not pending')
        if self._get_head_tx() <= tx_id:
            warnings.warn('Deleting transaction that does not seem to be older than the current head.')
        self._forget_tx(tx_id)