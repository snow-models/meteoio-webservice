# SPDX-License-Identifier: AGPL-3.0-or-later
from functools import cached_property
from pathlib import Path
from typing import Iterable

from pydantic import DirectoryPath
from pydantic.dataclasses import dataclass

from .config_file import TextConfigFile
from .cron import CronJobFile
from .meta import MetaConfigFile, MetaConfig


@dataclass(frozen=True)
class ConfigDirectory:
    path: DirectoryPath

    @cached_property
    def ini_dir_path(self) -> Path:
        return self.path / 'inis'

    @cached_property
    def cron_dir_path(self) -> Path:
        return self.path / 'cron'

    @cached_property
    def meta(self) -> MetaConfigFile:
        return MetaConfigFile(file=TextConfigFile(parent=self.path, name='meta'))

    def ini(self, name: str) -> TextConfigFile:
        return TextConfigFile(parent=self.ini_dir_path, name=name)

    def cron(self, job_id: str) -> CronJobFile:
        return CronJobFile(file=TextConfigFile(parent=self.cron_dir_path, name=job_id))

    def create(self):
        self.ini_dir_path.mkdir()
        self.cron_dir_path.mkdir()
        self.meta.create(MetaConfig())

    def list_ini_names(self) -> Iterable[str]:
        for path in self.ini_dir_path.iterdir():
            if path.is_file():
                yield path.name

    def list_cron_ids(self) -> Iterable[str]:
        for path in self.cron_dir_path.iterdir():
            if path.is_file():
                yield path.name

    def is_bare(self) -> bool:
        for _ in self.list_ini_names():
            return False
        return True

    # TODO?: FUSE mounts: this is a possible location where path to a json config file for fuse mounts could be added.