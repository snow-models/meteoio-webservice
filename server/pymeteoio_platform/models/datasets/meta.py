# SPDX-License-Identifier: AGPL-3.0-or-later
import json
from typing import Optional

from pydantic import TypeAdapter
from pydantic.dataclasses import dataclass
from pydantic.json import pydantic_encoder

from .config_file import TextConfigFile
from .heading_dtos import DatasetHeading


@dataclass(frozen=True)
class MetaConfig:
    main_ini_name: Optional[str] = None
    """@deprecated"""

    heading: Optional[DatasetHeading] = None


@dataclass(frozen=True)
class MetaConfigFile:
    file: TextConfigFile

    def create(self, meta: MetaConfig) -> None:
        self.file.create(json.dumps(meta, default=pydantic_encoder, indent=1))

    def read(self) -> MetaConfig:
        # noinspection PyTypeChecker
        return TypeAdapter(MetaConfig).validate_json(self.file.read())

    def update(self, meta: MetaConfig) -> None:
        self.file.update(json.dumps(meta, default=pydantic_encoder, indent=1))