# SPDX-License-Identifier: AGPL-3.0-or-later
from functools import cached_property
from pathlib import Path
from typing import Annotated

from pydantic import DirectoryPath, StringConstraints
from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class TextConfigFile:
    parent: DirectoryPath
    name: Annotated[str, StringConstraints(
        strip_whitespace=True, strict=True, min_length=3, max_length=100, pattern=r"^[a-zA-Z0-9_\-.]{3,100}$")]

    @cached_property
    def path(self) -> Path:
        return self.parent / self.name

    def create(self, content: str) -> None:
        if self.path.exists():
            raise FileExistsError
        self.path.write_text(content)

    def read(self) -> str:
        return self.path.read_text()

    def update(self, content: str) -> None:
        if not self.path.exists():
            raise FileNotFoundError
        self.path.write_text(content)

    def delete(self, allow_not_found: bool = False) -> None:
        if self.path.exists():
            self.path.unlink()
        elif not allow_not_found:
            raise FileNotFoundError