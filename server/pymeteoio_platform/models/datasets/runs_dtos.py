# SPDX-License-Identifier: AGPL-3.0-or-later
import datetime
from typing import Optional

from pydantic.dataclasses import dataclass


@dataclass(frozen=True)
class DsRunDto:
    job_id: str
    dataset_id: str
    created_at: datetime.datetime
    released: bool
    cron_id: Optional[str] = None
    ini_name: Optional[str] = None