# SPDX-License-Identifier: AGPL-3.0-or-later
import configparser
import re

from .heading_dtos import DatasetHeading, Actor, License, Location, ActorType


def _get_parser() -> configparser.ConfigParser:
    return configparser.ConfigParser(
        strict=False,  # NOTE: Allow duplicate options as we are not implementing validation here.
        delimiters=('=',),
        interpolation=None,
        empty_lines_in_values=False,
    )


_position_regex = re.compile(
    r"latlon\s*\((?P<lat>[0-9.]+),\s*(?P<lon>[0-9.]+)(,\s*(?P<h>[0-9.]+))?\)", flags=re.IGNORECASE)


def parse_heading_dto_from_ini(ini: str) -> DatasetHeading:
    parser = _get_parser()
    parser.read_string(ini)

    try:
        _creator_name = parser.get('Output', 'ACDD_CREATOR', fallback='')
        _creator_email = parser.get('Output', 'ACDD_CREATOR_EMAIL', fallback='')
        _creator_type = parser.get('Output', 'ACDD_CREATOR_TYPE', fallback='person')
        if _creator_name or _creator_email:
            creators = [Actor(type=ActorType.person if _creator_type == 'person' else None, email=_creator_email, full_name=_creator_name)]
        else:
            creators = []
    except ValueError:
        creators = []

    try:
        _license = parser.get('Output', 'ACDD_LICENSE', fallback='')
        licenses = [License(name=_license)] if _license else []
    except ValueError:
        licenses = []

    try:
        _coord_sys = parser.get('Input', 'COORDSYS', fallback='')
        _location = parser.get('Input', 'POSITION', fallback='')
        _loc_match = _position_regex.match(_location)
        locations = [Location(
            coordSys=_coord_sys,
            latitude=float(_loc_match.group('lat')),
            longitude=float(_loc_match.group('lon')),
            elevation=int(_loc_match.group('h')),
        )] if _location else []
    except ValueError:
        locations = []

    # for sec in parser.sections():
    #     print(f'[{sec}]')
    #     for opt in parser.options(sec):
    #         print(f'{opt}={parser.get(sec, opt)}')

    return DatasetHeading(
        title=parser.get('Output', 'ACDD_TITLE', fallback=''),
        description=parser.get('Output', 'ACDD_SUMMARY', fallback=''),
        # access=Access.private, [2a676a3d-17ee-4cc1-b9a5-ca08c883115a]
        creators=creators,
        licenses=licenses,
        locations=locations,
    )