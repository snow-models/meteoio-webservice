# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import warnings
from shutil import copytree
from time import sleep
from typing import Optional, Tuple, Set, Dict

from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.jobstores.base import JobLookupError
from apscheduler.jobstores.sqlalchemy import SQLAlchemyJobStore
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.schedulers.base import BaseScheduler
from apscheduler.triggers.cron import CronTrigger
from pytz import utc

from . import DatasetsManager, ConfigDirectory, Dataset, RunRelation
from ..di import get_global_entity_manager
from ..errors import NotFound

_logger = logging.getLogger(__name__)


def _get_scheduler_cron_job_id(dataset_id: str, cron_id: str) -> str:
    # NOTE: the returned id string must fit in Column('id', Unicode(191), primary_key=True)
    return f"{dataset_id}:{cron_id}"


def _reverse_scheduler_cron_job_id(job_id: str) -> Tuple[str, str]:
    dataset_id, cron_id = job_id.split(':', 1)
    return dataset_id, cron_id


def make_cron_scheduler(db_url: str) -> BaseScheduler:
    # NOTE: sqlite:///<path> with that many slashes before abs path, e.g. 'sqlite:////mnt/c/Users/...'
    # print(f"make_cron_scheduler {db_url=}")
    # assert Path(db_url.split(':///', maxsplit=1)[1]).parent.is_dir(), Path(db_url).parent
    scheduler = BackgroundScheduler(
        jobstores={"default": SQLAlchemyJobStore(url=db_url)},
        executors={"default": ThreadPoolExecutor(max_workers=1)},
        timezone=utc,
        job_defaults=dict(
            max_instances=1,
            coalesce=True,
            misfire_grace_time=30,
        ),
    )
    return scheduler


def update_scheduling_all(self: 'DatasetsManager'):
    # NOTE: for each configured cron, we update the scheduling.
    ds_id_to_cron_ids: Dict[str, Set[str]] = dict()  # also remember the ids found
    for dataset_id in self.iter_ids():
        ds = self.get(dataset_id, check_user_permission=False)
        config = ConfigDirectory(path=ds.config.continuation(read_only=True))
        update_scheduling_ds(self, dataset=ds, config=config)
        ds_id_to_cron_ids[dataset_id] = set(config.list_cron_ids())

    # NOTE: we are removing the unexpected jobs (stuff that has been deleted from configuration).
    for job in self.scheduler.get_jobs():
        dataset_id, cron_id = _reverse_scheduler_cron_job_id(job.id)
        if cron_id not in ds_id_to_cron_ids.get(dataset_id, set()):
            self.scheduler.remove_job(job.id)


def update_scheduling_ds(self: 'DatasetsManager', dataset: Dataset,
                         config: Optional[ConfigDirectory] = None):

    if config is None:
        config = ConfigDirectory(path=dataset.config.continuation(read_only=True))

    for cron_id in config.list_cron_ids():
        update_scheduling_one(self, dataset=dataset, cron_id=cron_id, config=config)

    # NOTE: we are safely ignoring the fact that datasets and cron jobs
    #       could have been removed from configurations but not from the scheduler.
    #       See _job_func: it checks for non-existence of dataset and cron.


def update_scheduling_one(self: 'DatasetsManager', dataset: Dataset, cron_id: str,
                          config: Optional[ConfigDirectory] = None):
    """Refreshes the scheduling by adding, rescheduling or removing the job."""

    if config is None:
        config = ConfigDirectory(path=dataset.config.continuation(read_only=True))

    dataset_id = dataset.dir.id
    scheduler_job_id = _get_scheduler_cron_job_id(dataset_id, cron_id)

    cron_job_conf = config.cron(cron_id)

    if cron_job_conf.file.path.exists():
        # NOTE: cron config exists => add or reschedule

        trigger = CronTrigger.from_crontab(
            expr=cron_job_conf.read().cron,
            timezone=utc,
        )
        trigger.jitter = 10

        if self.scheduler.get_job(scheduler_job_id) is None:
            # NOTE: schedule not found => add
            self.scheduler.add_job(
                replace_existing=True,
                id=scheduler_job_id,
                trigger=trigger,
                func=_job_func,
                max_instances=1,
                coalesce=True,
                misfire_grace_time=30,
                kwargs={
                    "dataset_id": dataset_id,
                    "cron_id": cron_id,
                },
            )
        else:
            # NOTE: schedule found => reschedule (the rest of the info is stable after creation)
            self.scheduler.reschedule_job(
                job_id=scheduler_job_id,
                trigger=trigger,
            )

    else:
        try:
            self.scheduler.remove_job(scheduler_job_id)
        except JobLookupError:
            pass


def _job_func(
        *,
        dataset_id: str,
        cron_id: str,
) -> None:
    _logger.debug(f"Going to run {cron_id=}")

    entity_manager = get_global_entity_manager()

    try:
        ds = entity_manager.datasets.get(dataset_id, must_exist=True, check_user_permission=False)
    except NotFound:
        warnings.warn('Ignoring cron job for a dataset that is not found.')
        return

    config_path = ds.config.continuation(read_only=True)
    config = ConfigDirectory(path=config_path)
    cron_job_config = config.cron(cron_id)

    if not cron_job_config.file.path.exists():
        warnings.warn('Ignoring cron job for a cron configuration that is not found.')
        return

    # TODO?: try to avoid the above "not found" warnings without relying on a periodic update_scheduling_all

    cron_job = cron_job_config.read()

    # Ok, let's prepare the run:
    #   - create guest job
    #   - add run relation
    #   - prepare input data files and config files
    #   - set command
    #   - start execution
    #   - wait end of execution
    #   - release run if success

    # create guest job
    job = entity_manager.guest_jobs.make_job(create=True)
    _logger.info(f"Created {job.id=} for {cron_id=} {dataset_id=} -- Preparing...")

    # add run relation
    with entity_manager.datasets.runs_index.transaction() as tx:
        tx.add(RunRelation(
            job_id=job.id,
            dataset_id=ds.dir.id,
            cron_id=cron_id,
            ini_name=cron_job.params.ini,  # NOTE: assuming params is a TimeSeriesJobParams
        ))

    # prepare input data and config files
    copytree(ds.user_path, job.dir.user_path, dirs_exist_ok=True)
    copytree(config.ini_dir_path, job.dir.user_path, dirs_exist_ok=True)

    # set command
    job.set_command(cron_job.get_cmd(inis_dir=job.dir.user_path))

    # start execution
    _logger.debug(f"Starting {job.id=} ...")
    entity_manager.guest_jobs.start(job, blocking=True, retry_remote=False)
    # above call does not return before end of execution
    sleep(0.1)
    _return_code = job.get_stats().return_code
    _logger.debug(f"Finished {job.id=} with return code {_return_code} ...")

    # release run if success
    if _return_code == 0:
        entity_manager.datasets.release_run(job=job, dataset=ds)
        _logger.info(f"Cron job finished with updated release of {job.id=} for {cron_id=} {dataset_id=}")
    else:
        _logger.warning(f"Cron job finished with failure of {job.id=} for {cron_id=} {dataset_id=}")