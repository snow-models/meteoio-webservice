# SPDX-License-Identifier: AGPL-3.0-or-later
import datetime
from typing import Iterable, Optional

from git import Repo, Actor
from pydantic import DirectoryPath
from pydantic.dataclasses import dataclass, Field


@dataclass(frozen=True)
class CommitMsgDto:
    author_name: str = Field(strip_whitespace=True, min_length=3, max_length=100)
    author_email: str = Field(strip_whitespace=True, to_lower=True, min_length=3, max_length=100)
    message: str = Field(strip_whitespace=True, min_length=3, max_length=1024)


@dataclass(frozen=True)
class LogCommitDto:
    hash: str
    message: str
    author_name: str
    author_email: str
    authored_datetime: datetime.datetime


@dataclass(frozen=True)
class RefLogDto:
    old_hash: str
    new_hash: str
    message: str
    actor_name: str
    actor_email: str
    datetime: datetime.datetime

    def __repr__(self):
        return f'{self.new_hash[:8]} {self.message} [{self.actor_name} <{self.actor_email}>]'


@dataclass(frozen=True)
class VersionController:
    working_tree_dir: DirectoryPath

    def init(self):
        with Repo.init(self.working_tree_dir) as repo:
            pass

    def commit_all(self, dto: CommitMsgDto) -> str:
        with Repo(self.working_tree_dir) as repo:
            actor = Actor(dto.author_name, dto.author_email)
            repo.git.add(all=True)
            commit = repo.index.commit(
                message=dto.message,
                author=actor,
                committer=actor,
            )
            return commit.hexsha

    def get_commits(self, /, *, max_count: Optional[int] = None) -> Iterable[LogCommitDto]:
        with Repo(self.working_tree_dir) as repo:
            for commit in repo.iter_commits('--all', max_count=max_count):
                yield LogCommitDto(
                    hash=commit.hexsha,
                    message=commit.message,
                    author_name=commit.author.name,
                    author_email=commit.author.email,
                    authored_datetime=commit.authored_datetime,
                )

    def get_reflog(self) -> Iterable[str]:
        with Repo(self.working_tree_dir) as repo:
            for row in repo.head.log():
                yield RefLogDto(
                    old_hash=row.oldhexsha,
                    new_hash=row.newhexsha,
                    message=row.message,
                    actor_name=row.actor.name,
                    actor_email=row.actor.email,
                    datetime=datetime.datetime.utcfromtimestamp(row.time[0]).replace(tzinfo=datetime.timezone.utc),
                )

    def reset(self, commit_hash: str):
        with Repo(self.working_tree_dir) as repo:
            repo.head.reset(
                index=True,
                working_tree=True,
                commit=repo.commit(commit_hash),
            )

# TODO: PROBLEM: files are kept open.