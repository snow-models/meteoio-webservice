# SPDX-License-Identifier: AGPL-3.0-or-later
import json
from pathlib import Path
from typing import Literal, Union, NamedTuple, Optional

import uuid6
from pydantic import model_validator, TypeAdapter, parse_obj_as
from pydantic.dataclasses import dataclass
from pydantic.json import pydantic_encoder

from .config_file import TextConfigFile
from ..di import get_global_entity_manager
from ..jobs import JobCommand, JobProcessPriority
from ..timeseries import TimeSeriesJobParams


class CronGlobalEntry(NamedTuple):
    dataset_id: str
    cron_id: str


@dataclass()
class CronJob:
    """Data transfer object for a cron job configuration"""
    cron: str
    type: Union[Literal['meteoio_timeseries']]
    params: Union[TimeSeriesJobParams]  # NOTE: _cron_scheduler assumes it is a TimeSeriesJobParams.
    id: Optional[str] = None

    # noinspection PyMethodParameters
    @model_validator(mode='after')
    def _check_type_params(self):
        if self.type == 'meteoio_timeseries':
            if not isinstance(self.params, TimeSeriesJobParams):
                raise ValueError(f'Unexpected type `{repr(type(self.params))}` for meteoio_timeseries job params')
            return self
        raise ValueError('Unexpected cron job type')

    def get_cmd(self, /, *, inis_dir: Path) -> JobCommand:
        assert isinstance(self.params, TimeSeriesJobParams)
        assert self.type == 'meteoio_timeseries'
        return JobCommand(
            executable=get_global_entity_manager().meteoio_timeseries_executable,
            args=self.params.get_command_args(inis_dir=inis_dir),
            priority=JobProcessPriority.below_normal,
        )

    @classmethod
    def make_id(cls) -> str:
        return str(uuid6.uuid7())


@dataclass(frozen=True)
class CronJobFile:
    file: TextConfigFile

    @staticmethod
    def _serialize(job: CronJob) -> str:
        _str = json.dumps(job, default=pydantic_encoder, indent=1)
        _obj = json.loads(_str)
        del _obj['id']
        return json.dumps(_obj)

    def create(self, job: CronJob) -> None:
        self.file.create(self._serialize(job))

    def read(self) -> CronJob:
        # noinspection PyTypeChecker
        job = TypeAdapter(CronJob).validate_json(self.file.read())
        job.id = self.file.name
        return job

    def update(self, job: CronJob) -> None:
        self.file.update(self._serialize(job))

    def delete(self):
        self.file.delete()