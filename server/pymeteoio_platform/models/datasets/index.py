# SPDX-License-Identifier: AGPL-3.0-or-later
"""
    Secondary storage for datasets indexing
"""

import datetime
import json
from contextlib import contextmanager
from functools import cached_property
from typing import Optional, ContextManager, Union, Dict, List

from pydantic import parse_obj_as
from pydantic.dataclasses import dataclass
from pydantic.json import pydantic_encoder
from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.orm import Mapped, registry, mapped_column, sessionmaker, Session, relationship

from .heading_dtos import Access, DatasetHeading, DatasetListEntry, DatasetRevisionsListEntry

tables_registry = registry()

AttributeValueT = Union[str, float, bool, 'AttributesT', None]  # TypeAlias requires python 3.10 ?
AttributesT = Dict[str, AttributeValueT]


@tables_registry.mapped_as_dataclass
class DatasetRelation:
    __tablename__ = "datasets"

    dataset_id: Mapped[str] = mapped_column(
        primary_key=True, nullable=False)

    created_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False)

    owner_user_id: Mapped[Optional[str]] = mapped_column(
        nullable=False)

    access: Mapped[Access] = mapped_column(
        nullable=False, default=Access.private)

    attributes: Mapped[str] = mapped_column(
        nullable=False, default='{}')

    revisions: Mapped[List['DatasetRevision']] = relationship(
        back_populates='dataset', default_factory=list, cascade='all')

    def _get_attributes(self) -> AttributesT:
        return json.loads(self.attributes)

    def _set_attribute(self, key: str, value: AttributeValueT):
        kv = self._get_attributes()
        kv.update({key: value})
        # noinspection PyTypeChecker
        self.attributes = json.dumps(kv, sort_keys=True, indent=0, default=pydantic_encoder)

    @property
    def heading(self) -> DatasetHeading:
        _h = self._get_attributes().get('heading')
        if _h:
            return parse_obj_as(DatasetHeading, _h)
        return DatasetHeading()

    @heading.setter
    def heading(self, value: Optional[DatasetHeading]):
        self._set_attribute('heading', None if value is None else pydantic_encoder(value))

    def get_dto(self) -> DatasetListEntry:
        return DatasetListEntry(
            id=self.dataset_id,
            owner_user_id=self.owner_user_id,
            access=self.access,
            heading=self.heading,
        )


@tables_registry.mapped_as_dataclass
class DatasetRevision:
    __tablename__ = "dataset_revisions"

    id: Mapped[str] = mapped_column(
        primary_key=True, nullable=False)

    dataset_id: Mapped[str] = mapped_column(
        ForeignKey('datasets.dataset_id'), nullable=False)

    created_at: Mapped[Optional[datetime.datetime]] = mapped_column(
        nullable=True, default=None)

    updated_at: Mapped[Optional[datetime.datetime]] = mapped_column(
        nullable=True, default=None)

    confirmed_at: Mapped[Optional[datetime.datetime]] = mapped_column(
        nullable=True, default=None)

    emptied_at: Mapped[Optional[datetime.datetime]] = mapped_column(
        nullable=True, default=None)

    created_from_id: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    number: Mapped[Optional[int]] = mapped_column(
        nullable=True, default=None)

    title: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    message: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    dataset: Mapped[DatasetRelation] = relationship(
        back_populates='revisions', default=None)

    def get_dto(self) -> DatasetRevisionsListEntry:
        return DatasetRevisionsListEntry(
            id=self.id,
            created_at=self.created_at,
            updated_at=self.updated_at,
            confirmed_at=self.confirmed_at,
            emptied_at=self.emptied_at,
            created_from_id=self.created_from_id,
            number=self.number,
            title=self.title,
            message=self.message,
        )


@dataclass(frozen=True)
class DatasetsIndexStore:
    url: str

    @cached_property
    def _session_maker(self) -> sessionmaker:
        e = create_engine(url=self.url)
        tables_registry.metadata.create_all(e)
        return sessionmaker(e, autoflush=False, expire_on_commit=True)

    @contextmanager
    def transaction(self) -> ContextManager[Session]:
        # TODO?: get global lock?
        s = self._session_maker()
        with s.begin():
            yield s