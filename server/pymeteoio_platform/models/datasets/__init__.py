# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import shutil
from datetime import datetime, timezone
from functools import cached_property
from pathlib import Path
from typing import Iterable, Optional, Union, Literal, List

from pydantic import DirectoryPath
from pydantic.dataclasses import dataclass
from sqlalchemy import select
from sqlalchemy.orm import Session

from .config_dir import ConfigDirectory
from .cron import CronGlobalEntry
from .heading_dtos import DatasetHeading, DatasetListEntry, FsEntry, FsEntryType, Access
from .index import DatasetsIndexStore, DatasetRelation
from .ini_parse import parse_heading_dto_from_ini
from .runs_dtos import DsRunDto
from .runs_index import RunsIndexStore, RunRelation
from .transactional_dir import TransactionalDirectory
from ..errors import Forbidden, NotFound
from ..guest_directories import GuestDirectory, GuestDirectoriesRoot
from ..jobs import Job

_LOGGER = logging.getLogger(__name__)

# NOTE: Caching of paths and directories: they should only be references.
#       Please remember that the file system content can change from external operations.


@dataclass(frozen=True)
class Dataset:
    dir: GuestDirectory

    # TODO?: encode access level also here and check permission in each method?

    @cached_property
    def _ds_path(self):
        return self.dir.root_path / 'ds'

    @cached_property
    def _config_path(self):
        return self._ds_path / 'config'

    @cached_property
    def _config_txs_path(self):
        return self._ds_path / 'config_txs'

    @cached_property
    def user_path(self):
        """Data files of the dataset"""
        return self.dir.user_path

    @cached_property
    def config(self) -> TransactionalDirectory:
        return TransactionalDirectory(
            head_path=self._config_path,
            transactions_dir=self._config_txs_path,
        )

    def create(self):
        self.dir.create()
        self._ds_path.mkdir()
        self._config_txs_path.mkdir()
        with self.config.init() as path:
            ConfigDirectory(path=path).create()

    def get_heading(self, /, *, tx_id: Optional[str] = None) -> Optional[DatasetHeading]:
        with self.config.transaction(read_only=True, from_id=tx_id) as path:
            config = ConfigDirectory(path=path)
            meta = config.meta.read()
            return meta.heading

    def iter_runs(self, /, *, tx: Session, released: Optional[bool] = None) -> Iterable[DsRunDto]:
        _st0 = (select(RunRelation)
                .filter(RunRelation.dataset_id == self.dir.id))

        _st1 = _st0 if released is None else _st0.filter(RunRelation.released == released)

        _st = (_st1
               .order_by(RunRelation.job_id.desc())
               .limit(20))  # TODO: paging?
        for run_rel, in tx.execute(_st).unique().all():
            run_rel: RunRelation
            yield run_rel.get_dto()

@dataclass(frozen=True)
class DatasetsManager:
    dir_ctrl: GuestDirectoriesRoot
    public_dir: Path  # NOTE: remember to check all accesses to this path for data access policy
    meta_dir: DirectoryPath

    @cached_property
    def scheduler(self):
        from ._cron_scheduler import make_cron_scheduler
        # NOTE: sqlite:///<path> with that many slashes before abs path, e.g. 'sqlite:////mnt/c/Users/...'
        return make_cron_scheduler(f"sqlite:///{(self.meta_dir / 'cron_scheduler.sqlite').absolute()}")

    @cached_property
    def index(self):
        return DatasetsIndexStore(
            url=f"sqlite:///{(self.meta_dir / 'db.sqlite').absolute()}")

    @cached_property
    def runs_index(self):
        return RunsIndexStore(
            url=f"sqlite:///{(self.meta_dir / 'runs_db.sqlite').absolute()}")

    def make(self, /, *, user_id: str, create: bool = False) -> Dataset:
        assert create, 'Making of a Dataset instance must also create its support files'
        _dir = self.dir_ctrl.make_dir()
        ds = Dataset(dir=_dir)
        ds.create()
        with self.index.transaction() as tx:
            tx.add(DatasetRelation(
                dataset_id=ds.dir.id,
                created_at=datetime.utcnow().replace(tzinfo=timezone.utc, microsecond=0),
                owner_user_id=user_id,
                access=Access.private,
            ))
        return ds

    def get(self, _id: str, /, *, must_exist: bool = True,
            user_id: Optional[str] = None, check_user_permission: Union[bool, Literal['readonly']] = True):
        """
        Get access to a dataset.

        NOTE: no exhaustive permission check is enforced here. See external APIs instead (guards, DI, etc.).

        :param _id: of the dataset
        :param must_exist: if True (default) then raises NotFound if the underlying data dir does not exist.
        :param user_id: to check permission for
        :param check_user_permission:
                    if True (default) then  Maximum access level is required.
                    if 'readonly'     then  Public access level is required.
                    if False          then  No access level is required.
        """
        _dir = self.dir_ctrl.get_dir(_id, must_exist=must_exist)
        if check_user_permission:
            with self.index.transaction() as tx:
                __ds_rel = tx.get(DatasetRelation, _id)
                __owner = __ds_rel.owner_user_id
                assert __owner, 'All datasets must specify some owner_user_id in order to check access permission.'
                if __owner != user_id:
                    if __ds_rel.access != Access.public:
                        raise Forbidden
                    if check_user_permission != 'readonly':
                        raise Forbidden
        return Dataset(dir=_dir)

    def iter_ids(self) -> Iterable[str]:
        for child in self.dir_ctrl.list_dirs():
            yield child.id

    def iter_headings_for_user(self, /, *, user_id: str) -> Iterable[DatasetListEntry]:
        with self.index.transaction() as tx:
            stm = select(DatasetRelation).filter(DatasetRelation.owner_user_id == user_id)
            for ds_rel, in tx.execute(stm).unique():
                ds_rel: DatasetRelation
                yield ds_rel.get_dto()

    def _publish_public_files_copy(self, /, *, job: Job, dataset: Dataset) -> None:
        # NOTE: All write access to self.public_dir should be mediated by this method!
        _pub_ds_dir = self.public_dir / dataset.dir.id  # checked write access
        # shutil.copytree(job.dir.user_path, _pub_ds_dir / job.id)  # This would expose INI files
        # NOTE: Duplicate access control policy implementation code. [bf99b2d5-56b9-4222-bed8-e1990ecf5627]
        _pub_job_dir = _pub_ds_dir / job.id
        _pub_job_dir.mkdir(parents=True, exist_ok=True)
        for row in job.get_strace().iter_unique_files(output_only=True, cwd=job.dir.user_path):
            _dst_path = _pub_job_dir / row.open_file_path
            _dst_path.parent.mkdir(parents=True, exist_ok=True)
            shutil.copy(
                job.dir.user_path / row.open_file_path,
                _dst_path,
            )

    def release_run(self, /, *, job: Job, dataset: Dataset):
        _LOGGER.debug(f'Releasing run {job.id=} {dataset.dir.id=} ...')

        _cron_id: Optional[str] = None
        with self.runs_index.transaction() as tx:
            run_rel: Optional[RunRelation] = tx.get(RunRelation, job.id)
            if run_rel is None:
                raise NotFound
            if run_rel.dataset_id != dataset.dir.id:
                raise NotFound

            # copytree(job.dir.root_path, dataset.runs_path / job.id, dirs_exist_ok=False)
            # NOTE: Do we need to copy the job?

            # with self.runs_index.transaction() as tx:
            run_rel: Optional[RunRelation] = tx.get(RunRelation, job.id)
            run_rel.released = True
            _cron_id = run_rel.cron_id

        # Publish run if dataset access is public
        _do_publish_run = False
        with self.index.transaction() as tx:
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            _do_publish_run = (ds_rel.access == Access.public)
        if _do_publish_run:
            self._publish_public_files_copy(job=job, dataset=dataset)

        # Discard other runs of the same cron, if it's a cron...
        if _cron_id is not None:
            _run_ids: List[str] = []
            with self.runs_index.transaction() as tx:
                _st = (select(RunRelation)
                       .filter(RunRelation.dataset_id == dataset.dir.id)
                       .filter(RunRelation.cron_id == _cron_id)
                       .filter(RunRelation.job_id != job.id))
                for _run_rel, in tx.execute(_st).unique().all():
                    _run_rel: RunRelation
                    _run_ids.append(_run_rel.job_id)

            from ..di import get_global_entity_manager
            em = get_global_entity_manager()

            if len(_run_ids) <= 1:
                _LOGGER.info(f'Removing {len(_run_ids)} past runs of {_cron_id=} {dataset.dir.id=} ...')
            else:
                _LOGGER.warning(f'Removing {len(_run_ids)} past runs of {_cron_id=} {dataset.dir.id=} ...')

            # Remove guest job dir
            for _run_id in _run_ids:
                _job: Job = em.guest_jobs.get_job(_run_id, must_exist=False)
                if _job.dir.user_path.exists():
                    _job.dir.remove(recursive=True, confirm=_run_id)
                em.guest_jobs.remove_lost_job_from_indexes(job=_job)  # TODO?: this runs txs in a loop -- make one?

            # Remove public copy
            if _do_publish_run:
                for _run_id in _run_ids:
                    _pub_run_dir = self.public_dir / dataset.dir.id / _run_id  # rm-only access to self.public_dir
                    if _pub_run_dir.exists():
                        shutil.rmtree(_pub_run_dir)

        _LOGGER.info(f'Successfully released {job.id=} {dataset.dir.id=}')

    def get_access(self, /, *, dataset: Dataset) -> Access:
        with self.index.transaction() as tx:
            ds_rel: Optional[DatasetRelation]
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            assert ds_rel, 'The provided dataset must exist.'
            return ds_rel.access

    def set_access(self, /, *, dataset: Dataset, access: Access) -> None:
        # Remove public directory if removing public access
        if access != Access.public:
            _pub_ds_dir = self.public_dir / dataset.dir.id  # rm-only access to self.public_dir
            if _pub_ds_dir.exists():
                shutil.rmtree(_pub_ds_dir)

        # Set value in DB
        with self.index.transaction() as tx:
            ds_rel: Optional[DatasetRelation]
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            assert ds_rel, 'The provided dataset must exist.'
            ds_rel.access = access

        # Copy files to public directory if granting public access
        if access == Access.public:
            _job_ids = []
            with self.runs_index.transaction() as tx:
                for _run in dataset.iter_runs(tx=tx, released=True):
                    _job_ids.append(_run.job_id)
            from ..di import get_global_entity_manager
            em = get_global_entity_manager()
            for _job_id in _job_ids:
                _job = em.guest_jobs.get_job(_job_id)
                self._publish_public_files_copy(job=_job, dataset=dataset)

    def transfer_dataset(self, /, *, dataset: Dataset, old_owner_user_id: str, new_owner_user_id: str):
        # NOTE: assuming the caller has already checked that the new owner has permissions to own datasets.
        with self.index.transaction() as tx:
            ds_rel: Optional[DatasetRelation]
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            if ds_rel.owner_user_id != old_owner_user_id:
                raise RuntimeError('Stopping dataset transfer due to consistency check failure: old owner mismatch.')
            ds_rel.owner_user_id = new_owner_user_id

    def delete_dataset(self, /, *, dataset: Dataset, dataset_id_for_check: str, owner_user_id_for_check: str):
        # TODO?: offer soft delete before permanent delete?
        if dataset.dir.id != dataset_id_for_check:
            raise RuntimeError('Stopping dataset deletion due to consistency check failure: id mismatch.')

        with self.index.transaction() as tx:
            ds_rel: Optional[DatasetRelation]
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            if ds_rel.owner_user_id != owner_user_id_for_check:
                raise RuntimeError('Stopping dataset deletion due to consistency check failure: owner mismatch.')

        _pub_ds_dir = self.public_dir / dataset.dir.id  # rm-only access to self.public_dir
        if _pub_ds_dir.exists():
            shutil.rmtree(_pub_ds_dir)

        dataset.dir.remove(confirm=dataset_id_for_check, recursive=True)

        with self.index.transaction() as tx:
            ds_rel: Optional[DatasetRelation]
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            tx.delete(ds_rel)

    def update_indexed_dataset_heading(self, dataset: Dataset, tx_id: Optional[str] = None) -> None:
        """This shall always be called before Dataset.config.apply"""
        # NOTE: the programmer is responsible for calling this function in all cases when it's appropriate.

        __heading = dataset.get_heading(tx_id=tx_id)
        with self.index.transaction() as tx:
            ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            ds_rel.heading = __heading

    def notify_dataset_config_after_update(self, dataset: Dataset) -> None:
        """This shall always be called after Dataset.config.apply"""
        # NOTE: the programmer is responsible for calling this function in all cases when it's appropriate.

        from ._cron_scheduler import update_scheduling_ds
        update_scheduling_ds(self, dataset=dataset)

    def update_scheduling_all(self):
        from ._cron_scheduler import update_scheduling_all
        update_scheduling_all(self)