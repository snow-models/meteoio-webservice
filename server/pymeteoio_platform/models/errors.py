# SPDX-License-Identifier: AGPL-3.0-or-later
class NotFound(LookupError):
    pass


class UserReportableException(RuntimeError):
    pass


class UserReportableBadRequestException(UserReportableException):
    pass


class AuthException(RuntimeError):
    pass


class AuthenticationFailure(AuthException):
    pass


class Forbidden(AuthException):
    pass
