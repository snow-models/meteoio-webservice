# SPDX-License-Identifier: AGPL-3.0-or-later
import datetime
from contextlib import contextmanager
from functools import cached_property
from typing import Optional, ContextManager

from pydantic.dataclasses import dataclass
from sqlalchemy import create_engine
from sqlalchemy.orm import Mapped, registry, mapped_column, sessionmaker, Session

tables_registry = registry()


@tables_registry.mapped_as_dataclass
class JobRelation:
    __tablename__ = "jobs"

    job_id: Mapped[str] = mapped_column(
        primary_key=True, nullable=False)

    created_at: Mapped[datetime.datetime] = mapped_column(
        nullable=False)

    submitted_by_user_id: Mapped[Optional[str]] = mapped_column(
        nullable=True, default=None)

    finished_at: Mapped[datetime.datetime] = mapped_column(
        nullable=True, default=None)

    # NOTE: Avoiding storage of reference to the dataset from which this job may be created from: extra coupling
    #       See dataset runs instead.
    # NOTE: Direct ref to data owner user id (from the dataset) is desirable. [7dde4b06-6969-4c1d-b488-9ca762af84ef]


@dataclass(frozen=True)
class JobsIndexStore:
    url: str

    @cached_property
    def _session_maker(self) -> sessionmaker:
        e = create_engine(url=self.url)
        tables_registry.metadata.create_all(e)
        return sessionmaker(e, autoflush=False, expire_on_commit=True)

    @contextmanager
    def transaction(self) -> ContextManager[Session]:
        s = self._session_maker()
        with s.begin():
            yield s
