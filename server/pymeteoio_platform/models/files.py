# SPDX-License-Identifier: AGPL-3.0-or-later
import os
import sys
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from typing import Union

if sys.version_info[:2] < (3, 9):
    PathLike = Union[str, os.PathLike]
else:
    # os.PathLike only becomes subscriptable from Python 3.9 onwards
    PathLike = Union[str, os.PathLike[str]]


class FsEntryType(str, Enum):
    file = 'file'
    folder = 'folder'


@dataclass()
class FsEntry:
    path: str
    date: datetime
    size: int
    type: FsEntryType
