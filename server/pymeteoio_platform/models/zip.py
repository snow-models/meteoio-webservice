# SPDX-License-Identifier: AGPL-3.0-or-later
import zlib
from datetime import timezone, datetime
from pathlib import Path
from typing import Iterable, Optional

from stream_unzip import stream_unzip
from stream_zip import stream_zip, ZIP_32


def export_zip_stream(*, root: Path, compression_level: int = 6) -> Iterable[bytes]:
    def _mk_files():
        for __path in root.glob('**/*'):
            if __path.is_file():
                _stat = __path.stat()

                def _mk_chunks():
                    with __path.open('rb') as f:
                        while chunk := f.read(4096):
                            yield chunk

                _rel_path = str(__path.relative_to(root))
                yield (  # name, modified_at, mode, method, chunks
                    _rel_path,
                    datetime.utcfromtimestamp(_stat.st_mtime).replace(tzinfo=timezone.utc),
                    _stat.st_mode,
                    ZIP_32,
                    _mk_chunks()
                )
                # print(f'Zipping {_rel_path}')

    yield from stream_zip(
        _mk_files(),
        get_compressobj=lambda: zlib.compressobj(wbits=-zlib.MAX_WBITS, level=compression_level)
    )


def import_zip_stream(*, root: Path, zipped_chunks: Iterable[bytes], password: Optional[bytes] = None) -> None:
    for file_name, file_size, unzipped_chunks in stream_unzip(zipped_chunks, password=password):
        __path = (root / file_name.decode()).resolve()
        # assert root in __path.parents
        # assert __path.relative_to(root)
        # print(f'Unzipping {__path}')
        __path.parent.mkdir(parents=True, exist_ok=True)
        with __path.open('wb') as f:
            for chunk in unzipped_chunks:
                f.write(chunk)
