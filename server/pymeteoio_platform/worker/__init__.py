# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import random
import time

from ..app.entity_manager import get_entity_manager
from ..ogc.wps.cleanup_old_files import cleanup_old_files

LOGGER = logging.getLogger(__name__)


def main():
    # noinspection PyBroadException
    try:
        entity_manager = get_entity_manager()
        time.sleep(random.uniform(0, 20))

        for i in range(60):
            if i > 0:
                time.sleep(60 + random.uniform(-4, 4))

            LOGGER.info(f'Running guest_jobs.enforce_quota_policy...')
            try:
                entity_manager.guest_jobs.enforce_quota_policy()
            except Exception as ex:
                LOGGER.exception(ex)
            LOGGER.info(f'Finished guest_jobs.enforce_quota_policy.')

            LOGGER.info(f'Running cleanup of old PyWPS files...')
            try:
                cleanup_old_files()
            except Exception as ex:
                LOGGER.exception(ex)
            LOGGER.info(f'Finished cleanup of old PyWPS files.')

        time.sleep(5)

        LOGGER.info(f'Letting this process die for a clean restart...')
    except:
        LOGGER.exception('Unexpected failure, will restart')
        exit(-1)
