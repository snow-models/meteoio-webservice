# SPDX-License-Identifier: AGPL-3.0-or-later
from pathlib import Path

from birdy import WPSClient

# https://birdy.readthedocs.io/en/latest/installation.html
# pip install birdhouse-birdy

# DO NOT pip install pymetalink -- WARNING: installing pymetalink can introduce broken dependencies.
# This is a client for testing purposes: we will manually inspect the result without automatic conversions.


client = WPSClient(
    'http://127.0.0.1:8095/api/public/ogc/wps',
    progress=True
)

# noinspection PyUnresolvedReferences
ex = client.meteoio_timeseries(
    begin='2008-12-09',
    duration_days='2',
    resolution_minutes=240,
    ini_file_name='io.ini',
    data=open(Path(__file__).parent.parent.parent.parent / 'test_job.zip', 'rb'),
)

print(repr(ex.get()))
