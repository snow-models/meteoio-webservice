# SPDX-License-Identifier: AGPL-3.0-or-later
from datetime import datetime
from typing import List

from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import make_transient

from ..models.errors import AuthException
from ..models.users import UsersStore, User

us = UsersStore(url="sqlite:///")

_test_user_id: str
with us.transaction() as tx:
    u = User()
    _test_user_id = u.id
    u.full_name = f"Test user {datetime.utcnow().isoformat()}"
    u.password_hash = u.make_new_hash("password")
    print("Created", u)
    tx.add(u)
print("Inserted.")

with us.transaction() as tx:
    u = tx.get(User, _test_user_id)
    u.verify_password("password")
    print("Verified as", u)

uu: List[User]
with us.transaction() as tx:
    uu = [User() for _ in range(1000)]
    tx.add_all(uu)
print("Added a lot of users.")

with us.transaction() as tx:
    _uu_back = tx.query(User).all()
    assert len(_uu_back) == len(uu) + len([_test_user_id])
    print("Got them back...")

    _uu_back_ids = set(u.id for u in _uu_back)
    _uu_back_ids.remove(_test_user_id)
    assert len(_uu_back_ids) == len(uu), "user id must be unique identifier"
    print("Checked the ids for uniqueness.")

_new_full_name = "Test user name 22934752034857483749086202"
try:
    with us.transaction() as tx:
        u = tx.get(User, _test_user_id)
        u.full_name = _new_full_name
        raise AuthException
except AuthException:
    with us.transaction() as tx:
        u = tx.get(User, _test_user_id)
        assert u.full_name
        assert u.full_name != _new_full_name
    print("Not updated as expected after exception.")
else:
    raise AssertionError("It should have not hidden the exception.")

with us.transaction() as tx:
    u = tx.get(User, _test_user_id)
    u.full_name = _new_full_name
with us.transaction() as tx:
    u = tx.get(User, _test_user_id)
    assert u.full_name == _new_full_name
print("Updated as expected.", u)

u = User()
u.id = _test_user_id
u.full_name = "Test 87348347"
try:
    with us.transaction() as tx:
        tx.add(u)
except IntegrityError as ex:
    print("Successfully raised the expected exception after trying to add duplicate user: ",
          ex.__class__, ex.orig)
else:
    raise AssertionError("It should have prevented duplicates.")

with us.transaction() as tx:
    u = tx.get(User, _test_user_id)
    assert u.full_name == _new_full_name
print("Read back original user.")

with us.transaction() as tx:
    tx.delete(u)
print("Deleted...")

with us.transaction() as tx:
    assert tx.get(User, _test_user_id) is None
print("Deleted and then not found, as expected.")

with us.transaction() as tx:
    make_transient(u)
    tx.add(u)
print("Re-inserted.")

with us.transaction() as tx:
    u = tx.get(User, _test_user_id)
    assert u.full_name == _new_full_name
    print("Successfully read back again original user.")

_test_email = "test@example.com"
with us.transaction() as tx:
    u = tx.get(User, _test_user_id)
    u.email = _test_email
print("Set user email.")

with us.transaction() as tx:
    u = tx.query(User).filter(User.email == _test_email).one()
    assert u.email == _test_email
    print("Successfully retrieved user by email")

try:
    with us.transaction() as tx:
        u = User()
        u.email = _test_email
        tx.add(u)
except IntegrityError as ex:
    print("Successfully raised the expected exception after trying to add duplicate user by email: ",
          ex.__class__, ex.orig)
else:
    raise AssertionError("It should have prevented duplicates by email.")
