# SPDX-License-Identifier: AGPL-3.0-or-later
import socket
from pathlib import Path
from unittest import IsolatedAsyncioTestCase

import asyncssh

from ..models import EntityManager
from ..models.di import set_global_entity_manager, has_global_entity_manager, get_global_entity_manager
from ..models.users import User
from ..models.users.dtos import SSHAuthorizedKey
from ..sftp_server import SSHServer, SFTPServer
from ..sftp_server.access import Accessor


# import logging
# logging.basicConfig(level=logging.DEBUG)

class SftpServerTestCase(IsolatedAsyncioTestCase):
    async def asyncSetUp(self) -> None:
        _root = Path(__file__).parent.parent.parent.parent / 'test'
        if has_global_entity_manager():
            self._em = get_global_entity_manager()
            assert self._em.data_root_directory == _root
        else:
            self._em = EntityManager(
                meteoio_timeseries_executable=_root / 'bin' / 'meteoio_timeseries',
                data_root_directory=_root,
                external_base_url='',
            )
            self._em.migrate()
            set_global_entity_manager(self._em)

        self._server_host_keys = asyncssh.generate_private_key('ssh-ed25519')
        self._known_hosts = None  # asyncssh.import_known_hosts(self._server_host_keys.export_public_key().decode('ascii'))
        self._server_socket = socket.create_server(('localhost', 0))
        self._acceptor = await asyncssh.listen(
            sock=self._server_socket,
            server_host_keys=[self._server_host_keys],
            server_factory=SSHServer,
            sftp_factory=SFTPServer,
        )
        self.assertTrue(self._acceptor._server.is_serving())
        self._client_key = asyncssh.generate_private_key('ssh-ed25519')
        with self._em.users.transaction() as tx:
            user = User()
            user.can_create_datasets = True
            user.ssh_authorized_keys = [SSHAuthorizedKey(
                public_key=self._client_key.export_public_key().decode('ascii'),
                fingerprint=self._client_key.get_fingerprint(),
            )]
            tx.add(user)
            self._user_id = user.id
        with self._em.users.transaction() as tx:
            self._user = tx.get(User, self._user_id)
            tx.expunge(self._user)
        assert self._user.id
        self._dataset = self._em.datasets.make(user_id=self._user_id, create=True)
        self._username = Accessor(user=self._user, dataset=self._dataset).get_username()

    def _get_sock(self):
        return socket.create_connection(self._server_socket.getsockname())

    async def asyncTearDown(self) -> None:
        self._acceptor.close()
        await self._acceptor.wait_closed()

    async def test_failure_with_invalid_username(self):
        try:
            async with asyncssh.connect(sock=self._get_sock(), username='invalid_user', known_hosts=self._known_hosts):
                self.fail('Should have raised an exception.')
        except asyncssh.PermissionDenied:
            pass

    async def test_failure_without_key(self):
        try:
            async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts):
                self.fail('Should have raised an exception.')
        except asyncssh.PermissionDenied:
            pass

    async def test_failure_with_unknown_key(self):
        unknown_key = asyncssh.generate_private_key('ssh-ed25519')
        try:
            async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                        client_keys=unknown_key):
                self.fail('Should have raised an exception.')
        except asyncssh.PermissionDenied:
            pass

    async def test_connect_with_key(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                    client_keys=self._client_key):
            pass

    async def test_shell_fails(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                    client_keys=self._client_key) as conn:
            try:
                await conn.create_session(asyncssh.SSHClientProcess)
            except asyncssh.ChannelOpenError:
                pass
            else:
                self.fail('Should have raised an exception.')

    async def test_shell_fails_2(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username,
                                    known_hosts=self._known_hosts,
                                    client_keys=self._client_key) as conn:
            try:
                await conn.run('ls', check=True)
            except asyncssh.ChannelOpenError:
                pass
            else:
                self.fail('Should have raised an exception.')

    async def test_sftp_client_connects(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                    client_keys=self._client_key) as conn:
            async with conn.start_sftp_client():
                pass

    async def test_sftp_client_can_list(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                    client_keys=self._client_key) as conn:
            async with conn.start_sftp_client() as sftp:
                _ls = await sftp.listdir('/')
                self.assertGreaterEqual(len(_ls), 2)  # there are always at least 2 files: . and ..

    async def test_sftp_client_lists_nothing_outside_root(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                    client_keys=self._client_key) as conn:
            async with conn.start_sftp_client() as sftp:
                self.assertListEqual(await sftp.listdir('/../'), ['.', '..'])

    async def test_sftp_client_fails_list_dir_outside_root(self):
        async with asyncssh.connect(sock=self._get_sock(), username=self._username, known_hosts=self._known_hosts,
                                    client_keys=self._client_key) as conn:
            async with conn.start_sftp_client() as sftp:
                try:
                    await sftp.listdir('/../user')
                except asyncssh.SFTPNoSuchFile:
                    pass
                else:
                    self.fail('Should have raised an exception.')
