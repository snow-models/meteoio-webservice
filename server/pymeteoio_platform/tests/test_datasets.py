# SPDX-License-Identifier: AGPL-3.0-or-later
import cProfile as profile
import time
from pathlib import Path

from ..models.datasets import DatasetsManager, GuestDirectoriesRoot, ConfigDirectory
from ..models.datasets.cron import CronJob
from ..models.timeseries import TimeSeriesJobParams, Range

# In outer section of code
pr = profile.Profile()
pr.enable()

_root = Path(__file__).parent.parent.parent.parent / 'test'
gdr = GuestDirectoriesRoot(_root / 'datasets')
dm = DatasetsManager(dir_ctrl=gdr, meta_dir=_root / 'datasets_meta')
ds = dm.make(create=True)

with ds.config.transaction() as path:
    ConfigDirectory(path=path).ini('test.ini').create('test 0')
    ConfigDirectory(path=path).ini('other.ini').create('ok')


class Success(BaseException):
    pass


try:
    with ds.config.transaction() as path:
        ConfigDirectory(path=path).ini('test_uncommitting.ini').create('uncommitting')
        ConfigDirectory(path=path).ini('other.ini').update('KOKOKOKOKO should not be committed')
        raise Success
except Success:
    pass

print(f'Disk usage: {ds.dir.get_total_bytes():.0f}B')

for _ in range(10):
    with ds.config.transaction() as path:
        config = ConfigDirectory(path=path)
        for _ in range(10):
            job_id = CronJob.make_id()
            job = CronJob(
                cron="* * * * *",
                type='meteoio_timeseries',
                params=TimeSeriesJobParams(
                    ini='test.ini',
                    range=Range(
                        end="NOW",
                        duration_days=30,
                    ),
                ),
            )
            config.cron(job_id).create(job)

print(f'Disk usage: {ds.dir.get_total_bytes() / 1000000:.0f}MB')

__start = time.perf_counter()
__n = 100
for _ in range(__n):
    with ds.config.transaction() as path:
        pass
print(f"Elapsed time for 100 empty transaction after big write: "
      f"{(time.perf_counter() - __start) * 1000:.0f}ms "
      f"{(time.perf_counter() - __start) / __n * 1000:.0f}ms/tx")

# with ds.config.transaction() as path:
#     vcs = VersionController(path)
#     vcs.init()
#     conf = ConfigDirectory(path=path)
#
#     conf.ini('test.ini').update('test 1')
#     h = vcs.commit_all(CommitMsgDto(message='test 0 to 1', author_name='tester', author_email='tester@example.com'))
#
#     conf.ini('test.ini').update('test 2')
#     vcs.commit_all(CommitMsgDto(message='test 1 to 2', author_name='tester', author_email='tester@example.com'))
#
#     vcs.reset(h)
#     # print('Reflog:')
#     # for x in vcs.get_reflog():
#     #     print(x)
#     # print('Commits:')
#     # for x in vcs.get_commits():
#     #     print(x)

print(ds)

input('Press enter to clean up the test dataset.')

ds.dir.remove(confirm=ds.dir.id, recursive=True)

pr.dump_stats(f'cProfile.pstat')
