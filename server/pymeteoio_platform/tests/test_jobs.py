# SPDX-License-Identifier: AGPL-3.0-or-later
import sys
from pathlib import Path

from ..models.jobs import JobCommand, JobsQuotaPolicy
from ..models.jobs_manager import GuestDirectoriesRoot, JobsManager

gdr = GuestDirectoriesRoot(Path(__file__).parent.parent.parent.parent / 'test' / 'guests')
jm = JobsManager(gdr, concurrency=1, index_url='sqlite://', quota_policy=JobsQuotaPolicy())

jj = []
for i in range(100):
    jj.append(jm.make_job(create=True))

ex = Path(sys.executable)
echo = str(Path(__file__).parent / 'test_echo.py')

for i, job in enumerate(jj):
    job.set_command(JobCommand(ex, [echo, f'ciao {i}']))
    jm.start(job)
print(f'Started {len(jj)} jobs.')

for i, job in enumerate(jj):
    s = job.get_status()
    assert s.is_queued
print(f'Got status of {len(jj)} jobs.')

for i, job in enumerate(jj[:10]):
    job.wait(10)
    s = job.get_status()
    assert s.is_queued
    assert s.is_started
    assert not s.is_running
    assert not s.is_broken
    assert s.is_finished
print(f'Waited {len(jj[:10])} jobs.')

for i, job in enumerate(jj):
    job.get_status()
print(f'Got status of {len(jj)} jobs.')

print(f'Found {len(list(jm.iter_jobs()))} jobs by listing')

for i, job in enumerate(jj):
    job.wait(10)
print(f'Waited {len(jj)} jobs.')

for i, job in enumerate(jj):
    job.dir.remove(recursive=True, confirm=job.id)
print(f'Removed {len(jj)} jobs.')
