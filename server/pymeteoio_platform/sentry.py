# SPDX-License-Identifier: AGPL-3.0-or-later
import logging

from pydantic import Field
from pydantic_settings import BaseSettings


class _SentryEnvConfig(BaseSettings):
    sentry_dsn: str = Field(default='')


def maybe_init_sentry():
    dsn = _SentryEnvConfig().sentry_dsn
    if not dsn:
        logging.debug('No Sentry DSN configuration found.')
        return

    import sentry_sdk

    sentry_sdk.init(
        dsn=dsn,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=1.0,
    )
    logging.info('Sentry has been initialized.')

    # TODO?: in case of perf. issues: https://docs.sentry.io/platforms/python/performance/instrumentation/opentelemetry/
