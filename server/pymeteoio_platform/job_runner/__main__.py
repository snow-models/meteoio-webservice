# SPDX-License-Identifier: AGPL-3.0-or-later
import logging

from . import main, JobRunnerEnvSettings
from ..sentry import maybe_init_sentry

if __name__ == '__main__':
    print('This is the job runner.')
    logging.basicConfig(level=logging.INFO)

    maybe_init_sentry()

    main(JobRunnerEnvSettings())