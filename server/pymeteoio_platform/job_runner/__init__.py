# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import socket
import time
from pathlib import Path
from typing import Iterable, Optional

from pydantic import DirectoryPath, FilePath, Field
from pydantic_settings import BaseSettings, SettingsConfigDict

from ..models.fuse.configs import FuseMountListConfig
from ..models.guest_directories import GuestDirectoriesRoot
from ..models.jobs import JobsQuotaPolicy
from ..models.jobs_manager import JobsManager
from ..models.zip import import_zip_stream, export_zip_stream

_LOGGER = logging.getLogger(__name__)


class JobRunnerEnvSettings(BaseSettings):
    guest_jobs_root: DirectoryPath = Field(
        default_factory=lambda: Path(__file__).parent.parent.parent.parent / 'test' / 'guest_jobs')
    meteoio_timeseries_executable: FilePath = Field(
        default_factory=lambda: Path(__file__).parent.parent.parent.parent / 'test' / 'bin' / 'meteoio_timeseries')

    do_collect_job_strace: bool

    job_dispatcher_host: str
    job_dispatcher_port: int

    fuse_mount_list_config: Optional[FuseMountListConfig] = Field(default=None)
    """@experimental"""

    model_config = SettingsConfigDict(
        extra="ignore",
        env_nested_delimiter='__',
    )


def main(_settings: JobRunnerEnvSettings):
    _addr = _settings.job_dispatcher_host, _settings.job_dispatcher_port
    _LOGGER.info('Connecting to job dispatcher...')

    _time_of_start = time.monotonic()

    # noinspection PyBroadException
    try:
        with socket.create_connection(_addr, timeout=30) as sock:
            sock.send(b'w')

            _LOGGER.info(f'Waiting for a job...')

            while True:
                _signal = sock.recv(1)
                if not _signal:
                    _LOGGER.info('Connection closed with no job. Terminating...')
                    exit(1)
                elif _signal == b'k':  # keep alive
                    continue
                elif _signal == b'j':  # got job
                    break
                else:
                    _LOGGER.info(f'Unexpected signal {repr(_signal)} before job. Terminating...')
                    exit(1)

            # TODO?: get rid of JobsManager, we can directly create the Job.

            jm = JobsManager(
                dir_ctrl=GuestDirectoriesRoot(root=_settings.guest_jobs_root),
                concurrency=1,
                index_url='sqlite://',
                quota_policy=JobsQuotaPolicy(),
                do_collect_job_strace=_settings.do_collect_job_strace
            )

            job = jm.make_job(create=True)
            if _settings.fuse_mount_list_config:
                object.__setattr__(job, 'fuse_mount_list_config', _settings.fuse_mount_list_config)

            _LOGGER.info(f'Receiving job {job.id}...')

            def _make_zipped_chunks() -> Iterable[bytes]:
                try:
                    while _chunk := sock.recv(4096):
                        yield _chunk
                except TimeoutError:
                    _LOGGER.info('Timed out. Terminating...')
                    exit(0)

            import_zip_stream(
                root=job.dir.root_path,
                zipped_chunks=_make_zipped_chunks())

            _LOGGER.info(f'Job {job.id} received. Starting processing...')
            sock.send(b's')

            job.start(block=True)

            _LOGGER.info(f'Job {job.id} processed. Sending result...')

            for chunk in export_zip_stream(root=job.dir.root_path, compression_level=2):
                sock.sendall(chunk)
            sock.shutdown(socket.SHUT_WR)

            _LOGGER.info(f'Job {job.id} finished. Terminating...')
    except:
        _LOGGER.exception(f'Unexpected error. Terminating...')
        exit(1)
    else:

        # https://docs.docker.com/config/containers/start-containers-automatically/#restart-policy-details
        # "A restart policy only takes effect after a container starts successfully.
        #  In this case, starting successfully means that the container is up for at least 10 seconds and Docker has
        #  started monitoring it. This prevents a container which doesn't start at all from going into a restart loop."
        _time_of_end = time.monotonic()
        _time_elapsed = _time_of_end - _time_of_start
        _time_elapsed_min = 11  # Not too much as this is going to impact the availability of runners.
        _LOGGER.info(f'Elapsed time: {_time_elapsed:.2f}s')
        if 0 < _time_elapsed < _time_elapsed_min:
            _LOGGER.info(f'Sleeping some time before exiting to avoid this being detected as a failure from docker...')
            time.sleep(_time_elapsed_min - _time_elapsed)

        exit(0)