# SPDX-License-Identifier: AGPL-3.0-or-later
"""
 ***********************************************************************************
 *  Copyright 2023 WSL Institute for Snow and Avalanche Research    SLF-DAVOS      *
 ***********************************************************************************
   This file is part of MeteoIO-WebService.
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.
"""
# Original version by Matteo Terruzzi - DkR S.r.l. for SLF-DAVOS

__author__ = "Original version by Matteo Terruzzi - DkR S.r.l. for SLF-DAVOS"
__copyright__ = "Copyright 2023 WSL Institute for Snow and Avalanche Research    SLF-DAVOS"
__license__ = "AGPL-3.0-or-later"
__version__ = "1.0"
