# SPDX-License-Identifier: AGPL-3.0-or-later
from ..app.entity_manager import get_entity_manager


def main():
    entity_manager = get_entity_manager(require_migrations=False)
    entity_manager.migrate()