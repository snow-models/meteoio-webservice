import logging
import re
import shutil
from time import time

from ...app.entity_manager import get_entity_manager

_LOGGER = logging.getLogger(__name__)

_COLLECTABLE_FILE_NAME_RE = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}.*')

def cleanup_old_files():

    em = get_entity_manager()

    _max_age_seconds = em.guest_jobs.quota_policy.max_age_seconds

    for _path in em.pywps_path.iterdir():
        if _COLLECTABLE_FILE_NAME_RE.match(_path.name):

            _age_seconds = time() - _path.stat().st_ctime
            if _age_seconds > _max_age_seconds:

                _path = _path.resolve()
                assert _path.is_relative_to(em.pywps_path), 'Must be under PyWPS files directory'

                if _path.is_file():
                    _LOGGER.info(f'Removing old PyWPS file: {str(_path)}')
                    _path.unlink()

                elif _path.is_dir():
                    _LOGGER.info(f'Removing old PyWPS directory: {str(_path)}')
                    shutil.rmtree(_path)

                else:
                    raise RuntimeError(f'Found strange path (not a file, not a dir) in PyWPS directory: {str(_path)}')
