# SPDX-License-Identifier: AGPL-3.0-or-later
from concurrent.futures import ThreadPoolExecutor

from pywps.processing import Processing

_THREAD_POOL = ThreadPoolExecutor()  # See max_workers default based on cpu count.


class ThreadProcessing(Processing):

    def start(self):
        _THREAD_POOL.submit(self.job.run)
        # NOTE: The above call enqueues the work item and returns fast, regardless of the size of the thread pool.
