# SPDX-License-Identifier: AGPL-3.0-or-later
from pathlib import Path

from pywps.app.Service import Service

from .processes.meteoio_timeseries import MeteoIOTimeseriesProcess

# pip install -e "git+https://github.com/geopython/pywps.git@4.6.0#egg=pywps"


processes = [
    MeteoIOTimeseriesProcess(),
]

app = Service(
    processes,
    [
        str(Path(__file__).parent / 'default.cfg')
    ]
)

# NOTE: this WSGI app is mounted inside the main ASGI app.
