# SPDX-License-Identifier: AGPL-3.0-or-later
import shutil

from pywps import Process, LiteralInput, ComplexOutput, ComplexInput, Format, FORMATS
from pywps.app import WPSRequest
from pywps.inout.outputs import MetaFile, MetaLink4
from pywps.response.execute import ExecuteResponse

from ..processing import ThreadProcessing
from ....app.entity_manager import get_entity_manager
from ....models.jobs import Job, JobCommand, JobProcessPriority
from ....models.timeseries import TimeSeriesJobParams, Range
from ....models.zip import import_zip_stream


def get_first_or_none_input_data(x):
    if x is not None and len(x) >= 1:
        return x[0].data
    return None


def get_num_input_data(x):
    if x is not None:
        return len(x)
    return 0


class MeteoIOTimeseriesProcess(Process):
    def __init__(self):
        inputs = [
            LiteralInput('begin', data_type='string', max_occurs=1, min_occurs=0),
            LiteralInput('end', data_type='string', max_occurs=1, min_occurs=0),
            LiteralInput('duration_days', data_type='integer', max_occurs=1, min_occurs=0),
            LiteralInput('resolution_minutes', data_type='integer', max_occurs=1, min_occurs=0),
            LiteralInput('ini_file_name', title='MeteoIO INI configuration file name', data_type='string', max_occurs=1,
                         min_occurs=0, default='io.ini'),
            ComplexInput('ini',
                         title='MeteoIO INI configuration file content (overrides the one from zipped working directory, if a file with the same name exists)',
                         min_occurs=0,
                         max_occurs=1,
                         supported_formats=[Format('text/plain', extension='ini')],
                         ),
            ComplexInput('data',
                         title='Zipped working directory',
                         abstract='Zipped directory of data and configuration files, where MeteoIO shall work on.',
                         min_occurs=0,
                         max_occurs=1,
                         supported_formats=[Format('application/zip', extension='.zip', encoding='base64')],
                         ),
        ]
        outputs = [
            ComplexOutput('result',
                          title='MetaLink4 result',
                          abstract='A metalink file storing URIs to multiple result files, does not exclude input files.',
                          as_reference=False,
                          supported_formats=[FORMATS.META4]),
        ]
        super().__init__(
            handler=MeteoIOTimeseriesProcess._handler,
            identifier="meteoio_timeseries",
            title="MeteoIO Timeseries",
            abstract="Process data and generate timeseries using MeteoIO",
            # More info here: https://code.wsl.ch/snow-models/meteoio/-/blob/master/applications/meteoio_timeseries.cc
            profile='',
            keywords=["meteoio", "timeseries"],
            inputs=inputs,
            outputs=outputs,
            store_supported=True,
            status_supported=True,
        )

    # This function may not raise exception and must return a valid wps_response
    # Failure must be reported as wps_response.status = WPS_STATUS.FAILED
    def _run_async(self, wps_request, wps_response):
        # Let's avoid issues with pickle -- we are not doing computation in this process/thread
        #   => no need for the default implementation that uses multiprocessing.
        process = ThreadProcessing(
            process=self,
            wps_request=wps_request,
            wps_response=wps_response)
        process.start()

    @staticmethod
    def _handler(request: WPSRequest, response: ExecuteResponse):
        # Check that at least one of ini and data are given:
        if (get_num_input_data(request.inputs.get('data')) <= 0 and
            get_num_input_data(request.inputs.get('ini')) <= 0):
            raise ValueError('At least one of ini and data must be given.')

        # Validate job params
        response.update_status('Reading the request...')
        ini_file_name: str = get_first_or_none_input_data(request.inputs.get('ini_file_name'))
        job_params: TimeSeriesJobParams = TimeSeriesJobParams(
            range=Range(
                begin=get_first_or_none_input_data(request.inputs.get('begin')),
                end=get_first_or_none_input_data(request.inputs.get('end')),
                duration_days=get_first_or_none_input_data(request.inputs.get('duration_days')),
            ),
            ini=ini_file_name,
            resolution_minutes=get_first_or_none_input_data(request.inputs.get('resolution_minutes')),
        )

        # Allocate Job
        response.update_status('Allocating the job workspace...')
        entity_manager = get_entity_manager()
        job: Job = entity_manager.guest_jobs.make_job(create=True)

        assert job.dir.get_full_user_path(ini_file_name)  # raises ValueError if ini_file_name tries to go outside root.

        # Unzip input data files, if present
        if get_num_input_data(request.inputs.get('data')) >= 1:
            def _fetch_chunks():
                _stream = request.inputs['data'][0].stream
                while chunk := _stream.read(1024):
                    yield chunk

            response.update_status('Unzipping job input data...')
            import_zip_stream(
                root=job.dir.user_path,
                zipped_chunks=_fetch_chunks(),
            )

        response.update_status('Preparing the job workspace...')

        # Copy (override) ini file, if present
        if get_num_input_data(request.inputs.get('ini')) >= 1:
            ini_file_path = job.dir.get_full_user_path(ini_file_name)
            shutil.copy(request.inputs['ini'][0].file, str(ini_file_path))

        # Write job command
        job.set_command(JobCommand(
            executable=entity_manager.meteoio_timeseries_executable,
            priority=JobProcessPriority.idle,
            args=job_params.get_command_args(inis_dir=job.dir.get_full_user_path(''))
        ))

        # Run
        # response.update_status('Processing...')
        response.update_status('Processing...')
        entity_manager.guest_jobs.start(job, blocking=True)
        # NOTE: if using local job_dispatch_type=local then the above may rise exceptions

        # Prepare result output
        response.update_status('Preparing result...')

        workdir = str(job.dir.root_path / 'wps_workdir')
        response.outputs['result'].workdir = workdir

        out_file_url_prefix = f'{entity_manager.external_base_url}/public/job_submission/{job.id}/files?path='

        ml = MetaLink4('result', 'MetaLink with links to job result files', workdir=workdir)
        for path in job.dir.iter_recursive_user_paths():
            _sp = str(path.relative_to(job.dir.user_path))
            # Create a MetaFile instance, which instantiates a ComplexOutput object.
            mf = MetaFile(_sp, _sp, fmt=Format('application/octet-stream', validate=None))
            mf.url = out_file_url_prefix + _sp
            mf.size = path.stat().st_size  # WARNING: Set it, otherwise PyWPS will try to fetch the URL to get the size.
            ml.append(mf)
        response.outputs['result'].data = ml.xml

        response.update_status('Finishing...')
        return response
