# SPDX-License-Identifier: AGPL-3.0-or-later
import asyncio
import sys
import uuid
from asyncio import sleep
from dataclasses import dataclass
from traceback import print_exc

import uvloop

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

JOBS_QUEUE = asyncio.queues.Queue()

FAILURES_COUNT: int = 0
FAILURES_LIMIT: int = 3


@dataclass
class JobTransmission:
    id: str
    data_reader: asyncio.StreamReader
    result_writer: asyncio.StreamWriter
    finish: asyncio.Event
    err: asyncio.Event


async def handle_connection(reader: asyncio.StreamReader, writer: asyncio.StreamWriter):
    global FAILURES_COUNT
    print(f'Got connection')
    header = await reader.readexactly(1)
    print(f'Got header: {header}')

    if header == b'w':
        w_id = str(uuid.uuid4())
        print(f'Worker {w_id} connected.')

        while True:
            try:
                job: JobTransmission = await asyncio.wait_for(JOBS_QUEUE.get(), 5)
                break
            except TimeoutError:
                writer.write(b'k')  # keep alive

        print(f'Worker {w_id} transferring data for job {job.id}...')
        # noinspection PyBroadException
        try:
            writer.write(b'j')  # got job

            while _chunk := await job.data_reader.read(4096):
                writer.write(_chunk)
            # Have to signal the end of the file
            writer.write_eof()
            await writer.drain()

            __started_signal = await reader.readexactly(1)
            if not __started_signal:
                FAILURES_COUNT += 1
                raise RuntimeError('Connection closed without starting.')
            assert __started_signal == b's', f'Expecting start signal, got {repr(__started_signal)}'
            job.result_writer.write(__started_signal)

            print(f'Worker {w_id} transferred data for job {job.id}. Waiting result...')

            __first = True
            while _chunk := await reader.read(4096):
                job.result_writer.write(_chunk)
                if __first:
                    __first = False
                    print(f'Worker {w_id} transferring result of job {job.id}...')

            job.result_writer.write_eof()
            await job.result_writer.drain()
            # job.result_writer.write_eof()
            job.result_writer.close()
            await job.result_writer.wait_closed()

            print(f'Worker {w_id} finished job {job.id}.')
        except:
            print_exc(file=sys.stdout)
            job.err.set()
            if FAILURES_COUNT >= FAILURES_LIMIT:
                print(f'Too many failures ({FAILURES_COUNT}): terminating in 1 sec...')
                await sleep(1)
                exit(1)
        finally:
            job.finish.set()

    elif header == b'j':
        j_id = str(uuid.uuid4())
        print(f'Job {j_id} arrived.')
        job = JobTransmission(
            id=j_id,
            data_reader=reader,
            result_writer=writer,
            finish=asyncio.Event(),
            err=asyncio.Event(),
        )
        await JOBS_QUEUE.put(job)
        await job.finish.wait()
        # noinspection PyBroadException
        try:
            job.result_writer.write_eof()
            job.result_writer.close()
        except:
            pass
        print(f'Job {j_id} finished (err: {bool(job.err.is_set())}).')


async def main():
    print('Starting server...')
    server = await asyncio.start_server(
        handle_connection, '0.0.0.0', 4222)

    addrs = ', '.join(str(sock.getsockname()) for sock in server.sockets)
    print(f'Serving on {addrs}')

    async with server:
        await server.serve_forever()