# SPDX-License-Identifier: AGPL-3.0-or-later
from pathlib import Path
from typing import Optional

from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class SftpServerEnvSettings(BaseSettings):
    # server_host_key: FilePath
    sftp_host: str = Field(default='')
    sftp_port: int = Field(default=22)
    allow_scp: bool = Field(default=True)

    ssh_host_key_path: Path = Field(default='/data/ssh_host_key')
    ssh_host_key_passphrase: Optional[str] = Field(default=None)

    model_config = SettingsConfigDict(extra="ignore")
