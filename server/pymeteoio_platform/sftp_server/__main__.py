# SPDX-License-Identifier: AGPL-3.0-or-later
import asyncio
import logging

import uvloop

from . import main
from .settings import SftpServerEnvSettings
from ..app.entity_manager import get_entity_manager

asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())

from ..sentry import maybe_init_sentry

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    maybe_init_sentry()

    get_entity_manager()  # let it set the global entity manager
    asyncio.run(main(SftpServerEnvSettings()))
