# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
from typing import AsyncIterator, Optional, cast

import asyncssh
from asyncssh import SFTPName

from .access import Accessor
from .ssh import SSHServer

LOGGER = logging.getLogger(__name__)


class SFTPServer(asyncssh.SFTPServer):
    __accessor: Optional[Accessor] = None

    def __init__(self, chan: asyncssh.SSHServerChannel):
        _owner = cast(SSHServer, chan.get_connection().get_owner())
        self.__accessor = _owner.get_authorized_accessor_3829a2ebba0a46b3a013bb933c414cbc()

        super().__init__(chan, chroot=str(self.__accessor.dataset.dir.user_path).encode('utf-8'))

    async def scandir(self, path: bytes) -> AsyncIterator[SFTPName]:
        #  LOGGER.info(f'Scanning path {path}')
        async for name in super().scandir(path):
            name.attrs.gid = 0
            name.attrs.uid = 0
            name.attrs.acl = b''
            name.attrs.owner = 0
            name.attrs.group = 0
            yield name
