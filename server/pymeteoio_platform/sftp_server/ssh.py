# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
from typing import Optional

import asyncssh
from asyncssh import SSHKey, SSHServerConnection

from .access import get_accessors, Accessor, InvalidUsernameFormat
from ..models.errors import NotFound, Forbidden

LOGGER = logging.getLogger(__name__)


class SSHServer(asyncssh.SSHServer):
    """ SSH server protocol handler, instantiated for each connection. """

    __conn: Optional[SSHServerConnection] = None
    __accessor: Optional[Accessor] = None  # NOTE: possession of the accessor does not imply authorization.
    __auth_completed: bool = False

    def connection_made(self, conn: SSHServerConnection) -> None:
        assert self.__conn is None
        self.__conn = conn

    async def begin_auth(self, username: str) -> bool:
        assert self.__conn is not None

        # NOTE: return True means that authentication is REQUIRED (False would mean unrestricted access).

        # NOTE: We are returning True in all cases: all caught exceptions and all clean paths.
        #       In case of uncaught exceptions, then the connection is closed.
        #       Also note: self.__accessor is only assigned with value in one step here.
        #       More important note: public key verification is performed by the asyncssh library.
        #           (See asyncssh.auth._ServerPublicKeyAuth._start)
        #       Later assignments of self.__accessor to None are redundant.

        try:
            self.__accessor = get_accessors(username)  # Required to complete authorization

        except InvalidUsernameFormat:
            LOGGER.info('InvalidUsernameFormat')
            self.__conn.send_auth_banner('It looks like you are not using the correct username (invalid format).')
            self.__accessor = None

        except NotFound as ex:
            LOGGER.info(f'NotFound: {ex}')
            self.__conn.send_auth_banner(f'It looks like you are not using the correct username: {str(ex)}')
            self.__accessor = None

        except Forbidden as ex:
            LOGGER.info(f'Forbidden: {ex}')
            self.__conn.send_auth_banner(f'It looks like you are not allowed to access the resource.')
            self.__accessor = None

        else:
            _user_keys = self.__accessor.user.ssh_authorized_keys
            LOGGER.info(f'User has {len(_user_keys)} ssh authorized keys')

            if len(_user_keys) <= 0:
                self.__conn.send_auth_banner('There are no SSH keys configured in your account.')
                self.__accessor = None

            # _authorized_keys = SSHAuthorizedKeys()
            # for _entry in _user_keys:
            #     _authorized_keys.load(_entry.public_key)
            # self.__conn.set_authorized_keys(_authorized_keys)
            # NOTE: we prefer to have our check in validate_public_key

        return True

    def public_key_auth_supported(self) -> bool:
        return True

    def password_auth_supported(self) -> bool:
        return False

    def validate_password(self, username: str, password: str) -> bool:
        return False
        # LOGGER.info(f'User {self.__accessor.user.id} is trying password verification.')
        # self.__accessor.user.verify_password(password)
        # NOTE: the above throws in case of wrong password

    def validate_public_key(self, username: str, key: SSHKey):
        if self.__accessor is None:
            return False

        _fingerprint = key.get_fingerprint()
        assert _fingerprint.startswith('SHA256:')
        assert len(_fingerprint) == 50

        for _entry in self.__accessor.user.ssh_authorized_keys:
            if _entry.fingerprint == _fingerprint:
                LOGGER.info(f'Accepting public key with authorized fingerprint: {_fingerprint}.')
                return True
                # NOTE: we have verified the public key is ok; asyncssh verifies that the client knows the private key.

        LOGGER.info(_msg := f'Denying access with unknown public key (fingerprint: {_fingerprint}).')
        self.__conn.send_auth_banner(f'Please check your SSH keys setup: {_msg}')
        self.__accessor = None
        return False

    def auth_completed(self):
        assert self.__accessor is not None
        assert self.__conn is not None
        self.__auth_completed = True
        LOGGER.info(f'Authentication was completed successfully for user {self.__accessor.user.id}.')

    def get_authorized_accessor_3829a2ebba0a46b3a013bb933c414cbc(self) -> Accessor:
        assert self.__accessor
        assert self.__auth_completed
        return self.__accessor
