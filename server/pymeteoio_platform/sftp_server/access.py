# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
import re
from dataclasses import dataclass

from ..models.datasets import Dataset
from ..models.di import get_global_entity_manager
from ..models.errors import NotFound
from ..models.users import User

LOGGER = logging.getLogger(__name__)

USERNAME_RE = re.compile(r'^(?P<user_id>[0-9a-fA-F-]+)-d-(?P<dataset_id>[0-9a-fA-F-]+)$')
"""disseminated c2794b16-b049-426c-99c2-f0012c0533a8"""

class InvalidUsernameFormat(Exception):
    pass

@dataclass(frozen=True)
class Accessor:
    user: User
    dataset: Dataset

    def get_username(self) -> str:
        # disseminated c2794b16-b049-426c-99c2-f0012c0533a8
        return f'{self.user.id}-d-{self.dataset.dir.id}'

def get_accessors(username: str) -> Accessor:
    em = get_global_entity_manager()

    _match = USERNAME_RE.match(username)
    if not _match:
        raise InvalidUsernameFormat

    user_id = _match.group('user_id').lower()
    dataset_id = _match.group('dataset_id').lower()

    with em.users.transaction() as tx:
        user = tx.get(User, user_id)
        if user is None:
            # from sqlalchemy import select
            # for user, in tx.execute(select(User).filter(User.attributes.contains('is_admin'))).unique():
            #     LOGGER.debug(f'Found user {user.id}')

            raise NotFound(f'User {user_id} not found.')

        tx.expunge(user)

    try:
        dataset = em.datasets.get(dataset_id, user_id=user_id)
    except NotFound as ex:
        raise NotFound(f'Dataset {dataset_id} not found.') from ex

    LOGGER.info(f'Will try access for user {user_id} to dataset {dataset_id}.')

    return Accessor(
        user=user,
        dataset=dataset,
    )
