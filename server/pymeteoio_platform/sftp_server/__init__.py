# SPDX-License-Identifier: AGPL-3.0-or-later
import logging

import asyncssh

from .settings import SftpServerEnvSettings
from .sftp import SFTPServer
from .ssh import SSHServer

LOGGER = logging.getLogger(__name__)


async def main(_settings: SftpServerEnvSettings):
    if _settings.ssh_host_key_path.exists():
        LOGGER.info('Reading existing ssh host private key...')
        _host_key = asyncssh.read_private_key(_settings.ssh_host_key_path, passphrase=_settings.ssh_host_key_passphrase)
    else:
        _settings.ssh_host_key_path.touch(exist_ok=False)
        LOGGER.info('Generating new ssh host private key...')
        _host_key = asyncssh.generate_private_key('ssh-rsa')
        _host_key.write_private_key(_settings.ssh_host_key_path, passphrase=_settings.ssh_host_key_passphrase)

    acceptor = await asyncssh.listen(
        host=_settings.sftp_host,
        port=_settings.sftp_port,
        server_host_keys=[_host_key],
        server_factory=SSHServer,
        sftp_factory=SFTPServer,
        allow_scp=_settings.allow_scp,
    )
    LOGGER.info('Listening for connections...')
    await acceptor.wait_closed()
