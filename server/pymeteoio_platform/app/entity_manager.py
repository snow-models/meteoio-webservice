# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
from functools import cache
from time import sleep

from .settings import AppEnvSettings
from ..models import EntityManager
from ..models.di import set_global_entity_manager, has_global_entity_manager, get_global_entity_manager, \
    get_global_entity_manager_lock

_LOGGER = logging.getLogger(__name__)

@cache
def get_entity_manager(
        *,
        require_migrations: bool = True,
) -> EntityManager:
    # NOTE: We have to do two things:
    # - read the AppEnvSettings
    # - set_global_entity_manager for use by some models that require global dependency injection for simplicity.

    with get_global_entity_manager_lock():
        if has_global_entity_manager():
            # NOTE: This implementation could rely on @cache to ensure this function is never called twice.
            #       Instead, we check for has_global_entity_manager so we don't inject two times.
            return get_global_entity_manager()
        else:

            app_settings = AppEnvSettings()

            _LOGGER.info(f'Using data files root: {app_settings.meteoio_platform.data_root_directory.resolve()}')

            if require_migrations and app_settings.meteoio_platform.have_to_migrate():
                _LOGGER.info('Have to migrate, but not from here...')
                sleep(5)
                if app_settings.meteoio_platform.have_to_migrate():
                    _LOGGER.warning('Please, run migrations.')
                    raise RuntimeError('Still have to migrate after timeout.')

            set_global_entity_manager(app_settings.meteoio_platform)

            return app_settings.meteoio_platform