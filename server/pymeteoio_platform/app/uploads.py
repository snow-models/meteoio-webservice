# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import Annotated

from litestar.datastructures import UploadFile
from litestar.enums import RequestEncodingType
from litestar.params import Body
from pydantic import BaseModel, BaseConfig


class FileUploadFormData(BaseModel):
    file: UploadFile

    class Config(BaseConfig):
        arbitrary_types_allowed = True


AnnotatedFileUploadFormData = Annotated[FileUploadFormData, Body(media_type=RequestEncodingType.MULTI_PART)]

# NOTE: this is far from perfect.