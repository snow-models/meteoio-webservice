# SPDX-License-Identifier: AGPL-3.0-or-later
if __name__ == '__main__':
    from pathlib import Path
    import json

    from litestar.serialization import get_serializer, encode_json

    from .server import app

    schema_out_path = Path(__file__).parent / 'openapi_schema.json'

    serializer = get_serializer(app.type_encoders)
    schema = encode_json(app.openapi_schema.to_schema(), serializer=serializer)
    schema = json.loads(schema)

    with open(schema_out_path, 'w') as fw:
        json.dump(schema, fw, indent=2, sort_keys=True)
