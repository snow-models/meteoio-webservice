# SPDX-License-Identifier: AGPL-3.0-or-later
# noinspection PyPackageRequirements
from IPython import embed

from .entity_manager import get_entity_manager

if __name__ == '__main__':
    em = get_entity_manager()

    embed(
        header="Use the variable `em` to access the application entities. Be cautious.",
        colors="neutral",
    )