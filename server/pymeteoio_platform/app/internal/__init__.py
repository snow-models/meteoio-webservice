# SPDX-License-Identifier: AGPL-3.0-or-later
from litestar import Router, Request
from litestar.exceptions import NotAuthorizedException

from .datasets import Datasets


def registered_user_guard(request: Request, _) -> None:
    if request.user is None or not request.user.id:
        raise NotAuthorizedException()


internal_router = Router(path="/internal", route_handlers=[
    Datasets,
], guards=[
    registered_user_guard,
])