# SPDX-License-Identifier: AGPL-3.0-or-later
import logging
from datetime import timezone, datetime
from shutil import copytree
from typing import List, Optional, cast

import litestar
from litestar import MediaType, Request
from litestar.di import Provide
from litestar.exceptions import NotAuthorizedException, NotFoundException
from litestar.response import File
from pydantic.dataclasses import dataclass
from sqlalchemy import select, func, update

from ..uploads import AnnotatedFileUploadFormData
from ...models import EntityManager
from ...models.datasets import ConfigDirectory, Dataset, DatasetsManager
from ...models.datasets.cron import CronJob
from ...models.datasets.heading_dtos import DatasetHeading, DatasetListEntry, FsEntry, Access, \
    DatasetRevisionsListEntry, DatasetRevisionMsg
from ...models.datasets.index import DatasetRevision, DatasetRelation
from ...models.datasets.meta import MetaConfig
from ...models.datasets.runs_dtos import DsRunDto
from ...models.datasets.runs_index import RunRelation
from ...models.errors import Forbidden, UserReportableException, NotFound

logger = logging.getLogger(__name__)


@dataclass
class GenericIdDto:
    """Our web framework does not like a bare string as a response schema."""
    id: str


def datasets_guard(request: Request, _) -> None:
    if not request.user.can_create_datasets:
        raise NotAuthorizedException()


# TODO: find a more uniform way to mediate access to a dataset.

def get_dataset(entity_manager: EntityManager, dataset_id: str, request: Request) -> Dataset:
    user_id = request.user.id
    dsm: DatasetsManager = entity_manager.datasets
    return dsm.get(dataset_id, user_id=user_id)


def get_ds_conf_writing(tx_id: str, dataset: Dataset) -> ConfigDirectory:
    return ConfigDirectory(path=dataset.config.continuation(from_id=tx_id))


def get_ds_conf_reading(tx_id: str, dataset: Dataset) -> ConfigDirectory:
    return ConfigDirectory(path=dataset.config.continuation(from_id=tx_id, read_only=True))


class Datasets(litestar.Controller):
    path = '/datasets'
    dependencies = {
        "dataset": Provide(get_dataset, sync_to_thread=True),
        "ds_conf_writing": Provide(get_ds_conf_writing, sync_to_thread=True),
        "ds_conf_reading": Provide(get_ds_conf_reading, sync_to_thread=True),
    }
    tags = ["datasets"]
    guards = [datasets_guard]

    @litestar.get(sync_to_thread=True, cache=2)
    def list(self, entity_manager: EntityManager, request: Request) -> List[DatasetListEntry]:
        return list(entity_manager.datasets.iter_headings_for_user(user_id=request.user.id))

    @litestar.post(sync_to_thread=True)
    def create(self, entity_manager: EntityManager, request: Request) -> GenericIdDto:
        ds = entity_manager.datasets.make(create=True, user_id=request.user.id)
        return GenericIdDto(ds.dir.id)

    @litestar.get(path='/{dataset_id:str}', cache=3,
                  sync_to_thread=True)
    def read(self, dataset: Dataset) -> Optional[DatasetHeading]:
        # TODO: Return access level from DB maybe using DatasetListEntry
        return dataset.get_heading()

    @litestar.delete(path='/{dataset_id:str}', sync_to_thread=True)
    def delete(self, dataset: Dataset, entity_manager: EntityManager, owner_user_id_for_check: str,
               dataset_id_for_check: str) -> None:
        entity_manager.datasets.delete_dataset(
            dataset=dataset, owner_user_id_for_check=owner_user_id_for_check, dataset_id_for_check=dataset_id_for_check)

    ######################

    @litestar.get(path='/{dataset_id:str}/access', sync_to_thread=True)
    def get_access(self, entity_manager: EntityManager, dataset: Dataset) -> GenericIdDto:
        return GenericIdDto(id=entity_manager.datasets.get_access(dataset=dataset))

    @litestar.put(path='/{dataset_id:str}/access', sync_to_thread=True)
    def set_access(self, entity_manager: EntityManager, dataset: Dataset, access: Access) -> None:
        entity_manager.datasets.set_access(dataset=dataset, access=access)

    ######################

    @litestar.get(path='/{dataset_id:str}/revisions', sync_to_thread=True)
    def get_revisions(self, entity_manager: EntityManager, dataset: Dataset) -> List[DatasetRevisionsListEntry]:
        _query = (
            select(DatasetRevision)
            .where(DatasetRevision.dataset_id == dataset.dir.id)
            .where(DatasetRevision.confirmed_at.isnot(None))
            .order_by(DatasetRevision.number.asc())
        )
        with entity_manager.datasets.index.transaction() as tx:
            return [cast(DatasetRevision, r).get_dto() for r, in tx.execute(_query)]

    @litestar.get(path='/{dataset_id:str}/revisions/{tx_id:str}/message', sync_to_thread=True)
    def get_revision(self, entity_manager: EntityManager, dataset: Dataset, tx_id: str) -> DatasetRevisionsListEntry:
        # TODO: Add query param to go back in the version number
        with entity_manager.datasets.index.transaction() as tx:
            _ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            if _ds_rel is None:
                raise UserReportableException('Dataset record not found in index')
            _rev = tx.get(DatasetRevision, tx_id)
            if _rev is None:
                raise NotFound()
            if _rev.dataset_id != dataset.dir.id:
                raise NotFound()
            return _rev.get_dto()

    @litestar.put(path='/{dataset_id:str}/revisions/{tx_id:str}/message', sync_to_thread=True)
    def put_revision_message(self, entity_manager: EntityManager, dataset: Dataset, tx_id: str,
                             data: DatasetRevisionMsg) -> None:
        with entity_manager.datasets.index.transaction() as tx:
            _ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            if _ds_rel is None:
                raise UserReportableException('Dataset record not found in index')
            _rev = tx.get(DatasetRevision, tx_id)
            if _rev is not None:
                if _rev.dataset_id != dataset.dir.id:
                    raise NotFound()
                _rev: DatasetRevision
                _rev.title = data.title
                _rev.message = data.message
            else:
                logger.warning(
                    f'Failed to update DatasetRevision(id={tx_id}, dataset_id={_ds_rel.dataset_id}), '
                    f'now going insert new record.')
                _now = datetime.utcnow().replace(tzinfo=timezone.utc, microsecond=0)
                _rev = DatasetRevision(
                    id=tx_id,
                    dataset_id=_ds_rel.dataset_id,  # Required...
                    dataset=_ds_rel,  # ...Even if relationship object has precedence over foreign key
                    created_at=_now,
                    updated_at=_now,
                    title=data.title,
                    message=data.message,
                )
                tx.add(_rev)

    # TODO: export zip of revision

    ######################

    @litestar.get(path='/{dataset_id:str}/config_txs',
                  sync_to_thread=True)
    def get_config_pending_txs(self, dataset: Dataset) -> List[str]:
        return list(dataset.config.get_pending_transaction_ids())

    @litestar.post(path='/{dataset_id:str}/config_txs',
                   sync_to_thread=True)
    def begin_config_write_tx(self, entity_manager: EntityManager, dataset: Dataset) -> GenericIdDto:
        from_tx_id = dataset.config.head_tx
        tx_id = dataset.config.begin_write()
        with entity_manager.datasets.index.transaction() as tx:
            _ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            if _ds_rel is None:
                raise UserReportableException('Dataset record not found in index')
            _now = datetime.utcnow().replace(tzinfo=timezone.utc, microsecond=0)
            _rev = DatasetRevision(
                id=tx_id,
                dataset_id=_ds_rel.dataset_id,  # Required...
                dataset=_ds_rel,  # ...Even if relationship object has precedence over foreign key
                created_at=_now,
                updated_at=_now,
                created_from_id=from_tx_id,
            )
            tx.add(_rev)
        return GenericIdDto(tx_id)

    @litestar.delete(path='/{dataset_id:str}/config_txs/{tx_id:str}',
                     sync_to_thread=True)
    def delete_config_write_tx(self, dataset: Dataset, tx_id: str) -> None:
        dataset.config.delete_pending(tx_id)

    @litestar.post(path='/{dataset_id:str}/config_txs/{tx_id:str}/apply',
                   sync_to_thread=True)
    def apply_config_write_tx(self, entity_manager: EntityManager, dataset: Dataset, tx_id: str) -> None:
        # NOTE: WARNING: Read this first!
        #                tx_id is the current ID of the dataset config transaction to be applied
        #                old_head_tx_id is the ID of the current dataset config head, that will be replaced
        #                new_head_tx_id will be the new ID of the same transaction after applying it
        #                tx will be the database session (transaction)

        dm = entity_manager.datasets
        dm.update_indexed_dataset_heading(dataset=dataset, tx_id=tx_id)
        old_head_tx_id = dataset.config.head_tx
        new_head_tx_id = dataset.config.apply(tx_id)
        with entity_manager.datasets.index.transaction() as tx:
            _ds_rel = tx.get(DatasetRelation, dataset.dir.id)
            if _ds_rel is None:
                raise UserReportableException('Dataset record not found in index')
            _number_query = (
                select(func.max(DatasetRevision.number))
                .select_from(DatasetRevision)
                .where(DatasetRevision.number.isnot(None))
                .where(DatasetRevision.dataset_id == _ds_rel.dataset_id)
            )
            # NOTE: we are only counting numbered revisions -- pending transactions are excluded.
            _number = tx.execute(_number_query).scalar()
            if _number is None:
                _number = 0
            else:
                _number = int(_number)

            _now = datetime.utcnow().replace(tzinfo=timezone.utc, microsecond=0)

            # Insert rev entry for old_head_tx_id, if not already present, but only if initial version
            _old_rev = tx.get(DatasetRevision, old_head_tx_id)
            if _old_rev is None:
                if _number > 0:
                    logger.warning(
                        f'Missing DatasetRevision(id={old_head_tx_id}, dataset_id={_ds_rel.dataset_id}), '
                        f'expected as parent of {new_head_tx_id}.')
                else:  # _number <= 0
                    # _number <= 0 can be interpreted as absence of numbered versions
                    # because all versions are created with positive number.

                    # This condition is possible due to no versioning feature at the time of dataset creation.
                    _number = 1
                    logger.info(
                        f'Missing DatasetRevision(id={old_head_tx_id}, dataset_id={_ds_rel.dataset_id}), '
                        f'now going to insert dummy initial version record as parent of {new_head_tx_id}.')
                    _old_rev = DatasetRevision(
                        id=old_head_tx_id,
                        dataset_id=dataset.dir.id,  # Required...
                        dataset=_ds_rel,  # ...Even if relationship object has precedence over foreign key
                        number=_number,
                        confirmed_at=_now,
                        # No other data? Well...
                    )
                    tx.add(_old_rev)
                    tx.flush()

            _new_number = _number + 1
            update_query = (
                update(DatasetRevision)
                .where(DatasetRevision.dataset_id == _ds_rel.dataset_id)
                .where(DatasetRevision.id == tx_id)
                .values(
                    id=new_head_tx_id,
                    updated_at=_now,
                    confirmed_at=_now,
                    number=_new_number,
                )
            )
            result = tx.execute(update_query)
            if result.rowcount <= 0:
                logger.warning(
                    f'Failed to update DatasetRevision(id={new_head_tx_id}, dataset_id={_ds_rel.dataset_id}), '
                    f'now going to insert new record.')
                _rev = DatasetRevision(
                    id=new_head_tx_id,
                    dataset_id=dataset.dir.id,  # Required...
                    dataset=_ds_rel,  # ...Even if relationship object has precedence over foreign key
                    created_at=_now,
                    updated_at=_now,
                    confirmed_at=_now,
                    created_from_id=old_head_tx_id,
                    number=_new_number,
                )
                tx.add(_rev)
        dm.notify_dataset_config_after_update(dataset=dataset)

    ######################

    @litestar.get(path='/{dataset_id:str}/config_txs/{tx_id:str}/inis',
                  sync_to_thread=True)
    def list_inis(self, ds_conf_reading: ConfigDirectory) -> List[str]:
        return list(ds_conf_reading.list_ini_names())

    @litestar.post(path='/{dataset_id:str}/config_txs/{tx_id:str}/inis/{name:str}',
                   media_type=MediaType.TEXT,
                   sync_to_thread=True)
    def create_ini(self, ds_conf_writing: ConfigDirectory, name: str,
                   data: AnnotatedFileUploadFormData) -> None:
        data = data.file.file.read().decode('utf-8')
        ds_conf_writing.ini(name).create(data)

    @litestar.get(path='/{dataset_id:str}/config_txs/{tx_id:str}/inis/{name:str}',
                  sync_to_thread=True)
    def read_ini(self, ds_conf_reading: ConfigDirectory, name: str) -> str:
        return ds_conf_reading.ini(name).read()

    @litestar.put(path='/{dataset_id:str}/config_txs/{tx_id:str}/inis/{name:str}',
                  media_type=MediaType.TEXT,
                  sync_to_thread=True)
    def update_ini(self, ds_conf_writing: ConfigDirectory, name: str,
                   data: AnnotatedFileUploadFormData) -> None:
        data = data.file.file.read().decode('utf-8')
        ds_conf_writing.ini(name).update(data)

    @litestar.delete(path='/{dataset_id:str}/config_txs/{tx_id:str}/inis/{name:str}',
                     sync_to_thread=True)
    def delete_ini(self, ds_conf_writing: ConfigDirectory, name: str) -> None:
        ds_conf_writing.ini(name).delete()

    ######################

    @litestar.get(path='/{dataset_id:str}/config_txs/{tx_id:str}/meta',
                  sync_to_thread=True)
    def get_meta(self, ds_conf_reading: ConfigDirectory) -> MetaConfig:
        return ds_conf_reading.meta.read()

    @litestar.put(path='/{dataset_id:str}/config_txs/{tx_id:str}/meta',
                  sync_to_thread=True)
    def set_meta(self, ds_conf_writing: ConfigDirectory, data: MetaConfig) -> None:
        ds_conf_writing.meta.update(data)

    ######################

    @litestar.get(path='/{dataset_id:str}/config_txs/{tx_id:str}/cron/jobs',
                  sync_to_thread=True)
    def list_cron_jobs(self, ds_conf_reading: ConfigDirectory) -> List[CronJob]:
        return [ds_conf_reading.cron(_id).read() for _id in ds_conf_reading.list_cron_ids()]

    @litestar.post(path='/{dataset_id:str}/config_txs/{tx_id:str}/cron/jobs',
                   sync_to_thread=True)
    def create_cron_job(self, ds_conf_writing: ConfigDirectory, data: CronJob) -> GenericIdDto:
        job_id = CronJob.make_id()
        ds_conf_writing.cron(job_id).create(data)
        return GenericIdDto(job_id)

    @litestar.get(path='/{dataset_id:str}/config_txs/{tx_id:str}/cron/jobs/{job_id:str}',
                  sync_to_thread=True)
    def read_cron_job(self, ds_conf_reading: ConfigDirectory, job_id: str) -> CronJob:
        return ds_conf_reading.cron(job_id).read()

    @litestar.put(path='/{dataset_id:str}/config_txs/{tx_id:str}/cron/jobs/{job_id:str}',
                  sync_to_thread=True)
    def update_cron_job(self, ds_conf_writing: ConfigDirectory, job_id: str, data: CronJob) -> None:
        ds_conf_writing.cron(job_id).update(data)

    @litestar.delete(path='/{dataset_id:str}/config_txs/{tx_id:str}/cron/jobs/{job_id:str}',
                     sync_to_thread=True)
    def delete_cron_job(self, ds_conf_writing: ConfigDirectory, job_id: str) -> None:
        ds_conf_writing.cron(job_id).delete()

    ######################

    # NOTE: The term `input` derives from design reiterations -- we are talking about mixed input and output data files.

    @litestar.put('/{dataset_id:str}/input/files', sync_to_thread=True)
    def put_input_file(self, dataset: Dataset, path: str, data: AnnotatedFileUploadFormData) -> None:
        _path = dataset.dir.check_user_path('./' + path)
        dataset.dir.write(path=_path, file=data.file.file)

    @litestar.delete('/{dataset_id:str}/input/files', sync_to_thread=True)
    def rm_input_file(self, dataset: Dataset, path: str) -> None:
        _path = dataset.dir.check_user_path('./' + path)
        dataset.dir.rm_file(path=_path, missing_ok=False)
    @litestar.post('/{dataset_id:str}/input/files/rename', sync_to_thread=True)
    def rename_input_file(self, dataset: Dataset, path: str, new_name: str) -> None:
        _path = dataset.dir.check_user_path('./' + path)
        dataset.dir.rename_file(path=_path, new_name=new_name)

    @litestar.post('/{dataset_id:str}/input/dir', sync_to_thread=True)
    def mk_input_dir(self, dataset: Dataset, path: str) -> None:
        _path = dataset.dir.check_user_path('./' + path)
        dataset.dir.mkdir(path=_path)

    @litestar.get('/{dataset_id:str}/input/files', sync_to_thread=True)
    def get_input_file(self, dataset: Dataset, path: str) -> File:
        _path = dataset.dir.check_user_path('./' + path)
        _path = dataset.dir.get_full_user_path(path=_path)
        if not _path.is_file():
            raise NotFoundException
        return File(
            path=_path,
            filename=_path.name,
        )

    @litestar.get('/{dataset_id:str}/input/list', sync_to_thread=True, cache=1)
    def list_input_files(self, dataset: Dataset, path: Optional[str] = '/') -> List[FsEntry]:
        _path = dataset.dir.check_user_path('./' + path)
        return list(dataset.dir.list(path=_path))

    ######################

    @litestar.post('/{dataset_id:str}/load_guest_job_files', sync_to_thread=True)
    def load_guest_job_files(self, dataset: Dataset, entity_manager: EntityManager,
                             ds_conf_reading: ConfigDirectory,
                             job_id: str, ini_name: str) -> None:
        # NOTE: There's a similar procedure for public use. [8881e2fd-bf4c-4e08-a78e-c43ac7f72eb1]

        job = entity_manager.guest_jobs.get_job(job_id)

        # Store job-dataset relationship. Mixing could not be supported and is prevented by primary key constraint.
        with entity_manager.datasets.runs_index.transaction() as tx:
            tx.add(RunRelation(
                job_id=job_id,
                dataset_id=dataset.dir.id,
                ini_name=ini_name,
            ))

        assert not job.get_status().is_queued, 'The job must not be already enqueued.'
        copytree(dataset.user_path, job.dir.user_path, dirs_exist_ok=True)
        copytree(ds_conf_reading.ini_dir_path, job.dir.user_path, dirs_exist_ok=True)

    ######################

    @litestar.get(path='/{dataset_id:str}/released_runs', sync_to_thread=True)
    def list_runs(self, dataset: Dataset, entity_manager: EntityManager) -> List[DsRunDto]:
        with entity_manager.datasets.runs_index.transaction() as tx:
            return list(dataset.iter_runs(tx=tx, released=True))

    @litestar.get(path='/{dataset_id:str}/all_runs', sync_to_thread=True)
    def list_all_runs(self, dataset: Dataset, entity_manager: EntityManager) -> List[DsRunDto]:
        with entity_manager.datasets.runs_index.transaction() as tx:
            return list(dataset.iter_runs(tx=tx))

    @litestar.get(path='/try_find_run/{job_id:str}', sync_to_thread=True)
    def try_find_run(self, entity_manager: EntityManager, job_id: str, request: Request) -> Optional[DsRunDto]:
        with entity_manager.datasets.runs_index.transaction() as tx:
            run_rel: RunRelation = tx.get(RunRelation, job_id)
            if run_rel is not None:
                try:
                    dataset = get_dataset(entity_manager, run_rel.dataset_id, request)
                except Forbidden:
                    return None
                else:
                    if run_rel.dataset_id == dataset.dir.id:
                        return run_rel.get_dto()
        return None

    @litestar.post(path='/{dataset_id:str}/runs/{job_id:str}/release', sync_to_thread=True)
    def release_run(self, dataset: Dataset, entity_manager: EntityManager, job_id: str) -> None:
        job = entity_manager.guest_jobs.get_job(job_id, must_exist=True)
        entity_manager.datasets.release_run(job=job, dataset=dataset)