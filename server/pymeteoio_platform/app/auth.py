# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import Any, Dict, Optional

from litestar.connection import ASGIConnection
from litestar.exceptions import NotFoundException
from litestar.middleware.session.server_side import (
    ServerSideSessionBackend,
    ServerSideSessionConfig,
)
from litestar.security.session_auth import SessionAuth

from .entity_manager import get_entity_manager
from .settings import OTHER_APP_ENV_SETTINGS
from ..models.users import User

_AUTH_ENV_SETTINGS = OTHER_APP_ENV_SETTINGS


def retrieve_user_handler(
        session: Dict[str, Any],
        connection: "ASGIConnection[Any, Any, Any, Any]"
) -> Optional[User]:
    # NOTE: See also pymeteoio_platform.app.public.user.reflection.get_my_user
    user_id = session.get('user_id')
    if user_id is None:
        return None
    entity_manager = get_entity_manager()  # TODO?: rework some dependency injection?
    with entity_manager.users.transaction() as tx:
        user = tx.get(User, user_id)
        if user is None:
            raise NotFoundException('User account lost.')

        tx.expunge(user)
        return user


session_auth = SessionAuth[User, ServerSideSessionBackend](
    retrieve_user_handler=retrieve_user_handler,
    session_backend_config=ServerSideSessionConfig(
        path=_AUTH_ENV_SETTINGS.auth_cookie_path,
        samesite="lax",  # TODO?: this is required for oic to work with session (maybe simplify and change to strict?)
        httponly=True,
        secure=_AUTH_ENV_SETTINGS.auth_cookie_secure,
    ),
    exclude=["/public", "/schema"],
)

# TODO: write docs about how session is secured.