# SPDX-License-Identifier: AGPL-3.0-or-later
from dataclasses import dataclass
from functools import lru_cache
from typing import Optional, Tuple, List, Literal

from litestar import Request, post, status_codes, get
from litestar.exceptions import NotFoundException
from litestar.response import Redirect
from sqlalchemy import select, and_

from ..entity_manager import get_entity_manager
from ..settings import OTHER_APP_ENV_SETTINGS
from ...models import EntityManager
from ...models.errors import AuthException
from ...models.users import OpenIDIdentifier, User
from ...oic import Connection, LoginSession, OicConnectionSetting


def is_known_issuer(config_key: str) -> bool:
    return config_key in OTHER_APP_ENV_SETTINGS.oic_providers


@lru_cache(maxsize=10)
def _get_oic_conn(config_key: str) -> Connection:
    assert is_known_issuer(config_key), 'unknown issuers must get here.'
    params = OTHER_APP_ENV_SETTINGS.oic_providers[config_key]
    return Connection(
        client_id=params.client_id,
        client_secret=params.client_secret.get_secret_value(),
        redirect_uri=f"{get_entity_manager().external_base_url}/public/oic/{config_key}/callback",
        issuer=params.issuer,
    )


@dataclass
class OicProvidersListItem:
    config_key: str
    login_button_label: str
    icon: Optional[Literal['orcid.org']]

    @classmethod
    def from_conf_key(cls, key: str) -> 'OicProvidersListItem':
        conf = OTHER_APP_ENV_SETTINGS.oic_providers[key]
        return cls(
            config_key=key,
            login_button_label=conf.login_button_label,
            icon='orcid.org' if 'orcid.org' in conf.issuer else None,
        )


@dataclass
class OicProvidersListResponse:
    providers: List[OicProvidersListItem]


@get('/oic/config', tags=['auth'])
async def oic_providers_list() -> OicProvidersListResponse:
    keys = list(OTHER_APP_ENV_SETTINGS.oic_providers.keys())
    keys.sort()  # Otherwise it would be random
    return OicProvidersListResponse(
        providers=[OicProvidersListItem.from_conf_key(key) for key in keys]
    )


@dataclass
class OicInitResponse:
    login_url: str


@post("/oic/{issuer:str}/init", sync_to_thread=True, tags=['auth'])
def oic_init(
        issuer: str,
        final_redirect_url: str,
        request: Request,
) -> OicInitResponse:
    if not is_known_issuer(issuer):
        raise NotFoundException("unknown issuer")

    conn = _get_oic_conn(issuer)
    sess = conn.start_login(final_redirect_url=final_redirect_url)
    request.set_session({'oic_' + issuer: sess})
    return OicInitResponse(
        login_url=sess.login_url,
    )


@get("/oic/{issuer:str}/callback", sync_to_thread=True, status_code=status_codes.HTTP_303_SEE_OTHER, tags=['auth'])
def oic_callback(
        issuer: str,
        entity_manager: EntityManager,
        request: Request,
) -> Redirect:
    if not is_known_issuer(issuer):
        raise NotFoundException("unknown issuer")

    _oic_status_sess_key = 'oic_status_' + issuer

    try:
        sess: LoginSession = LoginSession(**request.session['oic_' + issuer])
    except KeyError:
        raise NotFoundException("unknown session")

    conn = _get_oic_conn(issuer)
    oic_user_info = conn.verify_login(sess, query_string=request.url.query)

    if not oic_user_info.sub:
        raise AuthException("Verification failed.")

    with entity_manager.users.transaction() as tx:
        sess_user_id = request.session.get('user_id')
        sess_user: Optional[User] = tx.get(User, sess_user_id) if sess_user_id else None

        oid_results: List[Tuple[OpenIDIdentifier]] = list(tx.execute(
            select(OpenIDIdentifier).filter(and_(
                OpenIDIdentifier.issuer == conn.issuer),
                OpenIDIdentifier.subject == oic_user_info.sub,
            )
        ).unique())  # NOTE: this is for joined relation resolution and does not mean we get exactly one result.

        assert 0 <= len(oid_results) <= 1

        if len(oid_results) <= 0:
            oid = None
        else:
            oid = oid_results[0][0]
            assert oid.issuer == conn.issuer
            assert oid.subject == oic_user_info.sub

        oic_user: Optional[User] = oid.user if oid else None

        if oic_user and sess_user:
            if oic_user is sess_user:
                # Nothing to do.
                request.session[_oic_status_sess_key] = 'already_connected'  # TODO?
            else:
                # Already connected to another account.
                request.session[_oic_status_sess_key] = 'taken_by_other_user'  # TODO?
            if oic_user.full_name is None:
                oic_user.full_name = oic_user_info.full_name
        elif sess_user:
            # Connect existing user
            tx.add(OpenIDIdentifier(
                issuer=conn.issuer,
                subject=oic_user_info.sub,
                user_id=sess_user.id,
            ))
            request.session[_oic_status_sess_key] = 'connected'
        elif oic_user:
            # Once verified we can create a session.
            request.set_session({"user_id": oic_user.id})
            request.session[_oic_status_sess_key] = 'logged_in'
            if oic_user.full_name is None:
                oic_user.full_name = oic_user_info.full_name
        else:
            # # No self sign up.
            # request.session[_oic_status_sess_key] = 'self_sign_up_denied'  # TODO?

            # Do self sign-up with OpenID.  # TODO?: keep or remove this? Or add access levels
            __new_user = User()
            tx.add(__new_user)
            __oid_id = OpenIDIdentifier(
                id=None, user_id=None, user=__new_user, issuer=conn.issuer, subject=oic_user_info.sub)
            __new_user.openid_identifiers.append(__oid_id)
            __new_user.full_name = oic_user_info.full_name
            tx.add(__oid_id)
            request.set_session({"user_id": __new_user.id})
            request.session[_oic_status_sess_key] = 'self_signed_up'

    return Redirect(sess.final_redirect_url)