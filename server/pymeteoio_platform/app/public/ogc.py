# SPDX-License-Identifier: AGPL-3.0-or-later
from a2wsgi import WSGIMiddleware
from litestar import Router, asgi
from litestar.static_files import StaticFilesConfig

from ..entity_manager import get_entity_manager
from ...ogc.wps.app import app


class _NoCopyWSGIMiddleware(WSGIMiddleware):
    """
    Workaround of Litestar issue about excessive deepcopy
    https://github.com/litestar-org/litestar/commit/e15b93d8d0ef1b92b9127bd771630bcf78352bf2
    """
    def __copy__(self):
        return self
    def __deepcopy__(self, memo=None):
        return self


ogc_router = Router(
    route_handlers=[

        asgi(path='/wps/', is_mount=True)(_NoCopyWSGIMiddleware(app=app)),

        StaticFilesConfig(
            path='/data/',
            directories=[
                get_entity_manager().pywps_path,
            ],
        ).to_static_files_app(),

    ],
    path='/ogc',
    tags=["guest_jobs"],
    dependencies={
    }
)
