# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import Any, Optional, Annotated

from litestar import Request, get
from litestar.exceptions import NotFoundException
from litestar.params import Dependency

from ....models import EntityManager
from ....models.users import UserReflection, User
from ....models.users.dtos import UserOidReflection

MyUserDepT = Annotated[Optional[User], Dependency(skip_validation=True)]


def get_my_user(entity_manager: EntityManager, request: "Request[Any, Any, Any]") -> Optional[User]:
    # NOTE: compare this with pymeteoio_platform.app.auth.retrieve_user_handler:
    #       - retrieve_user_handler is for the auth scope; get_my_user does not require it.
    #       - retrieve_user_handler has a fixed signature that requires the connection object; get_my_user does not.
    #       - retrieve_user_handler may not support Dependency Injection; get_my_user does.
    #       - The implementation is almost identical.

    user_id = request.session.get('user_id')
    if user_id is None:
        return
    with entity_manager.users.transaction() as tx:
        user: User = tx.get(User, user_id)
        if user is None:
            raise NotFoundException('User account lost.')

        tx.expunge(user)
        return user


@get("/", sync_to_thread=True)
def get_me(my_user: MyUserDepT) -> UserReflection:
    if my_user is None:
        return UserReflection()
    else:
        user = my_user
        return UserReflection(
            id=user.id,
            email=user.email if user.has_email else None,
            full_name=user.full_name,
            has_password=True if user.password_hash else False,
            is_logged_in=True,
            openid=[UserOidReflection(issuer=oid.issuer, subject=oid.subject) for oid in user.openid_identifiers],
            can_create_datasets=user.can_create_datasets,
            is_admin=user.is_admin,
        )