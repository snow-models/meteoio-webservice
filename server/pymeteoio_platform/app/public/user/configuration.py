# SPDX-License-Identifier: AGPL-3.0-or-later
from dataclasses import dataclass

from litestar import get

from .reflection import MyUserDepT
from ....models import EntityManager
from ....models.general_settings.instance import AppCustomizationSettingsInstance
from ....models.users.access_level import AccessLevel


@dataclass
class FrontendConfiguration:
    general_access_level: AccessLevel
    welcome: str
    settings: AppCustomizationSettingsInstance
    has_ldap: bool = False


@get('/configuration')
async def _retrieve_frontend_configuration(my_user: MyUserDepT, entity_manager: EntityManager) -> FrontendConfiguration:
    general_access_level = AccessLevel.for_user(my_user)
    settings = await entity_manager.general_settings.read_async()

    if general_access_level != AccessLevel.GUEST:
        settings.welcome_guests = ''  # redacted
    if general_access_level != AccessLevel.AUTHENTICATED:
        settings.welcome_restricted_internal = ''  # redacted
    if general_access_level != AccessLevel.DATA_OWNER:
        settings.welcome_data_owners = ''  # redacted

    welcome = None
    if general_access_level == AccessLevel.GUEST:
        welcome = settings.welcome_guests
    if general_access_level == AccessLevel.AUTHENTICATED:
        welcome = settings.welcome_restricted_internal
    if general_access_level == AccessLevel.DATA_OWNER:
        welcome = settings.welcome_data_owners

    return FrontendConfiguration(
        welcome=welcome,
        settings=settings,
        general_access_level=general_access_level,
        has_ldap=entity_manager.ldap is not None,
    )
    # TODO: optimization: this and /api/public/me are both always called at first page load (see user router).

