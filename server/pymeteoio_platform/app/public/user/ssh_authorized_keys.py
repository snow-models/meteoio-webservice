# SPDX-License-Identifier: AGPL-3.0-or-later
import datetime
from dataclasses import dataclass
from typing import Optional, List, cast

import uuid6
from litestar import get, post, delete, Response, status_codes
from litestar.exceptions import NotFoundException, NotAuthorizedException

from .reflection import MyUserDepT
from ....models import EntityManager
from ....models.users import User
from ....models.users.dtos import SSHAuthorizedKey

ACCEPTED_SSH_PUB_KEY_ALGORITHMS: List[str] = [
    'ssh-rsa',
    'ssh-ed25519',
]

BAD_SSH_PUB_KEY_ALGO_ERROR_MSG = f"Bad algorithm, should be one of {', '.join(ACCEPTED_SSH_PUB_KEY_ALGORITHMS)}"


@get("/", sync_to_thread=True)
def get_ssh_authorized_keys(my_user: MyUserDepT) -> List[SSHAuthorizedKey]:
    if my_user is None:
        return []
    else:
        return my_user.ssh_authorized_keys


@dataclass(frozen=True)
class SSHAuthorizedKeyAddResponse:
    error: Optional[str] = None
    added_entry_id: Optional[str] = None


@post("/", sync_to_thread=True)
def add_ssh_authorized_key(my_user: MyUserDepT, data: SSHAuthorizedKey,
                           entity_manager: EntityManager) -> SSHAuthorizedKeyAddResponse:
    if my_user is None:
        raise NotAuthorizedException
    else:
        from asyncssh import import_public_key, KeyImportError
        try:
            _ssh_key = import_public_key(data.public_key)
        except KeyImportError as ex:
            return cast(SSHAuthorizedKeyAddResponse, Response(
                content=SSHAuthorizedKeyAddResponse(error=str(ex)),
                status_code=status_codes.HTTP_400_BAD_REQUEST))

        _algo = _ssh_key.get_algorithm()
        if _algo not in ACCEPTED_SSH_PUB_KEY_ALGORITHMS:
            return cast(SSHAuthorizedKeyAddResponse, Response(
                content=SSHAuthorizedKeyAddResponse(error=BAD_SSH_PUB_KEY_ALGO_ERROR_MSG),
                status_code=status_codes.HTTP_400_BAD_REQUEST))

        # _ssh_key.export_public_key('openssh').decode('ascii')
        # NOTE: The above step would standardize the format of the public key,
        #       but don't need it. It's preferable to let the user see the original, validated string.

        new_entry = SSHAuthorizedKey(
            entry_id=str(uuid6.uuid7()),
            insert_date=datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc, microsecond=0).isoformat(),
            public_key=data.public_key,
            public_title=data.public_title[:100],
            fingerprint=_ssh_key.get_fingerprint(),
        )

        with entity_manager.users.transaction() as tx:
            user = tx.get(User, my_user.id)
            user.ssh_authorized_keys = [new_entry, *my_user.ssh_authorized_keys]

        return SSHAuthorizedKeyAddResponse(added_entry_id=new_entry.entry_id)


@delete("/{entry_id:str}", sync_to_thread=True)
def rm_ssh_authorized_key(my_user: MyUserDepT, entry_id: str,
                          entity_manager: EntityManager) -> None:
    if my_user is None:
        raise NotAuthorizedException
    else:
        old_list = my_user.ssh_authorized_keys
        new_list = [entry for entry in old_list if entry.entry_id != entry_id]
        if len(old_list) == len(new_list):
            raise NotFoundException

        with entity_manager.users.transaction() as tx:
            user = tx.get(User, my_user.id)
            user.ssh_authorized_keys = new_list