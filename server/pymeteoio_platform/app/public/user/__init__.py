# SPDX-License-Identifier: AGPL-3.0-or-later
from litestar import Router
from litestar.di import Provide

from .configuration import _retrieve_frontend_configuration
from .reflection import get_me, get_my_user
from .ssh_authorized_keys import get_ssh_authorized_keys, add_ssh_authorized_key, rm_ssh_authorized_key

user_router = Router(
    path="/me",
    tags=['auth'],
    route_handlers=[
        get_me,
        _retrieve_frontend_configuration,
        Router(
            path='/ssh_authorized_keys',
            route_handlers=[
                get_ssh_authorized_keys,
                add_ssh_authorized_key,
                rm_ssh_authorized_key,
            ]
        )
    ],
    dependencies={
        'my_user': Provide(get_my_user, sync_to_thread=True),
    }
)