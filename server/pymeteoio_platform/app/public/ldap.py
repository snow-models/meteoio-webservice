import logging
from typing import Any

from litestar import Router, post, Request, Response, status_codes

from .login import LoginStatus, LoginStatusCode
from ...models import EntityManager
from ...models.ldap.srv import LdapAuthException, LdapLoginForm
from ...models.users import User

_logger = logging.getLogger(__name__)



@post("/authenticate", sync_to_thread=True)
def _authenticate(
    data: LdapLoginForm,
    entity_manager: EntityManager,
    request: "Request[Any, Any, Any]",
) -> Response[LoginStatus]:
    """Authenticate a user via LDAP"""

    try:
        user_info = entity_manager.ldap_server.authenticate(data)
    except LdapAuthException:
        _logger.exception("LDAP authentication failed")
        return Response(
            LoginStatus(status=LoginStatusCode.failed),
            status_code=status_codes.HTTP_203_NON_AUTHORITATIVE_INFORMATION
        )

    with entity_manager.users.transaction() as tx:
        user_id = str(user_info.unique_id)
        assert user_id, 'A user ID must have been produced in the LDAP entry.'
        user = tx.get(User, user_id)
        if user is None:
            _logger.info(f"Creating new user {user_id} from LDAP entry.")
            user = User(id=user_id)
            tx.add(user)
        else:
            _logger.info(f"Updating user {user_id} from LDAP entry.")
        user.full_name = user_info.display_name
        # Possible feature request: grant permissions based on LDAP groups. This also requires session invalidation.
    request.set_session({"user_id": user_id})

    return Response(
        LoginStatus(status=LoginStatusCode.welcome_back),
        status_code=status_codes.HTTP_202_ACCEPTED
    )


ldap_router = Router(
    path="/ldap",
    tags=['auth'],
    route_handlers=[
        _authenticate,
    ]
)
