# SPDX-License-Identifier: AGPL-3.0-or-later
from shutil import copytree
from typing import List

from litestar import Router, get, post, Request
from litestar.di import Provide
from litestar.pagination import OffsetPagination
from sqlalchemy import func, select

from ...models import EntityManager
from ...models.datasets import DatasetListEntry, DatasetRelation, Access, DsRunDto, Dataset, ConfigDirectory, \
    RunRelation


@get(path='/', cache=5, sync_to_thread=True)
def list_datasets(entity_manager: EntityManager, limit: int = 1000, offset: int = 0) -> OffsetPagination[
    DatasetListEntry]:
    with entity_manager.datasets.index.transaction() as tx:
        total_count = tx.scalar(
            select(func.count(DatasetRelation.dataset_id))
            .filter(DatasetRelation.access == Access.public))
        stm = (
            select(DatasetRelation)
            .filter(DatasetRelation.access == Access.public)
            .limit(limit).offset(offset))
        items: List[DatasetListEntry] = []
        for ds_rel, in tx.execute(stm).unique():
            ds_rel: DatasetRelation
            items.append(ds_rel.get_dto())
        return OffsetPagination(
            items=items,
            limit=limit,
            offset=offset,
            total=total_count,
        )


@get(path='/{dataset_id:str}', cache=5, sync_to_thread=True)
def get_dataset(entity_manager: EntityManager, dataset: Dataset) -> DatasetListEntry:
    with entity_manager.datasets.index.transaction() as tx:
        ds_rel: DatasetRelation = tx.get(DatasetRelation, dataset.dir.id)
        return ds_rel.get_dto()


@get(path='/{dataset_id:str}/released_jobs', cache=5, sync_to_thread=True)
def get_released_jobs(entity_manager: EntityManager, dataset: Dataset) -> List[DsRunDto]:
    with entity_manager.datasets.runs_index.transaction() as tx:
        return list(dataset.iter_runs(tx=tx, released=True))


@post('/{dataset_id:str}/load_guest_job_files_for_rerun', sync_to_thread=True)
def load_guest_job_files_for_rerun(entity_manager: EntityManager, dataset: Dataset, job_id: str) -> None:
    # NOTE: There's a similar procedure for internal use. [8881e2fd-bf4c-4e08-a78e-c43ac7f72eb1]

    # NOTE: What are we going to do? Load data files into a guest job for a rerun of a dataset config:
    #       - get the guest job where we are going to copy data files into
    #       - get dataset config path
    #       - copy trees of ds data files and config files into guest job dir
    job = entity_manager.guest_jobs.get_job(job_id)

    # Store job-dataset relationship. Mixing could not be supported and is prevented by primary key constraint.
    with entity_manager.datasets.runs_index.transaction() as tx:
        tx.add(RunRelation(
            job_id=job_id,
            dataset_id=dataset.dir.id,
            # ini_name=ini_name,
        ))

    assert not job.get_status().is_queued, 'The job must not be already enqueued.'
    ds_conf_reading = ConfigDirectory(path=dataset.config.continuation(read_only=True))
    copytree(dataset.user_path, job.dir.user_path, dirs_exist_ok=True)
    copytree(ds_conf_reading.ini_dir_path, job.dir.user_path, dirs_exist_ok=True)


def _get_dataset(entity_manager: EntityManager, dataset_id: str, request: Request) -> Dataset:
    user_id = request.session.get('user_id') or None
    # NOTE: the user_id is to allow the data owner to resample their private dataset via the public endpoint
    return entity_manager.datasets.get(dataset_id, check_user_permission='readonly', user_id=user_id)


public_datasets_router = Router(
    route_handlers=[
        list_datasets,
        get_dataset,
        get_released_jobs,
        load_guest_job_files_for_rerun,
    ],
    path='/datasets',
    tags=["public_datasets"],
    dependencies={
        'dataset': Provide(_get_dataset, sync_to_thread=True)
    }
)