# SPDX-License-Identifier: AGPL-3.0-or-later
import logging

from litestar import Router

from .datasets import public_datasets_router
from .job_submission import JobSubmission
from .login import login, logout
from .oic import oic_providers_list, oic_init, oic_callback
from .user import user_router
from ..entity_manager import get_entity_manager
from ..settings import OTHER_APP_ENV_SETTINGS

_other_handlers = []
_logger = logging.getLogger(__name__)


if OTHER_APP_ENV_SETTINGS.enable_ogc_api:
    from .ogc import ogc_router
    _other_handlers.append(ogc_router)

if get_entity_manager().ldap:
    from .ldap import ldap_router
    _other_handlers.append(ldap_router)
else:
    _logger.info('LDAP (optional) not configured, will not be available.')


public_router = Router(path="/public", route_handlers=[
    public_datasets_router,
    JobSubmission,
    login,
    logout,
    user_router,
    oic_providers_list, oic_init, oic_callback,
    *_other_handlers,
])