# SPDX-License-Identifier: AGPL-3.0-or-later
import dataclasses
import os
from enum import Enum
from typing import List, Optional, Tuple

import litestar
from litestar import Request
from litestar.di import Provide
from litestar.exceptions import NotFoundException
from litestar.response import File
from sqlalchemy import select

from ..uploads import AnnotatedFileUploadFormData
from ...models import EntityManager
from ...models.datasets import RunRelation, DatasetRelation
from ...models.errors import NotFound, Forbidden
from ...models.files import FsEntry
from ...models.jobs import Job, JobStatus, JobListEntryDTO, JobCommand, JobProcessPriority, JobStats
from ...models.jobs_index import JobRelation
from ...models.strace import StraceDto
from ...models.timeseries import TimeSeriesJobParams


def get_job(entity_manager: EntityManager, job_id: str) -> Job:
    # TODO?: Add dataset context as a possible way to reuse this infrastructure on a different job manager?
    #        Also note: there are only 5-7 methods that are relevant to read-only, released runs,
    #        while 5 methods are for job creation, input, launch, etc.
    return entity_manager.guest_jobs.get_job(job_id)


class _JobAccessLevel(str, Enum):
    full = 'full'
    limited = 'limited'  # e.g. confidential data/config has been imported from a dataset


@dataclasses.dataclass
class _JobAccessLevelResponse:
    access_level: _JobAccessLevel


def _get_job_access_level(entity_manager: EntityManager, job_id: str, request: Request) -> _JobAccessLevel:
    if request.session:
        user_id = request.session.get('user_id')
    else:
        user_id = None

    if user_id is None:
        # Anonymous guest users cannot be associated at job submission.
        # NOTE: Revised requirements forbid guest jobs from anonymous guests, but that's not true for ds reruns.
        # For ds reruns, limited access is OK.
        return _JobAccessLevel.limited

    # If the job is a dataset run, then the access is limited unless the user is the data owner.
    dataset_id: Optional[str]
    with entity_manager.datasets.runs_index.transaction() as tx:
        run_rel: Optional[RunRelation] = tx.get(RunRelation, job_id)
        if run_rel is None:
            dataset_id = None
        else:
            dataset_id = run_rel.dataset_id
            # Will need to dig the rel but outside this session

    if dataset_id is None:
        # No dataset relationship to think about, just see job submit user, if not anonymous
        job_submitted_by_user_id: Optional[str] = None
        with entity_manager.guest_jobs.index.transaction() as tx:
            job_rel: Optional[JobRelation] = tx.get(JobRelation, job_id)
            if job_rel is None:
                # NOTE: a job_rel should always be found if the job exists.
                raise NotFound()
            else:
                # job_rel.submitted_by_user_id can be None.
                job_submitted_by_user_id = job_rel.submitted_by_user_id

        if job_submitted_by_user_id and user_id and job_submitted_by_user_id == user_id:
            # I'm the job creator
            return _JobAccessLevel.full
        else:
            # The job has been shared or was from an anonymous guest user
            return _JobAccessLevel.limited

    else:
        # There's dataset relationship -- see owner [7dde4b06-6969-4c1d-b488-9ca762af84ef]
        with entity_manager.datasets.index.transaction() as tx:
            ds_rel: Optional[DatasetRelation] = tx.get(DatasetRelation, dataset_id)
            if ds_rel is None:
                raise NotFound()
            if user_id and ds_rel.owner_user_id and user_id == ds_rel.owner_user_id:
                return _JobAccessLevel.full
            else:
                return _JobAccessLevel.limited


class JobSubmission(litestar.Controller):
    path = '/job_submission'
    dependencies = {
        'job': Provide(get_job, sync_to_thread=True),
        'job_access_level': Provide(_get_job_access_level, sync_to_thread=True),
    }
    tags = ["guest_jobs"]

    @litestar.get(sync_to_thread=True)
    def list_my_jobs(self, entity_manager: EntityManager, request: Request) -> List[JobListEntryDTO]:
        # TODO?: paging with limit, offset?
        user_id = request.session.get('user_id')
        if not user_id:
            return []
        with entity_manager.guest_jobs.index.transaction() as tx:
            jrs: List[Tuple[JobRelation]] = tx.execute(
                select(JobRelation).filter(JobRelation.submitted_by_user_id == user_id)
            ).all()
            return [JobListEntryDTO(id=jr.job_id, created_at=jr.created_at) for (jr,) in jrs]

    @litestar.post(sync_to_thread=True)
    def create(self, entity_manager: EntityManager, request: Request) -> JobListEntryDTO:
        # NOTE: anonymous users can launch ds rerun jobs
        user_id: Optional[str] = request.session.get('user_id')
        job = entity_manager.guest_jobs.make_job(create=True, user_id=user_id)
        return JobListEntryDTO(id=job.id)

    @litestar.post('/{job_id:str}/start', sync_to_thread=True)
    def start(self, entity_manager: EntityManager, job_id: str, data: TimeSeriesJobParams) -> None:
        # NOTE: anonymous users can launch ds rerun jobs
        job = entity_manager.guest_jobs.get_job(job_id)
        job.set_command(JobCommand(
            executable=entity_manager.meteoio_timeseries_executable,
            priority=JobProcessPriority.idle,
            args=data.get_command_args(inis_dir=job.dir.get_full_user_path(''))
        ))
        entity_manager.guest_jobs.start(job)

    @litestar.get('/{job_id:str}/status', sync_to_thread=True)
    def status(self, job: Job) -> JobStatus:
        # NOTE: This method is OK also for users with limited access level
        return job.get_status()

    @litestar.get('/{job_id:str}/stats', sync_to_thread=True)
    def stats(self, job: Job) -> JobStats:
        # NOTE: This method is OK also for users with limited access level
        return job.get_stats()

    @litestar.get('/{job_id:str}/access_level', sync_to_thread=True)
    def get_job_access_level(self, job_access_level: _JobAccessLevel) -> _JobAccessLevelResponse:
        return _JobAccessLevelResponse(access_level=job_access_level)

    @litestar.get('/{job_id:str}/stdout')
    async def stdout(self, job: Job, job_access_level: _JobAccessLevel) -> File:
        if job_access_level != _JobAccessLevel.full:
            raise Forbidden
        return File(path=job.stdout_path if job.stdout_path.exists() else os.devnull)

    @litestar.get('/{job_id:str}/strace')
    async def strace(self, job: Job, job_access_level: _JobAccessLevel) -> File:
        if job_access_level != _JobAccessLevel.full:
            raise Forbidden
        return File(path=job.strace_log_path if job.strace_log_path.exists() else os.devnull)

    @litestar.get('/{job_id:str}/strace/parsed')
    async def strace_parsed(self, job: Job, job_access_level: _JobAccessLevel) -> List[StraceDto]:
        if job_access_level != _JobAccessLevel.full:
            raise Forbidden
        return list(job.get_strace().iter_dtos(open_files_only=True, cwd=job.dir.user_path))

    @litestar.get('/{job_id:str}/strace/outputs')
    async def strace_outputs(self, job: Job) -> List[StraceDto]:
        # NOTE: This method is OK also for users with limited access level
        return list(job.get_strace().iter_unique_files(output_only=True, cwd=job.dir.user_path))

    # @litestar.get('/{job_id:str}/stderr', sync_to_thread=True)
    # def stderr(self, job: Job) -> File:
    #     """We are merging stderr into stdout, so this request is redundant"""
    #     return File(path=job.stderr_path)

    @litestar.put('/{job_id:str}/files', sync_to_thread=True)
    def put_file(self, job: Job, path: str, data: AnnotatedFileUploadFormData,
                 job_access_level: _JobAccessLevel) -> None:
        if job_access_level != _JobAccessLevel.full:
            raise Forbidden
        # NOTE: limited access is sufficient for ds reruns because they do not require this method
        job.dir.write(path=job.dir.check_user_path(path), file=data.file.file)

    @litestar.get('/{job_id:str}/files', sync_to_thread=True)
    def get_file(self, job: Job, path: str, job_access_level: _JobAccessLevel) -> File:
        if job_access_level != _JobAccessLevel.full:
            # In case of limited access, only give access to output paths
            # NOTE: We are using strace to identify what files are safe to identify as non-confidential outputs.
            #       Yes, this is an access control rule. And yes, this is the best implementation option at the moment.
            # TODO?: Ask MeteoIO to generate the list of output files? And/or store user-configurable output files list?
            # NOTE: Duplicate access control policy implementation code. [bf99b2d5-56b9-4222-bed8-e1990ecf5627]
            _outputs_paths = {
                row.open_file_path
                for row in job.get_strace().iter_unique_files(output_only=True, cwd=job.dir.user_path)
            }
            if path not in _outputs_paths:
                raise Forbidden()

        _path = job.dir.get_full_user_path(path=job.dir.check_user_path(path))
        if not _path.is_file():
            raise NotFoundException
        return File(
            path=_path,
            filename=_path.name,
        )

    @litestar.get('/{job_id:str}/files_list', sync_to_thread=True, cache=1)
    def list_files(
        self, job: Job,
        job_access_level: _JobAccessLevel,
        path: Optional[str] = '/'
    ) -> List[FsEntry]:
        if job_access_level != _JobAccessLevel.full:
            raise Forbidden()
        return list(job.dir.list(path=job.dir.check_user_path(path)))


# TODO?: implement some kind of job restart procedure? (during dev, it would have been very useful).