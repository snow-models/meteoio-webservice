# SPDX-License-Identifier: AGPL-3.0-or-later
import enum
from dataclasses import dataclass
from typing import Any

from litestar import Request, post, status_codes

from ...models import EntityManager
from ...models.errors import AuthenticationFailure
from ...models.users import User, EmailPasswordLoginDto


class LoginStatusCode(enum.Enum):
    failed = "failed"
    welcome_back = "welcome_back"


@dataclass
class LoginStatus:
    status: LoginStatusCode


@post("/login", sync_to_thread=True, status_code=status_codes.HTTP_200_OK, tags=['auth'])
def login(
        entity_manager: EntityManager,
        data: EmailPasswordLoginDto,
        request: "Request[Any, Any, Any]"
) -> LoginStatus:
    with entity_manager.users.transaction() as tx:
        try:
            user = tx.query(User).filter(User.email == data.email).first()
            if user is None:
                raise AuthenticationFailure
            user.verify_password(data.secret_password)
        except:
            return LoginStatus(status=LoginStatusCode.failed)

        # once verified we can create a session.
        request.set_session({"user_id": user.id})
        return LoginStatus(status=LoginStatusCode.welcome_back)


@post("/logout", sync_to_thread=True, status_code=status_codes.HTTP_204_NO_CONTENT, tags=['auth'])
def logout(
        request: "Request[Any, Any, Any]"
) -> None:
    request.clear_session()