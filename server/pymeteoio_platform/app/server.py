# SPDX-License-Identifier: AGPL-3.0-or-later
from litestar import Litestar, Response, status_codes
from litestar.config.compression import CompressionConfig
from litestar.config.cors import CORSConfig
from litestar.di import Provide
from litestar.exceptions import NotFoundException, NotAuthorizedException
from litestar.logging import LoggingConfig
from litestar.openapi import OpenAPIConfig
from litestar.stores.file import FileStore

from .admin import admin_router
from .auth import session_auth
from .entity_manager import get_entity_manager
from .internal import internal_router
from .public import public_router
from ..models import EntityManager
from ..models.errors import NotFound, AuthException, Forbidden, UserReportableBadRequestException, \
    UserReportableException
from ..sentry import maybe_init_sentry

maybe_init_sentry()

entity_manager = get_entity_manager()

def _entity_manager_provider() -> EntityManager:
    # NOTE: we don't directly use get_entity_manager because we don't want to have extra unexpected query args.
    return entity_manager


# noinspection PyTypeChecker
app = Litestar(
    cors_config=CORSConfig(
        allow_methods=['GET'],
        allow_origins=[
            'run.niviz.org'
        ]
    ),
    route_handlers=[public_router, internal_router, admin_router],
    compression_config=CompressionConfig(backend="gzip"),
    stores={"sessions": FileStore(entity_manager.sessions_path)},
    openapi_config=OpenAPIConfig(
        title=entity_manager.general_settings.read().app_short_title,
        version=f'v{entity_manager.get_version()}',
    ),
    dependencies={
        'entity_manager': Provide(_entity_manager_provider, sync_to_thread=True, use_cache=True),
    },
    logging_config=LoggingConfig(
        log_exceptions='always'
    ),
    exception_handlers={
        # NOTE: Validation failures are already handled by the framework with HTTP 400 response with
        # ValidationException: lambda _, exc: Response(
        #     content={'error': 'validation failed', 'details': getattr(exc, 'detail', '')},
        #     status_code=status_codes.HTTP_400_BAD_REQUEST,
        # ),
        # NOTE: pydantic exceptions must be considered as internal exceptions and should not be reported as HTTP 400.
        # pydantic.ValidationError: lambda _, exc: Response(
        #     content={'error': 'validation failed'},
        #     status_code=status_codes.HTTP_400_BAD_REQUEST,
        # ),
        NotFoundException: lambda _, exc: Response(
            content={'detail': 'Not found'},
            status_code=status_codes.HTTP_404_NOT_FOUND,
        ),
        NotAuthorizedException: lambda _, exc: Response(
            content={'detail': 'Not authorized'},
            status_code=status_codes.HTTP_401_UNAUTHORIZED,
        ),
        NotFound: lambda _, exc: Response(
            content={'detail': f'Not found: {str(exc)}' if len(str(exc)) > 0 else 'Not found'},
            status_code=status_codes.HTTP_404_NOT_FOUND,
        ),
        UserReportableBadRequestException: lambda _, exc: Response(
            content={'detail': f'Error: {str(exc)}' if len(str(exc)) > 0 else 'Error'},
            status_code=status_codes.HTTP_400_BAD_REQUEST,
        ),
        Forbidden: lambda _, exc: Response(
            content={'detail': f'Forbidden: {str(exc)}' if len(str(exc)) > 0 else 'Forbidden'},
            status_code=status_codes.HTTP_403_FORBIDDEN,
        ),
        # NOTE: some review is required before giving more specific information about AuthException's
        AuthException: lambda _, exc: Response(
            content={'detail': f'Auth failure: {str(exc)}' if len(str(exc)) > 0 else 'Auth failure'},
            status_code=status_codes.HTTP_401_UNAUTHORIZED,
        ),
        UserReportableException: lambda _, exc: Response(
            content={'detail': f'Error: {str(exc)}' if len(str(exc)) > 0 else 'Error'},
            status_code=status_codes.HTTP_500_INTERNAL_SERVER_ERROR,
        ),
        Exception: lambda _, exc: Response(
            content={'detail': f'Error'},
            status_code=status_codes.HTTP_500_INTERNAL_SERVER_ERROR,
        ),
    },
    on_app_init=[session_auth.on_app_init]
)

# NOTE: We could refactor to make job_submission and datasets as plugins: https://docs.litestar.dev/2/usage/plugins.html
#       but the public/internal/admin distinction seems more relevant.

# TODO?: some simple power-on self-test to guard against bugs hidden by dynamic local imports and similar