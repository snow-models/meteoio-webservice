# SPDX-License-Identifier: AGPL-3.0-or-later
from pathlib import Path
from typing import Dict

from pydantic import Field, model_validator
from pydantic_settings import BaseSettings, SettingsConfigDict

from ..models import EntityManager
from ..oic import OicConnectionSetting, OIC_CONFIG_KEY_RE


class AppEnvSettings(BaseSettings):
    meteoio_platform: EntityManager

    model_config = SettingsConfigDict(
        env_file = Path(__file__).parent / '.env',  # This means the .env file shall be placed besides this source file.
        env_file_encoding = 'utf-8',
        env_nested_delimiter = '__',
        extra='ignore',
    )

        # NOTE: Settings can also be set via env variables (e.g. for docker-compose).
        #       Value priority is well documented:
        #       https://docs.pydantic.dev/1.10/usage/settings/#field-value-priority



class _OtherAppEnvSettings(BaseSettings):
    # server_host_key: FilePath
    auth_cookie_secure: bool = Field(default=False)
    auth_cookie_path: str = Field(default='/')
    enable_ogc_api: bool = Field(default=True)
    oic_providers: Dict[str, OicConnectionSetting] = Field(default_factory=dict)

    @model_validator(mode='after')
    def _validate_oic_providers_keys(self):
        issuers = set()
        for key, config in self.oic_providers.items():
            if not OIC_CONFIG_KEY_RE.match(key):
                raise ValueError(
                    'Bad key for oic_providers dictionary: '
                    'it must be a simple and short alphanumeric string, max 6 characters.'
                )
            if config.issuer in issuers:
                raise ValueError(f'Duplicate issuer found in oic_providers: {config.issuer}')
            else:
                issuers.add(config.issuer)
        return self

    model_config = SettingsConfigDict(
        env_file = Path(__file__).parent / '.env',  # This means the .env file shall be placed besides this source file.
        env_file_encoding = 'utf-8',
        env_nested_delimiter = '__',
        extra='ignore',
    )


OTHER_APP_ENV_SETTINGS = _OtherAppEnvSettings()
