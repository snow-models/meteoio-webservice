# SPDX-License-Identifier: AGPL-3.0-or-later
from litestar import Router, get, put

from ...models import EntityManager
from ...models.general_settings.instance import AppCustomizationSettingsInstance


@get("/")
async def _read(entity_manager: EntityManager) -> AppCustomizationSettingsInstance:
    """This is similar to GET /public/settings, but this is always fresh, not cached."""
    return await entity_manager.general_settings.read_async()


@put("/", sync_to_thread=True)
def _write(entity_manager: EntityManager, data: AppCustomizationSettingsInstance) -> None:
    entity_manager.general_settings.write(data)


@get("/factory_default")
async def _get_factory_default() -> AppCustomizationSettingsInstance:
    return AppCustomizationSettingsInstance()


settings_router = Router(path="/settings", route_handlers=[
    _read,
    _write,
    _get_factory_default,
])
