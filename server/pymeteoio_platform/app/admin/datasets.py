# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import List

from litestar import get, delete, post, Router
from litestar.pagination import OffsetPagination
from sqlalchemy import select, func

from ...models import EntityManager
from ...models.datasets.heading_dtos import DatasetListEntry
from ...models.datasets.index import DatasetRelation
from ...models.errors import NotFound, Forbidden
from ...models.users import User


@get(path='/', sync_to_thread=True)
def list_datasets(entity_manager: EntityManager, limit: int = 1000, offset: int = 0) -> OffsetPagination[DatasetListEntry]:
    with entity_manager.datasets.index.transaction() as tx:
        items: List[DatasetListEntry] = []
        total_count = tx.scalar(select(func.count(DatasetRelation.dataset_id)))
        stm = select(DatasetRelation).order_by(DatasetRelation.dataset_id.desc()).limit(limit).offset(offset)
        for ds_rel, in tx.execute(stm).unique():
            ds_rel: DatasetRelation
            items.append(ds_rel.get_dto())

        return OffsetPagination(
            items=items,
            offset=offset,
            limit=limit,
            total=total_count,
        )

@post(path='/{dataset_id:str}/owner_transfer', sync_to_thread=True)
def transfer_dataset(entity_manager: EntityManager, dataset_id: str,
                     old_owner_user_id: str, new_owner_user_id: str) -> None:
    with entity_manager.users.transaction() as tx:
        new_owner = tx.get(User, new_owner_user_id)
        if new_owner is None:
            raise NotFound('The specified user was not found')
        if not new_owner.can_create_datasets:
            raise Forbidden('The specified user has no permission to create datasets')
    ds = entity_manager.datasets.get(dataset_id, user_id=old_owner_user_id, must_exist=True)
    entity_manager.datasets.transfer_dataset(
        dataset=ds, old_owner_user_id=old_owner_user_id, new_owner_user_id=new_owner_user_id)

@delete(path='/{dataset_id:str}', sync_to_thread=True)
def delete_dataset(entity_manager: EntityManager, dataset_id: str, owner_user_id_for_check: str) -> None:
    ds = entity_manager.datasets.get(dataset_id, user_id=owner_user_id_for_check, must_exist=True)
    entity_manager.datasets.delete_dataset(
        dataset=ds, owner_user_id_for_check=owner_user_id_for_check, dataset_id_for_check=dataset_id)


datasets_router = Router(path="/datasets", route_handlers=[
    list_datasets,
    transfer_dataset,
    delete_dataset,
])