# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import List

from litestar import get, delete, Router
from litestar.pagination import OffsetPagination
from sqlalchemy import select, func

from ...models import EntityManager
from ...models.jobs import JobListEntryDTO
from ...models.jobs_index import JobRelation


@get(path='/', sync_to_thread=True)
def list_jobs(entity_manager: EntityManager, limit: int = 1000, offset: int = 0) -> OffsetPagination[JobListEntryDTO]:
    with entity_manager.guest_jobs.index.transaction() as tx:
        items: List[JobListEntryDTO] = []
        total_count = tx.scalar(select(func.count(JobRelation.job_id)))
        stm = select(JobRelation).order_by(JobRelation.job_id.desc()).limit(limit).offset(offset)
        for job_rel, in tx.execute(stm).unique():
            job_rel: JobRelation
            items.append(JobListEntryDTO(
                id=job_rel.job_id,
                created_at=job_rel.created_at,
            ))
        return OffsetPagination(
            items=items,
            offset=offset,
            limit=limit,
            total=total_count,
        )


@delete(path='/{job_id:str}', sync_to_thread=True)
def delete_job(entity_manager: EntityManager, job_id: str) -> None:
    job = entity_manager.guest_jobs.get_job(job_id)
    job.dir.remove(confirm=job_id, recursive=True)
    with entity_manager.guest_jobs.index.transaction() as tx:
        job_rel = tx.get(JobRelation, job_id)
        tx.delete(job_rel)


jobs_router = Router(path="/jobs", route_handlers=[
    list_jobs,
    delete_job,
])