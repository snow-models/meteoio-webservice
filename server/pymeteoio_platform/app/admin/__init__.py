# SPDX-License-Identifier: AGPL-3.0-or-later
from litestar import Router
from litestar.connection import ASGIConnection
from litestar.exceptions import NotAuthorizedException
from litestar.handlers import BaseRouteHandler

from .datasets import datasets_router
from .jobs import jobs_router
from .settings import settings_router
from .users import users_router


def admin_user_guard(connection: ASGIConnection, _: BaseRouteHandler) -> None:
    if not connection.user.is_admin:
        raise NotAuthorizedException()


admin_router = Router(path="/admin", tags=['admin'], route_handlers=[
    users_router,
    jobs_router,
    datasets_router,
    settings_router,
], guards=[
    admin_user_guard
])
