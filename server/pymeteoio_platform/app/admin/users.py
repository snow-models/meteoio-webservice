# SPDX-License-Identifier: AGPL-3.0-or-later
from typing import List

from litestar import get, Router, post, delete
from litestar.pagination import OffsetPagination
from sqlalchemy import select, func

from ...models import EntityManager
from ...models.users import User, UserReflection
from ...models.users.dtos import UserOidReflection


@get(path='/', sync_to_thread=True)
def list_users(entity_manager: EntityManager, limit: int = 1000, offset: int = 0) -> OffsetPagination[UserReflection]:
    with entity_manager.users.transaction() as tx:
        items: List[UserReflection] = []
        total_count = tx.scalar(select(func.count(User.id)))
        stm = select(User).order_by(User.id.desc()).limit(limit).offset(offset)
        for user, in tx.execute(stm).unique():
            user: User
            items.append(UserReflection(
                id=user.id,
                email=user.email if user.has_email else None,
                full_name=user.full_name,
                has_password=True if user.password_hash else False,
                can_create_datasets=user.can_create_datasets,
                openid=[UserOidReflection(issuer=oid.issuer, subject=oid.subject) for oid in
                        user.openid_identifiers]
            ))
        return OffsetPagination(
            items=items,
            offset=offset,
            limit=limit,
            total=total_count,
        )


@post(path="/{edit_user_id:str}/can_create_datasets", sync_to_thread=True)
def set_can_create_datasets(entity_manager: EntityManager, edit_user_id: str) -> None:
    with entity_manager.users.transaction() as tx:
        user: User = tx.get(User, edit_user_id)
        user.can_create_datasets = True


@delete(path="/{edit_user_id:str}/can_create_datasets", sync_to_thread=True)
def reset_can_create_datasets(entity_manager: EntityManager, edit_user_id: str) -> None:
    with entity_manager.users.transaction() as tx:
        user: User = tx.get(User, edit_user_id)
        user.can_create_datasets = False


users_router = Router(path="/users", route_handlers=[
    list_users,
    set_can_create_datasets,
    reset_can_create_datasets,
])