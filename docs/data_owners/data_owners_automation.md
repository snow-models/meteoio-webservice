---
title: Setting up automation
---

In order to automatically generate up-to-date datasets, you may automate:

* **the data processing** by defining [cron jobs in the web application](#cron-jobs) to run at regular intervals
* **the input data push** by defining a cron job on your computer to push new data through an [SSH connection](#input-data-push).


## Cron Jobs

For automated, periodic tasks, set up cron jobs: go to your dataset's page and navigate to the 'Cron' tab. You may have to start a revision, then you will be able to create the first entry. A cron job allows to automate the processing of your source data. The result is automatically released as dataset output in case of successful job execution.

After you have applied the revision, you may have to wait up to 10 minutes before the system scheduler gets to know the new configuration and starts running the new cron job. The same delay applies for other cron job scheduling changes.


## Input data push

You can securely upload and manage the source data files of a dataset via SFTP (SSH File Transfer Protocol). Both [sftp](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol)
and [scp](https://en.wikipedia.org/wiki/Secure_copy_protocol) protocols are supported, although the [`scp` command](https://linux.die.net/man/1/scp) (which currently [uses SFTP by default](https://superuser.com/a/1434253)) may be required for non-interactive usage.

Now go through the following steps (click to get more details):

1. <details><summary>Choose the computer that will serve your data</summary>

    Select a computer that has access to both your raw data (coming out of your data acquisition system) and to the
    Internet. It must also have *ssh* installed (It is usually installed by default on Linux and MacOS as well as
    Windows 10 build 1809 and higher);
   </details>

2. <details><summary>Generate an ssh key</summary>

    If you don't already have one, you need to generate one. In a terminal window under the username that will push the
    data, type `ssh-keygen↵` and answer all subsequent questions with `↵` (in order to keep the default values).
    See [OpenSSH key management](https://en.wikipedia.org/wiki/Secure_Shell#Authentication:_OpenSSH_key_management)
    for more information.
   </details>

3. <details><summary>Provide your **public** ssh key to the web platform</summary>
    * select the dataset you want to push data to;
    * select the *Source Data* tab;
    * click on the *SFTP access* and then select the *add new key* button;
    * provide a title that helps you understand which key is the one you are about to provide;
    * copy the content (as text) of your public key file in the *Public key* box. On Linux, your public key is in `/home/{your username}/.ssh/id_rsa.pub`. On MacOS, it is in `/Users/{your username}/.ssh/id_rsa.pub`. On Windows, it is in `C:\Users\{your username}\.ssh\id_rsa.pub`;
   </details>
4. <details><summary>Try manually pushing some data</summary>
    * from a terminal window, go to where your raw data is located;
    * from the dialog box where you have provided your key, copy your SSH username (provided as a long hash);
    * type the following command: `scp -P 2200 {path to your data file} {your username from above}@{web service url}:/{destination path}↵`. The destination path is understood relative to your dataset root: `/` would copy your data at the highest level of your dataset file structure while `/input` would copy your data in an *input* subdirectory. Please note that you should do it at least once so you can validate the host key of the server on your system (it will ask you to confirm this is OK the first time you connect over ssh).
   </details>
5. <details><summary>Automate the source data push</summary>
    This relies on tasks scheduling capabilities in your operating system: while this web platform allows to configure
    cron jobs for data processing, the following instructions will help you configure cron jobs to push data to the
    web platform from your machine where raw data resides.

    === "Linux / MacOS"

         Using [Cron](https://en.wikipedia.org/wiki/Cron):

          * if necessary, set your default text editor: `export EDITOR={name of your editor}` (for example, your editor might be *joe*, *nano*, *vi*…);
          * create a new conjob: `crontab -e`;
          * if you want to job outputs to be emailed to you, add/edit a line on top of your crontab like `MAILTO="myself@mydomain.eu"` with your email address. If you prefer not to have any emails, add/edit a line on top of your crontab such as `MAILTO=""`;
          * the crontab file contains one command per line, starting with when to execute the command (patterns for the minutes, hours, day of month, month, day of week) followed by the command itself. Please note that it does not necessarily execute in the same shell as your login shell!
              + you can use a [cron schedule expression generator](https://crontab.guru/) to configure when the source data push will run;
              + the command that follows the schedule is the following: `scp -P 2200 {path to your data file} {your username from above}@{web service url}:/{destination path}`
              + as an example, sending the file *WFJ2.csv* from the */mnt/data/weissfluhjoch/* directory once an hour would  look like: `0 * * * * scp -P 2200 /mnt/data/weissfluhjoch/WFJ2.csv 012a4180-d4a1-7c05@dame-server.slf.ch:/input`

    === "Windows"

         Using [Task Scheduler](https://en.wikipedia.org/wiki/Windows_Task_Scheduler):

          * from the *start* menu, type / search for the *Task Scheduler* application;
          * in the top menu, select *Action* then *New Folder…*;
          * give a meaningful name for the new folder (this is to keep things organized on your side);
          * select the created folder, then in the top menu, select *Action* then *Create Basic Task…*;
          * give your task a meaningful name, then *next*. Select the repetition interval you want, the *next*;
          * select the start time for your task, then *next*;
          * select *Start a program* as action you want to perform, then *next*;
          * as *program/script* write `scp`. As *arguments*, write `-P 2200 {path to your data file} {your username from above}@{web service url}:/{destination path}`. As *Start in* copy the path where your data is located, then *next*. Validate the last screen with *Finish*. If you want higher refresh rates than daily, you can now double -click on your task, move to the *triggers* tab, double-click on your trigger and fine tune the repetition intervals in the *Advanced settings* part of the window.

    </details>


## Synchronization

When setting up your cron jobs, please be aware of the following:

!!! warning inline end "Caution: Synchronization of Cron Jobs"

     When relying on the interaction of multiple cron jobs (such as one to push the data and another one to process the data), pay attention to the timing of your cron jobs to avoid such synchronization issues that could affect the integrity of your automatically generated datasets.

* **Schedule processing after source data push**: The data processing job in the web application should be scheduled to occur after the completion of the source data push job from your computer. This is to ensure that the processing job operates on the most recent data set.

* **Account for job duration**: The scheduled time for the data processing job must not only be after the start of the source data push job but also allow sufficient time for its completion. As this setup lacks a mechanism to trigger the processing job immediately after the source data push job, this buffer time is essential.

* **Monitor and adjust**: Since there is no external trigger to start the processing job right after the source data push, it is advisable to monitor the initial runs and adjust the timing accordingly. If the source data push job takes longer than expected, the scheduled time for the processing job should be updated to reflect this change.

By carefully scheduling the cron jobs and accounting for the duration of each task, you can minimize the risk of synchronization issues and ensure that the processing job always has access to the latest data.

Alternatively, you may wish to consult the [Software Developer's documentation](../dev/index.md), which may help you find alternative setups that better suit your deployment needs, e.g. including API calls after source data push.
