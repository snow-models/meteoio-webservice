---
title: Role
---

The Data Owner is the person who is responsible for a certain data stream (usually provided by an Automatic Weather Station). The data owner checks that the station fulfills its role (so data comes when expected and the measurements are reasonably useful) and informs the technical team if this is not the case. The data owner is also the main contact point for the data users if they have questions regarding the data as well as to inform the data producers of scientific publications or operational data products that might be based on the collected data. The MeteoIO webservice provides various tools to fulfill these tasks as efficiently as possible[^1].


First, in order to get the **Data Owner** role, follow these steps:

1. **ORCiD Login**: To initiate the process of becoming a data owner, first log in via ORCiD. This ensures a secure and verified entry into the system.
2. **Dataset Creation Permission**: After logging in, notify the system administrator of your intent to create datasets. The software does (yet) not provide any specific way to get in touch with administrators. Once you had success, the administrator may grant you the necessary permissions to commence dataset creation.


[^1]: Please note that some of these features are still in development and therefore not accessible yet.