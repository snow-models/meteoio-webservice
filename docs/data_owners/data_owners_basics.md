---
title: Manual jobs
---

## Dataset Creation

Please make sure you are familiar with the principle of operation as laid out in the [Quick overview](../about/intro.md) documentation. Moreover, please note that the foundation of your data management lies in creating a dataset. This dataset will house your raw meteorological data and is required for the later stages of data processing and publishing.

1. **Creating a Dataset**: Access the application home page and locate "Your Datasets & INIs" in the navigation. You will not see this option if you have not been granted the permission to create datasets. Then _"Create a new Dataset"_ to access a form.
2. **INI configuration**: You will be prompted to upload an INI file. Please ensure that you have prepared it with [Inishell](https://inishell.slf.ch/) and filled it with relevant metadata. Do not upload data files yet.
3. **Naming and Metadata**: It's crucial to ensure your dataset is named appropriately and is accompanied by relevant metadata, which will make future data referencing and searches more efficient. Once you have selected the INI file, the application should pre-fill the form with metadata, or it may suggest you to edit it in the INI file first.
4. **Confirm the creation** of the dataset, which will be private by default; this will bring you to the data owners interface.

## Revisions

Once a dataset is in place, you may still want to change the INI configurations, settings, naming and metadata that you entered before. You first have to _"start a revision"_. In the _"Settings"_ tab you will find the possibility to change the dataset naming and also the option to make it publicly accessible. The _"Settings"_ tab also gives the option to delete you dataset.

## Source Data

Your raw data is the heart of this entire process: through the web interface, navigate to the _"Source Data"_ tab within your dataset. For the best results, ensure your data adheres to the recommended [formats supported by MeteoIO](https://meteoio.slf.ch/doc-release/html/data_sources.html).

## Running Manual Jobs

For immediate processing, go to your dataset's page and navigate to the _"INI configuration"_ tab. Here, you can see the INI files and a button to run a manual job to test the configuration. This is also useful for ad-hoc data updates. Once the job is finished, you have to "Release" it to make it appear as a dataset output.

## Logs

Keep track of your data's integrity and processing history: within your dataset, locate the _"Logs"_ tab. These logs document data quality, invalid data points, and provide insights into data processing over time. They are invaluable for troubleshooting and ensuring data accuracy. In the case of cron jobs, only the last execution of that entry will be accessible: this is to preserve disk space. More extensive logs may be provided by your system administrator.