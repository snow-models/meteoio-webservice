---
title: Welcome!
---

The [MeteoIO Web Application](..) has been designed to manage **environmental timeseries** data between data collection and data publication.


Once logged in, you will be able to populate your online workspace with datasets and processing instructions in order to automatically standardize and correct your data before serving them over various protocols. As guest, you'll be able to view published datasets or even submit processing jobs (after [Orcid](https://orcid.org/) login) to have the results temporarily available for you to download.

This documentation is therefore aimed at various user profiles:

 * for **all users**, it is recommended to have a look at the [Quick overview](about/intro.md);
 * **Data Users** You want to use data that has been FAIRified or submit a guest processing job, see the [Data Users](data_users/intro.md) documentation.
 * **Data Owners** You are responsible for some Automatic Weather Station and would like to standardize your data, share it with some potential users or data portals, get notified when something goes wrong. Then have a look at the  [Data Owners documentation](data_owners/data_owners_role.md).
 * **System Administrators** You would like to deploy this web application on your premises, then have a look at the [System administrators documentation](sysadmin/intro.md).
 * **Developers** You want to extend and contribute to the system, have a look at the [Developer documentation](dev/index.md).


---
<center>
<span class="acknowledgements">
This software has received funding from the
[World Meteorological Organization](https://public.wmo.int/en)
under grant agreement No. 29539/2022-1.9
as well as the European Union’s
[Horizon 2020](https://commission.europa.eu/funding-tenders/find-funding/eu-funding-programmes/horizon-europe_en)
research and innovation programme under [grant agreement No. 101003472](https://cordis.europa.eu/project/id/101003472).
</span>
<center>

<table class="home_credits_logos">
<tbody>
    <tr>
    <td><a href="https://arcticpassion.eu/"><img src="img/Arctic_Passion_logo.png" width="150"></a></td>
    <td><a href="https://globalcryospherewatch.org/"><img src="img/gcw_logo.png" width="90"></a></td>
    <td><a href="https://slf.ch/"><img src="img/slf.svg" width="45"></a></td>
    <td><a href="https://wsl.ch/"><img src="img/wsl.svg" width="45"></a></td>
    </tr>
</tbody>
</table>

<div style="text-align:center; font-style: italic; font-size: 0.85em; opacity: 0.8">
The original application software has been
<span style="white-space: nowrap"> developed by <a href="https://dkr.srl" target="_blank">DkR</a> for <a href="https://slf.ch/" target="_blank">SLF</a></span>.
</div>
