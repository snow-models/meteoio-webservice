---
title: Data Users
---


## Downloading available data

As an anonymous user, browse for datasets that have been made public by their [data owners](../data_owners/data_owners_role.md) by selecting _"Explore Datasets"_ in the left menu. You can then download your datasets of choice and even request a rerun of MeteoIO on the configuration file provided by the data owner, allowing you to get the dataset with a different temporal coverage or sampling rate (data processing jobs usually take a few seconds to complete, at worst a few minutes).

## Submit a guest job

After login with an [Orcid](https://orcid.org), you can submit your own data together with a MeteoIO configuration file to be processed and download the result. There is an extensive [online documentation](https://meteoio.slf.ch/doc-dev/html/) for MeteoIO but you are encouraged to rely on the [Inishell](https://inishell.slf.ch)[^1] GUI to write and edit your configuration files. 

<figure markdown>
  ![Inishell screenshot](../img/Inishell.png){ width="600" }
  <figcaption>The Inishell GUI for writing INI configuration files</figcaption>
</figure>


So please go to [https://inishell.slf.ch](https://inishell.slf.ch), select _"Releases"_ in the top navigation bar and download a version to install on your computer. After launching Inishell, select _"MeteoIO"_ in its left panel and start configuring your data processing. When you are done, save your work under a meaningful file name and upload it as INI file in the MeteoIO web-service.


[^1]: Bavay, Mathias, Reisecker, Mickael, Egger, Thomas, and Korhammer, Daniella. _"Inishell 2.0: semantically driven automatic GUI generation for scientific models"_, Geosci. Model Dev., 15 (2022), 365–378, [10.5194/gmd-15-365-2022](https://doi.org/10.5194/gmd-15-365-2022).