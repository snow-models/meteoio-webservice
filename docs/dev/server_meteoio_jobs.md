---
title: MeteoIO guest jobs
---

## Guest directories and jobs

Meteo-IO operations, such as time-series sampling and filtering, are file operations performed by an executable. For
each job, a working directory is created where the files can be copied to and the output can be copied from. The name of
the directory is a UUID that is also the identifier of the Job. The Job command is started in the guest directory as CWD
and the PID of the created process is collected for monitoring. The stdout and stderr of the process is also collected
as a pair of files (or as a single file) in the metadata subdirectory.
`strace` may be used in order to reliably collect info about file accesses. NOTE: these "jobs" are one-off, unscheduled
job. The design of this software is meant to allow launching multiple jobs concurrently, with async execution mode. This
means that the platform can be queried to know the status of a job that has been created.

## Job execution and sandboxing

A desired property of the system is the isolation of jobs (or sandboxing). For example, a job should never write outside
its dedicated working directory and its output should never override the outputs of other jobs. This aspect is
non-trivial because of the broad configuration options supported by the job workload (meteoio_timeseries)
and the current implementation assumes that no unintended behaviour is performed by the jobs.

## Job status monitoring

Asynchronous job execution is a requirement: we need to be able to monitor the status of a job while it's running. We
don't want to naively rely on tracking by the parent process ownership (e.g. python subprocess), because we'd like to
support multiple server processes. That's why we store some metadata about the job and the child process it launches. A
set of small text files is used to store some metadata of the Job, including the PID and process creation time. We write
atomic attributes on individual files to support simple lock-free polling operations (without using a DBMS). The job
finish event is marked by storing the current timestamp as a last duty of the job executor.

The couple <PID, process creation time> may be used to identify a running Job in the list of processes managed by the OS
for purposes like killing or waiting without polling. The PID alone is not sufficient as it may get recycled; other
identifiers cannot be attached to the process.

In case of a multi-node server deployment it may not be possible to access the process of a job, so killing is not
supported and waiting can only be done with polling. In this case, the couple <PID, process creation time> has no use.
Other job metadata can still be accessed if the filesystem is shared.

## Discarded options for jobs implementation

- run the job in a VM: it would be perfect for sandboxing, but efficient nesting containers require privileges.
- chroot the job: it would be nice for sandboxing, but requires privileges at the application level.
- have a script in the sudoers to launch the chroot-ed job: `--cap-add=SYS_CHROOT` would still be required in docker.
- have a pool of unix users dedicated to running jobs isolated via unix file permissions: not a strong sandbox?
- intercept all file-related system calls: it would allow constraining file operations, but it's hard to get right.
- have one container wait for a job to run and then restart: does not scale to multiple, concurrent, async jobs.
- mount the docker.socket in the app container: it would allow allocating job containers, and also too much other stuff.

## Chosen architecture for sandboxed job execution

Have a pre-allocated set of containers only for job execution:

- job data have to be copied to the container before launch.
- job result have to be copied from the container after finish.
- job status may be read from the container during async execution.
- job-container association have to be memorized in order to access the job during execution.

The container shall expose some network interface to provide the following functionality:

- allocate *the* job directories (and retrieve its ID), without allowing for second job if it has already been created in this container.
- write a job data file in the required directory structure
- write job options and parameters
- launch the job process
- read a job output file in the required directory structure
- read the job stdout/stderr transcript
- read the job strace transcript?
- read the job exit code, or null if it did not terminate yet
- terminate (the server process, implicitly restarting the container, losing the job files)

These functionalities shall be protected for internal use of the application only, which implements access control.

The above is the responsibility of the job execution container. The following are the duties of the host application.

The termination shall be requested by the host after the job finish has been detected and the result has been copied. A
prompt termination is required in order to make the container available for future jobs as soon as possible. In other
words, the trick for sandboxing is to make the container instance handle at most one job and then restart.

Additionally, pre-allocation of multiple instances of the container allows for concurrency of jobs (and availability).
Each instance shall be uniquely addressable in order to asynchronously access it after job creation. 

The system shall implement a timeout after which it kills the container and releases its resources.

The job executor in the container could advise its own availability to the host application at a configured endpoint, 
so that the number of instances could be dynamically scaled without reconfiguring the host.

### Current implementation

* The job execution process is linearized into an internal protocol to work with simple TCP stream socket connections.
* A multi-instance container named `job_runner` is responsible for the execution of a single job as described above.
* A central, single-instance container named `job_dispatcher` is responsible for:
     * Accepting connections from `backend_*` containers to receive jobs.
     * Accepting connections from `job_runner` containers to receive availability to work on a job.

The `job_dispatcher` keeps a queue of jobs.

When the `backend` connects, it communicates the character `j` (a byte), the job is enqueued and the result is awaited.

When the `job_runner` connects, it communicates the character `w` (a byte), a job is dequeued and delegated. 
The `job_runner` waits if a job is not immediately available, with the character `k` sent by the `job_dispatcher` as keepalive message, followed by `j` to commence the transmission of the job.

The `backend` is able to wait while a `job_runner` is not available. In this side, a long TCP timeout is used; there's no keepalive message and a retry loop is implemented by the `backend`.

A job (both before and after the execution) is a zipped directory (a stream of bytes) in this network protocol.


``` mermaid
sequenceDiagram
  autonumber
  participant backend
  participant job_dispatcher

  job_runner ->> job_dispatcher: "w"
  
  loop while job not available
    job_dispatcher ->> job_runner: "k"
  end

  backend ->>+ job_dispatcher: "j"
  job_dispatcher ->> job_runner: "j"
  
  backend ->> job_dispatcher: job directory zip chunks
    
  job_dispatcher ->> job_runner: job directory zip chunks
  job_runner ->> job_dispatcher: "s"
  job_dispatcher ->> backend: "s"
  
  Note right of job_runner: job execution
  
  job_runner ->> job_dispatcher: job directory zip chunks

  
  job_dispatcher ->>- backend: job directory zip chunks
  
  destroy job_runner
  
  Note right of job_runner: container restart

```