---
title: Development Debugging
---


# Debugging

## Run dev backend server

Create and activate a python venv, then:

```shell
cd <repo root>/server
litestar --app pymeteoio_platform.app.server:app run --debug -r --reload-dir=./pymeteoio_platform
```

NOTE: the above command is for development only: it provides debug messages and reloads in case of code changes. More
debugging options: see specific instructions for `litestar`.

## Run dev frontend server

```shell
cd <repo root>/packages/meteoio-platform-web-pages
npm run dev
```

NOTE: this includes a proxy to the backend server that must be started independently.

# Troubleshooting

## NPM packages

You may have to run `npm install` also in order to have the local import statements work, as cross-package references
are backend by symlinks in the node_modules directory. If you encounter errors from React hooks indicating you may have
a "version mismatch", then fire `npm install` from `/`.

Dependencies must be installed with `npm install -w <workspace>` and not by navigating the package directory.
Consequently, the npm workspaces (packages) shall not contain any `package-lock.json`: only one `package-lock.json`
shall be present in the root of the repository; this means we avoid version mismatches in the dependencies.

## Windows development environment

Some backend components may require support for the creation of symlinks. This is standard functionality offered by UNIX
operating systems, but it's not always available by default to Windows users, depending on the Windows version. That's
why you may encounter errors like "A required privilege is not held by the client.". You may want to use `gpedit.exe` to
allow your user the right to create symbolic links.
