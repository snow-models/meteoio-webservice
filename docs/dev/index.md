---
title: Software developers introduction
---


This software is [open source](https://code.wsl.ch/snow-models/meteoio-webservice.git) under a [GNU Affero public licence](https://www.gnu.org/licenses/agpl-3.0.en.html) and an [OpenAPI documentation](../../api/schema) is available.

This repository is structured as one npm package (a.k.a. monorepo) and multiple npm workspaces (a.k.a. packages)
and a directory of python modules (_server_).

- **pymeteoio_platform**: python backend code
    - _models_: data structures and run-time representation of domain entities
    - _app_: web server based on Litestar, providing an OpenAPI spec
        - _public_: API endpoints that can be accessed by guest users
        - _internal_: API endpoints for qualified users
- **meteoio-platform-client**: generated React Query client for the API exposed by _pymeteoio_platform_
- **meteoio-platform-web-pages**: React application that can connect to the above API
- **meteoio-ui**: base set of UI components, based on React and FluentUI

# General considerations

## Client code generation

One option was to use openapi-typescript-codegen... openapi-codegen is actually fancier as it generates TanStack React
Query hooks.

```shell
cd <repo root>/server
python -m pymeteoio_platform.app.generate_schema
cd <repo root>/packages/meteoio-platform-client
npm run generate
```

## Dataclasses and pydantic models

Most model classes are defined with `@dataclass` decorator, from the `dataclasses` standard library. Dataclasses are a
clean, simple and standard way to implicitly declare constructors and properties from type hints. The use of the
non-standard `pydantic.dataclasses` is preferred as it provides deserialization and runtime validation, with seamless
conversion from dataclass to pydantic `BaseModel`, while maintaining the dataclasses semantics. The use of dataclasses
is recurrent also in the definition of controller classes as a way to support configurability.

## Async vs. sync operations

Near the web API endpoints we would prefer async procedures. Still, sync may be preferred for simplicity and ease of
interoperability with libs that are not async.
The web framework Litestar will still work also non-blocking also with sync endpoints thanks to a thread pool.
