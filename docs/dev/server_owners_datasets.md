---
title: MeteoIO data owners
---

## Dataset entity

A dataset is composed by:

- heading attributes like name, description, etc
  (stored in versioned configuration directory and in DB for the current version)
- access level (private or public) (stored in DB)
- a data owner (a user, stored in DB)
- ini configurations (stored in versioned configuration directory)
- input data files (stored in input directory)
- cron job configurations (stored in versioned configuration directory)
- cron execution history and logs (stored in runs directory)
- output data files from last execution of each cron job (stored in output directory)

A relational DB is used as a secondary storage for indexing. It is kept up to date with the head version of each dataset
configuration. The configuration attributes are copied to the index after applying a config update.

Access level is only stored in the DB. That's justified by the criticality and the default being private. Publication
shall be requested with a specific operation performed on the DB. Same for the data owner user_id.

The input and output directories can be accessed after the creation of a dataset. The contents of the output directory
is overridden by the execution of cron job. In fact, the execution of a cron job involves the creation of a working
directory with:

- a copy of the input directory,
- a copy of the ini files,
- a copy of the output directory.

Copying files is considered an acceptable solution while mounting or ref-linking may be considered in the future for
performance improvements.

## Dataset configuration and posts mechanism

A dataset is configured using files. We want file operations to be transactional. A temporary directory is used for
operations. A link designates the head version. Operations on the head are forbidden. A copy must be created as a
temporary directory to work on.
This allows for a basic multi-version concurrency control.
Concurrent user sessions can work on different versions.
Locking can be implemented on top of this, possibly forbidding a second pending version other than the head.
This mechanism is not meant to keep a log of revisions, for which keeping a copy is not the most efficient way.
In fact, old copies from old posts or transactions are forgotten (deleted) after applying the new head version.

## Dataset jobs and files management

A dataset has a configuration, some input data files and some output data files.
Dataset configuration and data files are handled differently: INI files are versioned while data files are not.

Output files are produced by the execution of a job.

As a first solution, there's no distinction between input and output files,
and there's one directory of data files that has the same lifetime of the dataset,
which is reused across job executions.
In other words, in this setting, successive job executions may take as input the output of a previous job execution
in the same dataset. In order to prevent using the output as input, which may be unwanted, the user is responsible to
configure the job to constrain writes to an output directory.
(The system isolates datasets from each other, but there's no specific protection of input data files).

As a second solution, the same posts mechanisms for configuration transactions could be reused also for data files.
Then it could be possible to provide the user with multiple versions of data files from multiple jobs.

In both solutions, the INI configuration files must be copied over the working directory of a job before running it,
possibly with replacement of existing data files with the same name.
Also, that copy shall remain as a data file after the job execution.
When displaying the data files to the user, the INI configuration files must also be shown at the location where they
would be copied for the execution of a job. The GUI should implement these entries as special references to
configuration
files if a specific GUI is available. If a data file exists with the same name of a configuration file, then it must be
hidden as it would be overridden by the configuration file. (It's like an overlay file system).

A particular functionality is implemented to allow to test the execution of a job without altering the front-page output
of a dataset: the infrastructure for guest jobs is reused and a slightly different GUI with no data files upload is
provided; in this case the files are copied from the dataset and the job result is later available as for guest jobs.

## Cron jobs scheduling

Most of the logic is delegated to APScheduler, with a sqlite store and a background executor. Scheduled jobs are
identified by the dataset id and the cron id. The scheduler shall run in a single process. The job parameters are read
from the configuration files when the job function is invoked. The scheduling of the job is updated from the
configuration files at the time of dataset.config.apply. Inter-process communication may not be immediate as it is
mediated by the scheduler store, so updates to the scheduling (frequency of a job) may be delayed.
Specifically, the delay is limited by the maximum execution time of an iteration in the ds_cron_scheduler container.
With the current settings, the average delay is 5 minutes.

## SFTP server for dataset files

The dataset files directory shall be accessible via SFTP. The main use case is third-party systems pushing or pulling
data on regular intervals. Authentication shall be based on username and public/private key. This application will
provide a specific username for each dataset and will be formed like `{user_id}-d-{dataset_id}`. For example, if your
user id is `a1` and your dataset id is `b2`, then the ssh username is `a1-d-b2`.
(See disseminated c2794b16-b049-426c-99c2-f0012c0533a8)
