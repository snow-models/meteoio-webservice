# MeteoIO Timeseries Processing via OGC WPS

## Introduction

In the realm of meteorological data processing,
the MeteoIO library stands as a comprehensive tool,
offering a range of functionalities to handle and analyze meteorological datasets efficiently.
This documentation outlines the usage of the "meteoio_timeseries" process,
a feature accessible through this software's implementation of the Open Geospatial Consortium (OGC) Web Processing Service (WPS) API, version 1.0.0.
This service, developed with PyWPS, is designed to provide users with the ability to access and utilize the capabilities of the MeteoIO library remotely,
without the need for local installation. 
By leveraging the OGC WPS API, a standardized interface for publishing geospatial processes on the web,
this service simplifies the interaction with meteorological data, enabling users to upload data and configuration files in a single zip format and retrieve the computed results conveniently.

For further information on the OGC WPS standard, please refer to the [Open Geospatial Consortium WPS documentation](https://www.ogc.org/standard/wps/).

## Overview

The "meteoio_timeseries" process allows users to process meteorological data using the functionality of the MeteoIO library via a web interface.
This eliminates the need for local installation of the library and facilitates remote data processing.
Both synchronous and asynchronous operation modes are supported.

Steps to Use the Service:

1. **Prepare Your Data**: Compile your meteorological data and any necessary configuration files into a single zip file.
2. **Upload Your Data**: Utilize the provided interface to upload your zip file to the service.
3. **Data Processing**: The "meteoio_timeseries" process will process your uploaded data according to the given configuration files.
4. **Retrieve Results**: Once processing is complete, download the results from the provided link.

### Process Details
- **Process Name**: `meteoio_timeseries`
- **API Version**: OGC WPS 1.0.0
- **Endpoint URL**: `<base_url>/api/public/ogc/wps`

#### Input Requirements
- **Date and time range:** Users must specify at least two of the following: `begin` (start time), `end` (end time), and `duration_days` (integer duration in days). This requirement ensures that the process has a clear timeframe for data processing. Valid values for `begin` and `end` include "NOW" and [ISO 8601 timestamps](https://meteoio.slf.ch/doc-release/html/classmio_1_1Date.html#a8953af1f836877332e85d3065785d091ab58eca5fdc032da556238801831a2afe).
- **Data Input:** The `data` input accepts a zipped directory containing data and additional configuration files, facilitating the process's operation within the MeteoIO framework.
- **Configuration Files:** There are two ways to provide the MeteoIO configuration:
    - **INI File Name:** Specify the name of the INI file in `ini_file_name`. The default is "io.ini".
    - **INI Content:** Directly input the text contents of the INI file in the `ini` literal input. If provided, this content takes precedence and overrides any INI file with the same name in the zipped directory. If not provided, the file must be present in the zipped directory.

#### Outputs
The primary output is a [MetaLink4](https://datatracker.ietf.org/doc/html/rfc5854) file, which contains URIs linking to multiple result files. 
These files may include both the processed output and the original input data.

#### Modes of Operation

- **Synchronous Processing:** The server processes the request and only responds once the results are ready.
- **Asynchronous Processing** (preferred): Users can submit their request and check the status of the process at intervals.

#### Status Information
Consistent with the OGC WPS standard, the "meteoio_timeseries" process provides comprehensive status information. 
This feature allows users to monitor the progress of their processing requests. 
The status information includes stages like 'Process Accepted', 'Process Started', and 'Process Succeeded', ensuring transparency and predictability in the processing workflow.


## Interaction example

Use high-level libraries to interact with the OGC WPS API. 
Here's a code example showing how to start a `meteoio_timeseries` job:

```python
from birdy import WPSClient   # https://birdy.readthedocs.io/

client = WPSClient(
    'http://127.0.0.1:8095/api/public/ogc/wps',
    progress=True
)

result = client.meteoio_timeseries(
    begin='2008-12-09',
    duration_days='2',
    resolution_minutes=240,
    ini_file_name='io.ini',
    data=open('test_job.zip', 'rb'),
)

print(repr(result.get()))
```


### Network Traffic Insight

Observe the API's request-response mechanism for a better understanding of the service's workflow:

```
> GET /api/public/ogc/wps?service=WPS&request=GetCapabilities&version=1.0.0 HTTP/1.1
< HTTP/1.1 200 OK <wps:Capabilities> ...

> GET /api/public/ogc/wps?service=WPS&request=DescribeProcess&version=1.0.0&identifier=all HTTP/1.1
< HTTP/1.1 200 OK <wps:ProcessDescriptions> ...

> POST /api/public/ogc/wps HTTP/1.1
< HTTP/1.1 200 OK <wps:ExecuteResponse> ... <wps:ProcessAccepted>

> GET /api/public/ogc/data/7e8c320c-abb6-11ee-bf0f-0242ac120009.xml HTTP/1.1
< HTTP/1.1 200 OK <wps:ExecuteResponse> ... <wps:ProcessStarted>

> GET /api/public/ogc/data/7e8c320c-abb6-11ee-bf0f-0242ac120009.xml HTTP/1.1
< HTTP/1.1 200 OK <wps:ExecuteResponse> ... <wps:ProcessSucceeded> ... <wps:ProcessOutputs> ...

> GET /api/public/ogc/data/7e8c320c-abb6-11ee-bf0f-0242ac120009/input.meta4 HTTP/1.1
< HTTP/1.1 200 OK <metalink xmlns="urn:ietf:params:xml:ns:metalink"> ...

> GET /api/public/job_submission/018cd933-7be3-745d-91cc-f6a86e0fb97e/files?path=FLU2.smet HTTP/1.1
< HTTP/1.1 200 OK [output file content]
```


---

This documentation provides a snapshot of the API capabilities and requirements.
Users are advised to consult the full XML process description.
