# Deployment behind a reverse proxy

## Introduction

In today's digital landscape, ensuring the security and efficiency of web applications is paramount.
Providing a web application with TLS (HTTPS) is a critical security measure and a requirement for all public web services to be accepted by major web browsers.
A critical component in achieving this is the implementation of TLS termination. 
Web traffic may then need to be processed unencrypted for various load balancing and monitoring reasons before being passed to the application software.
That's why the application does not encapsulate itself with TLS termination.
The use of a reverse proxy is also recommended as a protective gateway between your users and the web application.

One of the key features of a reverse proxy is its ability to implement TLS and redirect HTTP traffic based on the host specified in the HTTP headers.
This capability allows you to control access to your application, ensuring that only legitimate requests are processed.

See also the [overall services structure](./intro.md#exposed-services).



## Caddy configuration

Caddy is a powerful and easy-to-use web server software, known for its automatic HTTPS features. 

We refer to the [official documentation](https://caddyserver.com/docs/install) on how to install Caddy, optionally [using docker-compose](https://caddyserver.com/docs/running#docker-compose).

A basic example configuration is proveded below.

```text title="Caddyfile"
meteoio.example.com {
        reverse_proxy http://<server>:8095
}

```
In the above example the placeholder `<server>` must be properly replaced to reach the application.

Both the main web application and the TDS are served by the same service container in the application docker-compose, which binds to the port 8095 on the host. (The port 80 is not used by the application as it may be used by other services on the same host, like Caddy itself).


### Custom base path

Depending on your specific deployment needs and network configuration, you may prefer to customize the base path of the main web application.
Still, the TDS must be served with `/thredds` as base path, so a dedicated host may be required.
That's why there are two hosts (one for the main web application and one for the TDS) in the example provided below: 

```text title="Caddyfile"
meteoio.example.com {
        redir /my_custom_base_path /my_custom_base_path/
        handle_path /my_custom_base_path/* {
                reverse_proxy http://<server>:8095
        }
}

tds.example.com {
        redir / /thredds/
        redir /thredds /thredds/
        handle /thredds/* {
                reverse_proxy http://<server>:8095
        }
}
```


### Single-node deployment

Depending on your deployment needs and network configuration, Caddy may be installed using docker-compose on the same host machine that runs the main MeteoIO Web Application.

In this case, it's possible to connect the Caddy container to the docker network of the MeteoIO web application and using the port 80 internally exposed by the `server` container as in the following configuration where `meteoio-web_default` refers to the network created by the docker-compose of the main MeteoIO web application:

```text title="Caddyfile"
meteoio.example.com {
        reverse_proxy http://server:80
}
```
The Caddyfile is roughtly the same as before, but, in this case:

* `server` is the name of the container and not a placeholder.
* The internal port 80 is used.


The following is a small variation of the docker-compose configuration provided in the [official documentation](https://caddyserver.com/docs/running#docker-compose) 
to allow accessing the docker network of the application.

```yaml title="caddy.docker-compose.yml" hl_lines="7 23 24"
version: "3.7"
services:
  caddy:
    image: caddy:2.7-alpine
    restart: unless-stopped
    networks:
      - meteoio-web_default
    ports:
      - "80:80"
      - "443:443"
      - "443:443/udp"
    volumes:
      - $PWD/Caddyfile:/etc/caddy/Caddyfile
      - caddy_data:/data
      - caddy_config:/config

volumes:
  caddy_data:
    external: true
  caddy_config:

networks:
  meteoio-web_default:
    external: true
```