---
title: Deployment
---

## 1. Clone the Repository

First, ensure that you have access to the repository containing the `docker-compose.yml` file and other related files.

```bash
git clone https://code.wsl.ch/snow-models/meteoio-webservice.git
cd meteoio-webservice
```

## 2. Create the .env File

Before you start your containers, you'll need to create a `.env` file in the same directory as
your `docker-compose.yml`.
This file will contain all the environment variables required by your application. After an edit, you have
to issue the following command in order to force it to reload the `.env` file (instead of keeping a cached
version of it): `docker compose up -d --force-recreate --build`.

Here's a starting point:

!!! warning inline end
    Remember never to commit your `.env` file to the repository to keep sensitive information private.


```ini title=".env"
auth_cookie_path="/api"
meteoio_platform__external_base_url="https://example.com/api"
sentry_dsn="https://xxxxxxxxxxxxxxxx@xxxxxxx.ingest.sentry.io/xxxxxxxx"
```

The following options can be configured in the .env file:

 * **meteoio_platform__external_base_url**: the root url of your installation as it will be seen by your users
 * **sentry_dsn**: your credentials for application monitoring with [Sentry](https://sentry.io/)
 * **meteoio_build_branch**: specify the git branch name or tag to use for the internal MeteoIO application. For example, `METEOIO_BUILD_BRANCH="MeteoIO-2.10.0"`
 * **meteoio_build_command**: specify the cmake command to configure the build system for the internal MeteoIO application. For example, `METEOIO_BUILD_COMMAND="cmake -D CMAKE_INSTALL_PREFIX:PATH=/root/usr"`. The installation prefix must be set according to the setting `meteoio_timeseries_executable`, which has a default value: `/root/usr/bin/meteoio_timeseries`. The default configuration include the NetCDF plugin. Please refer to [Compiling MeteoIO](https://meteoio.slf.ch/Compiling-MeteoIO/) for other build configuration options.
 * **enable_ogc_api**: set to True or False in order to enable/disable the [OGC WPS api](https://www.ogc.org/standard/wps/). For example, `ENABLE_OGC_API=False`
 * **meteoio_platform__ldap__url** and **meteoio_platform__ldap__base_dn**: configure the LDAP(s) server to use for user authentication. See details in the dedicated section below.

## 3. Configure optional services


??? "Login with OpenID Connect"

    The [OpenID Connect](https://openid.net/developers/how-connect-works/) client configuration is only required if you'd like to login with an OpenID Connect provider.
    
    Utilizing OpenID Connect login requires setting up your server to be publicly accessible. 
    This is due to the secure authentication mechanism, which involves a callback to your server.
    For this reason, `meteoio_platform__external_base_url` is also required to login with OpenID Connect.

    Multiple providers can be configured in the .env file.
    The following is an example configuration for two providers: ORCID iD and Microsoft:
    
    ```
    oic_providers__orcid__issuer="https://orcid.org/"
    oic_providers__orcid__client_id="<your client ID>"
    oic_providers__orcid__client_secret="<your client secret>"
    oic_providers__orcid__login_button_label="Login with ORCID iD"
    
    oic_providers__ms__issuer="https://sts.windows.net/<uuid of the tenant>"
    oic_providers__ms__client_id="<your client ID>"
    oic_providers__ms__client_secret="<your client secret>"
    oic_providers__ms__login_button_label="Login with Microsoft"
    ```
    
    The strings `orcid` and `ms` are keys of a dictionary that allows to configure any OpenID Connect provider with your preferred key.
    That key will be publicly visible and must be a simple and short alphanumeric string, max 6 characters.
    
    Please refer to the specific instructions of your OpenID Connect provider to get your client ID and secret.
     
    You probably want to skip this setup if you are setting up this software on your personal computer.
    In this case, it's best to continue using email/password login, and you can leave the configuration as provided in the example.


??? "Login with ORCiD"

    The [ORCiD](https://orcid.org/) client configuration is a specific case of the OpenID Connect configuration.
    
    In order to get your client ID and secret, you first need to create an ORCiD account 
    and then access the developer tools for the [Public API](https://info.orcid.org/documentation/features/public-api/) (free of charge).


??? "Login with Microsoft (Entra ID or Microsoft 365)"
 
    The client configuration for login via the [Microsoft identity platform](https://learn.microsoft.com/en-us/entra/identity-platform)
    is a specific case of the OpenID Connect configuration.
    
    Please refer to your tenant administrator for the registration of a **Web application** and 
    [enable it for ID tokens](https://learn.microsoft.com/en-us/entra/identity-platform/v2-protocols-oidc#enable-id-tokens).
    
    Please note that the correct issuer domain can be `https://sts.windows.net/<uuid of the tenant>` 
    or `https://login.microsoftonline.com/<uuid of the tenant>` depending on the token version.
    Recent setup instructions may indicate `login.microsoftonline.com` while the correct issues may still be `sts.windows.net`.
    
    If your login attempts fail with an Error message in the UI mentioning "provider info issuer mismatch", 
    you can look for more detailed log messages from the backend, like the following one:
    ```
    oic.exception.PyoidcError: provider info issuer mismatch 'https://login.microsoftonline.com/[...]/' != 'https://sts.windows.net/[...]/'
    ```
    The full error message is not reported in the UI for security reasons.
    If you see the above error message, you can try to change the issuer setting in your .env file.


??? "Login with LDAPs"

    The [LDAP](https://ldap.com/) configuration is only required if you'd like to Login with LDAP. When troubleshooting authentication problems, please
    note that LDAP queries will be visible in the [logs](dockerCheatSheet.md#docker-commands), for example `Attempting to authenticate user cn=bavaym,ou=slf_users,dc=slf,dc=ch`.
    Such queries are read from right to left with *cn* being the object Common Name (here a user login), *ou* being the Organizational Unit and
    *dc* being the Domain Component. For the provided example, this means from the *ch* Domain Component, find the *slf* Domain Component and then
    inside it, the Organizational Unit called *slf-users*. Then find the object that has a common name of *bavaym*.

    
    Configuration settings, to be written in the `.env` file:
    
    * `meteoio_platform__ldap__url`: For example, `meteoio_platform__ldap__url="ldap://ldap.example.com:389"`.
    * `meteoio_platform__ldap__base_dn`: For example, `meteoio_platform__ldap__base_dn="dc=example,dc=com"`.
    * `meteoio_platform__ldap__default_user_domain`: Optional default OU. If not specified, the user will be required to provide it in the username field with the syntax: `username@domain`.
    * `meteoio_platform__ldap__search_user`: Optional service account DN for user information search.
    * `meteoio_platform__ldap__search_user_password`: Service account password.
    * `meteoio_platform__ldap__use_search_user_after_login`: If True, user information will be searched using the service account; if False, user information will be searched using the login user itself. Set True if you are required to use the service account."
     
    The API under `/public/ldap` will be available only if LDAP is configured.
     
    You probably want to skip the LDAP setup if you are setting up this software on your personal computer.
    In this case, it's best to continue using email/password login, and you can leave the configuration as provided in the example.
   
??? "Reverse proxy with custom base URL"
    
    Depending on you deployment needs, you may have a reverse proxy in front of the web service.
    A reverse proxy may also introduce a custom base URL.
    If this is the case, then you should properly configure the base path in `meteoio_platform__external_base_url` and `auth_cookie_path`.
    
    Otherwise, the value "/api" or no value should both be good for a simple setup.
    You only need to configure `meteoio_platform__external_base_url` if you want to use ORCiD login; otherwise, a blank value is ok.

    Please see also how to [configure TLS termination behind a reverse proxy](proxy.md).

??? "Sentry monitoring"
     Depending on you monitoring needs, you can optionally configure
     [Sentry](https://sentry.io/) in `sentry_dsn` or leave it blank.


## 4. Launch the application

The first time you deploy the application you have to create a volume for data files:

```bash
docker volume create meteoio_data
```

Once the previous steps are done, you can start your services:

```bash
docker compose up -d
```

This command starts the services in detached mode.
Also, if your docker daemon is enabled, this should be sufficient to make the service start automatically at boot.

The `server` container is configured to bind to the TCP port **8095**.


```bash title="Possible output of 'docker compose ps'" hl_lines="9 10"
NAME                                             IMAGE                                          COMMAND                  SERVICE                     CREATED             STATUS              PORTS
meteoio-webservice-backend-1                     meteoio-webservice-backend                     "litestar --app pyme…"   backend                     3 minutes ago       Up 3 minutes        4200/tcp
meteoio-webservice-backend_ds_cron_scheduler-1   meteoio-webservice-backend_ds_cron_scheduler   "python -m pymeteoio…"   backend_ds_cron_scheduler   3 minutes ago       Up 3 minutes
meteoio-webservice-backend_worker-1              meteoio-webservice-backend_worker              "python -m pymeteoio…"   backend_worker              3 minutes ago       Up 3 minutes
meteoio-webservice-job_dispatcher-1              meteoio-webservice-job_dispatcher              "python -u -m pymete…"   job_dispatcher              3 minutes ago       Up 3 minutes        4222/tcp
meteoio-webservice-job_runner-1                  meteoio-webservice-job_runner                  "python -u -m pymete…"   job_runner                  3 minutes ago       Up 3 minutes        4222/tcp
meteoio-webservice-job_runner-2                  meteoio-webservice-job_runner                  "python -u -m pymete…"   job_runner                  3 minutes ago       Up 3 minutes        4222/tcp
meteoio-webservice-job_runner-3                  meteoio-webservice-job_runner                  "python -u -m pymete…"   job_runner                  3 minutes ago       Up 3 minutes        4222/tcp
meteoio-webservice-server-1                      meteoio-webservice-server                      "/docker-entrypoint.…"   server                      3 minutes ago       Up 3 minutes        0.0.0.0:8095->80/tcp
meteoio-webservice-sftp_server-1                 meteoio-webservice-sftp_server                 "python -u -m pymete…"   sftp_server                 3 minutes ago       Up 3 minutes        0.0.0.0:2200->22/tcp
meteoio-webservice-thredds-1                     meteoio-webservice-thredds                     "/usr/bin/dumb-init …"   thredds                     3 minutes ago       Up 3 minutes        8080/tcp, 8443/tcp
```

The highlighted services shall be publicly accessible. 
You may change the published ports in the `docker-compose.yml` file.
Please note that for a production setup you are probably going to have a reverse proxy for TLS termination.

If you are using **Docker Desktop**, then you may notice that a container named `backend_migrator` is not running:
that's ok, it's a one-shot command that shall exit after success.

To find more Docker commands, have a look at the [Docker Cheat Sheet](dockerCheatSheet.md) documentation.

## 5. Create an admin user

Please follow the instructions provided in the [Docker Cheat Sheet](dockerCheatSheet.md#entering-the-docker-container) documentation:

1. [Enter the CLI](dockerCheatSheet.md#entering-the-docker-container) in the `backend` docker container.
2. [Create a user](administration.md#creating-users) with email and password.
3. [Grant](administration.md#granting-admin-privileges) the admin privilege.

## 6. Other steps

Depending on the specifics of your deployment, you may need to perform additional configuration or setup tasks.
This could include:

- Configuring your firewall: ports 80 (will be redirected to https by the MeteoIO-webservice), 443 (web interface), 2200 (for sftp data upload), 22 (if you want to allow administrative access over ssh from the outside)
- [Configuring a reverse proxy with TLS termination](./proxy.md).
- Migrating data.
- Installing additional log management and monitoring tools.

Ensure that you have read any application-specific documentation or notes to understand these requirements.

## Conclusion

By following these steps, you should have the application up and running.
Always ensure that you keep the `.env` file secure.
Monitoring and regular maintenance are also vital to ensure smooth operations.