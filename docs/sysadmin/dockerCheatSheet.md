---
title: Docker Cheat Sheet
---

## <a name="docker-commands">Docker Commands</a>
Here is a brief overview of the most important Docker commands that are relied upon for administering the system.

* ??? "Getting images from [Docker Hub](https://hub.docker.com)"
      [Docker Hub](https://hub.docker.com) has many different images to choose from that are ready to use.
      * Search for an image `docker search <image_name>`
      * Pull an image: `docker pull <image_name>`
      * List local images: `docker images`
      * Delete an image: `docker image rm <image_name>`

* ??? "Running & troubleshooting containers"
      * Create and run a container from an image, with a custom name: `docker run --name <container_name> <image_name>`
      * To run the same image interactively, add the `-it` option. To run it in the background, add the `-d` option
      * To list currently running containers: `docker ps`
      * Fetch and follow the logs of a running container: `docker logs -f <container_name>` (skip the `-f` option in order not to follow the logs)
      * Fetch the logs from docker compose: `docker compose logs`
      * Open a shell inside a running container: `docker exec -it <container_name> bash`
      * Start or stop an existing container: `docker start|stop <container_name>` 
* ??? "Removing unused containers"
      * It is recommended to do it before and after every update: `docker system prune`


## Docker Tasks

Here are a list of typical sysadmin tasks on the system:

* If you ever need to stop the services:
  ```bash
  docker compose down
  ```

* <a name="updating-software">Updating the software</a>:
    1. ??? "Get the latest version"
           (or any other one, see git's [documentation](https://githowto.com/getting_old_versions)):
           ```bash
           git pull
           docker compose up -d --build
           ```
    2. ??? "Cleanup unused containers"
           (do it before and after every update):
           ```bash
           docker system prune
           ```

* <a name="testing-meteoio-options">Testing MeteoIO's compilation options</a>. If you want to customize the compilation of MeteoIO, you should first check how any potential dependency is called in the docker image that is used to run MeteoIO. Therefore, it is a good idea to manually test it on your computer:

    1. ??? "Get a runner docker container running on your system"
           * Make sure docker is installed on your computer and that your user has the required permissions (it usually needs to belong to the 'docker' group).
           * Pull the runner image: `docker image pull python:3.11-slim`
           * Start an interactive test container based on this image: `docker run --name test -it -d python:3.11-slim`
           * Enter into this container with a shell: `docker exec -it test bash`

    2. ??? "Setup the system, get MeteoIO"
           * Update and install the default dependencies: `apt update; apt install -y git g++ make cmake libnetcdf-dev libproj-dev curl`
           * Install some dependencies to make your life easier during testing: `apt install cmake-curses-gui`
           * Change directory to some location and clone MeteoIO from git: `cd /opt; git clone https://code.wsl.ch/snow-models/meteoio.git; cd meteoio`

    3. ??? "Configure & compile MeteoIO"
           * Run cmake and configure what you want: `ccmake .`. If you select a plugin that has external dependencies, you will probably get an error message (cmake can not find the dependency), so exit cmake, install the package of the dependency and try again.
           * Compile to make sure everything is correct: `make`
           * When you're done, exite the container `exit`, stop it `docker stop test` and remove it `docker rm test`. You can also remove the now unused image, by getting its ID with `docker images` and then removing it with `docker image rm {ID}`

    4. ??? "Cleanup"
           * Stop your test container `docker stop test`
           * Remove it `docker rm test`
           * You can also remove the now unused image, by getting its ID with `docker images` and then removing it with `docker image rm {ID}`

* <a name="entering-the-docker-container">Entering the running docker container</a> and launching the provided CLI. This is required for some administrative tasks. 
    1. Use only the commands provided by the CLI, if you edit arbitrary files inside the container, your edits may not be persistant.
    2. ??? "Open a terminal on the backend container" 
           and open a python shell with super-admin privileges in the application:
           ```bash
           docker compose exec -it backend python -m pymeteoio_platform.app.cli
           ```
    3. ??? "Print some usage information"
           The following command will print some usage information for users management:
           ```py
           help(em.users.cli)
           ```
