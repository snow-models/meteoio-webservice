---
title: System Overview
---

This system relies on the [MeteoIO](https://meteoio.slf.ch) pre-processing library to process the provided input data according to a configuration file provided by the [data owner](../data_owners/data_owners_role.md). Each processing job runs in its own [Docker container](https://www.docker.com/resources/what-container/) for isolation purposes. The data owner and data users interact with the system through a web-base user interface that is also containerized (for ease of deployment). Therefore, this system is a multi-container application managed by Docker-compose. Docker-compose configurations are provided for evaluation and/or production deployments.

<figure markdown>
  ![Docker structure](../img/docker_structure.svg){ width="600" }
  <figcaption>Overview of the Docker structure of the application</figcaption>
</figure>

## Docker compose

Docker Compose is a tool for defining and running multi-container Docker applications. With Compose, you can use
a `docker-compose.yml` file to configure your application's services, networks, and volumes. This makes it easy to
deploy complex applications with multiple interdependent containers.

The [deployment guide](./deployment.md) provides a brief overview of the steps required to deploy this software using Docker Compose. It also
highlights the necessity of creating a `.env` file to store environment variables that might be needed by the containers for your specific deployment configuration.

### Prerequisites

For best compatibility and ease of installation, you need an up-to-date version of docker that supports the
version 2.4 of docker-compose specification files, which requires Docker Engine version 17.06.0+ (on the deployment machine).
Deployment without docker is possible but not advised (other than for testing): safely running MeteoIO jobs from guest users requires the isolation mechanisms provided by docker containers.


### Exposed services

The MeteoIO Web Application provides two main services that shall be exposed for incoming traffic:

* A docker container named `server` exposes the port 80 to the internal docker network and binds to the port 8095 of the host. This container serves HTTP requests for the MeteoIO Web Application, including the static resources of the web-based GUI, the API endpoints, and the TDS. It does not bind to the port 80 as it may be used by other services on the same host machine.
  This service should be published behind a [reverse proxy](./proxy.md) for TLS termination.
* A docker container named `sftp_server` exposes the port 22 to the internal docker network and binds to the port 2200 of the host. This container listens for TCP connections for SFTP data access.
  This service implements TLS and may require a reverse proxy only in some circumstances (e.g. if deployed in a different host machine). Both Caddy and Nginx could be configured for this purpose.

The docker-compose configuration is provided so that the rest of the docker containers are protected as they shall not receive untrusted traffic.


### Container dependencies

Some containers are required for the functionality of other containers. From the bottom up: 

* `backend_migrator` is responsible for database migrations automatically performed at startup in case of a software update.
* `job_dispatcher` is responsible for:
     * accepting connections from `backend_*` containers to receive requests to run MeteoIO jobs.
     * accepting connections from `job_runner` containers (scalable to multiple instances) to receive availability to delegate the isolated execution of MeteoIO jobs.
* `thredds` and `backend` are responsible for implementing the HTTP endpoints served by the `server` container.
* `sftp_server` is responsible for serving SSH connections with application-defined access control to the persistent data.


<figure markdown>
  ![Docker dependencies](../img/docker_compose.svg){ width="650" }
  <figcaption>Docker-composer containers graph of dependencies</figcaption>
</figure>


### Data persistence

In order to persist the application data, the docker volume `meteoio_data` is used by the following containers:
`backend`, `backend_worker`, `backend_ds_cron_scheduler`, `backend_migrator`, `sftp_server`, `thredds`.



## HTTP end points

The `server` container responds to HTTP requests for the following paths:

* A [THREDDS data server](https://www.unidata.ucar.edu/software/tds/) is provided by default in order to serve public datasets. It is accessed at `{base_url}/thredds`, for example `https://service-meteoio.slf.ch/thredds`.
* The backend API at `{base_url}/api`, specifically developed for this application, secured for public or authenticated access.
* The [OGC WPS API](https://www.ogc.org/standard/wps/) at `<base_url>/api/public/ogc/wps`, detailed in the [developer documentation](../dev/ogc_wps_api.md).
* Static files of this documentation at `{base_url}/docs`
* Static files of the frontend (a React single-page application) at `{base_url}/`

See also the [Deployment behind a reverse proxy](proxy.md), 
which allows customization of the `{base_url}` for the main web application.