---
title: Administration
---

## Creating users

After successfuly [opening a python shell](dockerCheatSheet.md#entering-the-docker-container) with super-admin privileges in the application, run the following to add a user with email and password:

```py
uid = em.users.cli.add_with_email("user@example.com")
em.users.cli.set_password(uid, <your secret password here>)
```

## Granting Admin privileges

After successfuly [opening a python shell](dockerCheatSheet.md#entering-the-docker-container) with super-admin privileges in the application, for a user that has already been created and for whom you have the _uid_ :

```py
em.users.cli.set_is_admin(uid, True)
```

## Updating the software
Please see [updating the software](dockerCheatSheet.md#updating-software) in the [dockerCheatSheet](dockerCheatSheet.md).

## Updating MeteoIO
In order to update the embedded version of [MeteoIO](https://meteoio.slf.ch), tweak the following variables in the `.env` file (see its [documentation](deployment.md)):

 * METEOIO_BUILD_BRANCH: this can point to a branch or a tag;
 * METEOIO_BUILD_COMMAND: you can chain mutliple command, such as the installation of additional dependencies with apt and running cmake with the appropriate compilation flags
