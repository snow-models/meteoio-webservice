---
title: Principle
---

The MeteoIO web-service is a web application based on the [MeteoIO](https://meteoio.slf.ch) pre-processing library to transform timeseries input data according to a configuration file that describes all operations. This is used to make the input data FAIR as well as apply filtering and corrections to the data.

<figure markdown>
  ![Principles](../img/principles_anim.svg){ width="400", .center }
</figure>


The configuration file is human readable and documents everything that is needed to read the original data, interpret it, filter it, correct it and convert it to a standard file format with standardized metadata. The web-service provides a user-friendly web interface to manage the datasets and share it either directly with data users or with data portals in a fully automated way. Although it has historically been designed for Automatic Weather Stations data, it works for many more data timeseries!

_If you haven't done it yet, please read the [original problem](use_case.md) that lead us to develop this solution!_