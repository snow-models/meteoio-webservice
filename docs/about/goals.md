---
title: Design goals
---

The system is made of two major components: the [MeteoIO](https://meteoio.slf.ch) pre-processing library and the web application built around it. Both have their own design goals.

# MeteoIO

Although MeteoIO was originally built for numerical models consuming meteorological data, it has significantly expanded since then. To its orignal goals, new goals hve been added as its application scope has been expanded.

## Unified data access

Originally, MeteoIO was designed in order to transparently give access to data in different formats (such as CSV or NetCDF) or using different protocols (databases, web services for example).


## Reproducibility

Because all data transformations are described in the configuration file, it is possible to regenerate the output dataset at any point in time, for any time period that is covered by the raw data. This is also important for open ended datasets (when a station is still in operation and new data come automatically) as it allows the dataset to grow. The [Data Owners](../data_owners/data_owners_role.md) are then responsible for adding more configuration in the configuration file when something changes in the input raw data (such as a file format change) or when something changes at the measuring station (such as the addition of a new sensor or a malfunctioning sensor whose data must be invalidated). This allows to describe the whole history of a given station in the INI file. Because this is a high level description (contrary to a source code that would implement such transformations), it should be easily understandable in several decades, even without MeteoIO itself.

## From raw data to high quality dataset

The transformations that are available in MeteoIO allow to describe the full transformation of a raw dataset (such as coming out of a station's data logger) all the way to a high quality dataset that could be published or fed into a numerical model. However, data harmonization is not specifically covered (although methods such as quantile mapping are available).

# MeteoIO web-service

As MeteoIO was getting used more and more for data management tasks, it was needed to offer a data management interface for it so the actors of the data lifecycle could benefit from it.

## Data delivery

The [Data Users](../data_users/intro.md) looking for data should be able to find what is available and get the data by themselves. The data should be well documented so in most cases, there would be no need for interaction with the Data Owners (FAIR data that does not forget the "I" and the "R").

## Interface and services for Data Owners

The system needs to be transparent to use for the [Data Owners](../data_owners/data_owners_role.md) so they can be supported in their data management tasks. This includes providing the data to potential users and detecting when something does not work as planned. Such a system empowers the Data Owners to take ownership of their data management tasks and strives to reduce the necessary workload.

## Interoperability with data portals

The data that is handled by the system must be automatically discoverable and usable by data portals (machine to machine). Technologies such as [OGC WPS API](https://www.ogc.org/standard/wps/), [THREDDS data server](https://www.unidata.ucar.edu/software/tds/), [OPeNDAP server](https://www.opendap.org/), NetCDF [CF Convention](https://cfconventions.org/) and [ACDD](https://wiki.esipfed.org/Attribute_Convention_for_Data_Discovery_1-3) search metadata are transparently integrated for this purpose.

