---
title: Use case
---

As users of data provided by Automatic Weather Stations (AWS) for numerical modeling and long term monitoring as well as producers of such data, we have identified key weaknesses in the current approaches at multiple research institutions. We have also developed a vision of how this could be improved. Please don't interpret the following points as criticism of the actors of the AWS data collection, but more as an encouragement to make the system better and more enjoyable for all.

## Weaknesses in current approaches

After years of struggling to better manage research Automatic Weather Stations' data, the following patterns emerged:

 * In research, it is easier to setup a new AWS than monitoring the proper operation of existing ones;
 * Many data problems went unnoticed for months or years;
 * Often nobody takes ownership of a given AWS:
      * The researchers who have taken the decision to set it up don't monitor its proper operation (it is too time consuming);
      * the technical team who does the deployments and maintenance does not check the quality of the data that is produced (they don't necessarily trust themselves to judge of the quality of the data).
 * The metadata that is necessary to use the data (such as maintenance operations, file format descriptions, notices of file format changes or sensor changes) are either not recorded or not in any relevant way for the data users (for example, file format changes are usually not documented)[^2];
 * as a consequence, lots of collected data is never going to be used. The time required to understand the data, convert it to a usable format and clean it up is simply overwhelming;
 * This also makes data sharing a mostly formal requirement: the data is shared but not necessarily findable and often not interoperable nor reusable.


The later point was also shown in a survey of cryospheric data conducted for ([Bavay et al., 2020](https://doi.org/10.5334/dsj-2020-006))[^1]: standards are almost never used and using third party data takes much more time than it should ([Fig. 1](#_figure-1)).

<figure markdown>
  ![Principles](../img/data_usage_survey.svg){ width="600", .center }
  <figcaption>Fig. 1: Data reading means and time data users spend on average for reading third party data, from (Bavay et al., 2020)</figcaption>
</figure>


## Our vision

We believe that this sad state of affairs can significantly be improved by providing technical tools in order to:

 * better enforce the various roles that are necessary for a successful, long term AWS operation;
 * make the monitoring of the proper operation of AWS significantly less time intensive;
 * make it easy to convert data as it comes from an AWS data logger truly FAIR;
 * make data sharing very easy through standard file formats and protocols



[^1]: Bavay, Mathias, Joel Fiddes, and Øystein Godøy. _"Automatic Data Standardization for the Global Cryosphere Watch Data Portal."_, Data Science Journal 19 (2020), 6-6, [10.5334/dsj-2020-006](https://doi.org/10.5334/dsj-2020-006).

[^2]: The technical teams doing the daily work to keep the stations running do a fantastic job. They usually painstakingly collect loads of metadata in paper notebooks. However, looking back at decade-old data, it turns out that the metadata they collected does not fully match the needs of a data user wishing to read, interpret and use the AWS data decades later.