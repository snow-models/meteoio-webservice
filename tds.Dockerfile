FROM ghcr.io/metno/tds:metno

# https://docs.unidata.ucar.edu/tds/current/userguide/tds_content_directory.html

COPY --chown=nobody:nobody --chmod=750 ./thredds/catalog.xml /usr/local/tomcat/content/thredds/catalog.xml
COPY --chown=nobody:nobody --chmod=750 ./thredds/threddsConfig.xml /usr/local/tomcat/content/thredds/threddsConfig.xml
