# MeteoIO Web Application

## Setup

This software is provided as a multi-container application.
Docker-compose configurations are provided for evaluation and/or production deployments.

#### Requirements

For best compatibility and ease of installation you need an up-to-date version of docker that supports the
version 2.4 of docker-compose specification files, which requires Docker Engine version 17.06.0+ (check your version with `docker version`).
Deployment without docker is possible even if not advised:
safely running MeteoIO jobs from guest users requires the isolation mechanisms provided by docker containers.

#### Deployment commands

You may first clone the code repository and then set up the containers using the following commands [^1]:

```shell
git clone https://code.wsl.ch/snow-models/meteoio-webservice.git
cd meteoio-webservice
docker volume create meteoio_data
docker compose up -d --build
```

Software updates can be deployed with the following commands:

```shell
git pull
docker compose up -d --build
#in order to force re-reading the .env file, do instead:
docker compose up -d --build --force-recreate 
```

[^1]: If you get an error message such as `unknown shorthand flag: 'd' in -d`, then it means that your distribution provides a package
for docker and a separate one for docker-compose. Then simply also install the `docker-compose` package and try again.

---

## Credits

This software has received funding from the
[World Meteorological Organization](https://public.wmo.int/en)
under grant agreement No. 29539/2022-1.9
as well as the European Union’s
[Horizon 2020](https://commission.europa.eu/funding-tenders/find-funding/eu-funding-programmes/horizon-europe_en)
research and innovation programme under [grant agreement No. 101003472](https://arcticpassion.eu/).