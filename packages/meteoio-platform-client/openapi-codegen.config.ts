import {generateReactQueryComponents, generateSchemaTypes,} from "@openapi-codegen/typescript";
import {defineConfig} from "@openapi-codegen/cli";

export default defineConfig({
  backend: {
    from: {
      relativePath: "../../server/pymeteoio_platform/app/openapi_schema.json",
      source: "file",
    },
    outputDir: "./generated",
    to: async (context) => {
      const filenamePrefix = "backend";
      const {schemasFiles} = await generateSchemaTypes(context, {
        filenamePrefix,
      });
      await generateReactQueryComponents(context, {
        filenamePrefix,
        schemasFiles,
      });
    },
  },
});
