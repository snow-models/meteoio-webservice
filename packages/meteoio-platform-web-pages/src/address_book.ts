/** UI components can refer to this file to build the path of the routes */
export namespace address_book {
    export const home = '/'

    export const after_logout = '/after_logout'

    export namespace jobs {
        export const list = '/jobs/'
        export const create = '/jobs/new'
        export const view = jobId => `/jobs/${jobId}`
        export const view_file = (jobId: string, filePath: string) => `/jobs/${jobId}/file/${filePath}`
        export const view_tab = (jobId, tab: 'output' | 'files' | 'logs' | string) => `/jobs/${jobId}/${tab}`
    }

    export namespace datasets {
        export const list = '/datasets/'
        export const create = '/datasets/new'
        export const view = datasetId => `/datasets/${datasetId}/`

        export const tab = (datasetId, tabValue: 'ini' | 'output' | 'input' | 'cron' | 'logs' | 'settings' | string) =>
            `/datasets/${datasetId}/${tabValue}/`

        export const input_path = (datasetId, path: string) =>
            `/datasets/${datasetId}/input/${path.startsWith('/') ? path.slice(1) : path}`

        export const cron = (datasetId, cron_job_id?: string) =>
            `/datasets/${datasetId}/cron/${cron_job_id ?? ''}`

        export const new_guest_job = (datasetId, ini_name?: string) =>
            `/datasets/${datasetId}/new_guest_job/${ini_name ?? ''}`

        export namespace ini {
            export const view = (datasetId) => `/datasets/${datasetId}/ini/`
            export const view_one = (datasetId, iniName) => `/datasets/${datasetId}/ini/files/${iniName}`

            // export const view_file = (datasetId, iniName) => `/datasets/${datasetId}/ini/file/${iniName}`  // TODO?

            export namespace revisions {
                export const list = datasetId => `/datasets/${datasetId}/ini/revisions/`
                export const create = datasetId => `/datasets/${datasetId}/ini/revisions/new`
                export const view = (datasetId, revId) => `/datasets/${datasetId}/ini/revisions/${revId}`
            }
        }

        export namespace settings {
            export const heading = datasetId => `/datasets/${datasetId}/settings/`
            export const publication = datasetId => `/datasets/${datasetId}/settings/publication`

            export const deletion = datasetId => `/datasets/${datasetId}/settings/deletion`
        }
    }

    export namespace explore {
        export const list = '/explore'
        export const view = datasetId => `/explore/${datasetId}`
    }
}