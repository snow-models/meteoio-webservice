import * as React from 'react'
import {lazy, Suspense} from "react";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {makeStyles, mergeClasses, Text, tokens} from "@fluentui/react-components";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {LoginBtn} from "./user/LoginBtn";
import {MainTopBar} from "./_common/MainTopBar";
import {MainNav} from "./_common/MainNav";

// @ts-ignore
import backgroundImageUrl from './abstract_snow_light.jpg'
import {CreditsText} from "./_common/CreditsText";
import {useAppCustomizationSettings} from "./_common/AppCustomizationSettingsProvider";
import {SimpleFormattingMarkdownView} from "meteoio-ui/src/components/SimpleFormattingMarkdownView";

const DatasetsListPage = lazy(async () => ({default: (await import("./datasets/DatasetsListPage")).DatasetsListPage}));
const JobSubmissionPage = lazy(async () => ({default: (await import("./jobs/JobSubmissionPage")).JobSubmissionPage}));


const useStyles = makeStyles({
    title: {
        textTransform: 'uppercase',
        fontWeight: 100,
        fontSize: tokens.fontSizeHero900,
        color: tokens.colorBrandForeground2,
        lineHeight: tokens.lineHeightHero800,
        // maxWidth: '300px',
        whiteSpace: 'pre-wrap'
        // display: 'block',
    },
    title_mobile: {
        fontSize: tokens.fontSizeHero800,
        lineHeight: tokens.lineHeightHero700,
    },
    subtitle: {
        marginTop: tokens.spacingVerticalM,
        textTransform: 'uppercase',
        fontWeight: 300,
        fontSize: tokens.fontSizeBase400,
        color: tokens.colorNeutralForeground4,
        lineHeight: tokens.lineHeightBase400,
        // maxWidth: '300px',
        whiteSpace: 'pre-wrap'
    },
    intro: {
        fontSize: tokens.fontSizeBase300,
        lineHeight: tokens.lineHeightBase300,
        color: tokens.colorNeutralForeground2,
        // whiteSpace: 'pre-wrap'  // let Markdown do the job
    }
})

export const HomePage: React.FC = () => {
    const {
        configuration,
        isLoading: isSettingsLoading,
    } = useAppCustomizationSettings()

    const {isMobile, isLarge} = useScreenSize()

    const styles = useStyles()

    // if (isLoading) {  NOTE: uncomment this to cause re-rendering loops.
    //     return <Spinner tall/>
    // }

    // NOTE: There hare 3 cases:
    //       - user has NOT logged in
    //       - user has logged in and CANNOT create datasets
    //       - user has logged in and CAN create datasets

    const nav = <>
        {/*<Text block size={400} weight="bold" style={{marginBottom: -10}}>Links</Text>*/}
        <div style={isLarge ? {position: 'fixed', top: 45, left: 20, /*borderRight: '1px solid #7772'*/} : undefined}>
            <MainNav variant={isLarge ? 'main' : 'box'} noInlineMargin/>
        </div>
    </>

    return <>
        <MainTopBar/>
        <div style={{
            // marginRight: `calc(-1 * ${tokens.spacingHorizontalL})`,
            background: `url(${backgroundImageUrl}) no-repeat`,
            backgroundPosition: isMobile ? 'top right' : 'top right',
            backgroundSize: 'contain',
            minHeight: 'calc(95vh - 40px)',
        }}>
            <Stack rowGap="XL" center width={600} maxWidth="85vw">
                <br/>
                <Stack horizontal columnGap="XXXL" justifyContent="space-between">
                    <h1>
                        <Text block className={mergeClasses(styles.title, isMobile && styles.title_mobile)}>
                            {configuration?.settings?.app_long_title}
                        </Text>
                        <Text block className={styles.subtitle}>
                            {configuration?.settings?.app_subtitle}
                        </Text>
                    </h1>
                    {!isMobile && <div className={styles.intro}>
                        <br/>
                        {nav}
                    </div>}
                </Stack>

                {isSettingsLoading && <Spinner tall/>}

                {configuration?.general_access_level === 'guest' && <>
                    <div className={styles.intro}>
                        <SimpleFormattingMarkdownView source={configuration?.welcome}/>
                    </div>
                    <Stack horizontal justifyContent="left" wrap>
                        <LoginBtn/>
                    </Stack>
                    {isMobile && nav}
                    <br/>
                    <CreditsText/>
                    <br/>
                </>}

                {configuration?.general_access_level === 'authenticated' && <>
                    <div className={styles.intro} style={{marginBottom: -30}}>
                        <SimpleFormattingMarkdownView source={configuration?.welcome}/>
                        {isMobile && <><br/>{nav}</>}
                    </div>
                    <Suspense fallback={<Spinner tall/>}>
                        <JobSubmissionPage/>
                    </Suspense>
                    <br/>
                    <br/>
                    <CreditsText/>
                    <br/>
                </>}

                {configuration?.general_access_level === 'data_owner' && <>
                    <div className={styles.intro}>
                        <SimpleFormattingMarkdownView source={configuration?.welcome}/>
                        <br/>
                        {isMobile && nav}
                    </div>
                    <Suspense fallback={<Spinner tall/>}>
                        <DatasetsListPage/>
                    </Suspense>
                    <br/>
                    <br/>
                    <CreditsText/>
                    <br/>
                </>}

                <br/>
            </Stack>
        </div>
    </>
}