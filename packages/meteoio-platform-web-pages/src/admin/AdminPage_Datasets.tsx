import * as React from 'react'
import {useState} from 'react'
import {
    DatasetListEntry,
    fetchAdminDatasetsDatasetIdDeleteDataset,
    fetchAdminDatasetsDatasetIdOwnerTransferTransferDataset,
    useAdminDatasetsListDatasets,
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Column, SortableTable} from "meteoio-ui/src/components/SortableTable";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Pagination} from "antd";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {Modal} from "meteoio-ui/src/components/Modal";
import {Button} from "meteoio-ui/src/components/Button";
import {ArrowClockwiseRegular, DeleteRegular, PersonArrowRightRegular} from '@fluentui/react-icons'
import {mergeClasses, Text} from "@fluentui/react-components";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {useJobViewStyles} from "../jobs/job/styles";
import {RepositoryAccessPill} from "meteoio-ui/src/components/RepositoryAccessPill";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {useQueryClient} from "../_common/backend";
import {FreeTextField} from "meteoio-ui/src/components/FreeTextField";

const COLUMNS: Column<DatasetListEntry>[] = [
    {
        field: 'owner_user_id',
        title: 'Owner',
        onRenderCellContent: row => <code style={{
            lineHeight: '0.9em',
            fontSize: '0.8em',
            display: 'block',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
        }}>
            {row.owner_user_id}
        </code>,
    },
    {
        field: 'id',
        title: 'Dataset ID',
        onRenderCellContent: row => <code style={{
            lineHeight: '0.9em',
            fontSize: '0.8em',
            display: 'block',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
        }}>
            {row.id}
        </code>,
    },
    {
        title: 'Title',
        onRenderCellContent: row => row.heading && row.heading.title,
    },
    {
        field: 'access',
        title: 'Access',
        onRenderCellContent: row => <RepositoryAccessPill access={row.access}/>,
    },
]

export const AdminPage_Datasets: React.FC = () => {
    const [selectedRow, setSelectedRow] = useState<DatasetListEntry | undefined>(undefined)
    const [limit, setLimit] = useState<number>(10);
    const [offset, setOffset] = useState<number>(0)
    const {
        data,
        isLoading,
        isFetching,
        error,
        refetch,
    } = useAdminDatasetsListDatasets({queryParams: {offset, limit}})

    const [newOwnerIdStr, setNewOwnerIdStr] = useState<string>(undefined)

    const styles = useJobViewStyles()

    const dsId = selectedRow ? selectedRow?.id ?? undefined : undefined

    const queryClient = useQueryClient()

    const dialogs = useDialogs()

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    if (isLoading) {
        return <Spinner tall/>
    }
    return <>
        <Stack rowGap="XL">
            <Stack horizontal columnGap="L">
                <PageTitle>Datasets</PageTitle>
                <Button
                    appearance="subtle"
                    icon={<ArrowClockwiseRegular/>}
                    onClick={() => refetch().then()}
                />
            </Stack>
            <Stack rowGap="S">
                <ProgressBar hide={!isFetching}/>
                <SortableTable
                    columns={COLUMNS}
                    rows={data.items}
                    missingPlaceholder={<span style={{opacity: 0.5}}>&#823;</span>}
                    onRowClick={row => {
                        setNewOwnerIdStr(undefined)
                        setSelectedRow(row)
                    }}
                />
            </Stack>
            <Pagination
                current={1 + Math.floor(offset / limit)}
                total={data.total}
                pageSize={limit}
                responsive
                hideOnSinglePage
                showSizeChanger
                onChange={(page, pageSize) => {
                    setOffset((page - 1) * pageSize)
                    setLimit(pageSize)
                    document?.body?.scrollTo?.({top: 0, behavior: 'smooth'})
                }}
            />
            <br/>
        </Stack>
        <Modal
            open={!!selectedRow}
            closable
            onCancel={() => {
                setSelectedRow(undefined)
                setNewOwnerIdStr(undefined)
            }}
            footer={null}
        >
            {selectedRow && dsId && <>
                <Stack rowGap="XL">
                    <Stack horizontal columnGap="XXXL">
                        <PageSectionTitle>Dataset details</PageSectionTitle>
                    </Stack>
                    <Text size={200} className={mergeClasses(styles.boxBorder, styles.statusInfo)}>
                        Owner ID: <code>{selectedRow?.owner_user_id}</code><br/>
                        Dataset ID: <code>{selectedRow?.id}</code><br/>
                        Access: <RepositoryAccessPill access={selectedRow?.access}/><br/>
                        Title: {selectedRow?.heading?.title}<br/>
                        Description: {selectedRow?.heading?.description}<br/>
                    </Text>

                    <div className={mergeClasses(styles.boxBorder, styles.statusInfo)}>
                        {/* newOwnerIdStr === undefined   -> transfer intent not initiated */}
                        {/* newOwnerIdStr === ''          -> transfer intent just initiated */}
                        {/* !!newOwnerIdStr               -> has some user id for transfer */}
                        <Stack horizontal alignItems="end">
                            {newOwnerIdStr === undefined && <Button
                                size="small"
                                danger
                                label="Transfer ownership"
                                icon={<PersonArrowRightRegular/>}
                                appearance="subtle"
                                // secondaryLine="Remove entry and all associated data"
                                onClick={async () => {
                                    setNewOwnerIdStr('')
                                }}
                            />}
                            {newOwnerIdStr !== undefined && <Stack grow>
                                <FreeTextField
                                    label="New owner user ID"
                                    fieldProps={{size: 'small'}}
                                    value={newOwnerIdStr}
                                    onChange={setNewOwnerIdStr}
                                    smallCode
                                />
                            </Stack>}
                            {newOwnerIdStr && <AsyncActionButton
                                size="medium"
                                danger
                                label="Transfer"
                                icon={<PersonArrowRightRegular/>}
                                appearance="secondary"
                                disabled={newOwnerIdStr?.trim?.() === selectedRow.owner_user_id}
                                // secondaryLine="Remove entry and all associated data"
                                onClick={async () => {
                                    if (!await dialogs.confirm('Are you sure you want to transfer the dataset to the specified user?', {
                                        okType: 'danger',
                                        okText: 'Transfer'
                                    })) {
                                        return
                                    }
                                    await fetchAdminDatasetsDatasetIdOwnerTransferTransferDataset({
                                        pathParams: {datasetId: dsId},
                                        queryParams: {
                                            old_owner_user_id: selectedRow.owner_user_id,
                                            new_owner_user_id: newOwnerIdStr.trim(),
                                        }
                                    })
                                    dialogs.message.success('Dataset transferred successfully.')
                                    await queryClient.invalidateQueries()
                                    setNewOwnerIdStr(undefined)
                                    setSelectedRow(undefined)
                                }}
                            />}
                        </Stack>
                        {newOwnerIdStr?.trim?.() === selectedRow.owner_user_id && <Text block size={200}>
                            The specified user ID matches the current owner user ID.
                        </Text>}
                        {newOwnerIdStr !== undefined && <Text block size={200}>
                            Please find the user ID of the new owner in the users administration page.
                        </Text>}
                    </div>

                    <div className={mergeClasses(styles.boxBorder, styles.statusInfo)}>
                        <AsyncActionButton
                            size="small"
                            danger
                            icon={<DeleteRegular/>}
                            label="Delete Dataset"
                            appearance="subtle"
                            // secondaryLine="Remove entry and all associated data"
                            onClick={async () => {
                                dialogs.message.warning('You are going to permanently delete the dataset.', 5)
                                await new Promise(resolve => setTimeout(resolve, 2000))
                                if (!await dialogs.confirm('Are you sure you want to permanently delete this dataset? This action cannot be undone.', {
                                    okType: 'danger',
                                    okText: 'Delete'
                                })) {
                                    return
                                }
                                await fetchAdminDatasetsDatasetIdDeleteDataset({
                                    pathParams: {datasetId: dsId},
                                    queryParams: {owner_user_id_for_check: selectedRow.owner_user_id}
                                })
                                dialogs.message.success('Dataset deleted successfully.')
                                await queryClient.invalidateQueries()
                                setNewOwnerIdStr(undefined)
                                setSelectedRow(undefined)
                            }}
                        />
                    </div>
                </Stack>
            </>}
        </Modal>
    </>
}
