import * as React from 'react'
import {useEffect, useMemo, useState} from 'react'
import {
    fetchAdminUsersEditUserIdCanCreateDatasetsResetCanCreateDatasets,
    fetchAdminUsersEditUserIdCanCreateDatasetsSetCanCreateDatasets,
    useAdminUsersListUsers,
    usePublicMeGetMe,
    UserOidReflection,
    UserReflection
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Column, SortableTable} from "meteoio-ui/src/components/SortableTable";
import {Pagination, Switch} from 'antd'
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {ORCIDiD_IconVector} from "meteoio-ui/src/components/ORCIDiD_IconVector";
import {Link} from "@fluentui/react-components";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {Modal} from 'meteoio-ui/src/components/Modal'
import {FreeTextField} from "meteoio-ui/src/components/FreeTextField";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {Button} from "meteoio-ui/src/components/Button";
import {ArrowClockwiseRegular} from "@fluentui/react-icons";
import {IssSub, useOrcidFullNames} from "../_common/orcid_user_data";
import {useQueryClient} from "../_common/backend";

const COLUMNS: Column<UserReflection>[] = [
    {
        field: 'full_name',
        title: 'Name',
    },
    {
        field: 'email',
        title: 'Email',
    },
    // {
    //     field: 'has_password',
    //     title: 'Has password',
    //     onRenderCellContent: row => row.has_password ? <>&#10003;</> : <>&#8709;</>,
    // },
    /*{
        title: 'Credentials',
        onRenderCellContent: row => <Stack horizontal columnGap="XXXL" wrap alignItems="center">
            {row.has_password && row.email && <>
                <Stack horizontal columnGap="XS" alignItems="center">
                    <KeyRegular fontSize="1.3em"/>
                    email/password
                </Stack>
            </>}
            {row.openid?.map?.(oid => ['https://sandbox.orcid.org/', 'https://orcid.org/'].includes(oid.issuer)
                ? <Stack horizontal columnGap="XS" alignItems="center">
                    <span style={{textAlign: 'right', fontSize: '1.35em'}}>
                        <ORCIDiD_IconVector/>
                    </span>
                    <Link href={`${oid.issuer}${oid.subject}`} style={{fontSize: '0.9em'}} target="_blank">
                        {oid.subject}
                    </Link>
                </Stack>
                : <span>{oid.issuer} {oid.subject}</span>)}
        </Stack>
    },*/
    {
        field: 'id',
        title: 'ID',
        onRenderCellContent: row => <code style={{
            lineHeight: '0.9em',
            fontSize: '0.8em',
            display: 'block',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
        }}>
            {row.id}
        </code>,
    },
]

const OID_COLUMNS: Column<UserOidReflection>[] = [
    {
        field: 'issuer',
        title: 'OpenID issuer',
        onRenderCellContent: row => ['https://sandbox.orcid.org/', 'https://orcid.org/'].includes(row.issuer)
            ? <Stack horizontal columnGap="S" alignItems="center">
                    <span style={{textAlign: 'right', fontSize: '1.4em'}}>
                        <ORCIDiD_IconVector/>
                    </span>
                <span>{row.issuer}</span>
            </Stack>
            : row.issuer
    },
    {
        field: 'subject',
        onRenderCellContent: row => ['https://sandbox.orcid.org/', 'https://orcid.org/'].includes(row.issuer)
            ? <Link href={`${row.issuer}${row.subject}`} target="_blank">
                {row.subject}
            </Link>
            : row.subject
    },
]

export const AdminPage_Users: React.FC = () => {
    const [selectedUser, setSelectedUser] = useState<UserReflection | undefined>(undefined)
    const [limit, setLimit] = useState<number>(10);
    const [offset, setOffset] = useState<number>(0)
    const {
        data,
        isLoading,
        error,
        isFetching,
        refetch,
    } = useAdminUsersListUsers({
        queryParams: {offset, limit}
    })

    const _iss_sub_batch_ = useMemo<IssSub[]>(() =>
        data?.items?.flatMap?.(row => row?.openid), [data])
    const {getFullName: getOrcidFullName, loadProgressive: _orcidFNLoadProg} = useOrcidFullNames(_iss_sub_batch_)

    const enriched_users = useMemo<UserReflection[] | undefined>(() =>
            data?.items?.map?.(row => ({
                ...row,
                full_name: row.full_name ?? (row?.openid?.[0] ? getOrcidFullName(row?.openid?.[0]) : undefined)
            }))
        , [data, _orcidFNLoadProg])

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    if (isLoading) {
        return <Spinner tall/>
    }

    const pagination = <Pagination
        current={1 + Math.floor(offset / limit)}
        total={data.total}
        pageSize={limit}
        responsive
        hideOnSinglePage
        showSizeChanger
        onChange={(page, pageSize) => {
            setOffset((page - 1) * pageSize)
            setLimit(pageSize)
            document?.body?.scrollTo?.({top: 0, behavior: 'smooth'})
        }}
    />

    return <>
        <Stack rowGap="XL">
            <Stack horizontal columnGap="L">
                <PageTitle>Users</PageTitle>
                <Button
                    appearance="subtle"
                    icon={<ArrowClockwiseRegular/>}
                    onClick={() => refetch().then()}
                />
            </Stack>
            {/*{pagination}*/}
            <Stack rowGap="S">
                <ProgressBar hide={!isFetching}/>
                <SortableTable
                    columns={COLUMNS}
                    rows={enriched_users ?? []}
                    missingPlaceholder={<span style={{opacity: 0.5}}>&#823;</span>}
                    onRowClick={row => {
                        setSelectedUser(row)
                    }}
                />
            </Stack>
            {pagination}
            <br/>
        </Stack>
        <Modal
            open={!!selectedUser}
            closable
            onCancel={() => {
                setSelectedUser(undefined)
                refetch?.()?.then?.()
            }}
            footer={null}
        >
            {selectedUser && <Stack rowGap="XL">
                <PageSectionTitle>User details</PageSectionTitle>
                <Stack rowGap="XXXL">
                    <Stack rowGap="M">
                        <FreeTextField
                            label="ID"
                            value={selectedUser.id ?? ''}
                            readOnly
                        />
                        <FreeTextField
                            label="Full name"
                            value={selectedUser.full_name ?? ''}
                            readOnly
                        />
                        <FreeTextField
                            label="Email"
                            value={selectedUser.email ?? ''}
                            readOnly
                        />
                    </Stack>
                    <span>
                        Can login with email and password?&nbsp;
                        {selectedUser.email && selectedUser.has_password && 'Yes' || 'No'}
                    </span>
                    {selectedUser.openid?.length >= 1
                        ? <SortableTable columns={OID_COLUMNS} rows={selectedUser.openid} size="extra-small"/>
                        : <span>OpenID credentials: none</span>}

                    <CanCreateDatasetsCtrl user={selectedUser}/>
                </Stack>
            </Stack>}
        </Modal>
    </>
}


export const CanCreateDatasetsCtrl: React.FC<{
    user: UserReflection
}> = props => {
    const [can_create_datasets, _set_can_create_datasets] = useState<boolean>(() => !!props.user.can_create_datasets)
    const [isPending, setIsPending] = useState<boolean>(false)
    const [error, setError] = useState<Error | undefined>()

    const {data: my_user_data} = usePublicMeGetMe({})
    const queryClient = useQueryClient()

    useEffect(() => {
        _set_can_create_datasets(!!props.user.can_create_datasets)
    }, [props.user])

    return <>
        <Stack horizontal>
            <span>Can create datasets?</span>
            <Switch
                checked={can_create_datasets}
                checkedChildren={"Yes"}
                unCheckedChildren={"No"}
                loading={isPending}
                onChange={async checked => {
                    if (isPending) {
                        return
                    }
                    setIsPending(true)
                    setError(undefined)
                    try {
                        if (checked) {
                            await fetchAdminUsersEditUserIdCanCreateDatasetsSetCanCreateDatasets({
                                pathParams: {
                                    editUserId: props.user.id,
                                }
                            })
                        } else {
                            await fetchAdminUsersEditUserIdCanCreateDatasetsResetCanCreateDatasets({
                                pathParams: {
                                    editUserId: props.user.id,
                                }
                            })
                        }
                        _set_can_create_datasets(checked)
                    }
                    catch (e) {
                        setError(e)
                    }
                    finally {
                        await new Promise(resolve => setTimeout(resolve, 500))  // better visual feedback
                        setIsPending(false)
                        if (my_user_data?.id === props.user.id) {
                            queryClient.clear()
                        }
                    }
                }}
            />
            <MaybeErrorAlert error={error}/>
        </Stack>
    </>
}