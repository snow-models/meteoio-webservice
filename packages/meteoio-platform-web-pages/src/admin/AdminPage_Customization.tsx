import * as React from 'react'
import {startTransition, useCallback} from 'react'
import {brand_color_comments, DEFAULT_brand} from "meteoio-ui/src/utils/theme";
import {FreeTextField} from "meteoio-ui/src/components/FreeTextField";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {ImageInputField} from "meteoio-ui/src/components/ImageInputField";
import {useAppCustomizationSettings} from "../_common/AppCustomizationSettingsProvider";
import {
    AppCustomizationSettingsInstance, BrandColors, fetchAdminSettingsWrite,
    useAdminSettingsFactoryDefaultGetFactoryDefault, useAdminSettingsRead
} from "meteoio-platform-client";
import {Collapse, ColorPicker} from 'antd'
import {useNavigate, useParams} from "react-router-dom";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {Body1, Field, Spinner} from "@fluentui/react-components";
import {ControlLabel} from "meteoio-ui/src/components/ControlLabel";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {TinyColor} from "@ctrl/tinycolor/dist";
import {BinRecycleRegular, SaveRegular} from "@fluentui/react-icons/lib/fonts";
import {Alert} from "meteoio-ui/src/components/Alert";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {RollbackOutlined} from "@ant-design/icons";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";

export const AdminPage_Customization: React.FC = () => {

    const {customizationSettingsPage} = useParams()
    const navigate = useNavigate()
    const {hasLocalChanges, localSettings: localSettings_, setLocalSettings, refetchRemoteSettings} = useAppCustomizationSettings()
    const dialogs = useDialogs()

    const {data: remoteSettings, error: remoteSettingsError, isLoading, refetch} = useAdminSettingsRead({})
    const {data: factoryDefaults, error: factoryDefaultsError} = useAdminSettingsFactoryDefaultGetFactoryDefault({})

    const localSettings = localSettings_ ?? remoteSettings

    const pushSettings = useCallback(async (settings: AppCustomizationSettingsInstance) => {
        await fetchAdminSettingsWrite({body: settings})
        await Promise.all([
            refetch(),
            refetchRemoteSettings(),
        ])
        setLocalSettings(null)
    }, [])

    const set: <T extends keyof AppCustomizationSettingsInstance>(field: T, value: AppCustomizationSettingsInstance[T]) => void = (field, value) => {
        setLocalSettings({...(localSettings ?? {} as AppCustomizationSettingsInstance), [field]: value})
    }

    // TODO?: it may also be possible to create a preview component that reuses the home page component...

    if (isLoading) {
        return <Spinner/>
    }

    if (remoteSettingsError) {
        return <MaybeErrorAlert error={remoteSettingsError}/>
    }

    return <Stack rowGap="None">
        <PageTitle>Customization</PageTitle>
        <br/>
        {hasLocalChanges
            ? <Alert
                level="warning"
                end={<Stack horizontal alignItems="center" justifyContent="end" grow>
                    <AsyncActionButton
                        size="small"
                        label="Discard"
                        appearance="subtle"
                        icon={<BinRecycleRegular/>}
                        onClick={async () => {
                            if (!await dialogs.confirm('Are you sure to want to discard the pending changes?', {
                                okText: 'Discard',
                                okType: 'danger',
                            })) {
                                return
                            }
                            setLocalSettings(null)
                        }}
                    />
                    <AsyncActionButton
                        size="small"
                        label="Save"
                        appearance="secondary"
                        icon={<SaveRegular/>}
                        onClick={async () => {
                            await pushSettings(localSettings)
                        }}
                    />
                </Stack>}
            >
                You have pending changes.
            </Alert>
            : <Alert
                level="low_info"
                end={<Stack horizontal alignItems="center" justifyContent="end" grow>
                </Stack>}
            >
                There are no pending changes
            </Alert>}
        <Collapse
            accordion
            activeKey={customizationSettingsPage?.split?.(',')}
            onChange={key => {
                navigate(`/admin/customization/${key}`)
            }}
            items={[
                {
                    key: 'main',
                    label: <PageSectionTitle>Appearance</PageSectionTitle>,
                    children: <>
                        <Stack rowGap="XL">
                            <Stack rowGap="S" columnGap="XXXL" alignItems="start">
                                <FreeTextField
                                    label="Application short title"
                                    value={localSettings.app_short_title}
                                    onChange={value => set('app_short_title', value)}
                                    maxLength={20}
                                />
                                <ImageInputField
                                    value={localSettings?.app_logo_url}
                                    onChange={value => set('app_logo_url', value)}
                                    label="Application Logo"
                                    maxWidth={128}
                                    maxHeight={128}
                                />
                            </Stack>
                            <Stack rowGap="S">
                                <Stack horizontal wrap columnGap="XXXL" rowGap="S" alignItems="end">
                                    <Field label={<ControlLabel>Primary colour</ControlLabel>}>
                                        <ColorPicker
                                            // disabled
                                            disabledAlpha
                                            size="large"
                                            value={localSettings?.brand_colors?.['80']}
                                            onChange={value => {
                                                startTransition(() => {
                                                    // Let's consider the default primary color...
                                                    const safe_base = new TinyColor(DEFAULT_brand['80']).toHsl()
                                                    // Will use the chosen primary color with variation from the base and default palette
                                                    const choice = (new TinyColor(value.toHex())).toHsl()
                                                    choice.l = Math.min(choice.l, 0.4)  // Avoid too bright colors (would not be readable)
                                                    // Let's generate the palette
                                                    const cc: BrandColors = Object.fromEntries(Object.keys(DEFAULT_brand).map((key, i) => {
                                                        const safe = new TinyColor(DEFAULT_brand[key]).toHsl()
                                                        const c = new TinyColor({
                                                            h: choice.h + (safe.h - safe_base.h),
                                                            s: Math.max(0, Math.min(1, choice.s + (safe.s - safe_base.s))), //safe.s * 0.75 + 0.25 * choice.s,
                                                            l: Math.max(0, Math.min(1, choice.l + (safe.l - safe_base.l))),
                                                        })
                                                        return [key, c.toHexString()]
                                                    })) as BrandColors

                                                    set('brand_colors', cc)
                                                })
                                            }}
                                            showText={color => <code>#{color.toHex()}</code>}
                                        />
                                    </Field>
                                    <AsyncActionButton
                                        size="large"
                                        appearance="subtle"
                                        onClick={async () => {
                                            set('brand_colors', DEFAULT_brand)
                                        }}
                                        label={<Body1>Reset factory brand colours</Body1>}
                                    />
                                </Stack>
                                <details>
                                    <summary style={{cursor: 'pointer'}}>See all palette</summary>
                                    <div style={{
                                        display: 'grid',
                                        gridTemplateColumns: 'auto auto 1fr',
                                        columnGap: '0.5rem',
                                        verticalAlign: 'baseline',
                                    }}>
                                        {Object.keys(DEFAULT_brand).map(key => <React.Fragment key={key}>
                                            <code style={{
                                                textAlign: 'right',
                                                opacity: 0.5,
                                                fontSize: '0.8em',
                                                verticalAlign: 'baseline',
                                                lineHeight: '1.5rem'
                                            }}>#{key}</code>
                                            <div>
                                                <ColorPicker
                                                    disabled
                                                    disabledAlpha
                                                    size="small"
                                                    style={{lineHeight: '1.75rem', verticalAlign: 'baseline', height: '1.5rem'}}
                                                    value={localSettings?.brand_colors?.[key]}
                                                    onChange={value => set('brand_colors', {
                                                        ...localSettings.brand_colors,
                                                        [key]: value.toHex()
                                                    })}
                                                    showText={color => <code>#{color.toHex()}</code>}
                                                />
                                            </div>
                                            <small style={{
                                                verticalAlign: 'baseline',
                                                lineHeight: '1.5rem',
                                                fontStyle: 'italic',
                                                overflow: 'hidden',
                                                textOverflow: 'ellipsis',
                                                whiteSpace: 'nowrap',
                                            }}>{brand_color_comments?.[key]}</small>
                                        </React.Fragment>)}
                                    </div>
                                </details>
                            </Stack>
                        </Stack>
                    </>
                },
                {
                    key: 'home',
                    label: <PageSectionTitle>Welcome messages</PageSectionTitle>,
                    children: <>
                        <Stack rowGap="S">
                            <FreeTextField
                                label="Home page title"
                                value={localSettings.app_long_title}
                                onChange={value => set('app_long_title', value)}
                            />
                            <FreeTextField
                                label="Subtitle"
                                value={localSettings.app_subtitle}
                                onChange={value => set('app_subtitle', value)}
                            />
                            <FreeTextField
                                label="Welcome text for guest visitors"
                                multiline
                                value={localSettings.welcome_guests}
                                onChange={value => set('welcome_guests', value)}
                            />
                            <FreeTextField
                                label="Welcome text for authenticated users without dataset creation permission"
                                multiline
                                value={localSettings.welcome_restricted_internal}
                                onChange={value => set('welcome_restricted_internal', value)}
                            />
                            <FreeTextField
                                label="Welcome text for data owners"
                                multiline
                                value={localSettings.welcome_data_owners}
                                onChange={value => set('welcome_data_owners', value)}
                            />
                        </Stack>
                    </>
                },
            ]}
        />
        <br/>
        <div>
            <AsyncActionButton
                size="small"
                label="Reset all settings to factory defaults"
                icon={<RollbackOutlined style={{fontSize: '0.7em'}}/>}
                onClick={async () => {
                    setLocalSettings(factoryDefaults)
                }}
            />
            <MaybeErrorAlert error={factoryDefaultsError}/>
        </div>
        <br/>
    </Stack>

    // TODO: for the brand colors, add some preview of some various components (especially the list of datasets, see PubDsCard.tsx and RepositoryCard.tsx)

    // TODO?: maybe use tabs for home page and branding colors instead of stacking them vertically.
}