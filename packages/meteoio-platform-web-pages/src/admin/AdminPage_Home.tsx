import * as React from 'react'
import {useEffect} from 'react'
import {useNavigate} from "react-router-dom";

export const AdminPage_Home: React.FC = () => {
    const navigate = useNavigate()
    useEffect(() => navigate('/admin/users', {replace: true}))
    return <>
    </>
}
