import * as React from 'react'
import {Route, Routes} from "react-router-dom";
import {AdminPage_Home} from "./AdminPage_Home";
import {Sticky} from "meteoio-ui/src/components/Sticky";
import {MainContentSplit} from "meteoio-ui/src/layouts/MainContentSplit";
import {NavLinksList} from "meteoio-ui/src/components/NavLinksList";
import {AdminPage_Users} from "./AdminPage_Users";
import {AdminPage_Jobs} from "./AdminPage_Jobs";
import {MainTopBar} from "../_common/MainTopBar";
import {NotFound} from "../_common/NotFound";
import {AdminPage_Datasets} from "./AdminPage_Datasets";
import {AdminPage_Customization} from "./AdminPage_Customization";
import {ChevronRightRegular} from "@fluentui/react-icons";

export const AdminPages: React.FC = () => {
    return <>
        <MainTopBar startExtra={<>
            <ChevronRightRegular/>
            <b>Admin</b>
        </>}/>
        <MainContentSplit
            nav={<Sticky top={65}>
                <NavLinksList
                    links={[
                        {to: '/admin/users', children: <>Users</>},
                        {to: '/admin/datasets', children: <>Datasets</>},
                        {to: '/admin/jobs', children: <>Guest Jobs</>},
                        {to: '/admin/customization', children: <>Customization</>},
                    ]}
                />
            </Sticky>}
        >
            <Routes>
                <Route index Component={AdminPage_Home}/>
                <Route path={"users"} Component={AdminPage_Users}/>
                <Route path={"jobs"} Component={AdminPage_Jobs}/>
                <Route path={"datasets"} Component={AdminPage_Datasets}/>
                <Route path={"customization"} Component={AdminPage_Customization}/>
                <Route path={"customization/:customizationSettingsPage"} Component={AdminPage_Customization}/>
                <Route path={"*"} Component={NotFound}/>
            </Routes>
        </MainContentSplit>
    </>
}
