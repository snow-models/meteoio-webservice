import * as React from 'react'
import {useMemo, useState} from 'react'
import {
    fetchAdminJobsJobIdDeleteJob,
    JobListEntryDTO,
    useAdminJobsListJobs,
    usePublicJobSubmissionJobIdStatsStats,
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Column, SortableTable} from "meteoio-ui/src/components/SortableTable";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Pagination} from "antd";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {Modal} from "meteoio-ui/src/components/Modal";
import {IJobContext, JobContext} from "../jobs/job/JobContext";
import {JobHeading} from "../jobs/job/JobHeading";
import {Button} from "meteoio-ui/src/components/Button";
import {ArrowClockwiseRegular, ChevronRightRegular, DeleteRegular} from '@fluentui/react-icons'
import {useNavigate} from "react-router-dom";
import {address_book} from "../address_book";
import {mergeClasses, Text, tokens} from "@fluentui/react-components";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {useJobViewStyles} from "../jobs/job/styles";

const COLUMNS: Column<JobListEntryDTO>[] = [
    {
        field: 'created_at',
        title: 'Submitted',
        onRenderCellContent: row => row.created_at &&
            <RelativeTimeText date={row.created_at}/>
    },
    {
        field: 'id',
        title: 'ID',
        onRenderCellContent: row => <code style={{
            lineHeight: '0.9em',
            fontSize: '0.8em',
            display: 'block',
            overflow: 'hidden',
            textOverflow: 'ellipsis'
        }}>
            {row.id}
        </code>,
    },
]

export const AdminPage_Jobs: React.FC = () => {
    const [selectedRow, setSelectedRow] = useState<JobListEntryDTO | undefined>(undefined)
    const [limit, setLimit] = useState<number>(10);
    const [offset, setOffset] = useState<number>(0)
    const {
        data,
        isLoading,
        isFetching,
        error,
        refetch,
    } = useAdminJobsListJobs({queryParams: {offset, limit}})

    const jobId = selectedRow ? selectedRow?.id ?? undefined : undefined

    const jobStats = usePublicJobSubmissionJobIdStatsStats({pathParams: {jobId}}, {
        enabled: !!jobId,
    })

    const jobCtx = useMemo<IJobContext>(() =>
            // NOTE: jobStats will contain data of previous jobIds while jobStats.isFetching
            ((jobStats.isLoading || jobStats.isFetching) ? {jobId} :
                {jobId, status: jobStats?.data?.status, stats: jobStats?.data}),
        [jobId, jobStats])

    const navigate = useNavigate()

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    if (isLoading) {
        return <Spinner tall/>
    }
    return <>
        <Stack rowGap="XL">
            <Stack horizontal columnGap="L">
                <PageTitle>Guest Jobs history</PageTitle>
                <Button
                    appearance="subtle"
                    icon={<ArrowClockwiseRegular/>}
                    onClick={() => refetch().then()}
                />
            </Stack>
            <Stack rowGap="S">
                <ProgressBar hide={!isFetching}/>
                <SortableTable
                    columns={COLUMNS}
                    rows={data.items}
                    missingPlaceholder={<span style={{opacity: 0.5}}>&#823;</span>}
                    onRowClick={row => {
                        setSelectedRow(row)
                    }}
                />
            </Stack>
            <Pagination
                current={1 + Math.floor(offset / limit)}
                total={data.total}
                pageSize={limit}
                responsive
                hideOnSinglePage
                showSizeChanger
                onChange={(page, pageSize) => {
                    setOffset((page - 1) * pageSize)
                    setLimit(pageSize)
                    document?.body?.scrollTo?.({top: 0, behavior: 'smooth'})
                }}
            />
            <br/>
        </Stack>
        <Modal
            open={!!selectedRow}
            closable
            onCancel={() => setSelectedRow(undefined)}
            footer={null}
        >
            {selectedRow && jobId && <JobContext.Provider value={jobCtx}>
                <Stack rowGap="XL">
                    <Stack horizontal columnGap="XXXL">
                        <PageSectionTitle>Job details</PageSectionTitle>
                        <Button
                            appearance="subtle"
                            label="Go to Result"
                            iconPosition="after"
                            icon={<ChevronRightRegular/>}
                            onClick={() => navigate(address_book.jobs.view(selectedRow.id))}
                        />
                    </Stack>
                    <Stack rowGap="XXXL">
                        {(jobStats.isLoading || jobStats.isFetching)
                            ? <ProgressBar thickness="large"/> : <JobHeading/>}

                    </Stack>
                    <JobDeleteBtn jobId={jobId} onDeleteSuccess={() => {
                        setSelectedRow(undefined)
                        refetch()?.then?.()
                    }}/>
                </Stack>
            </JobContext.Provider>}
        </Modal>
    </>
}

export const JobDeleteBtn: React.FC<{
    jobId: string
    onDeleteSuccess?: () => void
}> = props => {
    const [isDeleting, setIsDeleting] = useState<boolean>(false)
    const styles = useJobViewStyles()

    return <>
        {!isDeleting && <Button
            danger
            icon={<DeleteRegular/>}
            label="Delete Job"
            appearance="subtle"
            //secondaryLine="Remove entry and all associated data"
            onClick={() => setIsDeleting(true)}
        />}
        {isDeleting && <Text
            size={200}
            style={{columnCount: 1, borderColor: tokens.colorPaletteRedBorder1}}
            className={mergeClasses(styles.statusInfo, styles.boxBorder)}
        >
            <Text style={{color: tokens.colorPaletteRedForeground2}}>
                Confirm? You are going to erase this submission and all the associated files.
            </Text>
            <Stack horizontal justifyContent="center">
                <Button
                    label="Go back"
                    appearance="subtle"
                    //secondaryLine="Remove entry and all associated data"
                    onClick={() => setIsDeleting(false)}
                />
                <AsyncActionButton
                    danger
                    icon={<DeleteRegular/>}
                    label="Delete Job"
                    appearance="subtle"
                    //secondaryLine="Remove entry and all associated data"
                    onClick={async () => {
                        await fetchAdminJobsJobIdDeleteJob({pathParams: {jobId: props.jobId}})
                        props.onDeleteSuccess?.()
                        // Keep spinning while refreshing:
                        await new Promise(resolve => setTimeout(resolve, 1000))
                    }}
                />
            </Stack>
        </Text>}
    </>
}
