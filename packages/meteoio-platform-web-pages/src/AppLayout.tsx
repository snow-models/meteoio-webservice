import * as React from 'react'
import {MainTopBar} from "./_common/MainTopBar";
import {Outlet} from 'react-router-dom';
import {MainContentSplitWithMainNav} from "./_common/MainContentSplitWithMainNav";

export const AppLayout: React.FC = () => {
    return <>
        <MainTopBar/>
        <MainContentSplitWithMainNav>
            <Outlet/>
        </MainContentSplitWithMainNav>
    </>
}
