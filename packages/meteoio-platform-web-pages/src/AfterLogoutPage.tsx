import * as React from 'react'
import {useEffect} from 'react'
import {useQueryClient} from "./_common/backend";
import {useHref} from "react-router-dom";
import {address_book} from "./address_book";

export const AfterLogoutPage: React.FC = () => {

    const queryClient = useQueryClient()
    const homeHref = useHref(address_book.home)

    useEffect(() => {
        queryClient?.clear?.()
        window?.location?.replace?.(homeHref)
    }, [])

    return <>

    </>
}
