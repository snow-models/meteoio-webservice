import * as React from 'react'
import {useRouteError} from "react-router-dom";
import {MainTopBar} from "./_common/MainTopBar";
import {Stack} from "meteoio-ui/src/layouts/Stack";
//import {EmojiSadSlightRegular} from '@fluentui/react-icons'
import {Text} from "@fluentui/react-components";

export const AppErrorBoundary: React.FC = () => {
    const error = useRouteError();
    // @ts-ignore
    const statusText = error?.statusText ?? ''
    // @ts-ignore
    const isInternal = error?.internal

    const diagnosticsText = isInternal
        ? <details>
            <summary>Internal error</summary>
            {statusText}
        </details>
        : statusText;

    //console.error(error);

    // @ts-ignore
    return <>
        <MainTopBar/>
        <Stack alignItems="center" justifyContent="center" rowGap="XXXL">
            <span>&nbsp;</span>
            <Stack horizontal alignItems="center" justifyContent="center" columnGap="M">
                {/*<EmojiSadSlightRegular fontSize={tokens.fontSizeHero700}/>*/}
                <Stack maxWidth={400}>
                    <Text size={600}>Something went wrong.</Text>
                    <Text size={200} font="monospace">{diagnosticsText}</Text>
                </Stack>
            </Stack>
            <span>&nbsp;</span>
        </Stack>
    </>
}
