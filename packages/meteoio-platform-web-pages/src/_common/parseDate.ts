export const parseDate: (date?: string | Date) => (Date | undefined) = date => {
    if (!date) {
        return undefined
    }
    if (typeof date === 'string') {
        return date?.endsWith?.('Z') || date?.endsWith?.('+00:00') ? new Date(date) : new Date(date + 'Z')
    }
    return date
}
