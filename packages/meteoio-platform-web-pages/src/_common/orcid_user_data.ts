import {useCallback, useEffect, useState} from "react";

export interface IssSub {
    issuer: string
    subject: string
}

const __mk_iss_sub_cache_key: (iss_sub: IssSub) => string = iss_sub =>
    `${iss_sub.issuer}${iss_sub.subject}`

const __oid_browser_cache_promise = (() => {
    try {
        return caches.open('_oid_cache_9aea4dab-d50f-40b1-88e6-d6f2dca24ad4')
    } catch (e) {
        console.trace(e)
    }
})();


const __getOrcidFullName_cache = new Map<string, string>()
const __getOrcidFullName: (iss_sub: IssSub) => Promise<string> = async (iss_sub) => {
    const _cache_key = __mk_iss_sub_cache_key(iss_sub)
    if (__getOrcidFullName_cache.has(_cache_key)) {
        return __getOrcidFullName_cache.get(_cache_key)
    }
    // console.debug('FETCHING ORCID', {_cache_key})

    const {issuer, subject} = iss_sub

    if (!['https://sandbox.orcid.org/', 'https://orcid.org/'].includes(issuer)) {
        return undefined
    }

    const url = {
        'https://orcid.org/': `https://pub.orcid.org/v3.0/${subject}/person`,
        'https://sandbox.orcid.org/': `https://pub.sandbox.orcid.org/v3.0/${subject}/person`,
    }[issuer]

    const req: Request = new Request(url, {
        method: 'GET',
        referrerPolicy: 'no-referrer',
        credentials: 'omit',
        mode: 'cors',
        cache: 'force-cache',  // NOTE: this is ignored due to no-cache must-revalidate headers and CORS policies
        // headers: {
        //     'Cache-Control': 'max-stale=86400',
        // }
    })

    // app-managed caching of HTTP requests
    const resp: Response = await (async () => {
        const _oid_cache = await __oid_browser_cache_promise
        if (_oid_cache) {
            const _cached = await _oid_cache.match(req, {ignoreVary: true})
            if (_cached) {
                // NOTE: it seems like there's no way to know the date or age of the cached response.
                console.debug('__getOrcidFullName cache hit')
                setTimeout(async () => {
                    const kk = await _oid_cache.keys()
                    const i = Math.floor(Math.random() * (kk.length + 100))
                    if (i < kk.length && Math.random() > 0.5) {
                        console.debug('__getOrcidFullName removing some cache')
                        await _oid_cache.delete(kk[i])
                    }
                    await _oid_cache.add(req)
                }, Math.random() * 10 * 60 * 1000)
                return _cached.clone()
            }
        }
        console.debug('__getOrcidFullName cache miss')
        await new Promise<void>(resolve => setTimeout(resolve, 1000))  // self-moderation of API requests
        const _fetched = await fetch(req)
        if (_oid_cache) {
            _oid_cache.put(req, _fetched.clone()).then()
        }
        return _fetched.clone()
    })();

    const xml = await resp.clone().text()

    const parser = new DOMParser()
    const doc = parser.parseFromString(xml, 'application/xml')
    const given_names = doc.getElementsByTagName('personal-details:given-names').item(0)?.textContent
    const family_name = doc.getElementsByTagName('personal-details:family-name').item(0)?.textContent
    const full_name = `${given_names ?? ''} ${family_name ?? ''}`.trim()

    __getOrcidFullName_cache.set(_cache_key, full_name)
    return full_name
}

/** React hook that allows to have a reactive callback function that is eventually able to answer to fullName queries
 * after slowly caching a batch of required entries, with reactive cancellation of planned requests and cache.
 * The returned `loadProgressive` allows for reactive notification of loaded data.
 */
export function useOrcidFullNames(iss_sub_batch?: IssSub[]) {
    const [loadProgressive, setLoadProgressive] = useState<number>(0)
    useEffect(() => {
        if (!(iss_sub_batch?.length >= 1)) {
            return
        }
        let cancelled = false;
        (async () => {
            const _batch = iss_sub_batch.slice()
            for (let i = 0; i < _batch.length; i++) {
                await new Promise<void>(resolve => setTimeout(resolve, 50 + i * 10 + loadProgressive * 10))
                if (cancelled) {
                    return
                }
                __getOrcidFullName(_batch[i])   // NOTE: it does not fetch if cached.
                    .then(() => setLoadProgressive(v => v + 1))
            }
        })()
        return () => {
            cancelled = true
        }
    }, [iss_sub_batch])

    const getFullName = useCallback<(iss_sub: IssSub) => string | undefined>(iss_sub => {
        const _cache_key = __mk_iss_sub_cache_key(iss_sub)
        // console.debug('GETTING CACHED ORCID', {_cache_key})
        return __getOrcidFullName_cache.get(_cache_key)
    }, [])

    return {getFullName, loadProgressive}
}
