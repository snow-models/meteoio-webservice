import * as React from 'react'
import {TopBar} from "meteoio-ui/src/layouts/TopBar";
import {Link} from "@fluentui/react-components";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {address_book} from "../address_book";
import {useHref} from "react-router-dom";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {MainTopLogo} from "./MainTopLogo";
import {UserTopFrag} from "../user/UserTopFrag";
import {MainTopHamburgerBtn} from "./MainTopHamburgerBtn";

export const MainTopBar: React.FC<{
    startExtra?: React.ReactNode
    bg?: 'nb1' | 'nb3'
    showHamburger?: boolean
}> = props => {
    const hrefHome = useHref(address_book.home)

    const {isMobile} = useScreenSize()

    return <>
        <TopBar
            bg={props.bg}
            //showScrollToTop
            start={<Stack horizontal alignItems="center" columnGap="S" overflow="hidden">
                {props.showHamburger && <div style={{margin: '8px -8px 8px 8px'}}><MainTopHamburgerBtn/></div>}
                <Link href={hrefHome} style={{flexShrink: 0}}>
                    <MainTopLogo withText={!(isMobile && props.startExtra)}/>
                </Link>
                {props.startExtra}
            </Stack>}
            end={<>
                <UserTopFrag/>
            </>}
        />
    </>
}
