import * as React from 'react'
import {address_book} from "../address_book";
import {NavLinksList} from "meteoio-ui/src/components/NavLinksList";
import {usePublicMeGetMe} from "meteoio-platform-client";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {ExtLink} from "meteoio-ui/src/components/ExtLink";


export const MainNav: React.FC<{
    variant?: 'main' | 'box'
    noInlineMargin?: boolean
}> = props => {
    const {
        data,
        isLoading,
    } = usePublicMeGetMe({}, {
        cacheTime: 600_000,
    })

    return <>
        <NavLinksList
            noInlineMargin={props.noInlineMargin}
            variant={props.variant}
            links={[
                // {to: address_book.home, children: <>Home</>},
                {to: address_book.explore.list, children: <>Explore Datasets</>},
                ...(data?.is_logged_in ? [
                    {to: address_book.jobs.create, children: <>Guest Job Submission</>},
                    {to: address_book.jobs.list, children: <>View last submissions</>},
                ] : []),
                ...(data?.can_create_datasets ? [
                    {to: address_book.datasets.list, children: <>Your Datasets & INIs</>},
                ] : []),
                ...(data?.is_admin ? [
                    {to: '/admin', children: <>Administration</>}
                ] : []),
            ]}
        >
            {/* noinspection HtmlUnknownTarget */}
            <ExtLink href="docs/">Documentation</ExtLink>
        </NavLinksList>
        {isLoading && <Spinner tall/>}
    </>
}