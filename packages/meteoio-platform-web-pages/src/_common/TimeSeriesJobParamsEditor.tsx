import * as React from 'react'
import {useRef} from 'react'
import {TimeSeriesJobParams} from "meteoio-platform-client";
import {DropdownField} from "meteoio-ui/src/components/DropdownField";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {DocumentRegular} from "@fluentui/react-icons";
import {FieldDescription} from "meteoio-ui/src/components/FieldDescription";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {DateTimeRangePicker} from "meteoio-ui/src/components/DateTimeRangePicker";
import {Input, Text} from "@fluentui/react-components";
import {ControlLabel} from "meteoio-ui/src/components/ControlLabel";
import {Button} from "meteoio-ui/src/components/Button"

/**
 * editor GUI for CronJob['params'] in case of CronJob['type'] === 'meteoio_timeseries'
 */
export const TimeSeriesJobParamsEditor: React.FC<{
    value?: Partial<TimeSeriesJobParams>
    onChange?: (value: Partial<TimeSeriesJobParams>) => void
    iniFileNames?: string[]
    inisLoading?: boolean
    inisError?: unknown
}> = props => {

    const resMinInput = useRef<HTMLInputElement>()

    return <>
        <FieldDescription
            field={<DropdownField
                value={props.value?.ini ?? null}
                onChange={value => props.onChange?.({...(props.value ?? {}), ini: value})}
                label="INI configuration"
                options={props.iniFileNames?.map?.(ini => ({
                    value: ini,
                    text: ini,
                    label: <Stack horizontal alignItems="center">
                        <DocumentRegular/> {ini}
                    </Stack>
                })) ?? []}
                loading={props.inisLoading}
            />}
            description={<>
                Select the INI file to configure MeteoIO
            </>}
        />
        <MaybeErrorAlert error={props.inisError}/>

        <DateTimeRangePicker
            label="Range"
            onChange={value => {
                props.onChange?.({...props.value, range: value as TimeSeriesJobParams['range']})
                // NOTE: type assertion above due to Date objs that can be serialized correctly
            }}
            value={props.value?.range}
        />

        <Stack rowGap="None">
            <ControlLabel>Resolution</ControlLabel>
            {props.value?.resolution_minutes
                ? <>
                    <Input
                        ref={resMinInput}
                        key="custom_value"
                        value={props.value?.resolution_minutes?.toFixed?.(0) ?? ''}
                        type="number"
                        contentAfter={<Text size={200}>minutes</Text>}
                        onChange={(ev, data) => {
                            props.onChange?.({
                                ...props.value,
                                resolution_minutes: Math.max(1, parseInt(data.value) ?? 0)
                            })
                        }}
                    />
                    <Stack horizontal justifyContent="end">
                        <Button
                            label={<Text size={100}>... or switch to the value from INI configuration</Text>}
                            size="small"
                            appearance="subtle"
                            onClick={() => {
                                props.onChange?.({...props.value, resolution_minutes: null})
                            }}
                        />
                    </Stack>
                </>
                : <>
                    <Input
                        key="SAMPLING_RATE_MIN"
                        value={""}
                        type="number"
                        placeholder="SAMPLING_RATE_MIN from INI configuration"
                        onChange={(ev, data) => {
                            props.onChange?.({
                                ...props.value,
                                resolution_minutes: Math.max(1, parseInt(data.value) ?? 0)
                            })
                            setTimeout(() => {
                                resMinInput?.current?.focus?.()
                            }, 100)
                        }}
                    />
                    <Stack horizontal justifyContent="end">
                        <Button
                            label={<Text size={100}>... or specify a duration</Text>}
                            size="small"
                            appearance="subtle"
                            onClick={() => {
                                props.onChange?.({...props.value, resolution_minutes: 60})
                                setTimeout(() => {
                                    resMinInput?.current?.focus?.()
                                }, 100)
                            }}
                        />
                    </Stack>
                </>}
        </Stack>
    </>
}
