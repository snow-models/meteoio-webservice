import * as React from 'react'
import {Button} from "meteoio-ui/src/components/Button";
import { bundleIcon, NavigationRegular, NavigationFilled } from '@fluentui/react-icons';
import {useState} from "react";
import { Drawer } from "antd";
import {MainTopLogo} from "./MainTopLogo";
import {ThemeProvider} from "meteoio-ui/src/providers/ThemeProvider";
import {MainNav} from "./MainNav";
import {Link} from "@fluentui/react-components";
import {useHref} from "react-router-dom";
import {address_book} from "../address_book";

const HamburgerIcon = bundleIcon(NavigationFilled, NavigationRegular)

export const MainTopHamburgerBtn: React.FC<{
}> = props => {
    const [isOpen, setIsOpen] = useState<boolean>(false)
    const hrefHome = useHref(address_book.home)

    return <>
        <div >
            <Button
                style={{border: '1px solid #7773', borderRadius: '4px'}}
                appearance="subtle"
                icon={<HamburgerIcon />}
                onClick={() => setIsOpen(v => !v)}
            />
        </div>
        <Drawer
            open={isOpen}
            width={300}
            placement="left"
            onClose={() => setIsOpen(false)}
            title={<ThemeProvider>
                <Link href={hrefHome} style={{flexShrink: 0}}>
                    <MainTopLogo withText/>
                </Link>
            </ThemeProvider>}
            styles={{
                header: {padding: '8px 13px'},
                body: {padding: '0px 24px'},
            }}
            // closeIcon={<ChevronLeftRegular />}
        >
            <ThemeProvider>
                <MainNav noInlineMargin/>
            </ThemeProvider>
        </Drawer>
    </>
}