import * as React from 'react'
import {createContext, PropsWithChildren, useContext, useMemo} from 'react'
import {BrandVariants} from "@fluentui/react-theme";
import {BrandContext} from "meteoio-ui/src/providers/BrandContext";
import useLocalStorageState, {StoreCallback} from "meteoio-ui/src/hooks/useLocalStorageState";


const _DynamicBranding_local_storage_key = '01900b9f-66e6-7bcd-8d80-1d1e41b17393'

interface IDynamicBrandingContext {
    brand?: BrandVariants
    setBrand: StoreCallback<BrandVariants>
}

const DynamicBrandingContext = createContext<IDynamicBrandingContext>({
    setBrand() {
        console.error('DynamicBrandingContext not provided')
    }
})

export const DynamicBrandingProvider: React.FC<PropsWithChildren<{
}>> = props => {
    const [brand, setBrand] = useLocalStorageState<BrandVariants | undefined>(_DynamicBranding_local_storage_key)

    const ctx = useMemo(() => ({brand, setBrand}), [brand, setBrand])

    return <DynamicBrandingContext.Provider value={ctx}>
        <BrandContext.Provider value={brand}>
            {props.children}
        </BrandContext.Provider>
    </DynamicBrandingContext.Provider>
}

export function useDynamicBrandingLocalState(): IDynamicBrandingContext {
    return useContext(DynamicBrandingContext)
}
