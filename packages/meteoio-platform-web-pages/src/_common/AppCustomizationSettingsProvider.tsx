import * as React from 'react'
import {createContext, PropsWithChildren, useContext, useMemo} from 'react'
import {
    AppCustomizationSettingsInstance, FrontendConfiguration,
    usePublicMeConfigurationRetrieveFrontendConfiguration
} from "meteoio-platform-client";
import useLocalStorageState from "meteoio-ui/src/hooks/useLocalStorageState";
import { BrandContext } from 'meteoio-ui/src/providers/BrandContext';
import {BrandVariants} from "@fluentui/tokens";
import {DEFAULT_brand} from "meteoio-ui/src/utils/theme";

export interface IAppCustomizationSettingsContext {
    remoteSettings: AppCustomizationSettingsInstance | null
    localSettings: AppCustomizationSettingsInstance | null
    effectiveSettings: AppCustomizationSettingsInstance | null
    setLocalSettings: (value: AppCustomizationSettingsInstance | null) => void
    hasLocalChanges: boolean
    refetchRemoteSettings: () => Promise<unknown>
    isLoading: boolean
    configuration?: FrontendConfiguration
}

const AppCustomizationSettingsContext = createContext<IAppCustomizationSettingsContext>({
    localSettings: null,
    remoteSettings: null,
    effectiveSettings: null,
    hasLocalChanges: false,
    isLoading: false,
    setLocalSettings: () => {
        console.error('AppCustomizationSettingsContext not provided.')
    },
    refetchRemoteSettings: async () => {}
} as const)

/**
 *
 * NOTE: This is meant to provide data for all user classes, not just admins.
 *          => Welcome messages are redacted.
 *              => Other queries must be used by admins.
 *
 */
export const AppCustomizationSettingsProvider: React.FC<PropsWithChildren<{
}>> = props => {
    const [localSettings, setLocalSettings] = useLocalStorageState<AppCustomizationSettingsInstance>('01900bbe-54a7-786d-b307-08b37223f075', null)

    const {data: remoteConfiguration, refetch, isLoading} = usePublicMeConfigurationRetrieveFrontendConfiguration({}, {
        cacheTime: 600_000,
    })

    const ctx = useMemo<IAppCustomizationSettingsContext>(() => {
        const remoteSettings = remoteConfiguration?.settings
        return {
            remoteSettings,
            localSettings,
            effectiveSettings: localSettings ?? remoteSettings ?? ({} as AppCustomizationSettingsInstance),
            setLocalSettings,
            hasLocalChanges: !!localSettings,
            refetchRemoteSettings: refetch,
            isLoading,
            configuration: remoteConfiguration,
        }
    }, [localSettings, remoteConfiguration, isLoading])

    const brand_colors = useMemo<BrandVariants>(() => ({
        ...DEFAULT_brand,
        ...(ctx?.effectiveSettings?.brand_colors ?? {}),
    }), [ctx?.effectiveSettings?.brand_colors])

    return <AppCustomizationSettingsContext.Provider value={ctx}>
        <BrandContext.Provider value={brand_colors}>
            {props.children}
        </BrandContext.Provider>
    </AppCustomizationSettingsContext.Provider>
}


/**
 *
 * NOTE: This is meant to provide data for all user classes, not just admins.
 *          => Welcome messages are redacted.
 *              => Other queries must be used by admins.
 *
 */
export function useAppCustomizationSettings(): IAppCustomizationSettingsContext {
    return useContext(AppCustomizationSettingsContext)
}
