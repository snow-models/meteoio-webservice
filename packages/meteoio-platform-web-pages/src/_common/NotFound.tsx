import * as React from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Text, tokens} from "@fluentui/react-components";
import {Link} from "react-router-dom";

export const NotFound: React.FC = () => {
    return <>
        <Stack alignItems="center" justifyContent="center" rowGap="XXXL">
            <span>&nbsp;</span>
            <Stack maxWidth={400} center>
                <Text size={600}>Not found</Text>
                <Text size={300} style={{color: tokens.colorNeutralForeground3}}>Go back to <Link
                    to={"/"}>home page</Link>.</Text>
            </Stack>
            <span>&nbsp;</span>
        </Stack>
    </>
}
