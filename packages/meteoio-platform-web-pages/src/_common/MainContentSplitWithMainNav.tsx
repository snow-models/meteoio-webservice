import * as React from 'react'
import {MainNav} from "./MainNav";
import {MainContentSplit} from "meteoio-ui/src/layouts/MainContentSplit";
import {Sticky} from "meteoio-ui/src/components/Sticky";

export const MainContentSplitWithMainNav: React.FC<React.PropsWithChildren<{}>> = props => {
    return <>
        <MainContentSplit
            nav={<Sticky top={65}>
                <MainNav/>
            </Sticky>}
        >
            {props.children}
        </MainContentSplit>
    </>
}
