import * as React from 'react'
import {ExtLink} from "meteoio-ui/src/components/ExtLink";
import {makeStyles, Text, tokens} from "@fluentui/react-components";

const useStyles = makeStyles({
    root: {
        fontSize: tokens.fontSizeBase300,
        lineHeight: tokens.lineHeightBase300,
        color: tokens.colorNeutralForeground2,
    }
})

export const CreditsText: React.FC = () => {
    const styles = useStyles()
    return <>
        <Text block wrap className={styles.root}>
            This software has received funding from the&nbsp;
            <ExtLink href="https://public.wmo.int/en">World Meteorological Organization</ExtLink>
            &nbsp;under grant agreement No. 29539/2022-1.9&nbsp;
            as well as the European Union’s&nbsp;
            <ExtLink
                href="https://commission.europa.eu/funding-tenders/find-funding/eu-funding-programmes/horizon-europe_en">Horizon
                2020</ExtLink>
            &nbsp;research and innovation programme under&nbsp;
            <ExtLink href="https://arcticpassion.eu/">grant agreement No. 101003472</ExtLink>.
        </Text>
    </>
}
