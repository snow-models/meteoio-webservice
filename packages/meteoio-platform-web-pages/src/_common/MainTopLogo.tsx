import * as React from 'react'
import {Text} from "@fluentui/react-components";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {useAppCustomizationSettings} from "./AppCustomizationSettingsProvider";
import {Spinner} from "meteoio-ui/src/components/Spinner";

export const MainTopLogo: React.FC<{
    withText?: boolean
}> = props => {
    const {
        configuration,
        isLoading: isSettingsLoading,
    } = useAppCustomizationSettings()
    return <>
        <Stack horizontal alignItems="center" columnGap="S">
            {isSettingsLoading
                ? <span style={{margin: '0 0 0 12px'}}><Spinner size="tiny"/></span>
                : (configuration?.settings?.app_logo_url
                    ? <img
                        src={configuration?.settings?.app_logo_url}
                        alt="logo"
                        referrerPolicy="no-referrer"
                        style={{height: '1.4em', margin: '6px 0 6px 12px'}}
                    />
                    : <> &nbsp;</>)}

            {props.withText && <Text
                size={400}
                weight="bold"
                underline={false}
            >
                {configuration?.settings?.app_short_title ?? 'MeteoIO'}
            </Text>}
        </Stack>
    </>
}
