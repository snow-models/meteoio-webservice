import iniLib from 'ini'

export interface ParsedINI {
    title?: string
    summary?: string
    license?: string
    creator_name?: string
    creator_email?: string
}

export interface IniField {
    section: string
    key: string
}

export const KNOWN_FIELDS: IniField[] = [
    {section: 'Output', key: 'ACDD_TITLE'},
    {section: 'Output', key: 'ACDD_SUMMARY'},
    {section: 'Output', key: 'ACDD_LICENSE'},
    {section: 'Output', key: 'ACDD_CREATOR'},
    {section: 'Output', key: 'ACDD_CREATOR_EMAIL'},
]

export const field2str: (field: IniField) => string = field =>
    `${field.section}::${field.key}`  // NOTE: there's a preferred why to separate section and key already used in docs.


/** This is to assist the creation of a dataset. */
export const parseINI: (text: string) => { parsed: ParsedINI, foundFields: IniField[] } = text => {
    const obj: any = (text => {
        try {
            return iniLib.decode(text)
        } catch (e) {
            console.trace('Could not decode INI source', e)
            return {}
        }
    })(text);
    const parsed: ParsedINI = {
        title: obj?.Output?.ACDD_TITLE,
        summary: obj?.Output?.ACDD_SUMMARY,
        license: obj?.Output?.ACDD_LICENSE,
        creator_name: obj?.Output?.ACDD_CREATOR,
        creator_email: obj?.Output?.ACDD_CREATOR_EMAIL,
    }
    const foundFields = KNOWN_FIELDS.filter(ck => obj?.[ck.section]?.[ck.key] !== undefined)
    return {parsed, foundFields}
}


const _CACHE_isLikelyAnINI = new WeakMap<Blob, boolean>()

export const isLikelyAnINI: (file: Blob) => Promise<boolean> = async file => {
    const cached_value = _CACHE_isLikelyAnINI.get(file)
    if (cached_value !== undefined) {
        return cached_value
    }
    try {
        if (file instanceof File) {
            const file_extension = getFileExtension(file.name)?.toLowerCase?.()
            const may_be_ini_by_ext = file_extension === undefined || file_extension === 'ini'
            if (!may_be_ini_by_ext) {
                console.debug({file, hint: 'unknown filename extension'})
                return false
            }
        }

        if (file.size > 100_000) {
            console.debug({file, hint: 'too big'})
            return false
        }

        const text = await file.text()
        if (text.length <= 0) {
            console.debug({file, hint: 'empty'})
            return false
        }

        // Heuristic: a small binary file is often decoded as UTF-8 with lots of non-printable or non-ascii chars.
        let simpleChars = 0
        for (let i = text.length; i >= 0; i--) {
            const chc = text.charCodeAt(i)
            if (32 <= chc && chc <= 126) {
                ++simpleChars;
            }
        }
        if (simpleChars / text.length < 0.75) {
            console.debug({file, hint: 'too many strange chars'})
            return false
        }
    } catch (e) {
        console.trace(e)
        console.debug({file, hint: 'error'})
        return false
    }

    // If no check has failed, then we can accept the file as likely an INI
    console.debug({file, hint: 'ok'})
    return true
}

const getFileExtension = (filename?: string): string | undefined => {
    const lastDotIndex = (filename ?? '').lastIndexOf('.');
    if (lastDotIndex === -1) {
        return undefined  // No file extension found
    }
    return filename.slice(lastDotIndex + 1);
};
