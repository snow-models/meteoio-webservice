import {
    BACKEND_CONFIG,
    FileUploadFormData,
    QueryClient,
    useInternalDatasetsDatasetIdRead
} from "meteoio-platform-client";

import {persistQueryClient, removeOldestQuery} from '@tanstack/react-query-persist-client'
import {createSyncStoragePersister} from '@tanstack/query-sync-storage-persister'

export {useQueryClient} from "@tanstack/react-query";

const _loc = window?.location

BACKEND_CONFIG.baseUrl = `${_loc?.protocol}//${_loc?.host}${_loc?.pathname?.replace?.(/[^/]+$/, '')}api`
console.debug({BACKEND_CONFIG})

export function useDataset(datasetId: string) {
    return useInternalDatasetsDatasetIdRead({pathParams: {datasetId}})
}

export const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            cacheTime: 2000,
            staleTime: 300,
            refetchOnWindowFocus: false,
            retry: false,
            networkMode: 'always'
        },
    },
})

persistQueryClient({
    // @ts-ignore
    queryClient,
    persister: createSyncStoragePersister({
        storage: window.localStorage,
        retry: removeOldestQuery,
        throttleTime: 100,
    }),
})


export const mkFileUploadVariables: (file: Blob | string) => { body: FileUploadFormData, headers: object } = file => {
    const fd = new FormData()
    fd.set('file', typeof file === 'string' ? new Blob([file]) : file)
    return {
        body: fd as unknown as FileUploadFormData,
        headers: {
            'Content-Type': 'multipart/form-data',
        }
    }
}
