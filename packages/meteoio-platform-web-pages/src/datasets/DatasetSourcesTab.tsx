import * as React from 'react'
import {useCallback, useMemo, useState} from 'react'
import {MainContentSplit} from "meteoio-ui/src/layouts/MainContentSplit";
import {FileRow} from "meteoio-ui/src/components/FilesList";
import {useNavigate, useParams} from "react-router-dom";
import {FilesBrowser} from "meteoio-ui/src/components/FilesBrowser";
import {
    fetchInternalDatasetsDatasetIdInputDirMkInputDir,
    fetchInternalDatasetsDatasetIdInputFilesGetInputFile,
    fetchInternalDatasetsDatasetIdInputFilesPutInputFile, fetchInternalDatasetsDatasetIdInputFilesRenameRenameInputFile,
    fetchInternalDatasetsDatasetIdInputFilesRmInputFile,
    fetchInternalDatasetsDatasetIdInputListListInputFiles,
    useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis,
    useInternalDatasetsDatasetIdInputListListInputFiles
} from "meteoio-platform-client";
import {parseDate} from "../_common/parseDate";
import {address_book} from "../address_book";
import {Sticky} from "meteoio-ui/src/components/Sticky";
import {FoldersTree} from "meteoio-ui/src/components/FoldersTree";
import {Button} from "meteoio-ui/src/components/Button";
import {Modal} from "meteoio-ui/src/components/Modal";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {FilesField} from "meteoio-ui/src/components/FilesField";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {mkFileUploadVariables, queryClient, useQueryClient} from "../_common/backend";
import {AddRegular, ArrowUploadRegular, FolderAddRegular, PlayFilled} from '@fluentui/react-icons'
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {useDatasetConfigTx} from "./useDatasetConfigTx";
import {FolderCreationForm} from "meteoio-ui/src/components/FolderCreationForm";
import {SFTPAccessButton} from "./SFTPAccessButton";
import {useNotIniFilesFilter} from "./_common/useIniFilesFilter";


const ROOT_PATH = ''

// NOTE: this is a tab about user-provided data files, not only sources.
export const DatasetSourcesTab: React.FC = () => {
    const {datasetId, '*': path} = useParams()
    const [txId] = useDatasetConfigTx(datasetId)
    //const [path, setPath] = useState<string>('')

    const navigate = useNavigate()
    const setPath = useCallback((path: string) =>
        navigate(address_book.datasets.input_path(datasetId, path)), [datasetId])

    const {
        data,
        error,
        isLoading,
        isRefetching,
        refetch,
    } = useInternalDatasetsDatasetIdInputListListInputFiles({
        pathParams: {
            datasetId,
        },
        queryParams: {
            path,
        }
    })

    const {
        data: inis,
        isLoading: inisLoading,
        error: inisError,
    } = useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis({
        pathParams: {datasetId, txId}
    })

    const entries = useMemo<FileRow[]>(() => {
        if (data?.length >= 0) {
            return data?.map?.(f => ({
                name: f.path,  // FIXME?
                type: f.type,
                date: parseDate(f.date),
                size: f.size,
            }))
        }
    }, [data])

    const [isUploading, setIsUploading] = useState<boolean>(false)
    const [isCreatingFolder, setIsCreatingFolder] = useState<boolean>(false)

    const {isMobile} = useScreenSize()

    return <>
        <Modal
            centered
            open={isUploading}
            footer={null}
            onCancel={() => setIsUploading(false)}
        >
            <UploadForm onSuccess={() => setIsUploading(false)}/>
        </Modal>
        <Modal
            centered
            open={isCreatingFolder}
            footer={null}
            onCancel={() => setIsCreatingFolder(false)}
        >
            {isCreatingFolder && <FolderCreationForm
                onCreateRequested={async (name) => {
                    await fetchInternalDatasetsDatasetIdInputDirMkInputDir({
                        pathParams: {
                            datasetId,
                        },
                        queryParams: {
                            path: `${path}/${name}`,
                        }
                    })
                    await queryClient.invalidateQueries()
                    setIsCreatingFolder(false)
                }}
            />}
        </Modal>
        <MainContentSplit
            verticalLine
            keepNavAbove
            nav={<Sticky top={85}>
                <Stack rowGap="XL">
                    <Stack horizontal columnGap="S">
                        <span style={{whiteSpace: "nowrap"}}>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <Stack grow>
                            <Button
                                icon={<AddRegular/>}
                                appearance="primary"
                                label="Add..."
                                onClick={() => setIsUploading(true)}
                            />
                        </Stack>
                        <Stack grow>
                            <Button
                                icon={<FolderAddRegular/>}
                                label="Folder"
                                onClick={() => setIsCreatingFolder(true)}
                            />
                        </Stack>
                        <span style={{whiteSpace: "nowrap"}}>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    </Stack>
                    {!isMobile && <FoldersTree
                        currentPath={path}
                        rootLabel="/"
                        onNodeClick={node => navigate(address_book.datasets.input_path(datasetId, node.key?.toString?.()))}
                        listCallback={async (path: string) => {
                            const data = await fetchInternalDatasetsDatasetIdInputListListInputFiles({
                                pathParams: {
                                    datasetId,
                                },
                                queryParams: {
                                    path,
                                }
                            })
                            return data?.filter(f => f.type === 'folder')?.map?.(f => ({
                                name: f.path,  // FIXME?
                                type: f.type,
                                date: parseDate(f.date),
                                size: f.size,
                            }))
                        }}
                    />}
                    {!isMobile && <div style={{padding: '2em'}}>
                        <Stack>
                            <SFTPAccessButton/>
                            <AsyncActionButton
                                    label="Run Guest Job..."
                                    icon={<PlayFilled color="green"/>}
                                    onClick={async () => {
                                        navigate(address_book.datasets.new_guest_job(datasetId, ''))
                                    }}
                            />
                        </Stack>
                    </div>}
                    <br/>
                </Stack>
            </Sticky>}
        >
            <FilesBrowser
                rootLabel="/"
                path={path}
                onChDir={setPath}
                isLoading={isLoading || inisLoading}
                isRefetching={isRefetching}
                error={error ?? inisError}
                refetch={async () => {
                    await refetch()
                }}
                entries={entries}
                fileFetcher={path => fetchInternalDatasetsDatasetIdInputFilesGetInputFile({
                    pathParams: {
                        datasetId,
                    },
                    queryParams: {
                        path,
                    }
                })}
                overlayEntries={path === ROOT_PATH ? inis?.map?.(ini => ({
                    name: ini,
                    type: 'file',
                    isLink: true,
                })) : undefined}
                onOverlayActionsRender={path === ROOT_PATH ? row => {
                    return <Button
                        label="Open INI configuration"
                        onClick={() => navigate(address_book.datasets.ini.view_one(datasetId, row.name))}
                        appearance="primary"
                    />
                } : () => null}
                onFileDeleteRequested={async (row) => {
                    await fetchInternalDatasetsDatasetIdInputFilesRmInputFile({
                        pathParams: {
                            datasetId,
                        },
                        queryParams: {
                            path: `${path}/${row.name}`,
                        },
                    })
                    await refetch()
                }}
                onFileRenameRequested={async (row, new_name) => {
                    await fetchInternalDatasetsDatasetIdInputFilesRenameRenameInputFile({
                        pathParams: {
                            datasetId,
                        },
                        queryParams: {
                            path: `${path}/${row.name}`,
                            new_name,
                        },
                    })
                    await refetch()
                }}
            />
        </MainContentSplit>
    </>
}

export const UploadForm: React.FC<{
    onSuccess?: () => void
}> = props => {
    const {datasetId, '*': path} = useParams()

    const [dataFiles, setDataFiles] = useState<File[] | undefined>()

    const {acceptingFiles, filteringModal} = useNotIniFilesFilter(dataFiles, setDataFiles)

    const [progress, setProgress] = useState<number>(undefined)

    const queryClient = useQueryClient()

    return <>
        {filteringModal}
        <Stack rowGap="XL">
            <FilesField
                label="Select files from your device..."
                value={dataFiles}
                onChange={setDataFiles}
                minHeight={dataFiles?.length > 0 ? undefined : 100}
            />
            <Stack horizontal justifyContent="end" columnGap="XL" alignItems="center">
                <ProgressBar
                    hide={progress === undefined}
                    value={progress}
                />
                <AsyncActionButton
                    icon={<ArrowUploadRegular/>}
                    label="Upload"
                    onClick={async () => {
                        setProgress(0)
                        try {
                            const files = acceptingFiles?.slice?.() ?? []
                            // NOTE: this is async and the user may interfere so we use files, a copy of dataFiles.
                            for (let i = 0; i < files.length; ++i) {
                                const file = files[i]
                                if (file) {
                                    await fetchInternalDatasetsDatasetIdInputFilesPutInputFile({
                                        pathParams: {
                                            datasetId,
                                        },
                                        queryParams: {
                                            path: `${path}/${file.name}`,
                                        },
                                        ...mkFileUploadVariables(file),
                                    })
                                }
                                setProgress((i + 1) / files.length)
                            }
                            await queryClient.invalidateQueries()
                            props.onSuccess?.()
                            setDataFiles([])
                        } finally {
                            setProgress(undefined)
                        }
                    }}
                />
            </Stack>
        </Stack>
    </>
}