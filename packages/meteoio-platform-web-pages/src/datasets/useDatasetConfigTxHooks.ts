import {DependencyList, useEffect} from "react";

export interface ConfigTxListener {
    onBeforeApply?: () => Promise<void>
    onAfterApply?: () => Promise<void>
    onBeforeDiscard?: () => Promise<void>
    onAfterDiscard?: () => Promise<void>
}

const LISTENERS: ConfigTxListener[] = []

export function useDatasetConfigTxHooks(listener: ConfigTxListener, deps?: DependencyList) {
    useEffect(() => {
        LISTENERS.push(listener)
        return () => {
            const ind = LISTENERS.indexOf(listener)
            if (ind >= 0) {
                LISTENERS.splice(ind, 1)
            }
        }
    }, deps)
}

export const notifyListeners = async (ev: keyof ConfigTxListener) => {
    for (let i = 0; i < LISTENERS.length; i++) {
        const fn = LISTENERS[i][ev]
        if (fn) {
            await fn()
        }
    }
}
