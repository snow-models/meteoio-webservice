import * as React from 'react'
import {useMemo} from 'react'
import {Collapse, Empty} from "antd";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {useStyles as useDatasetRunOutFragStyles} from "./output/DatasetRunOutFrag";
import {Link, makeStyles, shorthands, Spinner, tokens} from "@fluentui/react-components";
import {
    DsRunDto,
    useInternalDatasetsDatasetIdAllRunsListAllRuns,
    usePublicJobSubmissionJobIdStatusStatus,
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {useHref, useParams} from "react-router-dom";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {address_book} from "../address_book";
import {IJobContext, JobContext} from "../jobs/job/JobContext";
import {Button} from "meteoio-ui/src/components/Button";
import {JobLogRegexes} from "../jobs/job/JobLogRegexes";
import {JobDsRunBanner} from "./JobDsRunBanner";

const useStyles = makeStyles({
    metadataLabels: {
        color: tokens.colorNeutralForeground3,
        fontSize: tokens.fontSizeBase200,
    },
    logCollapse: {
        '& > .ant-collapse-item > .ant-collapse-content > .ant-collapse-content-box': {
            ...shorthands.padding(0),
        }
    },
    latestHeader: {}
})

export const DatasetLogsTab: React.FC = () => {
    const {datasetId} = useParams()
    const {
        data: runsData,
        error: runsError,
        isLoading: runsLoading,
    } = useInternalDatasetsDatasetIdAllRunsListAllRuns({pathParams: {datasetId}})

    const styles = useStyles()
    const {isMobile} = useScreenSize()
    const inisHref = useHref(address_book.datasets.tab(datasetId, 'ini'))
    const cronHref = useHref(address_book.datasets.tab(datasetId, 'cron'))

    if (runsError) {
        return <MaybeErrorAlert error={runsError}/>
    }

    if (runsLoading) {
        return <Spinner/>
    }
    return <>
        <main>
            {runsData?.length >= 1 && <Collapse
                size="small"
                accordion
                rootClassName={styles.logCollapse}
                defaultActiveKey={isMobile ? undefined : runsData?.[0]?.job_id}
                destroyInactivePanel
                items={runsData?.map?.((run, i) => ({
                    key: run.job_id,
                    headerClass: i === 0 ? styles.latestHeader : undefined,
                    label: i === 0
                        ? <><b>Latest</b><small> (<RelativeTimeText date={run?.created_at}/>)</small></>
                        : <RelativeTimeText date={run?.created_at}/>,
                    children: <RunLogFrag run={run}/>,
                })) ?? []}
            /> || <>
                <Empty description={<>
                    No output data to show.<br/>
                    Go to <Link href={inisHref}>INI configurations</Link> to run a job, or setup <Link href={cronHref}>Cron
                    jobs</Link>.
                </>}/>
            </>}
        </main>
    </>
}

export const RunLogFrag: React.FC<{
    run: DsRunDto
}> = props => {
    const styles = useDatasetRunOutFragStyles()
    const jobId = props.run?.job_id
    const {data: status} = usePublicJobSubmissionJobIdStatusStatus({pathParams: {jobId}})
    const stats = undefined
    // const {data: stats} = usePublicJobSubmissionJobIdStatsStats({pathParams: {jobId}}, {
    //     enabled: !!status?.is_finished,
    // })
    const jobCtx = useMemo<IJobContext>(() =>
        ({jobId, status, stats}), [jobId, status, stats])

    const jobHref = useHref(address_book.jobs.view(jobId))

    return <>
        <JobContext.Provider value={jobCtx}>
            <div className={styles.headerButtonsContainer}>
                <div className={styles.headerButtons}>
                    <Button
                        size="small"
                        href={jobHref}
                        label="See job details"
                        target="_blank"
                    />
                </div>
            </div>
            {/*<JobHeading isInDsLayout/>*/}
            {/*<div style={{opacity: 0.5, height: 10, display: "block"}} />*/}
            <JobLogRegexes which="stdout"/>
            <JobDsRunBanner isInDsLayout assertIsDsRun/>
        </JobContext.Provider>
    </>
}
