import {LocationRegular, ScalesRegular, TriangleRegular} from "@fluentui/react-icons";
import {Text, tokens} from "@fluentui/react-components";
import * as React from "react";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {DatasetHeading} from "meteoio-platform-client";


export const DatasetMetadataLabels: React.FC<{
    ds: DatasetHeading
}> = props => {
    return <>
        {props.ds?.locations?.length > 0 && <CHStack icon={<LocationRegular/>}>
            {props.ds?.locations?.length === 1 && props.ds?.locations?.[0]?.latitude &&
            props.ds?.locations?.[0]?.longitude && <>
                {props.ds?.locations?.[0]?.latitude?.toFixed(6)},&nbsp;
                {props.ds?.locations?.[0]?.longitude?.toFixed(6)}
            </>}
            {props.ds?.locations?.length > 1 && <>
                {props.ds?.locations?.length} locations
            </>}
        </CHStack>}

        <DsElevationRange ds={props.ds}/>

        {props.ds?.licenses?.length >= 1 && <CHStack icon={<ScalesRegular/>}>
            {props.ds?.licenses?.map?.(l => l.name)?.join(", ")}
        </CHStack>}
    </>;
}

const DsElevationRange: React.FC<{ ds: DatasetHeading }> = props => {
    const hh = props.ds?.locations
        ?.map?.(loc => loc.elevation)
        ?.filter?.(h => h > 0)
    if (hh?.length <= 0) {
        return null
    }
    const min = Math.min(...hh).toFixed(0)
    const max = Math.max(...hh).toFixed(0)
    const max_n = Math.max(...hh)
    if (max_n < 1000) {
        return
    }
    if (min === max) {
        return <CHStack icon={<TriangleRegular/>}>
            {hh?.[0]}m a.s.l.
        </CHStack>
    }
    return <CHStack icon={<TriangleRegular/>}>
        {min}-{max}m a.s.l.
    </CHStack>
}

export const CHStack: React.FC<React.PropsWithChildren<{
    icon?: React.ReactNode
}>> = props => {
    return <Stack horizontal alignItems="center" columnGap="S">
        <Text size={400} style={{color: tokens.colorNeutralForeground3}}>{props.icon}</Text>
        <span>{props.children}</span>
    </Stack>
}
