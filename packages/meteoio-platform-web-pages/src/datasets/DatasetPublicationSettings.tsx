import * as React from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {
    fetchInternalDatasetsDatasetIdAccessSetAccess,
    useInternalDatasetsDatasetIdAccessGetAccess
} from "meteoio-platform-client";
import {useHref, useParams} from "react-router-dom";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Link, makeStyles, shorthands, Text, tokens} from "@fluentui/react-components";
import {RepositoryAccess, RepositoryAccessPill} from "meteoio-ui/src/components/RepositoryAccessPill";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {ControlLabel} from "meteoio-ui/src/components/ControlLabel";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {useQueryClient} from "../_common/backend";
import {address_book} from "../address_book";

const useStyles = makeStyles({
    box: {
        ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke2),
        ...shorthands.borderRadius(tokens.borderRadiusMedium),
        ...shorthands.padding(tokens.spacingVerticalL, tokens.spacingHorizontalL)
    }
})

export const DatasetPublicationSettings: React.FC = () => {
    const {datasetId} = useParams()
    const {
        data: access_level,
        error,
        isLoading
    } = useInternalDatasetsDatasetIdAccessGetAccess({pathParams: {datasetId}})

    const styles = useStyles()
    const dialogs = useDialogs()
    const queryClient = useQueryClient()
    const publicHref = useHref(address_book.explore.view(datasetId))

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    if (isLoading) {
        return <Spinner tall/>
    }
    return <>
        <br/>
        <Stack rowGap="XXXL" className={styles.box}>
            <Stack rowGap="XS">
                <ControlLabel>Current visibility</ControlLabel>
                <Stack horizontal columnGap="XL">
                    <span style={{transform: 'scale(1.2)', transformOrigin: 'left center'}}>
                        <RepositoryAccessPill access={access_level?.id as RepositoryAccess}/>
                    </span>
                    {access_level?.id === 'public' && <Link href={publicHref}>link</Link>}
                </Stack>
            </Stack>
            {access_level?.id === 'private' && <Stack>
                <Text>You can publish this dataset to make it readable by public visitors.</Text>
                <AsyncActionButton
                    label="Publish now"
                    onClick={async () => {
                        if (!await dialogs.confirm('Are you sure you want to make this dataset public?', {okText: 'Confirm'})) {
                            return
                        }
                        await fetchInternalDatasetsDatasetIdAccessSetAccess({
                            pathParams: {
                                datasetId,
                            },
                            queryParams: {
                                access: 'public'
                            }
                        })
                        await queryClient.invalidateQueries()
                    }}
                />
            </Stack>}
            {access_level?.id === 'public' && <Stack>
                <Text>You can withdraw the publication if you have made a mistake.</Text>
                <AsyncActionButton
                    label="Make private now"
                    onClick={async () => {
                        if (!await dialogs.confirm('Are you sure you want to make this dataset private?', {okText: 'Confirm'})) {
                            return
                        }
                        await fetchInternalDatasetsDatasetIdAccessSetAccess({
                            pathParams: {
                                datasetId,
                            },
                            queryParams: {
                                access: 'private'
                            }
                        })
                        await queryClient.invalidateQueries()
                    }}
                />
            </Stack>}
        </Stack>
        <br/>
        <br/>
    </>
}
