import {FilesList} from "meteoio-ui/src/components/FilesList";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Button} from "meteoio-ui/src/components/Button";
import {Modal} from "meteoio-ui/src/components/Modal";
import * as React from "react";
import {Dispatch, SetStateAction, useEffect, useState} from "react";
import {isLikelyAnINI} from "../../_common/parseINI";


/**
 * NOTE: this is to be used with isLikelyAnINI, which employs a cache.
 * @param dataFiles
 * @param setDataFiles
 * @param acceptanceCriteria this should be isLikelyAnINI or its negation
 * @param acceptanceWarning
 */
function _useIniFilesFilter_generic(
    dataFiles: File[],
    setDataFiles: Dispatch<SetStateAction<File[]>>,
    acceptanceCriteria: (file: File) => Promise<boolean>,
    acceptanceWarning: React.ReactNode,
) {
    const [acceptingFiles, setAcceptingFiles] = useState<File[] | undefined>([])
    const [ignoringFiles, setIgnoringFiles] = useState<File[] | undefined>([])

    useEffect(() => {
        (async () => {
            for (let i = 0; i < dataFiles?.length ?? 0; i++) {
                await acceptanceCriteria(dataFiles?.[i])  // let it cache
            }
            // reset the lists after loading the cache to avoid flashing
            setAcceptingFiles([])
            setIgnoringFiles([])
            // let's use the cache
            for (let i = 0; i < dataFiles?.length ?? 0; i++) {
                const file = dataFiles?.[i]
                if (await acceptanceCriteria(file)) {
                    setAcceptingFiles(ff => [...ff, file])
                } else {
                    setIgnoringFiles(ff => [...ff, file])
                }
            }
        })();
    }, [dataFiles])

    const filteringModal = <Modal
        open={ignoringFiles?.length > 0}
        footer={null}
        closable={false}
    >
        {acceptanceWarning}
        <FilesList
            hideHeading
            size="extra-small"
            files={ignoringFiles?.map?.(file => ({
                name: file.name,
                size: file.size,
                date: new Date(file.lastModified),
                type: 'file',
            })) ?? []}
        />
        <br/>
        <Stack horizontal justifyContent="end">
            <Button
                label="Use anyway"
                appearance="subtle"
                onClick={() => {
                    setAcceptingFiles(ff => [...ff, ...ignoringFiles])
                    setIgnoringFiles([])
                }}
            />
            <Button
                label="Skip"
                appearance="primary"
                onClick={() => {
                    setDataFiles(ff => ff.filter(f => !ignoringFiles.includes(f)))
                    setIgnoringFiles([])
                }}
            />
        </Stack>
    </Modal>

    return {filteringModal, acceptingFiles}
}

export function useIniFilesFilter(
    dataFiles: File[],
    setDataFiles: Dispatch<SetStateAction<File[]>>,
) {
    return _useIniFilesFilter_generic(
        dataFiles,
        setDataFiles,
        isLikelyAnINI,
        <p>
            The following files do not look like INI configurations:
        </p>,
    )
}

export function useNotIniFilesFilter(
    dataFiles: File[],
    setDataFiles: Dispatch<SetStateAction<File[]>>,
) {
    return _useIniFilesFilter_generic(
        dataFiles,
        setDataFiles,
        async file => !(await isLikelyAnINI(file)),
        <p>
            The following files look like INI configurations:
        </p>,
    )
}
