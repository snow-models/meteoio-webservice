import * as React from 'react'
import {RepositoryIcon} from "meteoio-ui/src/components/RepositoryIcon";
import {Link, tokens} from "@fluentui/react-components";
import {HandleBreadcrumb} from "meteoio-ui/src/components/HandleBreadcrumb";
import {RepositoryAccessIcon} from "meteoio-ui/src/components/RepositoryAccessIcon";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {useDataset} from "../../_common/backend";
import {useHref, useParams} from "react-router-dom";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {address_book} from "../../address_book";
import {DatasetHeading} from "meteoio-platform-client";
import {RepositoryAccess} from "meteoio-ui/src/components/RepositoryAccessPill";
import {useDatasetConfigTxHooks} from "../useDatasetConfigTxHooks";

export const DatasetTitle: React.FC<{
    size?: 'large'
    datasetId?: string
}> = props => {
    const {datasetId: datasetId_param} = useParams()
    const datasetId = props.datasetId ?? datasetId_param
    const {data: dataset, isLoading, refetch} = useDataset(datasetId)
    const datasetHomeHref = useHref(address_book.datasets.view(datasetId))

    useDatasetConfigTxHooks({
        onAfterApply: async () => {
            await refetch()
        }
    }, [refetch])

    // TODO: use list entry instead of heading only

    return <ImmediateDatasetTitle
        size={props.size}
        dataset={dataset}
        href={datasetHomeHref}
        isLoading={isLoading}
    />
}

export const ImmediateDatasetTitle: React.FC<{
    size?: 'large' | 'extra-large'
    isLoading?: boolean
    dataset?: DatasetHeading
    href?: string
    access?: RepositoryAccess
}> = props => {
    const {dataset, isLoading, href} = props

    const {isMobile} = useScreenSize()

    return <>
        <Stack horizontal alignItems="center" columnGap="S">
            {(!isMobile || props.size === 'extra-large') &&
                <RepositoryIcon
                    fontSize={props.size === 'large' ? tokens.fontSizeBase500 : (props.size === 'extra-large' ? tokens.fontSizeBase600 : tokens.fontSizeBase400)}/>}
            {isLoading && <Spinner/>}
            <Link
                href={href}
                onClick={() => {
                    document?.scrollingElement?.scrollTo?.({top: 0, left: 0, behavior: 'smooth'})
                }}
            >
                <HandleBreadcrumb handle={dataset?.title ?? ''}
                                  textProps={{
                                      size: props.size === 'large' ? 400 : (props.size === 'extra-large' ? 600 : 300),
                                      weight: 'regular',
                                      wrap: false,
                                      block: true,
                                      truncate: true
                                  }}/>
            </Link>
            <RepositoryAccessIcon access={props.access}/>
        </Stack>
    </>
}
