export const getSSHUsername = (userId: string, datasetId: string) => {
    // disseminated c2794b16-b049-426c-99c2-f0012c0533a8
    return `${userId}-d-${datasetId}`
}
