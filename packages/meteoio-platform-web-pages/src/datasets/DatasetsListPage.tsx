import * as React from 'react'
import {useMemo} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Button} from "meteoio-ui/src/components/Button";
import {address_book} from "../address_book";
import {AddRegular} from "@fluentui/react-icons";
import {useHref} from "react-router-dom";
import {DatasetCard} from "./DatasetCard";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {useInternalDatasetsList} from "meteoio-platform-client";
import {Empty} from "antd";

export const DatasetsListPage: React.FC = () => {
    const hrefCreate = useHref(address_book.datasets.create)
    const {data: datasets_, isLoading, error} = useInternalDatasetsList({})
    const datasets = useMemo(() => {
        if (datasets_?.length === undefined) {
            return undefined
        }
        const dss = datasets_.slice()
        dss.reverse()
        return dss
    }, [datasets_])

    if (error) {
        return <>
            <MaybeErrorAlert error={error}/>
        </>
    }

    if (isLoading) {
        return <>
            <Spinner tall/>
        </>
    }

    return <>
        <br/>
        <Stack rowGap="XL" maxWidth={600} center>
            <Stack horizontal wrap justifyContent="space-between">
                <PageTitle>
                    Your datasets
                </PageTitle>
                <Stack horizontal>
                    <Button
                        icon={<AddRegular/>}
                        size="small"
                        appearance="subtle"
                        href={hrefCreate}
                        label="Create a new Dataset"
                    />
                </Stack>
            </Stack>

            {datasets?.map?.(ds => <DatasetCard key={ds.id} ds={ds}/>)}

            {datasets?.length === 0 && <Empty description="You have no datasets."/>}
        </Stack>
        <br/>
    </>
}
