import * as React from 'react'
import {useMemo} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {DatasetMetadataLabels} from "./DatasetMetadataLabels";
import {RepositoryCard} from "meteoio-ui/src/components/RepositoryCard";
import {useHref} from "react-router-dom";
import {address_book} from "../address_book";
import {Link} from "@fluentui/react-components";
import {Handle} from "meteoio-ui/src/components/Handle";
import {DatasetListEntry} from "meteoio-platform-client";

export const DatasetCard: React.FC<{
    ds: DatasetListEntry
}> = props => {
    const ds = props.ds

    useMemo(() => {
        if (ds.heading == null) {
            console.warn(`Uninitialized dataset ${ds.id}`)
        }
    }, [ds])

    if (ds.heading == null) {
        return null
    }

    return <>
        <RepositoryCard
            key={ds.id}
            heading={<DsLink name={ds.heading?.title} dsId={ds.id}/>}
            access={ds.access}
            description={ds.heading?.description}
            metadata={<Stack horizontal wrap alignItems="center" rowGap="XXS">
                {ds?.heading && <DatasetMetadataLabels ds={ds?.heading}/>}
            </Stack>}
        />
    </>
}

export const DsLink: React.FC<{
    dsId: string
    name: string
}> = props => {
    const href = useHref(address_book.datasets.view(props.dsId))
    // TODO
    return <>
        <Link href={href}>
            <Handle handle={props.dsId} label={props.name}/>
        </Link>
    </>
}
