import * as React from 'react'
import {useMemo, useState} from 'react'
import {
    DsRunDto,
    usePublicJobSubmissionJobIdAccessLevelGetJobAccessLevel,
    usePublicJobSubmissionJobIdStatusStatus
} from "meteoio-platform-client";
import {JobStraceOutputFiles} from "../../jobs/job/Tab_Output";
import {IJobContext, JobContext} from "../../jobs/job/JobContext";
import {useHref} from "react-router-dom";
import {address_book} from "../../address_book";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Button} from "meteoio-ui/src/components/Button";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {Modal} from "meteoio-ui/src/components/Modal";
import {makeStyles, tokens} from "@fluentui/react-components";
import {DatasetRerunForm} from "./DatasetRerunForm";
import {PlayFilled} from "@fluentui/react-icons";

export const useStyles = makeStyles({
    headerButtonsContainer: {
        display: 'flex',
        flexDirection: 'row',
        height: '38px',
        marginTop: '-38px',
        lineHeight: '38px',
        fontSize: tokens.fontSizeBase200,
        boxSizing: 'border-box',
        paddingRight: '1em',
        textAlign: 'right',
        justifyContent: 'end',
        alignItems: 'center',
    },
    headerButtons: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative',
        columnGap: '0.66em',
        zIndex: 2,
    }
})

export const DatasetRunOutFrag: React.FC<{
    data?: DsRunDto
}> = props => {

    const styles = useStyles()

    const jobAccess = usePublicJobSubmissionJobIdAccessLevelGetJobAccessLevel({pathParams: {jobId: props?.data?.job_id}})
    const jobStatus = usePublicJobSubmissionJobIdStatusStatus({pathParams: {jobId: props?.data?.job_id}})

    const ctx = useMemo<IJobContext>(() => ({
        jobId: props.data.job_id,
        status: jobStatus.data,
    }), [props.data?.job_id, jobStatus?.data])

    const {isMobile} = useScreenSize()

    const jobHref = useHref(address_book.jobs.view(props.data?.job_id))

    const [isRerunModalOpen, setIsRerunModalOpen] = useState<boolean>(false)

    return <>
        <Modal
                open={isRerunModalOpen}
                footer={null}
                onCancel={() => setIsRerunModalOpen(false)}
        >
            {isRerunModalOpen && <DatasetRerunForm runData={props.data}/>}
        </Modal>
        <JobContext.Provider value={ctx}>
            <div className={styles.headerButtonsContainer}>
                <div className={styles.headerButtons}>
                    {props.data?.ini_name && <Button
                            size="small"
                            label="Rerun..."
                            icon={<PlayFilled color="green"/>}
                            onClick={() => setIsRerunModalOpen(true)}
                    />}
                    {jobAccess?.data?.access_level === 'full' && <Button
                        size="small"
                        href={jobHref}
                        label="See details"
                        target="_blank"
                    />}
                </div>
            </div>
            <Stack>
                <JobStraceOutputFiles/>
            </Stack>
        </JobContext.Provider>
    </>
}