import * as React from 'react'
import {useState} from 'react'
import {useNavigate} from "react-router-dom";
import {
    DsRunDto,
    fetchPublicDatasetsDatasetIdLoadGuestJobFilesForRerunLoadGuestJobFilesForRerun,
    fetchPublicJobSubmissionCreate,
    fetchPublicJobSubmissionJobIdStartStart,
    TimeSeriesJobParams,
} from "meteoio-platform-client";
import {useJobsHistory} from "../../jobs/useJobsHistory";
import {address_book} from "../../address_book";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Button} from "meteoio-ui/src/components/Button";
import {TimeSeriesJobParamsEditor} from "../../_common/TimeSeriesJobParamsEditor";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Alert} from "meteoio-ui/src/components/Alert";


// Heavily inspired from GuestJobFromDataset, which is heavily inspired from JobSubmissionPage.

export const DatasetRerunForm: React.FC<{
    runData: DsRunDto
}> = props => {

    const datasetId = props.runData?.dataset_id
    const ini_name = props.runData?.ini_name || undefined

    const [params, setParams] = useState<Partial<TimeSeriesJobParams>>(() => ({
        ini: ini_name,
        range: {
            end: 'NOW',
            duration_days: 30,
        }
    }))

    const [isSubmitPending, setIsSubmitPending] = useState<boolean>(false)
    const [jobId_pre, setJobId_pre] = useState<string | undefined>()
    const [error, setError] = useState<Error>()

    const navigate = useNavigate()
    const {add: addToHistory} = useJobsHistory()

    const maySubmit = true

    // TODO: refine layout with margins and maybe use `<main>` after refining also that (no flex but margin 0 auto)
    return <>
        <Stack rowGap="XXXL" center>
            <Stack rowGap="XL">
                <PageTitle>
                    Guest Job Submission
                </PageTitle>
                <Alert level="info">
                    You can get a sample of the dataset using this tool.
                    It will run a new MeteoIO job on the last available data and configuration.
                </Alert>
            </Stack>
            <Stack rowGap="M">
                <TimeSeriesJobParamsEditor
                        value={params}
                        onChange={setParams}
                        iniFileNames={[ini_name]}
                />
            </Stack>

            <MaybeErrorAlert error={error}/>

            <Stack horizontal justifyContent="right">
                <Button
                        size="large"
                        wide
                        appearance="primary"
                        label={<>
                            Launch Job
                            {isSubmitPending && <>
                                &nbsp; &nbsp;
                                <Spinner size="tiny"/>
                            </>}
                        </>}
                        disabled={isSubmitPending || !maySubmit}
                        onClick={async () => {
                            setIsSubmitPending(true)
                            try {
                                // Let's introduce some artificial delay to guarantee some visual feedback.
                                const a_delay = new Promise<void>(resolve => setTimeout(resolve, 1500))

                                let jobId = jobId_pre
                                if (!jobId) {
                                    const job = await fetchPublicJobSubmissionCreate({})
                                    jobId = job.id
                                    setJobId_pre(jobId)
                                }

                                await fetchPublicDatasetsDatasetIdLoadGuestJobFilesForRerunLoadGuestJobFilesForRerun({
                                    pathParams: {datasetId},
                                    queryParams: {
                                        job_id: jobId,
                                    },
                                })

                                await fetchPublicJobSubmissionJobIdStartStart({
                                    pathParams: {jobId},
                                    body: params as TimeSeriesJobParams
                                })

                                await new Promise<void>(resolve => setTimeout(resolve, 100))
                                await a_delay
                                // In the meantime, the job is being queued and will be processing.

                                addToHistory({
                                    id: jobId,
                                    submitted: new Date(),
                                })

                                navigate(address_book.jobs.view(jobId))  // TODO?: new dedicated page for ds rerun job?
                            } catch (e) {
                                console.trace(e)
                                setError(e)
                            } finally {
                                setIsSubmitPending(false)
                            }
                        }}
                />
            </Stack>
        </Stack>
    </>
}