import * as React from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {useHref, useParams} from "react-router-dom";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Divider, Link, makeStyles, shorthands, Spinner, tokens} from "@fluentui/react-components";
import {PersonRegular} from "@fluentui/react-icons";
import {CHStack, DatasetMetadataLabels} from "../DatasetMetadataLabels";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {Collapse, Empty} from "antd";
import {
    useInternalDatasetsDatasetIdRead,
    useInternalDatasetsDatasetIdReleasedRunsListRuns
} from "meteoio-platform-client";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {DatasetRunOutFrag} from "./DatasetRunOutFrag";
import {address_book} from "../../address_book";

const useStyles = makeStyles({
    metadataLabels: {
        color: tokens.colorNeutralForeground3,
        fontSize: tokens.fontSizeBase200,
    },
    filesCollapse: {
        '& > .ant-collapse-item > .ant-collapse-content > .ant-collapse-content-box': {
            ...shorthands.padding(0),
        }
    },
    latestHeader: {
    }
})

export const DatasetOutputTab: React.FC = () => {
    const {datasetId} = useParams()
    const {
        data: dataset,
        error: datasetError,
        isLoading: dsLoading
    } = useInternalDatasetsDatasetIdRead({pathParams: {datasetId}})

    const styles = useStyles()
    const {isMobile} = useScreenSize()
    const inisHref = useHref(address_book.datasets.tab(datasetId, 'ini'))
    const cronHref = useHref(address_book.datasets.tab(datasetId, 'cron'))

    const {
        data: runsData,
        error: runsError,
        isLoading: runsLoading,
    } = useInternalDatasetsDatasetIdReleasedRunsListRuns({pathParams: {datasetId}})

    if (datasetError || runsError) {
        return <MaybeErrorAlert error={datasetError ?? runsError}/>
    }

    if (dsLoading || runsLoading) {
        return <Spinner/>
    }

    return <>
        <main>
            {runsData?.length >= 1 && <Collapse
                size="small"
                accordion
                rootClassName={styles.filesCollapse}
                defaultActiveKey={isMobile ? undefined : runsData?.[0]?.job_id}
                destroyInactivePanel
                items={runsData?.map?.((run, i) => ({
                    key: run.job_id,
                    headerClass: i === 0 ? styles.latestHeader : undefined,
                    label: i === 0
                        ? <><b>Latest</b><small> (<RelativeTimeText date={run?.created_at}/>)</small></>
                        : <RelativeTimeText date={run?.created_at}/>,
                    children: <DatasetRunOutFrag data={run}/>,
                })) ?? []}
            /> || <>
                <Empty description={<>
                    No output data to show.<br/>
                    Go to <Link href={inisHref}>INI configurations</Link> to run a job, or setup <Link href={cronHref}>Cron
                    jobs</Link>.
                </>}/>
            </>}
        </main>
        <aside>
            <Stack rowGap="XL">
                <Stack rowGap="XS">
                    <PageSectionTitle>About</PageSectionTitle>
                    <span>{dataset.description}</span>
                </Stack>
                <Stack rowGap="XS" className={styles.metadataLabels}>
                    <DatasetMetadataLabels ds={dataset}/>
                </Stack>
                <Divider/>
                {dataset?.creators?.length > 0 && <Stack rowGap="XS">
                    <PageSectionTitle>Creator{dataset?.creators?.length > 1 ? 's' : ''}</PageSectionTitle>
                    {dataset?.creators?.map?.((c, i) =>
                        <CHStack key={i} icon={c.type === 'person' && <PersonRegular/>}>
                            {c.email
                                ? <Link href={`mailto:${c.email}`}
                                        title={`${c.full_name} <${c.email}>`}>{c.full_name}</Link>
                                : <span>{c.full_name}</span>}
                        </CHStack>)}
                </Stack>}
            </Stack>
        </aside>
    </>
}
