import * as React from 'react'
import {useEffect, useState} from 'react'
import {useNavigate, useParams} from "react-router-dom";
import {useDatasetConfigTx} from "./useDatasetConfigTx";
import {
    fetchInternalDatasetsDatasetIdLoadGuestJobFilesLoadGuestJobFiles,
    fetchPublicJobSubmissionCreate,
    fetchPublicJobSubmissionJobIdStartStart,
    TimeSeriesJobParams,
    useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis
} from "meteoio-platform-client";
import {useJobsHistory} from "../jobs/useJobsHistory";
import {address_book} from "../address_book";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Button} from "meteoio-ui/src/components/Button";
import {TimeSeriesJobParamsEditor} from "../_common/TimeSeriesJobParamsEditor";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Alert} from "meteoio-ui/src/components/Alert";

export const GuestJobFromDataset: React.FC = () => {
    const {datasetId, ini_name} = useParams()
    const [txId, {isHead}] = useDatasetConfigTx(datasetId)

    const [params, setParams] = useState<Partial<TimeSeriesJobParams>>(() => ({
        ini: ini_name || undefined,
        range: {
            end: 'NOW',
            duration_days: 30,
        }
    }))

    const {
        data: inis,
        error: inisError,
        isLoading: inisLoading
    } = useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis({
        pathParams: {datasetId, txId}
    })
    useEffect(() => {
        if (inis?.length === 1 && params.ini === undefined) {
            setParams(v => ({...v, ini: inis?.[0]}))
        }
    }, [inis])

    const [isSubmitPending, setIsSubmitPending] = useState<boolean>(false)
    const [jobId_pre, setJobId_pre] = useState<string | undefined>()
    const [error, setError] = useState<Error>()

    const navigate = useNavigate()
    const {add: addToHistory} = useJobsHistory()

    const maySubmit = !!params?.ini

    // TODO: refine layout with margins and maybe use `<main>` after refining also that (no flex but margin 0 auto)
    return <>
        <Stack rowGap="XXXL" width={500} maxWidth="90%" center>
            <Stack rowGap="XL">
                <PageTitle>
                    Guest Job Submission
                </PageTitle>
                <div>
                    <Alert level="info">
                        A copy of the current files will be used for the job.
                        The output will not override the dataset and will only be accessible as job result.
                    </Alert>
                    {!isHead && <Alert level="warning">
                        The INI configuration will be from your pending revision.
                    </Alert>}
                </div>
            </Stack>
            <Stack rowGap="M">
                <TimeSeriesJobParamsEditor
                    value={params}
                    onChange={setParams}
                    iniFileNames={inis}
                    inisError={inisError}
                    inisLoading={inisLoading}
                />
            </Stack>

            <MaybeErrorAlert error={error}/>

            <Stack horizontal justifyContent="right">
                <Button
                    size="large"
                    wide
                    appearance="primary"
                    label={<>
                        Launch Job
                        {isSubmitPending && <>
                            &nbsp; &nbsp;
                            <Spinner size="tiny"/>
                        </>}
                    </>}
                    disabled={isSubmitPending || !maySubmit}
                    onClick={async () => {
                        setIsSubmitPending(true)
                        try {
                            let jobId = jobId_pre
                            if (!jobId) {
                                const job = await fetchPublicJobSubmissionCreate({})
                                jobId = job.id
                                setJobId_pre(jobId)
                            }

                            await fetchInternalDatasetsDatasetIdLoadGuestJobFilesLoadGuestJobFiles({
                                pathParams: {datasetId},
                                queryParams: {
                                    job_id: jobId,
                                    tx_id: txId,
                                    ini_name,
                                }
                            })

                            await fetchPublicJobSubmissionJobIdStartStart({
                                pathParams: {jobId},
                                body: params as TimeSeriesJobParams
                            })

                            // Let's introduce some artificial delay to guarantee some visual feedback.
                            await new Promise<void>(resolve => setTimeout(resolve, 100))
                            // In the meantime, the job is being queued and will be processing.

                            addToHistory({
                                id: jobId,
                                submitted: new Date(),
                            })

                            navigate(address_book.jobs.view(jobId))  // TODO: new dedicated page for ds job
                        } catch (e) {
                            console.trace(e)
                            setError(e)
                        } finally {
                            setIsSubmitPending(false)
                        }
                    }}
                />
            </Stack>

            <br/>
        </Stack>
    </>
}