import * as React from 'react'
import {useCallback, useMemo} from 'react'
import {HeaderMainAsideLayout} from "meteoio-ui/src/layouts/HeaderMainAsideLayout";
import {Route, Routes, useMatch, useNavigate, useParams} from "react-router-dom";
import {ITab, TabListWithOverflow} from "meteoio-ui/src/components/TabListWithOverflow";
import {
    bundleIcon,
    ChevronRightRegular,
    CodeTextFilled,
    CodeTextRegular,
    DocumentBulletListFilled,
    DocumentBulletListRegular,
    FolderFilled,
    FolderListFilled,
    FolderListRegular,
    FolderRegular,
    SettingsFilled,
    SettingsRegular,
    ShiftsFilled,
    ShiftsRegular,
    HistoryFilled,
    HistoryRegular,
} from "@fluentui/react-icons";
import {address_book} from "../address_book";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {DatasetOutputTab} from "./output/DatasetOutputTab";
import {DatasetIniTab} from "./ini/DatasetIniTab";
import {DatasetIniIndex} from "./ini/DatasetIniIndex";
import {DatasetSourcesTab} from "./DatasetSourcesTab";
import {DatasetCronTab} from "./DatasetCronTab";
import {DatasetLogsTab} from "./DatasetLogsTab";
import {MainTopBar} from "../_common/MainTopBar";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {useDataset} from "../_common/backend";
import {ContentTabT, DatasetConfigTxBanner} from "./DatasetConfigTxBanner";
import {DatasetSettingsTabRoot} from "./DatasetSettingsTabRoot";
import {GuestJobFromDataset} from "./GuestJobFromDataset";
import {DatasetTitle} from "./_common/DatasetTitle";
import {DatasetIniRevisionsList} from "./ini/revisions/DatasetIniRevisionsList";

const OutputIcon = bundleIcon(FolderFilled, FolderRegular)
const IniIcon = bundleIcon(DocumentBulletListFilled, DocumentBulletListRegular)
//const InputIcon = bundleIcon(DatabaseFilled, DatabaseRegular)
//const InputIcon = bundleIcon(DatabaseMultipleFilled, DatabaseMultipleRegular)
//const InputIcon = bundleIcon(FolderListFilled, FolderListRegular)
const InputIcon = bundleIcon(FolderListFilled, FolderListRegular)
//const CronIcon = bundleIcon(TabDesktopArrowClockwiseFilled, TabDesktopArrowClockwiseRegular)
const CronIcon = bundleIcon(ShiftsFilled, ShiftsRegular)
//const LogsIcon = bundleIcon(FeedFilled, FeedRegular)
const LogsIcon = bundleIcon(CodeTextFilled, CodeTextRegular)
const SettingsIcon = bundleIcon(SettingsFilled, SettingsRegular)
const RevisionsIcon = bundleIcon(HistoryFilled, HistoryRegular)

const ALL_TABS: (ITab & { value: ContentTabT })[] = [
    {
        value: 'settings',
        children: 'Settings',
        icon: <SettingsIcon/>,
    },
    {
        value: 'input',   // This derives from design reiterations -- we are talking about mixed input and output
        children: 'Source Data',
        icon: <InputIcon/>,
    },
    {
        value: 'ini',
        children: 'INI configuration',
        icon: <IniIcon/>,
    },
    {
        value: 'revisions',
        children: 'Versions',
        icon: <RevisionsIcon/>,
    },
    {
        value: 'cron',
        children: 'Cron',
        icon: <CronIcon/>,
    },
    {
        value: 'logs',
        children: 'Logs',
        icon: <LogsIcon/>,
    },
    {
        value: 'output',
        children: 'Output',
        icon: <OutputIcon/>,
    },
]

// TODO?: add tab for revisions (find appropriate icon, remove ini/revisions route, add new route, add it to address_book)

const HOME_TAB_VALUE: ContentTabT = 'input'  // See also the component used for <Route index={true} .../>

export const DatasetLayout: React.FC = () => {
    const {datasetId} = useParams()
    const navigate = useNavigate()
    const match = useMatch('*')

    const {isLoading, error} = useDataset(datasetId)

    const tabs = ALL_TABS

    const selectedTabValue = useMemo<ContentTabT | undefined>(() =>
            tabs.find(tab => match?.pathname?.startsWith?.(address_book.datasets.tab(datasetId, tab.value)))?.value
            ?? (match?.pathname == address_book.datasets.view(datasetId) ? HOME_TAB_VALUE : null)
        , [datasetId, match, tabs])

    const setSelectedTabValue = useCallback((tabValue: string) => {
        navigate(address_book.datasets.tab(datasetId, tabValue))
    }, [])

    if (error) {
        return <>
            <MainTopBar showHamburger/>
            <MaybeErrorAlert error={error}/>
        </>
    }

    if (isLoading) {
        return <>
            <MainTopBar showHamburger/>
            <Spinner tall/>
        </>
    }

    return <>
        <MainTopBar
            bg="nb3"
            showHamburger
            startExtra={<>
                <ChevronRightRegular/>
                <DatasetTitle size="large"/>
            </>}
        />
        <HeaderMainAsideLayout
            // header={<RepositoryHeader handle={dataset?.name ?? ''} access="private"/>}
            nav={<>
                <TabListWithOverflow
                    tabs={tabs}
                    selectedTabValue={selectedTabValue}
                    onTabSelect={setSelectedTabValue}
                />
            </>}
            alert={<DatasetConfigTxBanner forContentTab={selectedTabValue}/>}
            body={<Routes>
                <Route index={true} Component={DatasetSourcesTab}/>
                <Route path={"output"} Component={DatasetOutputTab}/>
                <Route path={"revisions"} Component={DatasetIniRevisionsList}/>
                <Route path={"ini"} Component={DatasetIniTab}>
                    <Route index Component={DatasetIniIndex}/>
                    <Route path={"files/"} Component={DatasetIniIndex}/>
                    <Route path={"files/:iniName"} Component={DatasetIniIndex}/>
                    {/*<Route path={"revisions/new"} Component={DatasetIniRevisionCreation}/>*/}
                    {/*<Route path={"revisions/:revId"} Component={DatasetIniRevision}/>*/}
                </Route>
                <Route path={"input/*"} Component={DatasetSourcesTab}/>
                <Route path={"cron"} Component={DatasetCronTab}/>
                <Route path={"cron/:cron_job_id"} Component={DatasetCronTab}/>
                <Route path={"logs"} Component={DatasetLogsTab}/>
                <Route path={"settings/*"} Component={DatasetSettingsTabRoot}/>
                <Route path={"new_guest_job"} Component={GuestJobFromDataset}/>
                <Route path={"new_guest_job/:ini_name"} Component={GuestJobFromDataset}/>
            </Routes>}
        />
    </>
}