import {useCallback, useEffect, useState} from 'react'

// NOTE: It's not advisable to implement a React Context,
//       as only one config_tx shall be used at a time
//       and no read (or rw) access on other txs shall be eased to the user.
//       This also simplifies the implementation.
//       It will still be possible to reset to head in order to pause the editing.
//       The server is also able to list the pending transactions so we don't rely on the client session only.
// TODO?: It may be possible to refactor all of this as a middleware of the backend query client.

const _key = (dataset_id: string) => `DatasetConfigTx-${dataset_id}`

const _setter = (dataset_id: string, config_tx: string | null) => {
    if (config_tx) {
        window?.localStorage?.setItem(_key(dataset_id), config_tx)
    } else {
        window?.localStorage?.removeItem(_key(dataset_id))
    }
}

const _getter = (dataset_id: string) => {
    return window?.localStorage?.getItem(_key(dataset_id))
}

interface DatasetConfigTxListener {
    dataset_id: string,
    onTxChange: (txId: string) => void
}

const LISTENERS: DatasetConfigTxListener[] = []

/** This is to avoid leaving the browser session storage in some inconsistent state while changing tx,
 * i.e. an async network operation which may accidentally get aborted by closing the browser window. */
export const prepareForChangingTx = (dataset_id: string) => {
    _setter(dataset_id, null)
}

export function useDatasetConfigTx(dataset_id: string):
    [string, { isHead: boolean, setConfigTxId: (value: string) => void, resetConfigTxId: () => void }] {
    const [tx, setTx_] = useState<string | null>(() => _getter(dataset_id))

    useEffect(() => {
        setTx_(_getter(dataset_id))
        const handler: DatasetConfigTxListener = {
            dataset_id,
            onTxChange: setTx_,
        }
        LISTENERS.push(handler)
        return () => {
            LISTENERS.splice(LISTENERS.indexOf(handler), 1)
        }
    }, [dataset_id])

    const setTx = useCallback<(value: string) => void>(value => {
        _setter(dataset_id, value)
        setTx_(value)
        LISTENERS.forEach(listener => listener.onTxChange(value))
    }, [dataset_id])

    const reset = useCallback(() => {
        setTx(null)
    }, [dataset_id])

    const isHead = tx === null || tx === undefined || tx === 'head' || tx === 'null'

    return [isHead ? 'head' : tx, {
        isHead,
        setConfigTxId: setTx,
        resetConfigTxId: reset,
    }]
}
