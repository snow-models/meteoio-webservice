import {Alert} from "meteoio-ui/src/components/Alert";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {BinRecycleRegular, CheckmarkCircleRegular} from "@fluentui/react-icons";
import * as React from "react";
import {useMemo} from "react";
import {Link, useParams} from "react-router-dom";
import {prepareForChangingTx, useDatasetConfigTx} from "./useDatasetConfigTx";
import {
    fetchInternalDatasetsDatasetIdConfigTxsTxIdDeleteConfigWriteTx,
    fetchInternalDatasetsDatasetIdRevisionsTxIdMessageGetRevision
} from "meteoio-platform-client";
import {notifyListeners} from "./useDatasetConfigTxHooks";
import {useQueryClient} from "../_common/backend";
import {address_book} from "../address_book";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {Modal} from "meteoio-ui/src/components/Modal";
import {useDsRevisionMsgForm} from "./ini/revisions/useDsRevisionMsgForm";


export type ContentTabT = 'output' | 'ini' | 'input' | 'cron' | 'logs' | 'settings' | 'revisions'

const UNTRACKED_CONTENT_TABS: ContentTabT[] = [
    'output', 'input', 'logs'
]

const VERSIONING_CONTENT_TABS: ContentTabT[] = [
    'ini', 'cron', 'settings'
]


export const DatasetConfigTxBanner: React.FC<{
    forContentTab: ContentTabT
}> = props => {
    const {datasetId} = useParams()
    const [txId, {isHead, resetConfigTxId}] = useDatasetConfigTx(datasetId)
    const queryClient = useQueryClient()

    const isUntracked = useMemo<boolean>(() =>
        UNTRACKED_CONTENT_TABS.includes(props.forContentTab), [props.forContentTab])

    const isVersioning = useMemo<boolean>(() =>
        VERSIONING_CONTENT_TABS.includes(props.forContentTab) || props.forContentTab == 'revisions', [props.forContentTab])

    const dialogs = useDialogs()

    const revisionMsgForm = useDsRevisionMsgForm({withConfirmBtn: true})

    if (isHead) {
        return null
    }

    if (isUntracked) {
        return <Alert
            level="info"
        >
            {/*This is not in versioning.*/}
            Changes here are immediately effective and not tracked.
            You also have a pending revision for
            {VERSIONING_CONTENT_TABS.map((tab, i) => <>
                {i === 0 ? ' ' : ', '}
                <Link to={address_book.datasets.tab(datasetId, tab)}>{tab}</Link>
            </>)}.
        </Alert>
    }

    if (isVersioning) {
        return <Alert
            level="warning"
            end={<Stack horizontal alignItems="center" justifyContent="end" grow>
                <AsyncActionButton
                    size="small"
                    label="Discard"
                    appearance="subtle"
                    icon={<BinRecycleRegular/>}
                    onClick={async () => {
                        if (!await dialogs.confirm('Are you sure to want to discard this revision?', {
                            okText: 'Discard',
                            okType: 'danger',
                        })) {
                            return
                        }
                        await notifyListeners('onBeforeDiscard')
                        prepareForChangingTx(datasetId)
                        await fetchInternalDatasetsDatasetIdConfigTxsTxIdDeleteConfigWriteTx({
                            pathParams: {datasetId, txId},
                        })
                        resetConfigTxId()
                        await queryClient.invalidateQueries()
                        await notifyListeners('onAfterDiscard')
                        dialogs.message.success({
                            content: 'Pending revision discarded.'
                        })
                    }}
                />
                <AsyncActionButton
                    size="small"
                    label="Confirm changes..."
                    appearance="secondary"
                    icon={<CheckmarkCircleRegular/>}
                    onClick={async () => {
                        const pending_rev = await fetchInternalDatasetsDatasetIdRevisionsTxIdMessageGetRevision({
                            pathParams: {
                                datasetId,
                                txId,
                            }
                        })
                        revisionMsgForm.open(pending_rev)
                    }}
                />
                {revisionMsgForm.isOpen && <Modal
                    centered
                    footer={null}
                    onCancel={() => revisionMsgForm.close()}
                    closable={!revisionMsgForm.hasChanges}
                    open
                    title="New version"
                >
                    {revisionMsgForm.formNode}
                </Modal>}
            </Stack>}
        >
            You have a pending revision on this dataset.
        </Alert>;
    }

    return null
}
