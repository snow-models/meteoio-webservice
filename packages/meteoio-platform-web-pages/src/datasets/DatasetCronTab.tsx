import * as React from 'react'
import {useCallback, useEffect, useState} from 'react'
import {CronEditor} from "meteoio-ui/src/components/CronEditor";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {DropdownField} from "meteoio-ui/src/components/DropdownField";
import {InfoBox} from "meteoio-ui/src/components/InfoBox";
import {makeRandomUuid} from "meteoio-ui/src/utils/makeRandomUUID";
import {Sticky} from "meteoio-ui/src/components/Sticky";
import {Collapse, Empty} from 'antd'
import {CronHumanFrag} from "meteoio-ui/src/components/CronHumanFrag";
import {Badge, Text} from '@fluentui/react-components'
import {AddRegular, BinRecycleRegular, EditRegular} from "@fluentui/react-icons";
import {useNavigate, useParams} from "react-router-dom";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {
    CronJob,
    fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdCronJobsCreateCronJob,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdCronJobsJobIdDeleteCronJob,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdCronJobsJobIdUpdateCronJob,
    TimeSeriesJobParams,
    useInternalDatasetsDatasetIdConfigTxsTxIdCronJobsListCronJobs,
    useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis
} from "meteoio-platform-client";
import {useDatasetConfigTx} from "./useDatasetConfigTx";
import {TimeSeriesJobParamsEditor} from "../_common/TimeSeriesJobParamsEditor";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {useStateTransitionCallback} from "meteoio-ui/src/hooks/useStateTransitionEffect";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {address_book} from "../address_book";


export const DatasetCronTab: React.FC = () => {
    const {datasetId, cron_job_id} = useParams()
    const [txId, {isHead, setConfigTxId}] = useDatasetConfigTx(datasetId)
    const {data, error, isLoading, refetch} = useInternalDatasetsDatasetIdConfigTxsTxIdCronJobsListCronJobs({
        pathParams: {datasetId, txId}
    })

    const {data: inis} = useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis({
        pathParams: {datasetId, txId}
    })

    //const [openKey, setOpenKey] = useState<string | string[]>(undefined)
    const navigate = useNavigate()
    const openKey = cron_job_id
    const setOpenKey = useCallback((openKey: string) => navigate(address_book.datasets.cron(datasetId, openKey)), [datasetId])

    const [cronJobs, setCronJobs] = useState<CronJob[] | undefined>()
    useStateTransitionCallback(txId, () => {
        setCronJobs(data)
        refetch().then()
    }, [data])
    useEffect(() => {
        if (isHead || (cronJobs === undefined)) {
            setCronJobs(data)
        }
    }, [data, cronJobs, isHead])

    const [pushError, setPushError] = useState<Error>()

    const dialogs = useDialogs()

    if (isLoading || (cronJobs === undefined && !error)) {
        return <Spinner tall/>
    }
    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    return <>
        <main>
            <Stack rowGap="L">
                <MaybeErrorAlert error={pushError}/>
                {cronJobs?.length > 0 && <Collapse
                    activeKey={openKey}
                    accordion
                    onChange={setOpenKey}
                    items={cronJobs?.map?.(job => ({
                        key: job.id,
                        label: <Stack horizontal justifyContent="space-between">
                            <Text block truncate>
                                <Badge appearance="tint">
                                    <code style={{fontSize: '0.8em'}}>{job.id?.slice?.(-8)}</code>
                                </Badge>
                                {/*<code style={{fontSize: '0.8em', color: tokens.colorBrandForegroundLinkPressed}}>{job.id?.slice?.(-8)}</code>*/}
                                &nbsp;
                                &nbsp;
                                <CronHumanFrag cron={job.cron}/>
                                {/*{WORKFLOWS.find(wf => wf.option.value === job.workflow)?.option?.label}*/}
                            </Text>
                            <Stack horizontal>
                                {!isHead && <AsyncActionButton
                                    appearance="secondary"
                                    danger
                                    size="small"
                                    icon={<BinRecycleRegular/>}
                                    onClick={async () => {
                                        if (!await dialogs.confirm('Are you sure you want to remove this cron job?')) {
                                            return
                                        }
                                        await fetchInternalDatasetsDatasetIdConfigTxsTxIdCronJobsJobIdDeleteCronJob({
                                            pathParams: {
                                                datasetId,
                                                txId,
                                                jobId: job.id,
                                            }
                                        })
                                        await refetch()
                                        setCronJobs(cronJobs?.filter?.(j => j.id !== job.id))
                                    }}
                                />}
                            </Stack>
                        </Stack>,
                        children: <DatasetCronjobEditor
                            disabled={isHead}
                            value={job}
                            onChange={newJob => {
                                if (isHead) {
                                    return
                                }
                                setCronJobs(cronJobs?.map?.(oldJob => oldJob.id === newJob.id ? newJob : oldJob) ?? [newJob])
                                if (!newJob.id?.startsWith?.('uncreated')) {
                                    fetchInternalDatasetsDatasetIdConfigTxsTxIdCronJobsJobIdUpdateCronJob({
                                        pathParams: {
                                            datasetId,
                                            txId,
                                            jobId: newJob.id,
                                        },
                                        body: newJob,
                                    }).catch(setPushError)
                                }
                            }}
                        />,
                    }))}
                />}

                {cronJobs?.length <= 0 && <Empty description="No cron jobs"/>}

            </Stack>
            <br/>
        </main>
        <aside>
            <Sticky top={75}>
                <Stack>
                    {isHead && <AsyncActionButton
                        icon={<EditRegular/>}
                        label="Start editing..."
                        onClick={async () => {
                            // await new Promise(resolve => setTimeout(resolve, 300))
                            // throw new Error('Err')
                            const txIdDto = await fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx({
                                pathParams: {datasetId}
                            })
                            setConfigTxId(txIdDto.id)
                        }}
                    />}
                    {!isHead && <AsyncActionButton
                        label="Create a new Cron Job"
                        appearance="primary"
                        icon={<AddRegular/>}
                        onClick={async () => {
                            const newJob: CronJob = {
                                cron: '*/10 * * * *',
                                type: 'meteoio_timeseries',
                                params: {
                                    ini: inis?.[0],
                                    range: {
                                        duration_days: 30,
                                        end: 'NOW',
                                    },
                                    resolution_minutes: null,
                                },
                                id: 'uncreated' + makeRandomUuid(),
                            }
                            try {
                                const jobIdDto = await fetchInternalDatasetsDatasetIdConfigTxsTxIdCronJobsCreateCronJob({
                                    pathParams: {
                                        datasetId,
                                        txId,
                                    },
                                    body: newJob,
                                })
                                // Store the id in the client list
                                newJob.id = jobIdDto.id
                                setCronJobs(jobs => [...jobs, newJob])
                                setOpenKey(newJob.id)
                            } catch (e) {
                                console.trace(e)
                                setPushError(e)
                                setCronJobs(undefined)
                                refetch().then()
                            }
                        }}
                    />}
                </Stack>
                <br />
                <InfoBox>
                    A cron job is a command or script that is scheduled to run at specific intervals or
                    fixed times (e.g., every hour, daily, weekly).
                    By using this page, users can schedule cron jobs to run.
                    The system reads this configuration periodically and triggers the execution of the
                    corresponding cron jobs based on their schedules.
                    Take a look at the Logs to monitor their execution.
                </InfoBox>
            </Sticky>
        </aside>
    </>
}

export const DatasetCronjobEditor: React.FC<{
    disabled?: boolean
    value?: CronJob
    onChange?: (value: CronJob) => void
}> = props => {
    // TODO: decompose CronJob value into the following vars
    const {datasetId} = useParams() // TODO: remove router dependency
    const [txId] = useDatasetConfigTx(datasetId)
    const {data: inis, isLoading: inisLoading, error: inisErr} = useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis({
        pathParams: {datasetId, txId}
    })

    // useEffect(() => {
    //     setCron(props.value.cron)
    //     setWorkflowOptValue(props.value.workflow)
    //     setJobParams(props.value.jobParams)
    //     setIniFileName(props.value.ini)
    // }, [props.value.cron, props.value.workflow, props.value.jobParams, props.value.ini])
    // NOTE: the above effect would generate a re-rendering loop.
    // TODO: fix above state handling allowing for reactive updates also from outside.

    return <>
        <Stack rowGap="L">
            <CronEditor
                readOnly={props.disabled}
                label="Schedule"
                value={props.value?.cron}
                onChange={value => {
                    props.onChange?.({...props.value, cron: value})
                }}
            />
            <DropdownField
                label={"Workflow"}
                options={[{label: 'MeteoIO Timeseries', value: 'meteoio_timeseries'}]}
                value={props.value.type}
                onChange={value => {
                    props.onChange?.({...props.value, type: value})
                }}
            />
            <Stack rowGap="S">
                <TimeSeriesJobParamsEditor
                    iniFileNames={inis}
                    inisError={inisErr}
                    inisLoading={inisLoading}
                    value={props.value.params}
                    onChange={value => {
                        props.onChange?.({...props.value, params: value as TimeSeriesJobParams})
                    }}
                />
            </Stack>
        </Stack>
    </>
}