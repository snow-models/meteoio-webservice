import * as React from 'react'
import {SimpleProperties} from "meteoio-ui/src/components/SimpleProperties";
import {
    DATASET_CREATION_PROPS_SCHEMA,
    DatasetCreationProps,
    draftToMetaConfig,
    metaConfigToDraft
} from "./DatasetCreationPage";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {
    fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdMetaGetMeta,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdMetaSetMeta,
} from "meteoio-platform-client";
import {useDatasetConfigTx} from "./useDatasetConfigTx";
import {useParams} from "react-router-dom";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {EditRegular} from "@fluentui/react-icons";
import {useDatasetConfigTxDraftPusher} from "./useDatasetConfigTxDraftPusher";

export const DatasetHeadingSettings: React.FC = () => {
    const {datasetId} = useParams()
    const [txId, {isHead, setConfigTxId}] = useDatasetConfigTx(datasetId)

    return <>
        <Stack rowGap="XL" width={500} maxWidth="90%" center>
            {isHead && <AsyncActionButton
                icon={<EditRegular/>}
                label="Start editing..."
                onClick={async () => {
                    const res = await fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx({
                                    pathParams: {
                                        datasetId,
                                    }
                                })
                                setConfigTxId(res.id)
                            }}
                        />}
                        <HeadingEditor key={txId}/>
                        <br/>
                        <br/>
                    </Stack>
    </>
}

export const HeadingEditor: React.FC = () => {
    const {datasetId} = useParams()
    const [txId, {isHead}] = useDatasetConfigTx(datasetId)

    const [draft, setDraft, {error, isLoading}] = useDatasetConfigTxDraftPusher<Partial<DatasetCreationProps>>(
        `cf70dd7d-c28e-4575-913e-d87c8cd70cdb-ds-${datasetId}-tx-${txId}`,
        isHead ? undefined : async (draft) => {
            await fetchInternalDatasetsDatasetIdConfigTxsTxIdMetaSetMeta({
                pathParams: {datasetId, txId},
                body: draftToMetaConfig(draft)
            })
        },
        async () => {
            const data = await fetchInternalDatasetsDatasetIdConfigTxsTxIdMetaGetMeta({
                pathParams: {
                    datasetId,
                    txId,
                },
            })
            return metaConfigToDraft(data)
        })

    if (isLoading) {
        return <Spinner tall/>
    }

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    return <>
        <SimpleProperties
            schema={DATASET_CREATION_PROPS_SCHEMA}
            value={draft}
            onChange={isHead ? undefined : setDraft}
            disabled={isHead}
        />
    </>
}
