import * as React from 'react'
import {useEffect, useState} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {SimpleProperties, SimplePropertiesSchema} from "meteoio-ui/src/components/SimpleProperties";
import {RepositoryAccessPill} from "meteoio-ui/src/components/RepositoryAccessPill";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {Button} from "meteoio-ui/src/components/Button";
import {
    fetchInternalDatasetsCreate,
    fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdApplyApplyConfigWriteTx,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameCreateIni,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdMetaSetMeta,
    MetaConfig
} from "meteoio-platform-client";
import {useDatasetConfigTx} from "./useDatasetConfigTx";
import {useNavigate} from "react-router-dom";
import {address_book} from "../address_book";
import {mkFileUploadVariables} from "../_common/backend";
import {FilesField} from "meteoio-ui/src/components/FilesField";
import {ArrowEnterUpRegular} from "@fluentui/react-icons";
import {field2str, KNOWN_FIELDS, parseINI} from "../_common/parseINI";
import {useIniFilesFilter} from "./_common/useIniFilesFilter";
import {Field} from "@fluentui/react-components";
import {ControlLabel} from "meteoio-ui/src/components/ControlLabel";
import {FieldDescription} from "meteoio-ui/src/components/FieldDescription";


/*
 *   What are we doing here?
 *     - Collect INI files for upload in config directory
 *     - Parse INI files to retrieve some fields to be used for pre-filling the following form
 *     - Suggest some INI fields that were not found in the INIs, allow continue without them
 *     - Show a form with schema as DatasetCreationProps (pre-filled from above)
 *     - A button to start the dataset creation procedure
 */


export interface DatasetCreationProps {
    name: string
    description: string
    license: string,
    creator_name: string,
    creator_email: string,
}

export const DATASET_CREATION_PROPS_SCHEMA: SimplePropertiesSchema<DatasetCreationProps> = {
    name: {
        title: "Name",
        description: "Unique name for this dataset -- keep it short",
        type: "line",
        required: true,
    },
    description: {
        title: "Description",
        description: "Brief description of the dataset purpose or contents",
        type: "paragraph",
    },
    license: {
        title: 'License',
        description: 'License name or URL applicable to the dataset',
        type: 'line',
    },
    creator_name: {
        title: 'Creator name',
        description: 'The full name of the creator of the dataset',
        type: 'line',
    },
    creator_email: {
        title: 'Creator email',
        description: 'The email of the creator',
        type: 'line',
    },
}

export const draftToMetaConfig: (draft: Partial<DatasetCreationProps>) => MetaConfig = draft => ({
    heading: {
        title: draft.name,
        description: draft.description,
        creators: (draft.creator_name || draft.creator_email) ? [{
            full_name: draft.creator_name,
            email: draft.creator_email,
            type: 'person'
        }] : [],
        licenses: draft.license ? [{name: draft.license}] : [],
    },
})

export const metaConfigToDraft: (data: MetaConfig) => Partial<DatasetCreationProps> = data => ({
    name: data.heading?.title,
    description: data.heading?.description,
    creator_name: data.heading?.creators?.[0]?.full_name,
    creator_email: data.heading?.creators?.[0]?.email,
    license: data.heading?.licenses?.[0]?.name,
})

const makeDraftFromFiles: (dataFiles: File[]) => Promise<[Partial<DatasetCreationProps>, string[]]> = async dataFiles => {
    const draft: Partial<DatasetCreationProps> = {}
    const inis = dataFiles  //.filter(file => file.name?.toLowerCase?.()?.endsWith?.('ini'))
    const min_missing = new Set<string>(KNOWN_FIELDS.map(field2str))
    for (let i = 0; i < inis.length; i++) {
        const file = inis[i]
        try {
            const text = await file.slice(0, 1_000_000).text()  // NOTE: reading only the first 1MB
            const {parsed, foundFields} = parseINI(text)
            foundFields.forEach(f => min_missing.delete(field2str(f)))
            draft.name = draft.name ?? parsed.title
            draft.description = draft.description ?? parsed.summary
            draft.creator_name = draft.creator_name ?? parsed.creator_name
            draft.creator_email = draft.creator_email ?? parsed.creator_email
            draft.license = draft.license ?? parsed.license
        } catch (e) {
            console.trace(`Could not parse file ${file?.name}`, e)
        }
    }
    return [draft, [...min_missing]]
}

export const DatasetCreationPage: React.FC = () => {
    const [dataFiles, setDataFiles] = useState<File[] | undefined>([])
    // dataFiles? Well, they are INI configs...

    // const iniFileNames = useMemo(() => dataFiles
    //     ?.filter?.(file => file?.name?.toLowerCase?.()?.endsWith?.('ini'))
    //     ?.map(file => file.name), [dataFiles])

    const [draft, setDraft] = useState<Partial<DatasetCreationProps>>({})
    const [missingFields, setMissingFields] = useState<string[] | undefined>(undefined)
    const [pre_datasetId, setDatasetId] = useState<string | undefined>()
    const [pre_txId, {isHead, setConfigTxId}] = useDatasetConfigTx(pre_datasetId)
    const navigate = useNavigate()

    const {acceptingFiles: iniFiles, filteringModal} = useIniFilesFilter(dataFiles, setDataFiles)

    useEffect(() => {
        if (iniFiles?.length >= 1) {
            let cancelled = false
            makeDraftFromFiles(iniFiles).then(([filling, missing]) => {
                if (!cancelled) {
                    setDraft(draft => ({...draft, ...filling}))
                    setMissingFields(missing)
                }
            })
            return () => {
                cancelled = true
            }
        }
    }, [iniFiles])

    const [continueWithUndefinedFields, setContinueWithUndefinedFields] = useState<boolean>(false)

    const showForm = continueWithUndefinedFields || missingFields?.length <= 0

    const canCreate = !!draft?.name && iniFiles?.length >= 1 && showForm

    return <>
        <br/>
        <Stack center maxWidth={600} rowGap="XL">
            <PageTitle>Create a new Dataset</PageTitle>

            <Stack rowGap="M">
                <FilesField
                    label="INI configuration files"
                    value={dataFiles}
                    onChange={setDataFiles}
                    minHeight={dataFiles?.length > 0 ? undefined : 100}
                />
                {!(dataFiles?.length > 0) && <span style={{opacity: 0.8}}>
                        <ArrowEnterUpRegular/>
                    &nbsp; &nbsp;
                    Add an INI file to continue.
                </span>}

                {filteringModal}

                {iniFiles?.length > 0 && showForm && <>
                    <SimpleProperties
                        schema={DATASET_CREATION_PROPS_SCHEMA}
                        value={draft}
                        onChange={setDraft}
                    />
                    <FieldDescription
                        field={<Field
                            required
                            label={<ControlLabel>Visibility</ControlLabel>}
                        >
                            <RepositoryAccessPill access="private"/>
                        </Field>}
                        description="You will later be able to publish the dataset."
                    />
                </>}

                {!showForm && missingFields?.length > 0 && <Stack rowGap="S">
                    <span>The following attributes were not found in the INI configuration files:</span>
                    <ul>
                        {missingFields?.map?.(f => <li key={f}><code>{f}</code></li>)}
                    </ul>
                    <Stack horizontal justifyContent="end">
                        <Button
                            label="Continue anyway"
                            onClick={() => {
                                setContinueWithUndefinedFields(true)
                            }}
                        />
                    </Stack>
                </Stack>}
            </Stack>


            <Stack horizontal justifyContent="end">
                {showForm && <AsyncActionButton
                    disabled={!canCreate}
                    label="Create"
                    appearance="primary"
                    onClick={async () => {
                        let datasetId = pre_datasetId
                        let txId = pre_txId
                        if (!datasetId) {
                            const idDto = await fetchInternalDatasetsCreate({})
                            datasetId = idDto.id
                            txId = null
                            setDatasetId(datasetId)
                        }
                        if (!txId || isHead) {
                            const txIdDto = await fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx({
                                pathParams: {datasetId}
                            })
                            txId = txIdDto.id
                            setConfigTxId(txId)
                        }
                        for (let i = 0; i < iniFiles.length; ++i) {
                            const _file = iniFiles[i]
                            await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameCreateIni({
                                pathParams: {
                                    datasetId,
                                    name: _file.name,
                                    txId,
                                },
                                ...mkFileUploadVariables(_file)
                            })
                        }
                        await fetchInternalDatasetsDatasetIdConfigTxsTxIdMetaSetMeta({
                            pathParams: {datasetId, txId},
                            body: draftToMetaConfig(draft)
                        })
                        setConfigTxId(undefined)  // The txId is no longer valid after application
                        await fetchInternalDatasetsDatasetIdConfigTxsTxIdApplyApplyConfigWriteTx({
                            pathParams: {datasetId, txId},
                        })
                        navigate(address_book.datasets.view(datasetId))
                    }}
                />}
            </Stack>
            <br/>
        </Stack>
        <br/>
    </>
}
