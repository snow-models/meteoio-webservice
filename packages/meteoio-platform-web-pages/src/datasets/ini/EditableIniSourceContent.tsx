import * as React from 'react'
import {useState} from 'react'
import {
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameUpdateIni
} from "meteoio-platform-client";
import {useParams} from "react-router-dom";
import {useDatasetConfigTx} from "../useDatasetConfigTx";
import {getMaybeErrorMessageString, MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Text} from "@fluentui/react-components";
import CodeINIEditor from "meteoio-ui/src/components/CodeINIEditor";
import {mkFileUploadVariables, queryClient} from "../../_common/backend";
import {useDatasetConfigTxDraftPusher} from "../useDatasetConfigTxDraftPusher";
import {isLikelyAnINI} from "../../_common/parseINI";
import {App} from "antd";

export const EditableIniSourceContent: React.FC<{
    iniName: string
    // editingSessionParams?: IniEditSessionParams
}> = props => {
    const {datasetId} = useParams()
    const [txId, {isHead}] = useDatasetConfigTx(datasetId)
    const {modal} = App.useApp()
    const [showCode, setShowCode] = useState<boolean | null>(null)
    const [draftCode, setDraftCode, status] = useDatasetConfigTxDraftPusher(
        `71ad9f7e-e7d4-449c-8df7-92efb7566eff-${datasetId}-${props.iniName}`,  // NOTE: in case of changes, search for shared usage of the uuid.
        isHead ? undefined : async (draftCode: string) => {
            try {
                // console.debug({file})
                await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameUpdateIni({
                    pathParams: {
                        datasetId: datasetId,
                        txId,
                        name: props.iniName,
                    },
                    ...mkFileUploadVariables(draftCode)
                })
                await queryClient.invalidateQueries()
                setDraftCode(null)
            }
            catch(e) {
                modal.error({
                    content: getMaybeErrorMessageString(e)
                })
            }
        },
        async () => {
            setShowCode(null)
            const data = await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni({
                pathParams: {
                    datasetId,
                    txId,
                    name: props.iniName,
                },
            })
            if (!(await isLikelyAnINI(data))) {
                setShowCode(false)
            }
            setShowCode(true)
            return await (data as Blob)?.text?.()
        }
    )

    if (status?.error) {
        return <MaybeErrorAlert error={status.error}/>
    }

    if (status?.isLoading) {
        return <Spinner tall/>
    }

    if (showCode !== true) {
        return <Text block italic align="center">
            Preview not available
        </Text>
    }

    return <>
        <CodeINIEditor
            initialValue={draftCode}
            onChange={setDraftCode}
            readOnly={isHead}
        />
    </>
}
