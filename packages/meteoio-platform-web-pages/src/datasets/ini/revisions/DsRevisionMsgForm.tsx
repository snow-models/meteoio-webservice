import * as React from "react";
import {useRef} from "react";
import {DatasetRevisionMsg} from "meteoio-platform-client";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Button, Input, Popover, PopoverSurface, PopoverTrigger, Textarea} from "@fluentui/react-components";
import {ControlLabel} from "meteoio-ui/src/components/ControlLabel";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";

export const DsRevisionMsgForm: React.FC<{
    draft: Partial<DatasetRevisionMsg>
    onTitleChange: (title: string) => void
    onMsgChange: (message: string) => void
    hasChanges: boolean
    onSaveClick: () => Promise<void>
    onCancelClick: () => Promise<void>
    withConfirmBtn?: boolean
    onConfirmClick?: () => Promise<void>
}> = props => {
    const msgTextareaRef = useRef<HTMLTextAreaElement | null>(null)
    return <Stack rowGap="L">
        <Stack rowGap="S">
            <ControlLabel>Revision message</ControlLabel>
            <Input
                placeholder="Insert a title here..."
                value={props.draft.title ?? ''}
                onChange={ev => props.onTitleChange(ev.target.value)}
                style={{fontWeight: '600'}}
                onKeyDown={ev => {
                    if(ev.key == 'Enter') {
                        ev.preventDefault()
                        msgTextareaRef?.current?.focus?.()
                    }
                }}
            />
            <Textarea
                ref={msgTextareaRef}
                placeholder="Insert a message here..."
                resize="vertical"
                rows={5}
                value={props.draft.message ?? ''}
                onChange={ev => props.onMsgChange(ev.target.value)}
            />
        </Stack>
        <Stack horizontal columnGap="S">
            {props.withConfirmBtn && <AsyncActionButton
                appearance="primary"
                label="Save and Confirm"
                onClick={props.onConfirmClick}
            />}
            <AsyncActionButton
                appearance={props.withConfirmBtn ? "secondary" : "primary"}
                label={props.withConfirmBtn ? "Save draft" : "Save"}
                disabled={!props.hasChanges}
                onClick={props.onSaveClick}
            />
            <Popover positioning="below" withArrow>
                <PopoverTrigger>
                    <Button
                        disabled={!props.hasChanges}
                        appearance="subtle"
                    >
                        Cancel
                    </Button>
                </PopoverTrigger>
                <PopoverSurface>
                    Are you sure?
                    <br />
                    <br />
                    <AsyncActionButton
                        appearance="secondary"
                        danger
                        label="Yes, close without saving."
                        disabled={!props.hasChanges}
                        onClick={props.onCancelClick}
                    />
                </PopoverSurface>
            </Popover>
        </Stack>
    </Stack>;
}