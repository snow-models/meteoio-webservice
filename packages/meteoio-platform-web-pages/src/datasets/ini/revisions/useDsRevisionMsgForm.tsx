import * as React from "react";
import {useCallback, useState} from "react";
import {
    DatasetRevisionMsg,
    DatasetRevisionsListEntry,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdApplyApplyConfigWriteTx,
    fetchInternalDatasetsDatasetIdRevisionsTxIdMessagePutRevisionMessage
} from "meteoio-platform-client";
import {useParams} from "react-router-dom";
import {DsRevisionMsgForm} from "./DsRevisionMsgForm";
import {useQueryClient} from "../../../_common/backend";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {notifyListeners} from "../../useDatasetConfigTxHooks";
import {prepareForChangingTx, useDatasetConfigTx} from "../../useDatasetConfigTx";

export function useDsRevisionMsgForm(props: {
    withConfirmBtn?: boolean
}) {
    const {datasetId} = useParams()
    const [_, {resetConfigTxId}] = useDatasetConfigTx(datasetId)
    const [revId, setRevId] = useState<string | null>(null)

    const [isOpen, setIsOpen] = useState<boolean>(false)
    const [draft, setDraft] = useState<Partial<DatasetRevisionMsg>>(() => ({}))
    const [unmodifiedDraft, setUnmodifiedDraft] = useState<object>(null)
    const hasChanges = draft && (draft !== unmodifiedDraft)

    const open = useCallback((row: DatasetRevisionsListEntry) => {
        const draft = {
            title: row.title,
            message: row.message,
        }
        setRevId(row.id)
        setDraft(draft)
        setUnmodifiedDraft(draft)
        setIsOpen(true)
    }, [])

    const close = useCallback((notWithChanges: boolean = true) => {
        if (hasChanges && notWithChanges) {
            return  // Do nothing, ignore request.
            // confirm('Discard changes?', {
            //     type: 'confirm',
            // }).then(answer => {
            //     if (answer) {
            //         setIsOpen(false)
            //     }
            // })
        }
        setRevId(null)
        setDraft({})
        setUnmodifiedDraft(null)
        setIsOpen(false)
    }, [hasChanges])

    const queryClient = useQueryClient()

    const dialogs = useDialogs()

    const store = useCallback(async () => {
        await fetchInternalDatasetsDatasetIdRevisionsTxIdMessagePutRevisionMessage({
            pathParams: {
                datasetId,
                txId: revId,
            },
            body: draft
        })
        setUnmodifiedDraft(draft)
    }, [revId, draft, datasetId])

    const formNode = isOpen && <DsRevisionMsgForm
        draft={draft}
        onTitleChange={title => setDraft(d => ({...d, title}))}
        onMsgChange={message => setDraft(d => ({...d, message}))}
        hasChanges={hasChanges}
        onSaveClick={async () => {
            await store()
            await queryClient.invalidateQueries()
            dialogs.message.success({
                content: 'Revision message updated.'
            })
            close(false)
        }}
        onCancelClick={async () => {
            close(false)
        }}
        withConfirmBtn={props.withConfirmBtn}
        onConfirmClick={async () => {
            if (!await dialogs.confirm('Are you sure to want to apply this revision?', {
                okText: 'Apply',
            })) {
                return
            }
            await store();
            close(false)
            await notifyListeners('onBeforeApply')
            prepareForChangingTx(datasetId)
            await fetchInternalDatasetsDatasetIdConfigTxsTxIdApplyApplyConfigWriteTx({
                pathParams: {datasetId, txId: revId},
            })
            resetConfigTxId()
            await queryClient.invalidateQueries()
            await notifyListeners('onAfterApply')
            dialogs.message.success({
                content: 'New version created.'
            })
        }}
    />

    return {
        formNode,
        isOpen,
        open,
        close,
        hasChanges,
    }
}