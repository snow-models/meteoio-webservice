import * as React from 'react'
import {useState} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {SimpleProperties, SimplePropertiesSchema,} from "meteoio-ui/src/components/SimpleProperties";
import {Button} from "meteoio-ui/src/components/Button";
import {FilesField} from "meteoio-ui/src/components/FilesField";
import {FieldDescription} from "meteoio-ui/src/components/FieldDescription";

export type CommitMessage = Record<'title' | 'message', string>

const PROPS: SimplePropertiesSchema<CommitMessage> = {
    title: {
        required: true,
        type: 'line',
        title: 'Title',
        description: 'Brief summary of purpose or nature of the changes'
    },
    message: {
        required: false,
        type: 'paragraph',
        title: 'Message',
        description: 'Optional, more detailed explanation of the changes made'
    }
}

export const DatasetIniRevisionCreation: React.FC = props => {
    const [commitMsg, setCommitMsg] = useState<Partial<CommitMessage>>()
    return <>
        <Stack>
            <FieldDescription
                field={<FilesField
                    fieldProps={{required: true}}
                    label="Upload files"
                />}
                description={"Files that shall be uploaded and override the existing ones"}
            />
            <SimpleProperties
                schema={PROPS}
                value={commitMsg}
                onChange={setCommitMsg}
            />
            <Stack horizontal justifyContent="start">
                <Button
                    appearance="primary"
                    label="Commit"
                />
            </Stack>
        </Stack>
    </>
}
