import * as React from 'react'
import {Badge, Text, tokens} from "@fluentui/react-components";
import {Timeline} from 'antd'
import {
    useInternalDatasetsDatasetIdRevisionsGetRevisions,
    useInternalDatasetsDatasetIdRevisionsTxIdMessageGetRevision
} from "meteoio-platform-client";
import {useParams} from "react-router-dom";
import {useDatasetConfigTx} from "../../useDatasetConfigTx";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {DsRevisionRow} from "./DsRevisionRow";
import {Alert} from 'meteoio-ui/src/components/Alert';


export const DatasetIniRevisionsList: React.FC = () => {
    const {datasetId} = useParams()
    const [txId, {isHead}] = useDatasetConfigTx(datasetId)
    const revisions = useInternalDatasetsDatasetIdRevisionsGetRevisions({
        pathParams: {
            datasetId,
        }
    })
    const pending_rev = useInternalDatasetsDatasetIdRevisionsTxIdMessageGetRevision({
        pathParams: {
            datasetId,
            txId,
        }
    }, {
        enabled: !isHead
    })

    // const num_col_w = useMemo(() => `${Math.floor(2 + Math.log10((revisions?.data?.length ?? 1) + 1))}ch`, [revisions.data])

    if (revisions.isLoading || (!isHead && pending_rev.isLoading)) {
        return <Spinner tall/>
    }

    return <main style={{
        paddingLeft: '4em',
        // paddingTop: '1em',
    }}>
        <MaybeErrorAlert error={revisions.error}/>
        <MaybeErrorAlert error={pending_rev.error}/>
        <br/>
        <Timeline
            mode="left"
            reverse
            items={[
                ...(revisions?.data ?? []).map(row => {
                    return {
                        key: row.id,
                        // label: <>{row.number}</>,
                        children: <DsRevisionRow
                            row={row}
                            // num_col_w={num_col_w}
                        />,
                        dot: <Badge appearance="tint">
                            {row.number}
                        </Badge>,
                    }
                }),
                ...(isHead || !pending_rev?.data ? [] : [
                    {
                        key: 'draft',
                        pending: true,
                        color: tokens.colorNeutralForegroundDisabled,
                        // label: '',
                        children: <DsRevisionRow
                            pending
                            row={pending_rev?.data}
                            // num_col_w={num_col_w}
                        />,
                    }
                ])
            ]}
        />
        {revisions?.data?.length > 0 ? <Text block style={{marginTop: '1em', fontStyle: 'italic', color: tokens.colorNeutralForegroundDisabled}}>
            This dataset has {revisions?.data?.length} confirmed version{revisions?.data?.length > 1 ? 's' : ''}.
        </Text> : <Alert level="info">
            This dataset was created without versioning. There is only the current version.
        </Alert>}
    </main>
}
