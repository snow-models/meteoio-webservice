import * as React from "react";
import {DatasetRevisionsListEntry} from "meteoio-platform-client";
import {Drawer} from "antd";
import {ThemeProvider} from "meteoio-ui/src/providers/ThemeProvider";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {makeStyles, mergeClasses, Text, tokens} from "@fluentui/react-components";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {useDsRevisionMsgForm} from "./useDsRevisionMsgForm";


const useStyles = makeStyles({
    rowBtn: {
        borderRadius: '0.4em',
        marginTop: '-0.2em',
        padding: '0.3em 1em',
        cursor: 'pointer',
        margin: 0,
        display: 'block',
        border: '0 none',
        minWidth: '200px',
        maxWidth: 'calc(95vw - 8em)',
        backgroundColor: 'transparent',

        '&:hover': {
            backgroundColor: tokens.colorNeutralBackground2Hover
        }
    },

    rowBtn_open: {
        backgroundColor: tokens.colorBrandBackground2Hover
    },

    rowBtn_pending: {
        backgroundColor: tokens.colorPaletteYellowBackground2,

        '&:hover': {
            backgroundColor: tokens.colorPaletteYellowBackground3
        }
    }
})

export const DsRevisionRow: React.FC<{
    pending?: boolean
    row: DatasetRevisionsListEntry,
    // num_col_w: CSSProperties['width'],
}> = props => {
    const {row} = props
    const revisionMsgForm = useDsRevisionMsgForm({withConfirmBtn: props.pending})
    const styles = useStyles()

    const displayTitle = row.title || (
        row.number == 1
            ? 'Initial version'
            : (props.pending ? 'Pending revision' : `Version ${row.number}`)
    )

    return <>
        <Stack horizontal alignItems="flex-start" columnGap="M">
            {/*<div style={{width: props.num_col_w}}>*/}
            {/*    {row.number != null*/}
            {/*        ? <Badge appearance="tint">{row.number}.</Badge>*/}
            {/*        : null}*/}
            {/*</div>*/}
            {/*<div>&nbsp;</div>*/}
            <button
                type="button"
                role="navigation"
                className={mergeClasses(styles.rowBtn, props.pending && styles.rowBtn_pending, revisionMsgForm.isOpen && styles.rowBtn_open)}
                onClick={() => revisionMsgForm.open(row)}
            >
                <Text block truncate size={300} wrap={false} style={{maxHeight: '2em', fontWeight: '600'}}>
                    {displayTitle}
                </Text>
                {/*{row.title ? <Text block truncate size={300} wrap={false} style={{maxHeight: '2em', fontWeight: '600'}}>*/}
                {/*    {row.title}*/}
                {/*</Text> : <Text block size={200} style={{color: tokens.colorNeutralForeground4}}>*/}
                {/*    <code>{row.id}</code>*/}
                {/*</Text>}*/}
                {!props.pending ? <>
                    <Text block truncate size={200} style={{color: tokens.colorNeutralForeground3}}>
                        {row.confirmed_at && <RelativeTimeText date={row.confirmed_at}/>}
                        {row.updated_at !== row.confirmed_at ? <>
                            &nbsp;- Updated <RelativeTimeText date={row.updated_at}/>
                        </> : <></>}
                    </Text>
                </> : <>
                    <Text block truncate size={200} style={{color: tokens.colorPaletteYellowForeground2}}>
                        Click to edit message and confirm changes
                    </Text>
                </>}
            </button>
        </Stack>

        {revisionMsgForm.isOpen && <Drawer
            open={revisionMsgForm.isOpen}
            // width="80vw"
            size="large"
            placement="right"
            onClose={() => revisionMsgForm.close()}
            title={<ThemeProvider>
                <PageSectionTitle> &nbsp; {props.pending ? 'Pending revision' : `Version ${row.number}`}</PageSectionTitle>
            </ThemeProvider>}
            styles={{
                // header: {padding: '8px 13px'},
                body: {padding: '1em 24px'},
            }}
        >
            <ThemeProvider>
                <Stack rowGap="XXXL">
                    {revisionMsgForm.formNode}
                    <Stack rowGap="XXS">
                        {/*<Text block size={200} style={{color: tokens.colorNeutralForeground3}}>*/}
                        {/*    Version ID: <code>{row.id}</code>*/}
                        {/*</Text>*/}
                        <Text block size={200} style={{color: tokens.colorNeutralForeground3}}>
                            Version number: {row.number === null ? '(to be assigned)' : row.number}
                        </Text>
                        {row.created_at && <Text block size={200} style={{color: tokens.colorNeutralForeground3}}>
                            Created: <RelativeTimeText date={row.created_at}/>
                        </Text>}
                        {row.confirmed_at && <Text block size={200} style={{color: tokens.colorNeutralForeground3}}>
                            Confirmed: <RelativeTimeText date={row.confirmed_at}/>
                        </Text>}
                        {row.confirmed_at && row.updated_at && row.updated_at != row.confirmed_at &&
                            <Text block size={200} style={{color: tokens.colorNeutralForeground3}}>
                                Updated: <RelativeTimeText date={row.updated_at}/>
                            </Text>}
                    </Stack>
                </Stack>
            </ThemeProvider>
        </Drawer>}
    </>
}