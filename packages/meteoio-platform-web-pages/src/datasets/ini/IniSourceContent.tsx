import * as React from 'react'
import {useEffect, useState} from 'react'
import {useInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni} from "meteoio-platform-client";
import {useParams} from "react-router-dom";
import {useDatasetConfigTx} from "../useDatasetConfigTx";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {usePromise} from "meteoio-ui/src/utils/usePromise";
import {isLikelyAnINI} from "../../_common/parseINI";
import {Text} from "@fluentui/react-components";
import CodeINIEditor from "meteoio-ui/src/components/CodeINIEditor";

export const IniSourceContent: React.FC<{
    iniName: string
}> = props => {
    const {datasetId} = useParams()
    const [txId] = useDatasetConfigTx(datasetId)
    const {
        data,
        isLoading,
        error,
    } = useInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni({
        pathParams: {
            datasetId,
            txId,
            name: props.iniName,
        }
    }, {
        staleTime: 4000,
    })

    const [showCode, {
        error: err3,
        isLoading: isLoading3
    }] = usePromise(() => data ? isLikelyAnINI(data) : Promise.resolve(undefined), [data])

    const [code, {
        error: err2,
        isLoading: isLoading2
    }] = usePromise(() => data && showCode ? (data as Blob)?.text?.() : Promise.resolve(undefined), [showCode, data])

    const [code_cache, set_code_cache] = useState()
    useEffect(() => {
        if (code) {
            set_code_cache(code)
        }
        const tio = setTimeout(() => {
            set_code_cache(code)  // set anyway after some time, even if it's clearing away
        }, 1000)
        return () => {
            clearTimeout(tio)
        }
    }, [code])

    if (error ?? err2 ?? err3) {
        return <MaybeErrorAlert error={error ?? err2 ?? err3}/>
    }

    if (showCode === false) {
        return <Text block italic align="center">
            Preview not available
        </Text>
    }

    if ((isLoading || isLoading2 || isLoading3) && !code_cache) {
        return <Spinner tall/>
    }

    if (!code_cache) {
        return <></>
    }

    return <>
        {/*<CodeINIHighlighting code={code_cache}/>*/}
        <CodeINIEditor
            initialValue={code_cache}
            onChange={() => {}}
            readOnly
        />
    </>
}
