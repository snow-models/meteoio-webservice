import * as React from 'react'
import {useState} from 'react'
import {MainContentSplit} from "meteoio-ui/src/layouts/MainContentSplit";
import {Outlet, useParams} from 'react-router-dom';
import {Sticky} from "meteoio-ui/src/components/Sticky";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Button} from "meteoio-ui/src/components/Button";
import {mkFileUploadVariables, useQueryClient} from "../../_common/backend";
import {FilesField} from "meteoio-ui/src/components/FilesField";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {AddRegular, ArrowUploadRegular, EditRegular} from "@fluentui/react-icons";
import {
    fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisListInis,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameCreateIni,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameUpdateIni,
} from "meteoio-platform-client";
import {Modal} from "meteoio-ui/src/components/Modal";
import {useDatasetConfigTx} from "../useDatasetConfigTx";
import {useIniFilesFilter} from "../_common/useIniFilesFilter";
import {InfoBox} from "meteoio-ui/src/components/InfoBox";
import {PlayRegular} from "@fluentui/react-icons";

export const DatasetIniTab: React.FC = () => {
    const {datasetId} = useParams()
    const [txId, {isHead, setConfigTxId}] = useDatasetConfigTx(datasetId)
    const [isUploading, setIsUploading] = useState<boolean>(false)
    return <main>
        <Modal
            centered
            open={isUploading}
            footer={null}
            onCancel={() => setIsUploading(false)}
        >
            <IniUploadForm onSuccess={() => setIsUploading(false)}/>
        </Modal>
        <MainContentSplit
            keepNavAbove
            nav={<Sticky top={75}>
                {isHead && <>
                    <Stack>
                        <AsyncActionButton
                            icon={<EditRegular/>}
                            label="Start a revision..."
                            onClick={async () => {
                                const res = await fetchInternalDatasetsDatasetIdConfigTxsBeginConfigWriteTx({
                                    pathParams: {
                                        datasetId,
                                    }
                                })
                                setConfigTxId(res.id)
                            }}
                        />
                    </Stack>
                    <br/>
                </>}
                {!isHead && <>
                    <Stack>
                        <Button
                            appearance="primary"
                            icon={<AddRegular/>}
                            label="Add INI configuration..."
                            onClick={() => setIsUploading(true)}
                        />
                    </Stack>
                    <br/>
                    <InfoBox>
                        You can&nbsp;<PlayRegular style={{position: 'relative', top: 2, transform: 'scale(1.3)'}}/>&nbsp;Run a test job that will use your pending revision, before confirming changes.
                        {/*You can click the "Run..." button before confirming changes, for a test job that will use your pending revision.*/}
                    </InfoBox>
                    <br/>
                </>}
                {/*<NavLinksList*/}
                {/*    noTopMargin*/}
                {/*    variant='box'*/}
                {/*    links={[*/}
                {/*        {*/}
                {/*            to: address_book.datasets.ini.view(datasetId), // NOTE: trailing slash makes it match only without other stuff*/}
                {/*            children: <>Current</>*/}
                {/*        },*/}
                {/*        // {*/}
                {/*        //     to: address_book.datasets.ini.revisions.list(datasetId), // NOTE: trailing slash makes it match only without other stuff*/}
                {/*        //     children: <>Revisions</>*/}
                {/*        // },*/}
                {/*        // {*/}
                {/*        //     to: address_book.datasets.ini.revisions.create(datasetId),*/}
                {/*        //     children: <>Create new revision</>*/}
                {/*        // },*/}
                {/*    ]}*/}
                {/*/>*/}
            </Sticky>}
        >
            <Outlet/>
        </MainContentSplit>
    </main>
}


export const IniUploadForm: React.FC<{
    onSuccess?: () => void
}> = props => {
    const {datasetId} = useParams()
    const [txId] = useDatasetConfigTx(datasetId)

    const [dataFiles, setDataFiles] = useState<File[] | undefined>()

    const [progress, setProgress] = useState<number>(undefined)

    const queryClient = useQueryClient()

    const {filteringModal, acceptingFiles: iniFiles} = useIniFilesFilter(dataFiles, setDataFiles)

    // TODO?: add a feature in FilesField to annotate existing files and warn the user before updating

    return <>
        {filteringModal}
        <Stack rowGap="XL">
            <FilesField
                label="Select files from your device..."
                value={dataFiles}
                onChange={setDataFiles}
                minHeight={dataFiles?.length > 0 ? undefined : 100}
            />
            <Stack horizontal justifyContent="end" columnGap="XL" alignItems="center">
                <ProgressBar
                    hide={progress === undefined}
                    value={progress}
                />
                <AsyncActionButton
                    icon={<ArrowUploadRegular/>}
                    label="Upload"
                    onClick={async () => {
                        setProgress(0)
                        try {
                            const existingInis = await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisListInis({
                                pathParams: {datasetId, txId}
                            })

                            const files = iniFiles?.slice?.() ?? []
                            // NOTE: this is async and the user may interfere so we use files, a copy of dataFiles.
                            for (let i = 0; i < files.length; ++i) {
                                const file = files[i]
                                if (file) {
                                    const fetch__fn = existingInis.includes(file.name)
                                        ? fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameUpdateIni
                                        : fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameCreateIni;

                                    await fetch__fn({
                                        pathParams: {
                                            datasetId,
                                            txId,
                                            name: file.name,
                                        },
                                        ...mkFileUploadVariables(file),
                                    })
                                }
                                setProgress((i + 1) / files.length)
                            }
                            await queryClient.invalidateQueries()
                            props.onSuccess?.()
                            setDataFiles([])
                        } finally {
                            setProgress(undefined)
                        }
                    }}
                />
            </Stack>
        </Stack>
    </>
}
