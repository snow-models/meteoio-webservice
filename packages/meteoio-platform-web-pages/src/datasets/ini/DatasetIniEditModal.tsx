import * as React from 'react'
import {lazy, Suspense, useEffect, useState} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {useDatasetConfigTx} from "../useDatasetConfigTx";
import {DocumentBulletListRegular, SaveRegular} from "@fluentui/react-icons";
import {App, Drawer} from "antd";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Text} from "@fluentui/react-components";
import {fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameUpdateIni} from "meteoio-platform-client";
import {mkFileUploadVariables, queryClient} from "../../_common/backend";
import {ThemeProvider} from 'meteoio-ui/src/providers/ThemeProvider';

const CodeINIEditor = lazy(() => import("meteoio-ui/src/components/CodeINIEditor"));

export interface IniEditSessionParams {
    datasetId: string
    iniName: string
    sourceCode: string
}

// TODO?: maybe useDatasetConfigTxDraftPusher to avoid the modal, given we can exploit transactions?

export const DatasetIniEditModal: React.FC<{
    editingSessionParams: IniEditSessionParams
    onDismiss?: () => void
}> = props => {
    const [txId] = useDatasetConfigTx(props.editingSessionParams.datasetId)

    const [storedCode, setStoredCode] = useState<string>(props.editingSessionParams?.sourceCode ?? '')
    const [draftCode, setDraftCode] = useState<string>(props.editingSessionParams?.sourceCode ?? '')
    const isChanged = draftCode !== storedCode

    const {modal} = App.useApp()

    const saveCallback = async () => {
        // console.debug({file})
        await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameUpdateIni({
            pathParams: {
                datasetId: props.editingSessionParams.datasetId,
                txId,
                name: props.editingSessionParams.iniName,
            },
            ...mkFileUploadVariables(draftCode)
        })
        await queryClient.invalidateQueries()
        setStoredCode(draftCode)
    }

    useEffect(() => {
        if (isChanged) {
            const h = (ev: BeforeUnloadEvent) => {
                ev.preventDefault()
                return "You have some pending changes."
            }
            window?.addEventListener?.('beforeunload', h)
            return () => {
                window?.removeEventListener?.('beforeunload', h)
            }
        }
    }, [isChanged])

    return <>
        <Drawer
            open
            // width="95vw"
            height="calc(100svh - 50px)"
            placement="bottom"

            onClose={() => {
                if (isChanged) {
                    modal.confirm({
                        type: 'confirm',
                        content: 'Are you sure you want to close the editor before saving the changes?',
                        okText: 'Close',
                        okType: 'danger',
                        onOk: () => {
                            props.onDismiss?.()
                        }
                    })
                    return
                }
                props.onDismiss?.()
            }}
            styles={{
                // header: {padding: '8px 13px'},
                body: {padding: '0 0 200px'},
            }}
            title={<ThemeProvider>
                <Stack horizontal alignItems="center" columnGap="XL">
                    <Stack horizontal alignItems="center" columnGap="XS">
                        <DocumentBulletListRegular opacity={0.66} fontSize={20}/>
                        <Text
                            block
                            truncate
                            size={400}
                            weight="semibold"
                            style={{maxWidth: 'calc(90vw - 200px)', overflow: 'hidden'}}
                        >
                            {props.editingSessionParams.iniName}
                        </Text>
                    </Stack>
                    <div style={{flexGrow: 1}}>&nbsp;</div>
                    <Stack horizontal alignItems="center" columnGap="M">
                        <AsyncActionButton
                            icon={<SaveRegular/>}
                            disabled={!isChanged}
                            appearance="primary"
                            label="Save"
                            onClick={saveCallback}
                        />
                        {/*<AsyncActionButton // no reload button*/}
                        {/*    icon={<SaveRegular/>}*/}
                        {/*    disabled={!isChanged}*/}
                        {/*    appearance="secondary"*/}
                        {/*    label="Reload"*/}
                        {/*    onClick={async () => {*/}

                        {/*    }}*/}
                        {/*/>*/}
                    </Stack>
                </Stack>
            </ThemeProvider>}
        ><ThemeProvider>
            <div style={{height: 8, overflow: 'hidden'}}>&nbsp;</div>
            <Suspense fallback={<Spinner tall/>}>
                <CodeINIEditor
                    initialValue={draftCode}
                    onChange={setDraftCode}
                    // maxHeight="calc(95svh - 250px)"
                    // outlined
                />
            </Suspense>
        </ThemeProvider></Drawer>
    </>
}