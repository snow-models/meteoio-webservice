import * as React from 'react'
import {Text} from "@fluentui/react-components";
import {ArrowDownloadRegular, BinRecycleRegular, DocumentBulletListRegular, PlayFilled} from "@fluentui/react-icons";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {useNavigate, useParams} from "react-router-dom";
import {Collapse, message} from 'antd'
import {
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameDeleteIni,
    fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni,
    useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis
} from "meteoio-platform-client";
import {useDatasetConfigTx} from "../useDatasetConfigTx";
import {IniSourceContent} from "./IniSourceContent";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {downloadFile} from "meteoio-ui/src/utils/downloadFile";
import {queryClient} from "../../_common/backend";
import {address_book} from "../../address_book";
import {EditableIniSourceContent} from "./EditableIniSourceContent";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {stopPushes} from "../useDatasetConfigTxDraftPusher";

export const DatasetIniIndex: React.FC = () => {
    const {datasetId, iniName} = useParams()
    const [txId, {isHead}] = useDatasetConfigTx(datasetId)
    const {data: inis, error, isLoading, refetch} = useInternalDatasetsDatasetIdConfigTxsTxIdInisListInis({
        pathParams: {datasetId, txId}
    })

    const navigate = useNavigate()

    const [messageApi, messageContextHolder] = message.useMessage()

    const {confirm} = useDialogs()

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    if (isLoading) {
        return <Spinner/>
    }

    return <>
        {messageContextHolder}
        <Stack>
            {/*<ButtonRadioToggles*/}
            {/*    value="code"*/}
            {/*    options={[*/}
            {/*        {value: 'code', label: 'Code'},*/}
            {/*        {value: 'inishell', label: 'INIshell'},*/}
            {/*    ]}*/}
            {/*/>*/}
            {/*<Space.Compact>*/}
            {/*    <AButton type="primary" disabled style={{color: tokens.colorNeutralForeground1, fontWeight: tokens.fontWeightSemibold}}>*/}
            {/*        Code*/}
            {/*    </AButton>*/}
            {/*    <AButton >*/}
            {/*        INIshell*/}
            {/*    </AButton>*/}
            {/*</Space.Compact>*/}
            <Collapse
                defaultActiveKey={inis?.[inis?.length - 1]} // NOTE: default open last so we see all file names
                activeKey={iniName || undefined}
                onChange={key => {
                    navigate(address_book.datasets.ini.view_one(datasetId, key))
                }}
                accordion
                bordered
                size="small"
                destroyInactivePanel
                items={inis?.map?.(ini => ({
                    key: ini,
                    label: <>
                        <Stack horizontal alignItems="center" columnGap="S">
                            <DocumentBulletListRegular opacity={0.66} fontSize={20}/>
                            <Text block truncate wrap={false}
                                  style={{overflow: 'hidden', maxWidth: 'calc(98vw - 120px)'}}>
                                {ini}
                            </Text>
                        </Stack>
                    </>,
                    children: <>
                        <div style={{marginTop: -52, height: 52}}>
                            <Stack horizontal alignItems="center" columnGap="S" justifyContent="end" height={40}>
                                <div style={{position: 'relative', zIndex: 2}}>
                                    <Stack horizontal alignItems="center" columnGap="S" justifyContent="end"
                                           height={40}>
                                        {!isHead && <AsyncActionButton
                                            appearance="secondary"
                                            danger
                                            size="small"
                                            icon={<BinRecycleRegular/>}
                                            onClick={async () => {
                                                if (!await confirm('Are you sure you want to delete the INI file?', {
                                                    okText: 'Delete',
                                                    okType: 'danger',
                                                })) {
                                                    return
                                                }
                                                await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameDeleteIni({
                                                    pathParams: {
                                                        datasetId,
                                                        txId,
                                                        name: ini,
                                                    }
                                                })
                                                stopPushes(`71ad9f7e-e7d4-449c-8df7-92efb7566eff-${datasetId}-${ini}`)  // NOTE: in case of changes, search for shared usage of the uuid.
                                                await refetch()
                                                await queryClient.invalidateQueries()  // This may be redundant...
                                            }}
                                        />}
                                        {/*!isHead && <AsyncActionButton
                                            size="small"
                                            label="Edit"
                                            icon={<EditRegular/>}
                                            onClick={async () => {
                                                const res = await fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni({
                                                    pathParams: {
                                                        datasetId,
                                                        txId,
                                                        name: ini,
                                                    }
                                                })
                                                if (!(await isLikelyAnINI(res))) {
                                                    messageApi.error('Unable to edit this file: not likely an INI text file.')
                                                    return
                                                }
                                                const sourceCode = await (res as unknown as Blob).text()
                                                setEditingSessionParams({
                                                    datasetId,
                                                    iniName: ini,
                                                    sourceCode,
                                                })
                                            }}
                                        />*/}
                                        <AsyncActionButton
                                            size="small"
                                            label="Run..."
                                            icon={<PlayFilled color="green"/>}
                                            onClick={async () => {
                                                navigate(address_book.datasets.new_guest_job(datasetId, ini))
                                            }}
                                        />
                                        <AsyncActionButton
                                            size="small"
                                            label="Download"
                                            icon={<ArrowDownloadRegular/>}
                                            onClick={async () => {
                                                await downloadFile(messageApi, undefined, () => fetchInternalDatasetsDatasetIdConfigTxsTxIdInisNameReadIni({
                                                    pathParams: {
                                                        datasetId,
                                                        txId,
                                                        name: ini,
                                                    }
                                                }), ini)
                                            }}
                                        />
                                    </Stack>
                                </div>
                            </Stack>
                        </div>
                        {isHead ? <IniSourceContent iniName={ini}/> : <EditableIniSourceContent iniName={ini}/>}
                    </>
                }))}
            />
            <br/>
        </Stack>
    </>
}
