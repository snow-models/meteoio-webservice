import * as React from 'react'
import {MainContentSplit} from "meteoio-ui/src/layouts/MainContentSplit";
import {NavLinksList} from "meteoio-ui/src/components/NavLinksList";
import {address_book} from "../address_book";
import {Route, Routes, useParams} from "react-router-dom";
import {DatasetHeadingSettings} from "./DatasetHeadingSettings";
import {DatasetPublicationSettings} from "./DatasetPublicationSettings";
import {SFTPAccessButton} from "./SFTPAccessButton";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {DatasetDeletionSettings} from "./DatasetDeletionSettings";

export const DatasetSettingsTabRoot: React.FC = () => {
    const {datasetId} = useParams()

    const {isMobile} = useScreenSize()

    return <>
        <MainContentSplit
            nav={<>
                <NavLinksList variant="main" links={[
                    {to: address_book.datasets.settings.heading(datasetId), children: "General settings"},
                    {to: address_book.datasets.settings.publication(datasetId), children: "Publication"},
                    {to: address_book.datasets.settings.deletion(datasetId), children: "Deletion"},
                ]}/>
                {!isMobile && <Stack horizontal justifyContent='center'>
                    <SFTPAccessButton/>
                </Stack>}
            </>}
            keepNavAbove
        >
            <Routes>
                <Route path="/" Component={DatasetHeadingSettings}/>
                <Route path="/publication" Component={DatasetPublicationSettings}/>
                <Route path="/deletion" Component={DatasetDeletionSettings}/>
            </Routes>
        </MainContentSplit>
    </>
}
