import * as React from 'react'
import {useContext} from 'react'
import {
    fetchInternalDatasetsDatasetIdRunsJobIdReleaseReleaseRun,
    useInternalDatasetsTryFindRunJobIdTryFindRun,
    usePublicMeGetMe
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {DatasetTitle} from "./_common/DatasetTitle";
import {JobContext} from "../jobs/job/JobContext";
import {ArchiveArrowBackRegular} from "@fluentui/react-icons";
import {Alert} from "meteoio-ui/src/components/Alert";
import {useQueryClient} from "../_common/backend";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {Spinner} from "meteoio-ui/src/components/Spinner";

export const JobDsRunBanner: React.FC<{
    assertIsDsRun?: boolean
    isInDsLayout?: boolean
}> = props => {
    /*
        TEST CASES:
            - no login session -> render null
            - no can_create_datasets -> render null
            - not my dataset -> render null
            - my dataset, not released -> render info banner
            - my dataset, released -> render success banner
    */

    const {jobId, status} = useContext(JobContext)

    const {data: me_data} = usePublicMeGetMe({})

    const {
        data,
        error,
        isLoading,
    } = useInternalDatasetsTryFindRunJobIdTryFindRun({pathParams: {jobId}}, {
        enabled: me_data?.can_create_datasets && (status?.is_queued || status?.is_finished)
    })

    const dialogs = useDialogs()

    const queryClient = useQueryClient()

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    if (isLoading && !props.assertIsDsRun) {
        return null
    }

    if ((!data || !data?.dataset_id) && !props.assertIsDsRun) {
        return null
    }

    const not_released_alert = data?.released === false && <Alert
        level="info"
        end={<AsyncActionButton
            size="small"
            icon={<ArchiveArrowBackRegular/>}
            label={"Release"}
            disabled={!status?.is_finished}
            onClick={async () => {
                if (!await dialogs.confirm(
                    'Are you sure you want to release this result as dataset output?',
                    {okText: 'Release'}
                )) {
                    return
                }
                await fetchInternalDatasetsDatasetIdRunsJobIdReleaseReleaseRun({
                    pathParams: {
                        jobId,
                        datasetId: data?.dataset_id,
                    }
                })
                await queryClient.invalidateQueries()
            }}
        />}
    >
        This is a temporary dataset run.
    </Alert>

    const released_alert = data?.released === true && <Alert
        level="success"
    >
        This result has been released.
    </Alert>

    const alert = released_alert || not_released_alert || (props.assertIsDsRun && <Alert
        level="info"
    >
        <Spinner/>
    </Alert>)

    return <>
        <Stack rowGap="XS">
            {!props.isInDsLayout && data?.dataset_id && <DatasetTitle datasetId={data?.dataset_id}/>}
            {alert}
        </Stack>
    </>
}
