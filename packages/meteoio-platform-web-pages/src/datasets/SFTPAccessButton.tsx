import * as React from 'react'
import {useState} from 'react'
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {InfoBox} from "meteoio-ui/src/components/InfoBox";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Badge, Text} from "@fluentui/react-components";
import {DatasetTitle} from "./_common/DatasetTitle";
import {getSSHUsername} from "./_common/getSSHUsername";
import {CopyToClipboardButton} from "meteoio-ui/src/components/CopyToClipboardButton";
import {MySSHAuthorizedKeysCtrl} from "../user/MySSHAuthorizedKeysCtrl";
import {Modal} from "meteoio-ui/src/components/Modal";
import {Button} from "meteoio-ui/src/components/Button";
import {PlugConnectedSettingsRegular} from "@fluentui/react-icons";
import {usePublicMeGetMe} from "meteoio-platform-client";
import {useParams} from "react-router-dom";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {ExtLink} from "meteoio-ui/src/components/ExtLink";
import {Spinner} from "meteoio-ui/src/components/Spinner";

export const SFTPAccessButton: React.FC = () => {
    const {datasetId} = useParams()

    const [isSSHKeysOpen, setIsSSHKeysOpen] = useState<boolean>(false)
    const [hasChanges, setHasChanges] = useState<boolean>(false)

    const {data: my_user_data, isLoading: my_user_isLoading} = usePublicMeGetMe({})

    const dialogs = useDialogs()

    return <>

        <Button
            onClick={() => setIsSSHKeysOpen(true)}
            label="SFTP access"
            icon={<PlugConnectedSettingsRegular/>}
        />

        <Modal
            centered
            open={isSSHKeysOpen}
            footer={null}
            onCancel={async () => {
                if (hasChanges) {
                    if (!await dialogs.confirm('Are you sure to discard your pending changes?', {cancelText: 'Continue editing'})) {
                        return
                    }
                }
                setIsSSHKeysOpen(false)
            }}
            width={700}
            maskClosable
        >
            <PageTitle>SFTP Access</PageTitle>
            <br/>
            <br/>
            <InfoBox>
                You can access the files of this dataset via SFTP (SSH File Transfer Protocol).
                Take note of your login username for this dataset and setup your SSH keys for secure authentication.
                <br/>
                See also: &nbsp;
                <ExtLink href="https://meteoio.slf.ch/DAME_end-users/">
                    MeteoIO docs
                </ExtLink>, &nbsp;
                <ExtLink href="https://en.wikipedia.org/wiki/Secure_Shell#Authentication:_OpenSSH_key_management">
                    OpenSSH key management
                </ExtLink>
            </InfoBox>
            <br/>
            <Stack horizontal columnGap="S" alignItems="top">
                <Stack horizontal columnGap="S" alignItems="baseline">
                    <PageSectionTitle>
                        Your SSH username
                    </PageSectionTitle>
                    <Text>
                        for
                    </Text>
                </Stack>
                <Badge size="medium" color="informative" appearance="outline" shape="circular">
                    <DatasetTitle/>
                </Badge>
            </Stack>
            {my_user_isLoading ? <Spinner/> : <>
                {/* TODO?: show table with host, port, username, password=do not use password but ssh keys */}
                <code style={{fontSize: '0.8em'}}>{getSSHUsername(my_user_data?.id, datasetId)}</code>
                &nbsp;
                <CopyToClipboardButton data={getSSHUsername(my_user_data?.id, datasetId)}/>
            </>}
            <br/>
            <br/>
            {isSSHKeysOpen &&
                <MySSHAuthorizedKeysCtrl onCancel={() => setIsSSHKeysOpen(false)} onHasChangesChange={setHasChanges}/>}
        </Modal>
    </>
}
