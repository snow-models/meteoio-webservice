import React, {useCallback, useDeferredValue, useEffect, useState} from "react";
import {useDatasetConfigTxHooks} from "./useDatasetConfigTxHooks";

const _STOP_SIGNALS: Set<string> = new Set<string>()

/** Use case: prevent pushing accumulated updates after a delete command has been sent, to avoid failed update errors */
export function stopPushes(key: string) {
    _STOP_SIGNALS.add(key)
    console.debug('useDatasetConfigTxDraftPusher stopped key', key)
}
export function unstopPushes(key: string) {
    _STOP_SIGNALS.delete(key)
    console.debug('useDatasetConfigTxDraftPusher unstopped key', key)
}

export function useDatasetConfigTxDraftPusher<T>(key: string, pusher?: (draft?: T) => Promise<void>, preloader?: () => Promise<T>): [
    T,
    React.Dispatch<React.SetStateAction<T>>,
    {
        error: unknown,
        isLoading: boolean,
    }
] {
    const [isLoading, setIsLoading] = useState<boolean>(true)
    const [error, setError] = useState()
    const [draft, setDraft] = useState<T | undefined>()

    const draft_deferred = useDeferredValue(draft)

    // store state on session storage so that we later avoid depending on the state for hooked actions
    useEffect(() => {
        if (draft_deferred) {
            sessionStorage?.setItem?.(key, JSON.stringify(draft_deferred))
        }
    }, [key, draft_deferred])

    // load draft from server
    useEffect(() => {
        if (draft === undefined) {
            (async () => {
                setIsLoading(true)
                try {
                    const data = await preloader()
                    setDraft(data)
                } catch (e) {
                    setError(e)
                } finally {
                    setIsLoading(false)
                }
            })();
        }
    }, [key])

    // define push from sessionStorage to avoid depending on the state
    const push_ = useCallback(async () => {
        if (!pusher) {
            return
        }
        if (_STOP_SIGNALS.has(key)) {
            console.debug('useDatasetConfigTxDraftPusher not pushing stopped key', key)
            return
        }
        const draft_str = sessionStorage?.getItem?.(key)
        if (draft_str) {
            const draft = JSON.parse(draft_str)
            await pusher(draft)
            // sessionStorage?.removeItem?.(key)
        }
    }, [key])

    useDatasetConfigTxHooks({
        // push before tx application
        onBeforeApply: async () => {
            await push_()
            sessionStorage?.removeItem?.(key)
        },

        // avoid pushing after tx discard
        onBeforeDiscard: async () => {
            sessionStorage?.removeItem?.(key)
        }
    }, [key])

    // push before abrupt window close
    useEffect(() => {
        const h = () => {
            //console.debug('useDatasetConfigTxDraftPusher', 'window beforeunload')
            push_().then()
        }
        window?.addEventListener?.('beforeunload', h)
        return () => {
            //console.debug('useDatasetConfigTxDraftPusher', 'key deactivation')
            push_().then(() => {
                window?.removeEventListener('beforeunload', h)
            })
        }
    }, [key])

    return [
        draft,
        setDraft,
        {error, isLoading},
    ]
}
