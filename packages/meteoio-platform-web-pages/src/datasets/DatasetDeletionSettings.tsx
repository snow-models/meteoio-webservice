import * as React from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {fetchInternalDatasetsDatasetIdDelete, usePublicMeGetMe} from "meteoio-platform-client";
import {useNavigate, useParams} from "react-router-dom";
import {makeStyles, shorthands, Text, tokens} from "@fluentui/react-components";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {useQueryClient} from "../_common/backend";
import {address_book} from "../address_book";

const useStyles = makeStyles({
    box: {
        ...shorthands.border('thin', 'solid', tokens.colorPaletteRedBorder1),
        ...shorthands.borderRadius(tokens.borderRadiusMedium),
        ...shorthands.padding(tokens.spacingVerticalL, tokens.spacingHorizontalL)
    }
})

export const DatasetDeletionSettings: React.FC = () => {
    const {datasetId} = useParams()

    const {data: my_user_data} = usePublicMeGetMe({})

    const styles = useStyles()
    const dialogs = useDialogs()
    const queryClient = useQueryClient()

    const navigate = useNavigate()

    return <>
        <br/>
        <Stack rowGap="XXXL" className={styles.box}>
            <Stack>
                <Text>You can permanently delete this dataset if you have created it by mistake or if it's no longer
                    useful.</Text>
                <AsyncActionButton
                    danger
                    disabled={!my_user_data}
                    label="Permanently delete"
                    onClick={async () => {
                        dialogs.message.warning('You are going to permanently delete this dataset...', 5)
                        await new Promise(resolve => setTimeout(resolve, 2_000))
                        if (!await dialogs.confirm(
                            'Are you sure you want to permanently delete this dataset? This action cannot be undone.',
                            {okText: 'Delete permanently', okType: 'danger'})) {
                            return
                        }
                        await new Promise(resolve => setTimeout(resolve, 2_000))
                        await fetchInternalDatasetsDatasetIdDelete({
                            pathParams: {datasetId},
                            queryParams: {
                                dataset_id_for_check: datasetId,
                                owner_user_id_for_check: my_user_data?.id,
                            }
                        })
                        dialogs.message.success('The dataset has been deleted successfully.')
                        queryClient.clear()
                        navigate(address_book.home)
                    }}
                />
            </Stack>
        </Stack>
        <br/>
        <br/>
    </>
}
