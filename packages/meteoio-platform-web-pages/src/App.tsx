import * as React from 'react'
import {lazy, Suspense} from 'react'
import {HashRouter, Route, Routes} from "react-router-dom";
import {ThemeProvider} from "meteoio-ui/src/providers/ThemeProvider";
import {AppErrorBoundary} from "./AppErrorBoundary";
import {AppLayout} from "./AppLayout";
import {HomePage} from "./HomePage";
import {NotFound} from "./_common/NotFound";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {QueryClientProvider} from 'meteoio-platform-client';
import {queryClient} from "./_common/backend";
import {AfterLogoutPage} from "./AfterLogoutPage";
import {AppCustomizationSettingsProvider} from "./_common/AppCustomizationSettingsProvider";
import {ReactQueryDevtools} from "@tanstack/react-query-devtools";
import {ErrorBoundary} from "meteoio-ui/src/components/ErrorBoundary";
import {LoginUiContextProvider} from "./user/LoginUiContext";

const DatasetLayout = lazy(async () => ({default: (await import('./datasets/DatasetLayout')).DatasetLayout}))
const JobsListPage = lazy(async () => ({default: (await import('./jobs/JobsListPage')).JobsListPage}))
const JobSubmissionPage = lazy(async () => ({default: (await import('./jobs/JobSubmissionPage')).JobSubmissionPage}))
const JobViewPage = lazy(async () => ({default: (await import('./jobs/JobViewPage')).JobViewPage}))
const JobFileViewPage = lazy(async () => ({default: (await import('./jobs/JobFileViewPage')).JobFileViewPage}))
const AdminPages = lazy(async () => ({default: (await import('./admin/AdminPages')).AdminPages}))
const DatasetsListPage = lazy(async () => ({default: (await import ('./datasets/DatasetsListPage')).DatasetsListPage}))
const DatasetCreationPage = lazy(async () => ({default: (await import ('./datasets/DatasetCreationPage')).DatasetCreationPage}))
const ExplorePublicDatasetsPage = lazy(async () => ({default: (await import ('./explore/ExplorePublicDatasetsPage')).ExplorePublicDatasetsPage}))
const PubDsLayout = lazy(async () => ({default: (await import('./explore/PubDsLayout')).PubDsLayout}))

// TODO?: unify routes for internal datasets management and public dataset access.

export const App: React.FC = () => {
    return <>
        <QueryClientProvider client={queryClient}><AppCustomizationSettingsProvider><ThemeProvider targetDocument={document}>
            <ReactQueryDevtools initialIsOpen={false} />
            <HashRouter>
                <Suspense fallback={<Spinner tall label="Loading..."/>}>
                    <ErrorBoundary>
                    <LoginUiContextProvider>
                    <Routes>
                        <Route path="/after_logout" Component={AfterLogoutPage}/>
                        <Route path={"admin/*"} ErrorBoundary={AppErrorBoundary} Component={AdminPages}/>
                        <Route path={"datasets/:datasetId/*"} ErrorBoundary={AppErrorBoundary}
                               Component={DatasetLayout}/>
                        <Route path={"explore/:datasetId/*"} ErrorBoundary={AppErrorBoundary}
                               Component={PubDsLayout}/>
                        <Route index Component={HomePage}/>
                        <Route path="/" Component={AppLayout} ErrorBoundary={AppErrorBoundary}>
                            <Route path={"jobs"} Component={JobsListPage}/>
                            <Route path={"jobs/new"} Component={JobSubmissionPage}/>
                            <Route path={"jobs/:jobId/file/*"} Component={JobFileViewPage}/>
                            <Route path={"jobs/:jobId/*"} Component={JobViewPage}/>
                            <Route path={"datasets"} Component={DatasetsListPage}/>
                            <Route path={"datasets/new"} Component={DatasetCreationPage}/>
                            <Route path={"explore"} Component={ExplorePublicDatasetsPage}/>
                            <Route path="*" Component={NotFound}/>
                        </Route>
                    </Routes>
                    </LoginUiContextProvider>
                    </ErrorBoundary>
                </Suspense>
            </HashRouter>
        </ThemeProvider></AppCustomizationSettingsProvider></QueryClientProvider>
    </>
}
