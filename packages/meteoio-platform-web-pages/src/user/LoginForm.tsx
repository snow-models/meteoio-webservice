import * as React from 'react'
import {LoginField} from "meteoio-ui/src/components/LoginField";
import {FormEvent, useCallback, useEffect, useRef, useState} from "react";
import {fetchPublicLoginLogin, usePublicMeGetMe} from "meteoio-platform-client";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Button} from "meteoio-ui/src/components/Button";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {tokens, Text} from "@fluentui/react-components";
import {useQueryClient} from "../_common/backend";
import {Result} from "antd";

export const LoginForm: React.FC<{
    onLoginSuccess?: () => void
}> = props => {
    const [email, setEmail] = useState<string>()
    const [password, setPassword] = useState<string>()
    const [error, setError] = useState<Error | undefined>()
    const [isPending, setIsPending] = useState<boolean>(false)
    const [isFailed, setIsFailed] = useState<boolean>(false)
    const [isSuccess, setIsSuccess] = useState<boolean>(false)

    const emailRef = useRef<HTMLInputElement>()
    useEffect(() => {
        emailRef?.current?.focus?.()
    }, [emailRef?.current])

    const canSubmit = email && password && !isPending

    const queryClient = useQueryClient()

    const submit = useCallback((ev?: FormEvent) => {
        ev?.preventDefault?.();
        // NOTE: semicolon required at previous code line.
        (async () => {
            setError(undefined)
            setIsPending(true)
            setIsFailed(false)
            setIsSuccess(false)
            try {
                const resp = await fetchPublicLoginLogin({
                    body: {email, secret_password: password}
                });
                const status = resp?.status
                if (status === "welcome_back") {
                    await queryClient.invalidateQueries()
                    setIsSuccess(true)
                    props.onLoginSuccess?.()
                    return
                }
                if (status === "failed") {
                    setIsFailed(true)
                    return
                }
                const []: never[] = [status]  // static completeness check
            } catch (err) {
                console.trace(err)
                setError(err)
            } finally {
                await new Promise(resolve => setTimeout(resolve, 2000))
                setIsPending(false)
            }
        })();
        return false
    }, [email, password])

    const {data: me_data} = usePublicMeGetMe({}, {enabled: isSuccess})

    if (isSuccess) {
        return <Result
            status="success"
            title={`Welcome back${me_data?.full_name ? `, ${me_data.full_name}` : ''}`}
        />
    }

    return <form onSubmit={submit}>
        <Stack rowGap="XL">
            <Stack rowGap="S">
                <LoginField
                    innerRef={emailRef}
                    label="Email"
                    type="email"
                    autoComplete="username"
                    value={email}
                    onChange={setEmail}
                    onEnter={canSubmit ? submit : undefined}
                    required
                    authFailure={isFailed}
                    disabled={isPending}
                />
                <LoginField
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={setPassword}
                    onEnter={canSubmit ? submit : undefined}
                    required
                    authFailure={isFailed}
                    disabled={isPending}
                />
            </Stack>

            <MaybeErrorAlert error={error}/>

            {isFailed && <Text weight="semibold" style={{color: tokens.colorPaletteRedForeground1}}>
                    Wrong email or password
            </Text>}

            <Stack horizontal columnGap="XL" alignItems="center" justifyContent="space-between">
                <Button
                    appearance="primary"
                    onClick={submit}
                    disabled={isPending || !canSubmit}
                    label={isPending ? <Spinner/> : "Login"}
                />
                <Text size={200} strikethrough>
                    Reset password
                </Text>
            </Stack>
        </Stack>
    </form>
}
