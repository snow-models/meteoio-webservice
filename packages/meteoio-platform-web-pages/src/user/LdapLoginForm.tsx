import * as React from 'react'
import {FormEvent, useCallback, useEffect, useRef, useState} from 'react'
import {Divider, Text, tokens} from "@fluentui/react-components";
import {useQueryClient} from "../_common/backend";
import {
    fetchPublicLdapAuthenticateAuthenticate,
    usePublicMeGetMe
} from "meteoio-platform-client";
import {Result} from "antd";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {LoginField} from "meteoio-ui/src/components/LoginField";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Button} from "meteoio-ui/src/components/Button";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {useAppCustomizationSettings} from "../_common/AppCustomizationSettingsProvider";

export const LdapLoginForm: React.FC<{
    onOpen?: () => void
    onLoginSuccess?: () => void
}> = props => {

    const [isOpen, setIsOpen] = useState<boolean>(false)
    const {
        configuration,
        isLoading: isSettingsLoading,
    } = useAppCustomizationSettings()

    // return <>
    //     <Button
    //         label="Login with LDAP"
    //         onClick={() => setIsOpen(true)}
    //     />
    //     <Modal
    //         width={350}
    //         open={isOpen}
    //         title="LDAP Login"
    //         onCancel={() => setIsOpen(false)}
    //         onLoginSuccess={props.onLoginSuccess}
    //         footer={null}
    //     >
    //         <FormForm_/>
    //     </Modal>
    // </>

    if (!configuration?.has_ldap) {
        if (isSettingsLoading) {
            return <Spinner size="tiny"/>
        }
        return null
    }

    if (isOpen) {
        return <>
            <Divider>or</Divider>
            <FormForm_ withTitle onLoginSuccess={props.onLoginSuccess}/>
        </>
    }
    return <Button
        label="Login with LDAP"
        onClick={() => {
            setIsOpen(true)
            props.onOpen?.()
        }}
    />
}


const FormForm_: React.FC<{
    withTitle?: boolean
    onLoginSuccess?: () => void
}> = props => {
    const [username_, setUsername] = useState<string>()
    // const [userDomain_, setUserDomain] = useState<string | null>(null)
    const [password, setPassword] = useState<string>()
    const [error, setError] = useState<Error | undefined>()
    const [isPending, setIsPending] = useState<boolean>(false)
    const [isFailed, setIsFailed] = useState<boolean>(false)
    const [isSuccess, setIsSuccess] = useState<boolean>(false)

    const _username_split = username_?.split?.('@', 2) ?? []
    const username = _username_split?.[0] ?? null
    const userDomain = _username_split?.[1] ?? null

    const firstInputRef = useRef<HTMLInputElement>()
    useEffect(() => {
        firstInputRef?.current?.focus?.()
    }, [firstInputRef?.current])

    const canSubmit = username && password && !isPending

    const queryClient = useQueryClient()

    const submit = useCallback((ev?: FormEvent) => {
        ev?.preventDefault?.();
        // NOTE: semicolon required at previous code line.
        (async () => {
            setError(undefined)
            setIsPending(true)
            setIsFailed(false)
            setIsSuccess(false)
            try {
                const resp = await fetchPublicLdapAuthenticateAuthenticate({
                    body: {
                        username: username,
                        password: password,
                        user_domain: userDomain,
                    }
                });
                const status = resp?.status
                if (status === "welcome_back") {
                    await queryClient.invalidateQueries()
                    setIsSuccess(true)
                    props?.onLoginSuccess()
                    return
                }
                if (status === "failed") {
                    setIsFailed(true)
                    return
                }
                const []: never[] = [status]  // static completeness check
            } catch (err) {
                console.trace(err)
                setError(err)
            } finally {
                await new Promise(resolve => setTimeout(resolve, 2000))
                setIsPending(false)
            }
        })();
        return false
    }, [username, password])

    const {data: me_data} = usePublicMeGetMe({}, {enabled: isSuccess})

    if (isSuccess) {
        return <Result
            status="success"
            title={`Welcome back${me_data?.full_name ? `, ${me_data.full_name}` : ''}`}
        />
    }

    return <form onSubmit={submit}>
        <Stack rowGap="XL">
            <Stack rowGap="S">
                {props.withTitle && <PageSectionTitle>LDAP</PageSectionTitle>}
                <LoginField
                    innerRef={firstInputRef}
                    label={userDomain !== null ? "Username@domain" : "Username"}
                    type="text"
                    autoComplete="username"
                    value={username_}
                    onChange={setUsername}
                    onEnter={canSubmit ? submit : undefined}
                    required
                    authFailure={isFailed}
                    disabled={isPending}
                />
                {/*<LoginField*/}
                {/*    innerRef={firstInputRef}*/}
                {/*    label="User domain"*/}
                {/*    type="text"*/}
                {/*    autoComplete="username"*/}
                {/*    value={userDomain}*/}
                {/*    onChange={setUserDomain}*/}
                {/*    onEnter={canSubmit ? submit : undefined}*/}
                {/*    required*/}
                {/*    authFailure={isFailed}*/}
                {/*    disabled={isPending}*/}
                {/*/>*/}
                <LoginField
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    value={password}
                    onChange={setPassword}
                    onEnter={canSubmit ? submit : undefined}
                    required
                    authFailure={isFailed}
                    disabled={isPending}
                />
            </Stack>

            <MaybeErrorAlert error={error}/>

            {isFailed && <Text weight="semibold" style={{color: tokens.colorPaletteRedForeground1}}>
                Wrong username or password
            </Text>}

            <Stack horizontal columnGap="XL" alignItems="center" justifyContent="space-between">
                <Button
                    appearance="primary"
                    onClick={submit}
                    disabled={isPending || !canSubmit}
                    label={isPending ? <Spinner/> : "Login"}
                />
            </Stack>
        </Stack>
    </form>
}