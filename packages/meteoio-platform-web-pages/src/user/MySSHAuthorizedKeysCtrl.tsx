import * as React from 'react'
import {useCallback, useEffect, useMemo, useState} from 'react'
import {
    fetchPublicMeSshAuthorizedKeysAddSshAuthorizedKey,
    fetchPublicMeSshAuthorizedKeysEntryIdRmSshAuthorizedKey,
    SSHAuthorizedKey,
    SSHAuthorizedKeyAddResponse,
    usePublicMeSshAuthorizedKeysGetSshAuthorizedKeys
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Button} from "meteoio-ui/src/components/Button";
import {Badge, Text, tokens} from "@fluentui/react-components";
import {FreeTextField} from "meteoio-ui/src/components/FreeTextField";
import {
    BinRecycleFilled,
    BinRecycleRegular,
    bundleIcon,
    KeyRegular,
    SaveFilled,
    SaveRegular
} from '@fluentui/react-icons'
import {FieldDescription} from "meteoio-ui/src/components/FieldDescription";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {useDialogs} from "meteoio-ui/src/hooks/useDialogs";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {Column, SortableTable} from "meteoio-ui/src/components/SortableTable";
import {FreeCodeDisplayLikeField} from "meteoio-ui/src/components/FreeTextDisplayLikeField";

const SaveIcon = bundleIcon(SaveFilled, SaveRegular)
const RemoveIcon = bundleIcon(BinRecycleFilled, BinRecycleRegular)


export const MySSHAuthorizedKeysCtrl: React.FC<{
    onCancel?: () => void
    onHasChangesChange?: (hasChanges: boolean) => void
}> = props => {

    const {
        data,
        isLoading,
        error,
        refetch,
    } = usePublicMeSshAuthorizedKeysGetSshAuthorizedKeys({})

    const [draft, setDraft] = useState<SSHAuthorizedKey | undefined>(undefined)

    useEffect(() => {
        props.onHasChangesChange?.(!!draft)
    }, [draft])

    const dialogs = useDialogs()

    const SSH_KEYS_TABLE_COLUMNS = useMemo<Column<SSHAuthorizedKey>[]>(() => [
        {
            title: 'Title',
            field: 'public_title',
        },
        {
            title: 'Key fingerprint',
            field: 'fingerprint',
            onRenderCellContent(row) {
                return <Text block wrap={false} font="monospace" size={100}>
                    {row.fingerprint}
                </Text>
            }
        },
        {
            title: 'Added',
            field: 'insert_date',
            onRenderCellContent(row) {
                return <RelativeTimeText date={row.insert_date}/>
            }
        },
        {
            title: 'Actions',
            textAlign: 'end',
            onRenderCellContent(row) {
                return <>
                    <RmKeyBtn iconOnly value={row} onDelete={async () => {
                        await refetch()
                    }}/>
                </>
            }
        }
    ], [refetch])

    const showKeyDetails = useCallback((entry: SSHAuthorizedKey) => {
        const pop = dialogs.popup(<KeyEntry
            value={entry}
            onDelete={async () => {
                await refetch()
                pop.destroy()
            }}
        />)
    }, [dialogs])

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    if (isLoading) {
        return <Spinner tall/>
    }

    // TODO: show explanation and reference to some docs or guide

    return <>
        <Stack rowGap="L">
            <Stack horizontal alignItems="center" justifyContent="space-between" wrap>
                <Stack horizontal alignItems="baseline">
                    <PageSectionTitle>
                        Your SSH Keys
                    </PageSectionTitle>
                    <Badge
                        appearance="outline"
                        color="informative"
                        iconPosition="after"
                        size="medium"
                        //icon={<KeyFilled/>}
                    >
                        <KeyRegular/>
                        &nbsp;
                        {data?.length ?? <Spinner/>}
                    </Badge>
                </Stack>
                <Stack horizontal alignItems="center">
                    {!draft && <Button
                        size="small"
                        label="Add new key"
                        disabled={!!draft}
                        onClick={() => {
                            if (!draft) {
                                setDraft({
                                    public_key: '',
                                    public_title: '',
                                })
                            }
                        }}
                    />}
                </Stack>
            </Stack>

            {draft && <div style={{
                padding: '1em',
                border: `1px solid ${tokens.colorNeutralStroke2}`,
                borderRadius: tokens.borderRadiusMedium
            }}>
                <PageSectionTitle>
                    Add an SSH key
                </PageSectionTitle>
                <Text block style={{margin: '0.5em 0'}} size={200}>
                    To add an SSH key you need to generate one (e.g. using `ssh-keygen`) or use an existing key.
                    The public key is usually contained in the file '~/.ssh/id_ed25519.pub' or '~/.ssh/id_rsa.pub'
                    and begins with 'ssh-ed25519' or 'ssh-rsa'.
                    Do not paste your private SSH key.
                </Text>
                <NewKeyForm
                    value={draft}
                    onChange={setDraft}
                    onDismiss={() => {
                        setDraft(undefined)
                    }}
                    onSuccess={async (added_entry_id) => {
                        const data2 = await refetch()
                        setDraft(undefined)
                        const entry = data2?.data?.find?.(entry => entry.entry_id === added_entry_id)
                        if (entry) {
                            //showKeyDetails(entry)
                            dialogs.message.success('Key added successfully')
                        }
                    }}
                />
            </div>}

            {data?.length >= 1 && <SortableTable
                rowKeyField="entry_id"
                columns={SSH_KEYS_TABLE_COLUMNS}
                rows={data ?? []}
                onRowClick={row => {
                    showKeyDetails(row)
                }}
            />}

            {data?.length === 0 && !draft && <>
                <Stack>
                    <Text block style={{color: tokens.colorNeutralForegroundDisabled, textAlign: 'center'}}>
                        There are no SSH keys with access to your account.
                    </Text>
                </Stack>
            </>}
        </Stack>
    </>
}

const KeyEntry: React.FC<{
    value: SSHAuthorizedKey
    onDelete: () => Promise<void>
}> = props => {
    return <>
        <Stack rowGap="L">
            <PageSectionTitle>
                Key: {props.value.public_title}
            </PageSectionTitle>
            <FreeCodeDisplayLikeField
                label="Public key"
                value={props.value.public_key}
            />
            <FreeCodeDisplayLikeField
                label="Fingerprint"
                value={props.value.fingerprint}
            />
            <FreeCodeDisplayLikeField
                label="Added"
                value={props.value.insert_date}
            />
            <Stack horizontal>
                <RmKeyBtn value={props.value} onDelete={props.onDelete}/>
            </Stack>
        </Stack>
    </>
}

const RmKeyBtn: React.FC<{
    iconOnly?: boolean
    value: SSHAuthorizedKey,
    onDelete: () => Promise<void>,
    stopClickEv?: boolean
}> = props => {
    const dialogs = useDialogs()
    return <AsyncActionButton
        appearance="outline"
        noInlinePadding={props.iconOnly}
        label={props.iconOnly ? null : "Delete"}
        size="small"
        danger
        icon={<RemoveIcon color={tokens.colorPaletteRedForeground1}/>}
        onClick={async (ev) => {
            ev.stopPropagation()
            if (await dialogs.confirm('Are you sure to want to remove this SSH key from your account?',
                {cancelText: 'Keep it', okType: 'danger', okText: 'Remove'})) {
                await fetchPublicMeSshAuthorizedKeysEntryIdRmSshAuthorizedKey({
                    pathParams: {entryId: props.value.entry_id}
                })
                await props.onDelete()
                dialogs.message.success('Key removed successfully')
            }
        }}
    />
}

const NewKeyForm: React.FC<{
    value: SSHAuthorizedKey
    onChange: (value: SSHAuthorizedKey) => void
    onDismiss: () => void
    onSuccess: (added_entry_id: string) => Promise<void>
}> = props => {
    const dialogs = useDialogs()
    const [response, setResponse] = useState<SSHAuthorizedKeyAddResponse | undefined>()

    const canSubmit = props.value.public_key && props.value.public_title

    return <>
        <Stack rowGap="L">
            <Stack rowGap="S">
                <FieldDescription
                    field={<FreeTextField
                        label="Title"
                        value={props.value.public_title}
                        onChange={value => props.onChange({...props.value, public_title: value})}
                        maxLength={40}
                    />}
                    description="You can give a title to this key (e.g. Office laptop)."
                />
                <FieldDescription
                    field={<FreeTextField
                        label="Public key"
                        value={props.value.public_key}
                        multiline
                        smallCode
                        onChange={value => props.onChange({...props.value, public_key: value})}
                    />}
                    description="Paste here your public key (don't share your private key)."
                />
            </Stack>
            <Stack horizontal>
                <AsyncActionButton
                    appearance="primary"
                    label="Add key"
                    disabled={!canSubmit}
                    icon={<SaveIcon/>}
                    onClick={async () => {
                        setResponse(undefined)
                        try {
                            const resp = await fetchPublicMeSshAuthorizedKeysAddSshAuthorizedKey({
                                body: props.value
                            })
                            setResponse(resp)
                            if (resp.added_entry_id) {
                                await props.onSuccess(resp.added_entry_id)
                            }
                        } catch (e) {
                            setResponse(e.stack)
                        }
                    }}
                />
                <AsyncActionButton
                    appearance="outline"
                    label="Cancel"
                    icon={<RemoveIcon/>}
                    onClick={async () => {
                        if (await dialogs.confirm('Are you sure to cancel adding this key?', {cancelText: "Continue editing"})) {
                            props.onDismiss()
                        }
                    }}
                />
            </Stack>
            <MaybeErrorAlert error={response?.error}/>
        </Stack>
    </>
}
