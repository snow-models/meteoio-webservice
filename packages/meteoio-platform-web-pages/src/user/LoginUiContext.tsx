import * as React from 'react'
import {createContext, PropsWithChildren, useMemo, useState} from "react";


interface ILoginUiContext {
    isLoginOpen?: boolean
    openLoginForm?: () => void
    closeLoginForm?: () => void
    setIsLoginOpen?: (isLoginOpen: boolean) => void
}

export const LoginUiContext = createContext<ILoginUiContext>({})

export const LoginUiContextProvider: React.FC<PropsWithChildren> = props => {
    const [isLoginOpen, setIsLoginOpen] = useState<boolean>(false)

    const ctx = useMemo(() => {
        return {
            isLoginOpen,
            openLoginForm: () => setIsLoginOpen(true),
            closeLoginForm: () => setIsLoginOpen(false),
            setIsLoginOpen: (isLoginOpen: boolean) => setIsLoginOpen(isLoginOpen)
        }
    }, [isLoginOpen, setIsLoginOpen])

    return <LoginUiContext.Provider value={ctx}>
        {props.children}
    </LoginUiContext.Provider>
}