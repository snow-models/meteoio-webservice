import * as React from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PersonAccountsRegular, SignOutRegular} from "@fluentui/react-icons";
import {Link, tokens} from "@fluentui/react-components";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {fetchPublicLogoutLogout, usePublicMeGetMe} from "meteoio-platform-client";
import {useHref} from "react-router-dom";
import {address_book} from "../address_book";
import {ORCIDiD_IconVector} from "meteoio-ui/src/components/ORCIDiD_IconVector";
import {useQueryClient} from "../_common/backend";

export const UserSheet: React.FC<{
    onLoggedOut?: () => void
}> = props => {
    const afterLogoutHref = useHref(address_book.after_logout)
    const queryClient = useQueryClient()

    const {
        data,
        error,
        isLoading,
    } = usePublicMeGetMe({})

    return <>
        <Stack rowGap="XL">
            <Stack columnGap="XL" horizontal>
                <PersonAccountsRegular fontSize={tokens.fontSizeHero700}/>
                <Stack rowGap="XL">
                    <MaybeErrorAlert error={error}/>
                    <div>
                        <div
                            style={{display: 'grid', gridTemplateColumns: 'auto 1fr', columnGap: '1em', rowGap: '1em'}}>
                            {data?.email && <><span>Email:</span><span>{data?.email}</span></>}
                            {data?.full_name && <><span>Name:</span><span>{data?.full_name}</span></>}
                            {/*{data?.id && <><span>User ID:</span><code><small>{data?.id}</small></code></>}*/}
                            {data?.openid?.map?.((oid, i) => <React.Fragment key={`${i}-${oid.issuer}`}>
                                {['https://sandbox.orcid.org/', 'https://orcid.org/'].includes(oid.issuer) && <>
                                    <span style={{textAlign: 'right', fontSize: '1.4em'}}>
                                        <ORCIDiD_IconVector/>
                                    </span>
                                    <Link href={`${oid.issuer}${oid.subject}`} target="_blank">
                                        {oid.subject}
                                    </Link>
                                </>}
                            </React.Fragment>)}
                        </div>
                    </div>
                </Stack>
            </Stack>

            {isLoading && <Spinner/>}

            <MaybeErrorAlert error={error}/>

            <AsyncActionButton
                appearance="secondary"
                label="Log out"
                icon={<SignOutRegular/>}
                onClick={async () => {
                    await fetchPublicLogoutLogout({})
                    const cache_invalidation1 = queryClient.invalidateQueries()
                    await new Promise(resolve => setTimeout(resolve, 1000))
                    const cache_invalidation2 = queryClient.invalidateQueries()
                    props?.onLoggedOut?.()
                    window?.location?.assign?.(afterLogoutHref)
                    setTimeout(() => {location?.reload?.()}, 100)  // in case the location is the same
                    await cache_invalidation1
                    await cache_invalidation2
                }}
            />
        </Stack>
    </>
}
