import * as React from 'react'
import {ORCIDiD_LoginButton} from "meteoio-ui/src/components/ORCIDiD_LoginButton";
import {fetchPublicOicIssuerInitOicInit, usePublicOicConfigOicProvidersList} from "meteoio-platform-client";
import {useQueryClient} from "../_common/backend";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";

export const OicLoginForm: React.FC = props => {
    const queryClient = useQueryClient()
    const cfg = usePublicOicConfigOicProvidersList({})

    if (cfg.isLoading) {
        return <Spinner/>
    }
    if (cfg.error) {
        return <MaybeErrorAlert error={cfg.error}/>
    }
    return <>
        {cfg?.data?.providers?.map?.(config => {
            const Btn = config.icon === 'orcid.org'
                ? ORCIDiD_LoginButton
                : AsyncActionButton
            return <Btn
                key={config.config_key}
                label={<>
                    &nbsp;
                    {config.login_button_label}
                </>}
                onClick={async () => {
                    const res = await fetchPublicOicIssuerInitOicInit({
                        pathParams: {
                            issuer: config.config_key,
                        },
                        queryParams: {
                            final_redirect_url: window.location.href
                        }
                    })
                    // const invalidation = queryClient.invalidateQueries()
                    // NOTE: Do not invalidate. Invalidation can trigger refetches that can go cancelled.
                    queryClient.clear()  // This should clear the cache for the after the login.
                    window.location.assign(res.login_url)
                    // NOTE: the following delay keeps the spinner displayed while the page is redirected.
                    await new Promise(resolve => setTimeout(resolve, 3000))
                    // await invalidation
                }}
            />
        })}
    </>
}
