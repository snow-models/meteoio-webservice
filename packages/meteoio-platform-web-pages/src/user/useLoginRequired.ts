import {useEffect, useState} from "react";


export type LoginRequiredListener = (isLoginRequired: boolean) => void


const SIGNALS: object[] = []
const LISTENERS: LoginRequiredListener[] = []


export function useLoginRequiredSignaler(enable: boolean) {
    useEffect(() => {
        if (enable) {
            const sig = {}
            SIGNALS.push(sig)
            LISTENERS.forEach(li => li(true))
            return () => {
                //setTimeout(() => {
                    SIGNALS.splice(SIGNALS.indexOf(sig), 1)
                    const isLoginRequired = SIGNALS.length > 0
                    LISTENERS.forEach(li => li(isLoginRequired))
                //}, 500);
            }
        }
    }, [enable])
}

export function useLoginRequiredListener(callback: LoginRequiredListener) {
    useEffect(() => {
        LISTENERS.push(callback)
        return () => {
            LISTENERS.splice(LISTENERS.indexOf(callback), 1)
        }
    }, [])
}

export function useLoginRequiredErrorSignaler(error?: Error | {stack?: {status_code?: number}} | any): boolean {

    // @ts-ignore
    const isLoginRequired = error?.stack?.status_code === 401 ||
        // @ts-ignore
        error?.stack?.status_code === 403

    useLoginRequiredSignaler(isLoginRequired)

    return isLoginRequired
}

// NOTE: this unusual pattern may not be the best UX ... maybe just implement a button in MaybeErrorAlert.
