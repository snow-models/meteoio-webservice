import * as React from 'react'
import {useContext, useMemo, useState} from 'react'
import {PersonAccountsRegular, PersonRegular} from "@fluentui/react-icons";
import {Divider, Text, tokens} from "@fluentui/react-components";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Button} from "meteoio-ui/src/components/Button";
import {Modal} from "antd";
import {LoginForm} from "./LoginForm";
import {usePublicMeGetMe, UserReflection} from "meteoio-platform-client";
import {ThemeProvider} from "meteoio-ui/src/providers/ThemeProvider";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {UserSheet} from "./UserSheet";
import {useLoginRequiredListener} from "./useLoginRequired";
import {OicLoginForm} from "./OicLoginForm";
import {useOrcidFullNames} from "../_common/orcid_user_data";
import {LdapLoginForm} from "./LdapLoginForm";
import {LoginUiContext} from "./LoginUiContext";

export const UserTopFrag: React.FC = () => {
    const {isLoginOpen, setIsLoginOpen} = useContext(LoginUiContext)
    const [isLogoutOpen, setIsLogoutOpen] = useState<boolean>(false)

    //const [isLoginRequired, setIsLoginRequired] = useState<boolean>(false)

    useLoginRequiredListener(isLoginRequired => {
        //setIsLoginRequired(isLoginRequired)
        if (isLoginRequired) {
            setIsLoginOpen(true)
        }
    })

    const {
        data,
        error,
        isLoading,
        refetch,
    } = usePublicMeGetMe({}, {
        cacheTime: 600_000,
    })

    const [showEmailLogin, setShowEmailLogin] = useState<boolean>(false)

    const {getFullName: getOrcidFullName, loadProgressive: _orcidFNLoadProg} = useOrcidFullNames(data?.openid)
    const enriched_user = useMemo<UserReflection | undefined>(() => ({
        ...(data ?? {}),
        full_name: data?.full_name ?? (data?.openid?.[0] ? getOrcidFullName(data?.openid?.[0]) : undefined)
    }), [data, _orcidFNLoadProg])

    return <>
        <Stack horizontal alignItems="center" columnGap="XS">
            {isLoading ? <Spinner/> : <>
                {data?.is_logged_in
                    ? <Button
                        appearance="subtle"
                        icon={<PersonAccountsRegular fontSize={tokens.fontSizeBase400}/>}
                        label={enriched_user?.full_name ?? enriched_user?.email ?? `User ${(data.id ?? '').slice(-8)}`}
                        onClick={() => setIsLogoutOpen(true)}
                    />
                    : <Button
                        appearance="subtle"
                        icon={<PersonRegular fontSize={tokens.fontSizeBase400}/>}
                        label="Guest"
                        onClick={() => setIsLoginOpen(true)}
                    />}
            </>}
        </Stack>
        <Modal
            width={350}
            open={isLoginOpen} // || isLoginRequired}
            onCancel={() => setIsLoginOpen(false)} //isLoginRequired ? () => {} : () => setIsLoginOpen(false)}
            //closable={!isLoginRequired}
            footer={null}
        >
            <ThemeProvider>
                {/*isLoginRequired && <>
                    <Button
                        size="small"
                        appearance="subtle"
                        noInlinePadding
                        icon={<ArrowLeftRegular/>}
                        label={<>Back &nbsp;</>}
                        onClick={() => {navigate(-1)}}
                    />
                    <br />
                    <br />
                </>*/}
                <Stack rowGap="XXXL">
                    <Text size={600}>
                        User Login
                    </Text>
                    <MaybeErrorAlert error={error}/>
                    <Stack rowGap="M">
                        <OicLoginForm/>
                        <LdapLoginForm
                            onOpen={() => setShowEmailLogin(false)}
                            onLoginSuccess={() => {
                                refetch().then()
                                setTimeout(() => {
                                    setIsLoginOpen(false)
                                }, 2000)
                            }}
                        />
                    </Stack>
                    {showEmailLogin ? <>
                        <Divider>or</Divider>
                        <LoginForm
                            onLoginSuccess={() => {
                                refetch().then()
                                setTimeout(() => {
                                    setIsLoginOpen(false)
                                }, 2000)
                            }}
                        />
                    </> : <>
                        <Button
                            size="small"
                            appearance="subtle"
                            label="Login with email"
                            onClick={() => setShowEmailLogin(true)}
                        />
                    </>}
                </Stack>
            </ThemeProvider>
        </Modal>
        <Modal
            width={400}
            open={isLogoutOpen}
            onCancel={() => setIsLogoutOpen(false)}
            footer={null}
        >
            <ThemeProvider>
                <UserSheet onLoggedOut={() => setIsLogoutOpen(false)}/>
            </ThemeProvider>
        </Modal>
    </>
}