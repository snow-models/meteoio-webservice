import * as React from 'react'
import {Button} from "meteoio-ui/src/components/Button";
import {useContext} from "react";
import {LoginUiContext} from "./LoginUiContext";
import {ArrowEnterRegular} from '@fluentui/react-icons'

export const LoginBtn: React.FC = () => {
    const {openLoginForm} = useContext(LoginUiContext)
    return <>
        <Button
            onClick={() => {
                openLoginForm?.()
            }}
            label="Login"
            icon={<ArrowEnterRegular/>}
        />
    </>
}