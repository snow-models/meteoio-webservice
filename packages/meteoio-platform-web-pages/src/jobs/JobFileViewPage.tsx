import * as React from 'react'
import {useHref, useParams} from "react-router-dom";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Link} from "@fluentui/react-components";
import {address_book} from "../address_book";
import {NiVizFileView} from "./job/NiVizFileView";
import {AsyncActionButton} from "meteoio-ui/src/components/AsyncActionButton";
import {downloadJobFile} from "./job/downloadJobFile";
import {message} from "antd";
import {ArrowDownloadRegular, ArrowLeftRegular} from "@fluentui/react-icons";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";


export const JobFileViewPage: React.FC = () => {
    const {jobId, '*': filePath} = useParams()

    const [messageApi, messageContextHolder] = message.useMessage({maxCount: 1})
    const jobHref = useHref(address_book.jobs.view(jobId))

    return <>
        {messageContextHolder}
        <br/>
        <Stack rowGap="XL" center>
            <Link href={jobHref}>
                <ArrowLeftRegular/> &nbsp; Job <code>{jobId}</code>
            </Link>
            <Stack horizontal justifyContent="flex-start" wrap columnGap="XL">
                <AsyncActionButton
                    appearance="primary"
                    label={"Download"}
                    icon={<ArrowDownloadRegular/>}
                    onClick={async () => {
                        const path_parts = filePath.split('/')
                        const name = path_parts[path_parts.length - 1]
                        await downloadJobFile(messageApi, jobId, filePath, name)
                    }}
                />
                <PageSectionTitle>
                    {filePath}
                </PageSectionTitle>
            </Stack>

            <NiVizFileView
                jobId={jobId}
                path={filePath}
            />
        </Stack>
    </>
}
