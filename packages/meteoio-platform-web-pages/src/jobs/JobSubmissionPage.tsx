import * as React from 'react'
import {useEffect, useState} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Button} from "meteoio-ui/src/components/Button";
import {Spinner} from 'meteoio-ui/src/components/Spinner';
import {FilesField} from "meteoio-ui/src/components/FilesField";
import {useHref, useNavigate} from "react-router-dom";
import {address_book} from "../address_book";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {mkFileUploadVariables} from '../_common/backend'
import {
    fetchPublicJobSubmissionCreate,
    fetchPublicJobSubmissionJobIdFilesPutFile,
    fetchPublicJobSubmissionJobIdStartStart,
    TimeSeriesJobParams
} from "meteoio-platform-client";
import {useJobsHistory} from "./useJobsHistory";
import {TimeSeriesJobParamsEditor} from "../_common/TimeSeriesJobParamsEditor";
import {ArrowEnterUpRegular} from "@fluentui/react-icons";


// TODO?: Let the user choose:
//          - Login in order to save the job submissions and continue later
//          - Do not save


export const JobSubmissionPage: React.FC = () => {
    const [params, setParams] = useState<Partial<TimeSeriesJobParams>>(() => ({
        range: {
            end: 'NOW',
            duration_days: 30,
        }
    }))

    const [dataFiles, setDataFiles] = useState<File[] | undefined>([])

    const iniFileNames = dataFiles
        ?.filter?.(file => file?.name?.toLowerCase?.()?.endsWith?.('ini'))
        ?.map(file => file.name)

    useEffect(() => {
        if (!params?.ini && iniFileNames?.length > 0) {
            setParams(pp => ({...pp, ini: iniFileNames?.[0]}))
            window?.scrollTo?.({
                top: document?.body?.scrollHeight,
                behavior: 'smooth'
            })
        }
    }, [iniFileNames])

    const [isSubmitPending, setIsSubmitPending] = useState<boolean>(false)
    const [jobId_pre, setJobId_pre] = useState<string | undefined>()
    const [error, setError] = useState<Error>()

    const navigate = useNavigate()
    const {add: addToHistory} = useJobsHistory()
    const histHref = useHref(address_book.jobs.list)

    const maySubmit = dataFiles?.length > 0

    return <>
        <br/>
        <Stack rowGap="XXXL" maxWidth={600} center>
            <Stack horizontal justifyContent="space-between" wrap>
                <PageTitle>
                    Guest Job Submission
                </PageTitle>
                <Button
                    href={histHref}
                    appearance="subtle"
                    label={"View last submissions"}
                />
            </Stack>

            <Stack rowGap="M">
                <FilesField
                    label="Working directory"
                    value={dataFiles}
                    onChange={setDataFiles}
                    minHeight={dataFiles?.length > 0 ? undefined : 100}
                />

                {dataFiles?.length > 0
                    ? <TimeSeriesJobParamsEditor
                        value={params}
                        onChange={setParams}
                        iniFileNames={iniFileNames}
                        inisError={iniFileNames?.length > 0 ? undefined
                            : 'No suitable INI file in the working directory.'}
                    />
                    : <span style={{opacity: 0.8}}>
                        <ArrowEnterUpRegular/>
                        &nbsp; &nbsp;
                        Add an INI file to continue.
                </span>}
            </Stack>

            <MaybeErrorAlert error={error}/>

            <Stack horizontal justifyContent="right">
                <Button
                    size="large"
                    wide
                    appearance="primary"
                    label={<>
                        Launch Job
                        {isSubmitPending && <>
                            &nbsp; &nbsp;
                            <Spinner size="tiny"/>
                        </>}
                    </>}
                    disabled={isSubmitPending || !maySubmit}
                    onClick={async () => {
                        setIsSubmitPending(true)
                        try {
                            let jobId = jobId_pre
                            if (!jobId) {
                                const job = await fetchPublicJobSubmissionCreate({})
                                jobId = job.id
                                setJobId_pre(jobId)
                            }

                            await Promise.all(dataFiles.map(file =>
                                fetchPublicJobSubmissionJobIdFilesPutFile({
                                    pathParams: {
                                        jobId,
                                    },
                                    queryParams: {
                                        path: `/${file.name}`,
                                    },
                                    // @ts-ignore
                                    ...mkFileUploadVariables(file)
                                })
                            ))

                            await fetchPublicJobSubmissionJobIdStartStart({
                                pathParams: {jobId},
                                body: params as TimeSeriesJobParams
                            })

                            // Let's introduce some artificial delay to guarantee some visual feedback.
                            await new Promise<void>(resolve => setTimeout(resolve, 100))
                            // In the meantime, the job is being queued and will be processing.

                            addToHistory({
                                id: jobId,
                                submitted: new Date(),
                            })

                            navigate(address_book.jobs.view(jobId))
                        } catch (e) {
                            console.trace(e)
                            setError(e)
                        } finally {
                            setIsSubmitPending(false)
                        }
                    }}
                />
            </Stack>

            <br/>
        </Stack>
    </>
}
