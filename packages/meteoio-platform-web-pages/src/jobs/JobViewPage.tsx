import * as React from 'react'
import {useMemo} from 'react'
import {Route, Routes, useHref, useMatch, useNavigate, useParams} from "react-router-dom";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {
    usePublicJobSubmissionJobIdAccessLevelGetJobAccessLevel,
    usePublicJobSubmissionJobIdStatsStats,
    usePublicJobSubmissionJobIdStatusStatus,
} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Tab, TabList} from "@fluentui/react-components";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {address_book} from "../address_book";
import {JobHeading} from "./job/JobHeading";
import {IJobContext, JobContext} from "./job/JobContext";
import {Tab_Logs} from "./job/Tab_Logs";
import {Tab_Files} from "./job/Tab_Files";
import {Tab_Output} from "./job/Tab_Output";
import {Button} from "meteoio-ui/src/components/Button";

const ALL_TABS = ['output', 'files', 'logs']


export const JobViewPage: React.FC = () => {
    const {jobId} = useParams()
    const locationMatch = useMatch('*')
    const navigate = useNavigate()
    const initialLoadDate = useMemo<Date>(() => new Date(), [])

    const selectedTabValue = useMemo<string | undefined>(() =>
            ALL_TABS.find(tab => locationMatch?.pathname?.startsWith?.(address_book.jobs.view_tab(jobId, tab))),
        [locationMatch, jobId])

    const jobAccess = usePublicJobSubmissionJobIdAccessLevelGetJobAccessLevel({pathParams: {jobId}})

    const jobStatus = usePublicJobSubmissionJobIdStatusStatus({pathParams: {jobId}}, {
        refetchIntervalInBackground: false,
        refetchInterval: (data, query) => {
            if (query?.state?.error) {
                return 30_000
            }
            const i = Math.min((query?.state?.dataUpdateCount ?? 10) / 10, 1)
            if (data?.is_finished) {
                return 24 * 3600_000
            }
            if (data?.is_running || data?.is_started) {
                return 500 + 2500 * i
            }
            return 200 + 2800 * i
        },
    })

    const doLoadStats = !!jobStatus?.data?.is_finished

    const jobStats = usePublicJobSubmissionJobIdStatsStats({pathParams: {jobId}}, {
        enabled: doLoadStats,
    })

    const defaultTabValue = (jobStatus?.data?.is_finished && jobStats?.data?.return_code === 0) || jobAccess.data?.access_level !== 'full' ? 'output' : 'logs'
    const defaultTabComp = defaultTabValue === 'output' ? Tab_Output : Tab_Logs

    const ctx = useMemo<IJobContext>(() =>
            ({jobId, status: jobStatus?.data, stats: jobStats?.data}),
        [jobId, jobStatus, jobStats])

    const histHref = useHref(address_book.jobs.list)

    if (jobStatus.error) {
        return <>
            <br/>
            <MaybeErrorAlert error={jobStatus.error}/>
        </>
    }

    if (jobStatus.isLoading ||
        (doLoadStats && jobStats.isInitialLoading && (Date.now() - initialLoadDate.getTime()) < 2000)) {
        return <Spinner tall/>
    }

    return <JobContext.Provider value={ctx}>
        <br/>
        <Stack rowGap="XL" maxWidth={600} center>
            <Stack horizontal justifyContent="space-between" wrap>
                <PageTitle>
                    Job {jobStatus?.data?.is_finished ? `Result` : `Status`}
                </PageTitle>
                <Button
                    href={histHref}
                    appearance="subtle"
                    label={"View last submissions"}
                />
            </Stack>

            <MaybeErrorAlert error={jobStatus.error ?? jobStats?.error}/>

            <JobHeading/>

            {jobStatus?.data?.is_started && <>
                <Stack rowGap="S">
                    <Stack horizontal>
                        <TabList
                            onTabSelect={(_, data) => {
                                // @ts-ignore
                                navigate(address_book.jobs.view_tab(jobId, data?.value?.toString?.()), {
                                    replace: true,
                                })
                            }}
                            selectedValue={selectedTabValue ?? defaultTabValue}
                        >
                            <Tab value="output" disabled={!jobStatus?.data?.is_finished}>Output</Tab>
                            {/*<Tab value="stderr">Stderr</Tab>*/}
                            <Tab value="files" disabled={!jobStatus?.data?.is_finished || jobAccess.data?.access_level !== 'full'}>Files</Tab>
                            <Tab value="logs" disabled={jobAccess.data?.access_level !== 'full'}>Logs</Tab>
                        </TabList>
                    </Stack>
                    <Routes>
                        <Route index Component={defaultTabComp}/>
                        <Route path="output" Component={Tab_Output}/>
                        <Route path="files" Component={Tab_Files}/>
                        <Route path="logs" Component={Tab_Logs}/>
                    </Routes>
                </Stack>
            </>}

            <br/>
        </Stack>
    </JobContext.Provider>
}
