import {usePublicJobSubmissionListMyJobs} from "meteoio-platform-client";
import {useMemo} from "react";
import {parseDate} from "../_common/parseDate";

export interface JobHistEntry {
    id: string
    submitted: string | Date
}

const _local_key = 'SUBMITTED_JOB_IDS_15e00b1e-9803-435e-8cc2-83c0d1bd9194'

const _local_list: () => JobHistEntry[] = () => {
    const s = window?.sessionStorage?.getItem(_local_key)
    if (s) {
        return JSON.parse(s)
    } else {
        return []
    }
}

const _local_write = (jobs: JobHistEntry[]) => {
    window?.sessionStorage?.setItem(_local_key, JSON.stringify(jobs))
}

const _local_add = (job: JobHistEntry) => {
    _local_write([..._local_list().slice(-10), job])
}

export function useJobsHistory() {
    const {
        data,
        isLoading,
    } = usePublicJobSubmissionListMyJobs({})

    const jobs = useMemo<JobHistEntry[]>(() => {
        const jobs: JobHistEntry[] = _local_list()
        const ids = new Set<string>(jobs.map(j => j.id));
        data?.forEach?.(j => {
            if (!ids.has(j.id)) {
                ids.add(j.id)
                jobs.push({
                    id: j.id,
                    submitted: parseDate(j.created_at),
                })
            }
        })
        jobs.sort((a, b) => {
            const ta = new Date(a?.submitted).getTime()
            const tb = new Date(b?.submitted).getTime()
            return ta == tb ? 0 : (ta < tb ? -1 : 1)
        })
        return jobs
    }, [data])

    return {
        isLoading,
        add: _local_add,
        jobs,
    }
}
