import * as React from 'react'
import {useMemo} from 'react'
import {Link, useHref, useNavigate} from "react-router-dom";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {address_book} from "../address_book";
import {Button} from "meteoio-ui/src/components/Button";
import {Column, SortableTable} from "meteoio-ui/src/components/SortableTable";
import {JobHistEntry, useJobsHistory} from "./useJobsHistory";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Empty} from "antd";

const COLUMNS: Column<JobHistEntry>[] = [
    {
        field: 'submitted',
        title: 'Submitted',
        onRenderCellContent(job) {
            return <RelativeTimeText date={job.submitted}/>
        }
    },
    {
        field: 'id',
        title: 'ID',
        onRenderCellContent(job) {
            return <Link to={address_book.jobs.view(job.id)}>
                <code>{job.id}</code>
            </Link>
        }
    },
]

export const JobsListPage: React.FC = () => {
    const {isLoading, jobs: _jobs} = useJobsHistory()
    const jobs = useMemo(() => _jobs.slice().reverse(), [_jobs])
    const newSubmissionHref = useHref(address_book.jobs.create)
    const navigate = useNavigate()

    if (isLoading) {
        return <Spinner tall/>
    }

    return <>
        <br/>
        <Stack rowGap="XXXL" maxWidth={600} center>
            <Stack horizontal justifyContent="space-between">
                <PageTitle>
                    Last Job Submissions
                </PageTitle>
                <Button
                    href={newSubmissionHref}
                    appearance="subtle"
                    label={"Submit new job"}
                />
            </Stack>

            {jobs?.length > 0 ? <>
                <SortableTable
                    columns={COLUMNS}
                    rows={jobs}
                    onRowClick={job => navigate(address_book.jobs.view(job.id))}
                />
            </> : <>
                <Empty/>
            </>}

            <br/>
        </Stack>
    </>
}
