import * as React from 'react'
import {useContext, useState} from 'react'
import {usePublicJobSubmissionJobIdFilesListListFiles} from "meteoio-platform-client";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {mergeClasses, Text, tokens} from "@fluentui/react-components";
import {useJobViewStyles} from "./styles";
import {JobContext} from "./JobContext";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {Button} from "meteoio-ui/src/components/Button";
import {ArrowClockwiseFilled, ArrowUpRegular, ChevronRightRegular, FolderOpenRegular} from "@fluentui/react-icons";
import {Breadcrumb} from "meteoio-ui/src/components/Breadcrumb";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {FilesList} from "meteoio-ui/src/components/FilesList";
import {Empty, message} from 'antd'
import {downloadJobFile} from "./downloadJobFile";

export const Tab_Files: React.FC = () => {
    return <>
        <JobResultFilesView/>
    </>
}


const JobResultFilesView: React.FC = () => {
    const {jobId, status} = useContext(JobContext)
    const styles = useJobViewStyles()
    const [path, setPath] = useState<string>('')

    if (!status?.is_finished || !jobId) {
        return <Spinner tall/>
    }
    return <div className={mergeClasses(styles.filesBox, styles.boxBorder)}>
        <JobResultFilesList path={path} onChDir={setPath}/>
    </div>
}


const JobResultFilesList: React.FC<{
    path?: string
    onChDir?: (path: string) => void
}> = props => {
    const {jobId, status} = useContext(JobContext)

    const {
        data,
        error,
        isLoading,
        refetch,
        isRefetching,
    } = usePublicJobSubmissionJobIdFilesListListFiles({
        pathParams: {jobId: jobId},
        queryParams: {path: props.path},
    }, {
        enabled: !!status?.is_finished,
        cacheTime: 60_000,
        staleTime: 10_000,
    })

    const [messageApi, messageContextHolder] = message.useMessage({maxCount: 1})

    return <>
        {messageContextHolder}
        <Stack rowGap="XXS">
            <Stack horizontal columnGap="None" alignItems="center">
                <Button
                    size="small"
                    disabled={props.path === ''}
                    appearance="subtle"
                    icon={<ArrowUpRegular title="Go back"/>}
                    onClick={() => {
                        props.onChDir?.(props.path.split('/').slice(0, -1).join('/'))
                    }}
                />
                <div style={{flexGrow: 1, border: `1px solid ${tokens.colorNeutralStroke2}`}}>
                    <Stack horizontal columnGap="None" alignItems="center" grow wrap>
                        <Button
                            size="small"
                            appearance="subtle"
                            noInlinePadding={props.path === ''}
                            icon={<FolderOpenRegular title="Go back to root"/>}
                            label={props.path === '' && 'Job working directory'}
                            onClick={() => props.onChDir?.('')}
                        />
                        <Breadcrumb
                            items={props.path?.split?.('/')?.map?.((name, i, items) => <Button
                                size="small"
                                appearance="subtle"
                                label={name}
                                noInlinePadding
                                onClick={name ? () => {
                                    props.onChDir?.(items?.slice?.(0, i + 1)?.join?.('/') ?? '/')
                                } : undefined}
                            />)}
                            sep={<ChevronRightRegular/>}
                        />
                    </Stack>
                </div>
                <Button
                    size="small"
                    appearance="subtle"
                    disabled={isLoading || isRefetching}
                    //icon={<ArrowClockwiseRegular title="Refresh"/>}
                    onClick={() => refetch?.()}
                    noInlinePadding
                    label={<Text size={300}>
                        &nbsp;&nbsp;
                        <ArrowClockwiseFilled title="Refresh"/>
                        &nbsp;&nbsp;
                    </Text>}
                />
            </Stack>
            <MaybeErrorAlert error={error}/>
            {(isLoading || isRefetching) ? <ProgressBar/> : <ProgressBar hide value={0}/>}
            <FilesList
                noTopBottomBorder
                files={[
                    // ...(props.path != '/' ? [{
                    //     name: '..',
                    //     type: 'folder',
                    //     date: new Date(),
                    // }] : []),
                    ...data?.map?.(item => ({...item, name: item.path})) ?? [],
                ]}
                onRowClick={row => {
                    if (row?.type === 'folder') {
                        props.onChDir?.([...props.path?.split?.('/') ?? [''], row.name].join('/'))
                    }
                    if (row?.type === 'file') {
                        downloadJobFile(messageApi, jobId, props.path + '/' + row.name, row.name).then()
                    }
                }}
            />
            {data === undefined && <Spinner tall/>}
            {data?.length === 0 && <>
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No files here"/>
            </>}
        </Stack>
    </>
}
