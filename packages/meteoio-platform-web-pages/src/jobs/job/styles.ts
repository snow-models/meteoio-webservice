import {makeStyles, shorthands, tokens} from "@fluentui/react-components";


export const useJobViewStyles = makeStyles({
    statusInfo: {
        columnRuleColor: tokens.colorNeutralStroke2,
        columnRuleStyle: 'solid',
        columnRuleWidth: 'thin',
        columnGap: `calc(2 * ${tokens.spacingHorizontalM})`,
        ...shorthands.padding(tokens.spacingVerticalM, tokens.spacingHorizontalM),
    },
    boxBorder: {
        ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke2),
    },
    filesBox: {
        paddingTop: tokens.spacingVerticalXS
    },
    scrollX: {
        overflowX: 'auto',
    }
})
