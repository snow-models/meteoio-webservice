import * as React from "react";
import {useContext} from "react";
import {
    usePublicJobSubmissionJobIdStdoutStdout,
    usePublicJobSubmissionJobIdStraceStrace
} from "meteoio-platform-client";
import {useStateTransitionCallback} from "meteoio-ui/src/hooks/useStateTransitionEffect";
import {usePromise} from "meteoio-ui/src/utils/usePromise";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {TerminalOut} from "meteoio-ui/src/components/TerminalOut";
import {Button} from "meteoio-ui/src/components/Button";
import {ArrowDownloadRegular} from "@fluentui/react-icons";
import {downloadBlob} from "meteoio-ui/src/utils/downloadBlob";
import {JobContext} from "./JobContext";


const jobFileHooks = {
    'stdout': usePublicJobSubmissionJobIdStdoutStdout,
    // 'stderr': usePublicJobSubmissionJobIdStderrStderr,
    'strace': usePublicJobSubmissionJobIdStraceStrace,
}

export const JobLogFileTerm: React.FC<{
    which: 'stdout' | 'strace' // | 'stderr'
}> = props => {
    const useFile = jobFileHooks[props.which]
    const {jobId, status} = useContext(JobContext)

    const {
        data,
        error: err1,
        refetch,
        isLoading: isLoading1,
    } = useFile({pathParams: {jobId}}, {
        enabled: jobId && status?.is_started,
        cacheTime: 60_000,
        staleTime: 10_000,
        refetchInterval: (_, query) => {
            if (status?.is_finished) {
                return 24 * 3600_000
            }
            const i = Math.min((query?.state?.dataUpdateCount ?? 10) / 10, 1)
            return 200 + 2800 * i
        },
    })

    useStateTransitionCallback(status?.is_finished, (prevState, newState) => {
        if (prevState === false && newState) {
            refetch?.()
        }
    }, [])

    const [text, {
        error: err2,
        isLoading: isLoading2
    }] = usePromise(() => data ? (data as Blob)?.text?.() : Promise.resolve(undefined), [data], true)

    const error = err1 ?? err2;
    const isLoading = isLoading1 || isLoading2

    if (isLoading && !text) {
        return <Spinner tall/>
    }
    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    return <Stack rowGap="S">
        <TerminalOut text={text ?? ''}/>
        <Stack horizontal justifyContent="end">
            <Button
                size="small"
                icon={<ArrowDownloadRegular/>}
                label="Download transcript"
                onClick={() => downloadBlob(data, 'log.txt')}
            />
        </Stack>
    </Stack>
}
