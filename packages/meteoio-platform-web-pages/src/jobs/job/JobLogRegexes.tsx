import * as React from "react";
import {useContext, useMemo} from "react";
import {
    usePublicJobSubmissionJobIdStdoutStdout,
    usePublicJobSubmissionJobIdStraceStrace
} from "meteoio-platform-client";
import {useStateTransitionCallback} from "meteoio-ui/src/hooks/useStateTransitionEffect";
import {usePromise} from "meteoio-ui/src/utils/usePromise";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {JobContext} from "./JobContext";
import {Alert} from 'meteoio-ui/src/components/Alert'
import {matchLines, ReducedT, reduceRows, RegexRow} from "./status.sh";
import {Empty} from "antd";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";


const jobFileHooks = {
    'stdout': usePublicJobSubmissionJobIdStdoutStdout,
    // 'stderr': usePublicJobSubmissionJobIdStderrStderr,
    'strace': usePublicJobSubmissionJobIdStraceStrace,
}

export const JobLogRegexes: React.FC<{
    which: 'stdout' | 'strace' // | 'stderr'
}> = props => {
    const useFile = jobFileHooks[props.which]
    const {jobId, status} = useContext(JobContext)

    const {
        data,
        error: err1,
        refetch,
        isLoading: isLoading1,
        isRefetching,
    } = useFile({pathParams: {jobId}}, {
        enabled: jobId && status?.is_started,
        cacheTime: 60_000,
        staleTime: 10_000,
        refetchInterval: (_, query) => {
            if (status?.is_finished) {
                return 24 * 3600_000
            }
            const i = Math.min((query?.state?.dataUpdateCount ?? 10) / 10, 1)
            return 200 + 2800 * i
        },
    })

    useStateTransitionCallback(status?.is_finished, (prevState, newState) => {
        if (prevState === false && newState) {
            refetch?.()
        }
    }, [])

    const [text, {
        error: err2,
        isLoading: isLoading2
    }] = usePromise(() => data ? (data as Blob)?.text?.() : Promise.resolve(undefined), [data], true)

    const error = err1 ?? err2;
    const isLoading = isLoading1 || isLoading2

    const rows = useMemo<RegexRow[]>(() =>
        typeof text === 'string' ? matchLines(text) : [], [text])

    const reduced = useMemo<ReducedT>(() =>
        reduceRows(rows), [rows])

    if (isLoading && !text) {
        return <Spinner tall/>
    }
    if (error) {
        return <MaybeErrorAlert error={error}/>
    }
    return <Stack rowGap="S">
        <Stack rowGap="None">
            {rows.map((row, i) =>
                //<Alert key={i} banner message={row.line} type={row.rule.level}/>
                <Alert key={i} level={row.rule.level} size="small">{row.line}</Alert>
            )}

            {status?.is_finished && rows?.length <= 0 && <>
                <br/>
                <Empty description="No recognized operation has been traced."/>
                <br/>
            </>}
            {(!status?.is_finished || isRefetching) && <ProgressBar thickness="large"/>}
        </Stack>
        {/*<SortableTable*/}
        {/*    size="extra-small"*/}
        {/*    noTopBottomBorder*/}
        {/*    columns={COLUMNS}*/}
        {/*    rows={rows}*/}
        {/*/>*/}
    </Stack>
}
