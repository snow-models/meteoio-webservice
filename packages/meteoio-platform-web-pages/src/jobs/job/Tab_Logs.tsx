import * as React from 'react'
import {useContext} from 'react'
import {usePublicJobSubmissionJobIdStraceParsedStraceParsed} from "meteoio-platform-client";
import {useJobViewStyles} from "./styles";
import {JobContext} from "./JobContext";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {SortableTable} from "meteoio-ui/src/components/SortableTable";
import {Empty, Segmented} from "antd";
import {JobLogFileTerm} from "./JobLogFile";
import {useStateTransitionCallback} from "meteoio-ui/src/hooks/useStateTransitionEffect";
import {mergeClasses} from "@fluentui/react-components";
import useLocalStorageState from "meteoio-ui/src/hooks/useLocalStorageState";
import {JobLogRegexes} from "./JobLogRegexes";


const SUB_TAB_STORAGE_KEY = '_LAST_PREFERRED_TAB_VQyATcZ7UIMy1LSe9A1T'

export const Tab_Logs: React.FC = () => {
    const [tab, setTab] = useLocalStorageState<'stdout' | 'strace' | 'opens' | 'regexes'>(SUB_TAB_STORAGE_KEY, 'regexes')

    return <>
        <Segmented
            block
            size="small"
            value={tab}
            options={[
                {value: 'regexes', label: 'Highlights'},
                {value: 'stdout', label: 'Stdout/stderr'},
                {value: 'strace', label: 'Strace'},
                {value: 'opens', label: 'Opened files'},
            ]}
            onChange={value => setTab(value as typeof tab)}
        />

        {tab === 'regexes' && <JobLogRegexes which="stdout"/>}
        {tab === 'stdout' && <JobLogFileTerm which="stdout"/>}
        {tab === 'strace' && <JobLogFileTerm which="strace"/>}
        {tab === 'opens' && <JobStraceParsed/>}
    </>
}


const JobStraceParsed: React.FC = () => {

    const styles = useJobViewStyles()
    const {jobId, status} = useContext(JobContext)

    const {
        data,
        error,
        isLoading,
        refetch,
        isRefetching,
    } = usePublicJobSubmissionJobIdStraceParsedStraceParsed({
        pathParams: {jobId},
    }, {
        cacheTime: 60_000,
        staleTime: 10_000,
    })

    useStateTransitionCallback(status?.is_finished, (prevState, newState) => {
        if (prevState === false && newState) {
            refetch?.()
        }
    }, [])

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    if (isLoading) {
        return <Spinner tall/>
    }

    return <>
        <Stack rowGap="None" className={mergeClasses(styles.boxBorder, data?.length > 0 && styles.scrollX)}>
            {data?.length > 0 && <SortableTable
                noTopBottomBorder
                size="extra-small"
                rows={data}
                columns={[
                    {
                        field: 'offset_seconds',
                        title: 'Time ',
                        textAlign: 'end',
                        onRenderCellContent: row => <time>{row.offset_seconds?.toFixed?.(2) ?? '?'}s&nbsp;&nbsp;</time>
                    },
                    {
                        field: 'syscall',
                        title: 'Syscall',
                        onRenderCellContent: row => <code>{row.syscall}</code>
                    },
                    {
                        field: 'open_file_flags',
                        title: 'Flags',
                        onRenderCellContent: row => <code>{row.open_file_flags?.join?.('|') ?? ''}</code>
                    },
                    {
                        field: 'open_file_path',
                        title: 'File path',
                        // TODO?: onRenderCellContent: row => link to download the file?:
                    },
                ]}
            />}
            {status?.is_finished && data?.length <= 0 && <>
                <br/>
                <Empty description="No recognized operation has been traced."/>
                <br/>
            </>}
            {(!status?.is_finished || isRefetching) && <ProgressBar thickness="large"/>}
        </Stack>
    </>
}
