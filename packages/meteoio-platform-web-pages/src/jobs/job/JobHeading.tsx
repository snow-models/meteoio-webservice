import * as React from 'react'
import {useContext, useMemo} from 'react'
import {Alert} from "meteoio-ui/src/components/Alert";
import {mergeClasses, Text} from "@fluentui/react-components";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {fileSizeFormat} from "meteoio-ui/src/utils/fileSizeFormat";
import {ProgressBar} from "meteoio-ui/src/components/ProgressBar";
import {JobStatus} from "meteoio-platform-client";
import {JobContext} from "./JobContext";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {useJobViewStyles} from "./styles";
import {parseDate} from "../../_common/parseDate";
import {JobDsRunBanner} from "../../datasets/JobDsRunBanner";
import {Stack} from "meteoio-ui/src/layouts/Stack";

export const JobHeading: React.FC<{
    isInDsLayout?: boolean
}> = props => {
    const {jobId, status, stats} = useContext(JobContext)
    const {isMobile} = useScreenSize()
    const styles = useJobViewStyles()

    const created = useMemo(() => parseDate(stats?.created), [stats?.created])

    return <>

        {/*{stats?.return_code === 0 && <Alert level="info">*/}
        {/*    Job completed.*/}
        {/*</Alert>}*/}
        <Stack rowGap="None">
            {stats?.return_code !== undefined && stats?.return_code !== null && stats?.return_code !== 0 &&
                <Alert level="error">
                    Job failed, see logs
                    (return code: <code>{stats?.return_code}</code>).
                </Alert>}

            <Text size={200} style={{columnCount: isMobile ? 1 : 2}}
                  className={mergeClasses(styles.statusInfo, !props.isInDsLayout && styles.boxBorder)}>
                ID: <code>{jobId}</code><br/>
                {created && <>
                    Created: <RelativeTimeText date={created}/><br/>
                </>}
                Status: {getJobStatusLabel(status)}<br/>
                {stats?.wait_seconds > 0 && <>
                    Wait time: {stats?.wait_seconds >= 1
                    ? `${stats.wait_seconds.toFixed?.(0) ?? '?'}s`
                    : `${(stats?.wait_seconds * 1000).toFixed(0)}ms`}<br/>
                </>}
                {stats?.processing_seconds > 0 && <>
                    Processing time: {stats?.processing_seconds >= 1
                    ? `${stats.processing_seconds.toFixed?.(stats?.processing_seconds >= 10 ? 0 : 1) ?? '?'}s`
                    : `${(stats?.processing_seconds * 1000).toFixed(0)}ms`}<br/>
                </>}
                {stats?.disk_usage_bytes > 0 && <>
                    Disk usage: {fileSizeFormat(stats?.disk_usage_bytes)}<br/>
                </>}
            </Text>
            {status?.is_finished !== true && <ProgressBar thickness="large"/>}
        </Stack>

        {!props.isInDsLayout && <JobDsRunBanner isInDsLayout={props.isInDsLayout}/>}
    </>
}

function getJobStatusLabel(status?: JobStatus): string {
    if (status?.is_finished) {
        return 'Finished.'
    }
    if (status?.is_running) {
        return 'Processing...'
    }
    if (status?.is_started) {
        return 'Processing...'  // NOTE: we expect it's processing even if we don't see the process.
    }
    if (status?.is_queued) {
        return 'Waiting...'
    }
    return ''
}
