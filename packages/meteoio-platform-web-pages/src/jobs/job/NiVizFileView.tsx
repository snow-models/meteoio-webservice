import * as React from "react";
import {usePublicJobSubmissionJobIdFilesGetFile} from "meteoio-platform-client";
import {usePromise} from "meteoio-ui/src/utils/usePromise";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {NiViz} from "meteoio-ui/src/components/NiViz";

export const NiVizFileView: React.FC<{
    jobId: string
    path: string
}> = ({jobId, path}) => {
    const data = usePublicJobSubmissionJobIdFilesGetFile({
        pathParams: {jobId},
        queryParams: {path},
    })
    const [text, {
        error: textError,
        isLoading: textIsLoading
    }] = usePromise(() => data.data ? (data.data as Blob).text() : undefined, [data.data])

    return <>
        <MaybeErrorAlert error={data?.error ?? textError}/>
        {(data?.isLoading || textIsLoading) && <Spinner tall/>}
        {text && <NiViz data={text}/>}
    </>
}