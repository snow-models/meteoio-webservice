import * as React from 'react'
import {useContext} from 'react'
import {StraceDto, usePublicJobSubmissionJobIdStraceOutputsStraceOutputs} from "meteoio-platform-client";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {SortableTable} from "meteoio-ui/src/components/SortableTable";
import {useJobViewStyles} from "./styles";
import {JobContext} from "./JobContext";
import {Empty, message} from "antd";
import {FileNameLabel, SizeLabel} from "meteoio-ui/src/components/FilesList";
import {downloadJobFile, MessageInstance} from "./downloadJobFile";
import {MenuItem, MenuItemLink, MenuList} from "@fluentui/react-components";
import {ArrowDownloadFilled, ArrowDownloadRegular, bundleIcon, OpenFilled, OpenRegular, EyeFilled, EyeRegular} from "@fluentui/react-icons";
import {useHref} from "react-router-dom";
import {address_book} from "../../address_book";

const DownloadIcon = bundleIcon(ArrowDownloadFilled, ArrowDownloadRegular)
const ViewIcon = bundleIcon(EyeFilled, EyeRegular)
const OpenIcon = bundleIcon(OpenFilled, OpenRegular)

export const Tab_Output: React.FC = () => {
    return <>
        <JobStraceOutputFiles
            boxBorder
        />
    </>
}

export const JobStraceOutputFiles: React.FC<{
    boxBorder?: boolean
}> = props => {
    const {jobId, status} = useContext(JobContext)

    const styles = useJobViewStyles()

    const [messageApi, messageContextHolder] = message.useMessage({maxCount: 1})

    const {
        data,
        error,
        isLoading,
    } = usePublicJobSubmissionJobIdStraceOutputsStraceOutputs({
        pathParams: {jobId},
    }, {
        enabled: !!status?.is_finished,
        cacheTime: 60_000,
        staleTime: 10_000,
    })

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    if (isLoading || !status?.is_finished) {
        return <Spinner tall/>
    }

    return <>
        {messageContextHolder}
        <Stack rowGap="None" className={props.boxBorder ? styles.boxBorder : undefined}>
            {data?.length > 0 && <SortableTable
                hideHeading
                noTopBottomBorder
                columns={[
                    {
                        field: 'open_file_path',
                        title: 'Path',
                        onRenderCellContent: row =>
                            <FileNameLabel row={{type: 'file', name: row.open_file_path}}/>
                    },
                    {
                        field: 'current_file_size',
                        title: 'Size',
                        sortCast: 'number',
                        textAlign: 'end',
                        onRenderCellContent: row =>
                            <SizeLabel row={{size: row.current_file_size}}/>
                    }
                ]}
                rows={data ?? []}
                showMenuOnRowContextMenu
                showMenuOnRowClick
                onRenderMenuChildren={row => {
                    return <RowMenuList jobId={jobId} row={row} messageApi={messageApi} />
                }}
            />}
            {data?.length <= 0 && <>
                <br/>
                <br/>
                <Empty description="No output file has been written."/>
                <br/>
            </>}
        </Stack>
    </>
}


const RowMenuList: React.FC<{
    jobId: string
    messageApi: MessageInstance
    row: StraceDto
}> = props => {
    const {jobId, messageApi, row} = props
    const openHref = useHref(address_book.jobs.view_file(jobId, row?.open_file_path))
    const canOpen = row?.open_file_path?.toLowerCase?.()?.endsWith?.('.smet')
    return <MenuList>
        <MenuItem
            icon={<DownloadIcon/>}
            onClick={() => {
                const path_parts = row.open_file_path.split('/')
                const name = path_parts[path_parts.length - 1]
                downloadJobFile(messageApi, jobId, row.open_file_path, name).then()
            }}
        >
            Download
        </MenuItem>
        <MenuItemLink
            icon={<ViewIcon/>}
            href={openHref}
            disabled={!canOpen}
        >
            View
        </MenuItemLink>
        <MenuItemLink
            icon={<OpenIcon/>}
            href={openHref}
            disabled={!canOpen}
            target='_blank'
        >
            Open in new tab
        </MenuItemLink>
    </MenuList>
}
