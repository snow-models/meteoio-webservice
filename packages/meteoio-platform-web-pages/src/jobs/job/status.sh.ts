// NOTE: inspired from status.sh containing an awk script that processes the stdout of MeteoIO

// import type React from "react";

export interface ReducedT {
    station?: string
    duration?: string
    // errors?: string[]
    // warnings?: string[]
}

export interface RegexRule {
    regex: RegExp
    level: 'info' | 'warning' | 'error' | 'success'
    // onRender?: (row: RegexRow) => React.ReactNode
    reduceFn?: (previous: ReducedT, row: RegexRow) => ReducedT
}

export interface RegexRow {
    line: string
    match: RegExpMatchArray
    rule: RegexRule
}

// noinspection RegExpRedundantEscape
const RULES: RegexRule[] = [
    {
        regex: /^Working on (?<path>.*)/,
        level: 'info',
        reduceFn: (previous, row) =>
            ({...previous, station: row.match.groups['path']?.split?.(/[\\/]/)?.slice(-1)?.[0]})
    },
    {
        regex: /^Powered by MeteoIO /,  // rest of the line: version number and compilation date time
        level: 'info',
    },
    {
        regex: /^Reading data from /,  // rest of the line: start/end timestamps
        level: 'info',
    },
    {
        regex: /^Writing output data/,
        level: 'info',
    },
    {
        regex: /^\[I\] Writing data to (.*?) for (.*?)$/,
        level: 'info',
        // TODO?: understand that long awk script and write some reduceFn ?
    },
    {
        regex: /\[E\] Error with Station /,
        level: 'error',
        // reduceFn: (previous, _, line) => ({...previous, errors: [...(previous.errors??[]), line.replace(//g, '')]})
    },
    {
        regex: /\[W\] .*jumping back to /,
        level: 'warning',
        // TODO?: understand that long awk script and write some reduceFn ?
    },
    {
        regex: /\[W\] /,
        level: 'warning',
    },
    {
        regex: /\[E\] /,
        level: 'error',
    },
    {
        regex: /Date or time could not be read/,
        level: 'error',
    },
    {
        regex: /\033\[0m\[[a-zA-Z0-9\.:]+\] \033\[31;1m/,
        level: 'error',
    },
    {
        regex: /^Done!! in (?<duration>.*)/,
        level: 'success',
        reduceFn: (previous, row) => ({...previous, duration: row.match.groups?.duration})
    }
]

export const matchLines: (text: string) => RegexRow[] = text => text
    .split('\n')
    .map(line => line.trim())
    .flatMap(line => {
        for (let rule of RULES) {
            const match = line.match(rule.regex)
            const line_noAnsi = line.replace(/\033\[[0-9;]+[a-z]/g, '')
            if (match) {
                return [{line: line_noAnsi, rule, match}]
            }
        }
        return []
    })

export const reduceRows: (rows: RegexRow[]) => ReducedT = rows => rows
    .reduce((prev, current) => current.rule.reduceFn?.(prev, current) ?? prev, {})
