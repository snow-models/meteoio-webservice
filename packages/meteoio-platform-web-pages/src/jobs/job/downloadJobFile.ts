import {makeRandomUuid} from "meteoio-ui/src/utils/makeRandomUUID";
import {fetchPublicJobSubmissionJobIdFilesGetFile} from "meteoio-platform-client";
import {downloadBlob} from "meteoio-ui/src/utils/downloadBlob";
import {getMaybeErrorMessageString} from "meteoio-ui/src/components/MaybeErrorAlert";
import {message} from 'antd'


export type MessageInstance = ReturnType<typeof message.useMessage>[0]


export async function downloadJobFile(messageApi: MessageInstance, jobId: string, path: string, name: string) {
    const key = makeRandomUuid()
    messageApi.open({
        key,
        type: 'loading',
        duration: 60_000,
        content: 'Downloading file...'
    })
    try {
        const file = await fetchPublicJobSubmissionJobIdFilesGetFile({
            pathParams: {jobId},
            queryParams: {
                path,
            },
        });
        downloadBlob(file as Blob, name)
        messageApi.open({
            key,
            type: 'success',
            duration: 2000,
            content: 'Download succeeded.',
        })
        setTimeout(() => {
            messageApi.destroy(key)
        }, 2000)
    } catch (err) {
        messageApi.open({
            key,
            type: 'error',
            content: getMaybeErrorMessageString(err),
        })
    } finally {
        setTimeout(() => {
            messageApi.destroy(key)
        }, 10_000)
    }
}
