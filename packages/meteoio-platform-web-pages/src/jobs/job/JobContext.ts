import {createContext} from "react";
import {JobStats, JobStatus} from "meteoio-platform-client";

export interface IJobContext {
    jobId?: string
    status?: JobStatus
    stats?: JobStats
}

export const JobContext = createContext<IJobContext>({})
