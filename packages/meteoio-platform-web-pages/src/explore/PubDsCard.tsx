import * as React from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {DatasetMetadataLabels} from "../datasets/DatasetMetadataLabels";
import {RepositoryCard} from "meteoio-ui/src/components/RepositoryCard";
import {DatasetListEntry} from "meteoio-platform-client";
import {useHref} from "react-router-dom";
import {address_book} from "../address_book";
import {Link} from "@fluentui/react-components";
import {Handle} from "meteoio-ui/src/components/Handle";

export const PubDsCard: React.FC<{
    ds: DatasetListEntry
}> = props => {
    const {ds} = props
    return <>
        <RepositoryCard
            key={ds.id}
            heading={<DsLink name={ds.heading?.title} dsId={ds.id}/>}
            access={ds.access}
            description={ds.heading?.description}
            metadata={<Stack horizontal wrap alignItems="center" rowGap="XXS">
                {ds?.heading && <DatasetMetadataLabels ds={ds?.heading}/>}
            </Stack>}
        />
    </>
}

export const DsLink: React.FC<{
    dsId: string
    name: string
}> = props => {
    const href = useHref(address_book.explore.view(props.dsId))
    return <>
        <Link href={href}>
            <Handle handle={props.dsId} label={props.name}/>
        </Link>
    </>
}
