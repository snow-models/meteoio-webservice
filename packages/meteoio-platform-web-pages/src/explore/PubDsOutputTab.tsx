import * as React from 'react'
import {
    usePublicDatasetsDatasetIdGetDataset,
    usePublicDatasetsDatasetIdReleasedJobsGetReleasedJobs
} from "meteoio-platform-client";
import {useHref, useParams} from "react-router-dom";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Divider, Link, makeStyles, shorthands, tokens} from "@fluentui/react-components";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";
import {RelativeTimeText} from "meteoio-ui/src/components/RelativeTimeText";
import {DatasetRunOutFrag} from "../datasets/output/DatasetRunOutFrag";
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageSectionTitle} from "meteoio-ui/src/components/PageSectionTitle";
import {CHStack, DatasetMetadataLabels} from "../datasets/DatasetMetadataLabels";
import {PersonRegular} from "@fluentui/react-icons";
import {Collapse, Empty} from "antd";
import {ImmediateDatasetTitle} from "../datasets/_common/DatasetTitle";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";

const useStyles = makeStyles({
    metadataLabels: {
        color: tokens.colorNeutralForeground3,
        fontSize: tokens.fontSizeBase200,
    },
    filesCollapse: {
        '& > .ant-collapse-item > .ant-collapse-content > .ant-collapse-content-box': {
            ...shorthands.padding(0),
        }
    },
    latestHeader: {}
})

export const PubDsOutputTab: React.FC = () => {
    const {datasetId} = useParams()
    const {data: dsData, isLoading, error} = usePublicDatasetsDatasetIdGetDataset({pathParams: {datasetId}})
    const {
        data: runsData,
        isLoading: runsLoading,
        error: runsError
    } = usePublicDatasetsDatasetIdReleasedJobsGetReleasedJobs({pathParams: {datasetId}})

    const dataset = dsData?.heading

    const styles = useStyles()
    const {isMobile} = useScreenSize()

    const selfHref = useHref('')

    if (error ?? runsError) {
        return <MaybeErrorAlert error={error ?? runsError}/>
    }

    if (isLoading || runsLoading) {
        return <Spinner tall/>
    }

    return <>
        <main>
            <PageTitle>
                <ImmediateDatasetTitle
                    dataset={dsData?.heading}
                    size="extra-large"
                    href={selfHref}
                    access={dsData?.access}
                />
            </PageTitle>
            <br/>

            {runsData?.length >= 1 && <Collapse
                size="small"
                accordion
                rootClassName={styles.filesCollapse}
                defaultActiveKey={isMobile ? undefined : runsData?.[0]?.job_id}
                destroyInactivePanel
                items={runsData?.map?.((run, i) => ({
                    key: run.job_id,
                    headerClass: i === 0 ? styles.latestHeader : undefined,
                    label: i === 0
                        ? <><b>Latest</b><small> (<RelativeTimeText date={run?.created_at}/>)</small></>
                        : <RelativeTimeText date={run?.created_at}/>,
                    children: <DatasetRunOutFrag data={run}/>,
                })) ?? []}
            /> || <>
                <Empty description="No output data to show."/>
            </>}
        </main>
        <aside className="right">
            <PageTitle>
                &nbsp;
            </PageTitle>
            <br/>
            {!isMobile && <>
                <br/>
                <br/>
            </>}
            <Stack rowGap="XL">
                <Stack rowGap="XS">
                    <PageSectionTitle>About</PageSectionTitle>
                    <span>{dataset.description}</span>
                </Stack>
                <Stack rowGap="XS" className={styles.metadataLabels}>
                    <DatasetMetadataLabels ds={dataset}/>
                </Stack>
                <Divider/>
                {dataset?.creators?.length > 0 && <Stack rowGap="XS">
                    <PageSectionTitle>Creator{dataset?.creators?.length > 1 ? 's' : ''}</PageSectionTitle>
                    {dataset?.creators?.map?.((c, i) =>
                        <CHStack key={i} icon={c.type === 'person' && <PersonRegular/>}>
                            {c.email
                                ? <Link href={`mailto:${c.email}`}
                                        title={`${c.full_name} <${c.email}>`}>{c.full_name}</Link>
                                : <span>{c.full_name}</span>}
                        </CHStack>)}
                </Stack>}
            </Stack>
        </aside>
    </>
}
