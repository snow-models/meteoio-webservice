import * as React from 'react'
import {HeaderMainAsideLayout} from "meteoio-ui/src/layouts/HeaderMainAsideLayout";
import {Route, Routes} from "react-router-dom";
import {MainTopBar} from "../_common/MainTopBar";
import {PubDsOutputTab} from "./PubDsOutputTab";
import {MainContentSplitWithMainNav} from "../_common/MainContentSplitWithMainNav";
import {useScreenSize} from "meteoio-ui/src/hooks/useScreenSize";


export const PubDsLayout: React.FC = () => {
    // const {datasetId} = useParams()
    //
    // const {data, isLoading, error} = usePublicDatasetsDatasetIdGetDataset({pathParams: {datasetId}})
    //
    // if (error) {
    //     return <>
    //         <MainTopBar/>
    //         <MaybeErrorAlert error={error}/>
    //     </>
    // }
    //
    // if (isLoading) {
    //     return <>
    //         <MainTopBar/>
    //         <Spinner tall/>
    //     </>
    // }

    const {isLarge} = useScreenSize()

    const content = <>
        <HeaderMainAsideLayout
                body={<Routes>
                    <Route index={true} Component={PubDsOutputTab}/>
                </Routes>}
        />
    </>

    return <>
        <MainTopBar/>
        {isLarge
                ? <MainContentSplitWithMainNav>{content}</MainContentSplitWithMainNav>
                : content}
    </>
}