import * as React from 'react'
import {useEffect, useRef, useState} from 'react'
import {Stack} from "meteoio-ui/src/layouts/Stack";
import {PageTitle} from "meteoio-ui/src/components/PageTitle";
import {MaybeErrorAlert} from "meteoio-ui/src/components/MaybeErrorAlert";
import {Spinner} from "meteoio-ui/src/components/Spinner";
import {Empty} from "antd";
import {usePublicDatasetsListDatasets} from "meteoio-platform-client";
import {PubDsCard} from "./PubDsCard";

export const ExplorePublicDatasetsPage: React.FC = () => {

    const searchRef = useRef<HTMLInputElement>()
    const [isSearching, setIsSearching] = useState<boolean>(false)
    const [searchInput, setSearchInput] = useState<string>('')
    useEffect(() => {
        searchRef?.current?.focus?.()
    }, [isSearching, searchRef, searchRef?.current])

    const {
        data: datasets,
        error,
        isLoading,
    } = usePublicDatasetsListDatasets({queryParams: {}})

    if (error) {
        return <MaybeErrorAlert error={error}/>
    }

    if (isLoading) {
        return <Spinner tall/>
    }

    return <>
        <br/>
        <Stack rowGap="XL" maxWidth={600} center>
            <Stack horizontal wrap justifyContent="space-between">
                <PageTitle>
                    Explore Datasets
                </PageTitle>

                {/*<Stack horizontal>
                    {!isSearching && <Button
                        appearance="subtle"
                        icon={<SearchRegular/>}
                        onClick={() => {
                            setIsSearching(v => !v)
                        }}
                        label="___"
                        noInlinePadding
                    />}
                    {isSearching && <Input
                        ref={searchRef}
                        type="search"
                        appearance="underline"
                        value={searchInput}
                        onChange={(ev, data) => setSearchInput(data.value)}
                        onBlur={() => {
                            if (searchInput?.length <= 0) {
                                setIsSearching(false)
                            }
                        }}
                    />}
                </Stack>*/}
            </Stack>

            {datasets?.items?.map?.(ds => <PubDsCard key={ds.id} ds={ds}/>)}
            {datasets?.items?.length === 0 && <Empty description="There are no published datasets."/>}
        </Stack>
    </>
}
