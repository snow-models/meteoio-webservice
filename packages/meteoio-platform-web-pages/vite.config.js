import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react';

export default defineConfig({
    plugins: [
        react({}),
    ],
    root: 'src',
    base: './',
    build: {
        rollupOptions: {
            input: './src/index.html',
            output: {
                entryFileNames: "assets/[hash].js",
                chunkFileNames: "assets/[hash].js",
                assetFileNames: "assets/[hash][extname]"
            },
        },
        outDir: '../dist',
        emptyOutDir: true,
    },
    server: {
        proxy: {
            "/api": {
                target: "http://localhost:8000",
                changeOrigin: true,
                secure: false,
                rewrite: path => path.replace(/^\/api/, '')
            },
        },
    },
})
