import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react';
import {viteSingleFile} from "vite-plugin-singlefile"

export default defineConfig({
    plugins: [
        react({}),
        viteSingleFile(),
    ],
    root: 'src',
    base: '.',
    build: {
        rollupOptions: {
            input: './src/index.html',
        },
        outDir: '../dist',
        emptyOutDir: true,
        minify: true,
        sourcemap: false,
    },
})
