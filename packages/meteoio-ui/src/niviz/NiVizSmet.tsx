import * as React from "react";
import {useCallback, useEffect, useRef, useState} from "react";
import niviz from './niviz'
import {useStation} from "./useStation";
import {Select} from "antd";


export const NiVizSmet: React.FC<{
    data?: string
}> = props => {
    const [parameters, setParameters] = useState<string[]>(() => [])

    const plot = useRef<SVGSVGElement>();

    const {station} = useStation(props.data, 'smet')
    console.debug({station})

    const [timelineGraph, setTimelineGraph] = useState(undefined)

    useEffect(() => {
        if (!station || !plot.current) {
            return
        }
        let cancelled = false
        const t = setTimeout(() => {
            if (cancelled) {
                return
            }
            const label = `niviz draw - ${Math.random().toString().substring(2)}`
            console.time(label)
            try {
                plot.current.innerHTML = ''
                // console.debug('niviz drawing', file.data)
                const timelineObj = niviz.draw('Meteograph', station, plot.current, {})
                // const parameters = station.fields.slice(1)
                setParameters(timelineObj.parameters)
                // timelineObj.parameters = parameters
                // timelineObj.draw()
                setTimelineGraph(timelineObj)
                console.debug({timelineObj})

                // profileObj.barcfg.monochrome = true  // NOTE: DOES NOT WORK -- like ignored
                // profileObj.setParameter(props.parameter ?? 'grainshape')  // NOTE: DOES NOT WORK -- like ignored


                console.timeEnd(label)
            } catch (e) {
                console.trace('niviz draw error:', e)
            }
        }, 100)
        return () => {
            cancelled = true
            clearTimeout(t)
            if (plot.current) {
                plot.current.innerHTML = ''
            }
        }
    }, [plot.current, station])

    const keyHandler = useCallback((ev: KeyboardEvent) => {
        if (ev.key === "ArrowRight") {
            timelineGraph?.next?.();
            ev.preventDefault();
        } else if (ev.key === "ArrowLeft") {
            timelineGraph?.previous?.();
            ev.preventDefault();
        }
    }, [timelineGraph])

    useEffect(() => {
        document?.addEventListener?.('keydown', keyHandler, true)
        return () => {
            document?.removeEventListener?.('keydown', keyHandler, true)
        }
    }, [keyHandler])

    const graphsDivRef = useRef<HTMLDivElement>()

    useEffect(() => {
        const h = (ev: MouseEvent) => {
            graphsDivRef?.current?.focus?.()
        }
        graphsDivRef?.current?.addEventListener?.('click', h, true)
        return () => {
            graphsDivRef?.current?.removeEventListener?.('click', h, true)
        }
    }, [graphsDivRef?.current])

    // useEffect(() => {
    //     const h = (ev: UIEvent) => {
    //         timelineGraph.draw?.()
    //         // timelineGraph.setup?.()
    //         // timelineGraph.reset?.()
    //         // console.debug({timelineGraph})
    //     }
    //     window?.addEventListener?.('resize', h)
    //     return () => {
    //         window?.removeEventListener?.('resize', h)
    //     }
    // }, [timelineGraph])

    return <div style={{position: 'relative'}}>
        <div style={{
            marginBottom: 8,
            position: 'relative',
            right: 0,
            top: 0,
            width: '100%',
            display: 'flex',
            flexFlow: 'row',
            justifyContent: 'end',
            paddingRight: 41,
            boxSizing: 'border-box',
        }}>
            <Select
                mode="multiple"
                maxTagCount={2}
                style={{minWidth: 200}}
                value={parameters}
                options={station?.fields?.filter?.(field => field !== 'timestamp')?.map?.(field => ({
                    value: field,
                    label: station?.plot?.[field]?.description
                        ? `${station?.plot?.[field]?.description} (${field})`
                        : field,
                })) ?? []}
                onChange={value => {
                    // timelineGraph.parameters = value
                    timelineGraph.set(value)
                    timelineGraph.draw()
                    setParameters(value)
                }}
                popupMatchSelectWidth={false}
                placement="bottomRight"
            />
        </div>
        <div tabIndex={100} ref={graphsDivRef} style={{outline: 'none', display: 'flex', flexFlow: 'column', minHeight: 400}}>
            <svg
                key="plot"
                ref={plot}
                // style={{minWidth: 650, minHeight: 400}}
                // style={{width: 650, height: 400}}
                style={{flexGrow: 1}}
            >
            </svg>
        </div>
        <div style={{
            position: 'absolute',
            bottom: -12,
            left: 0,
            opacity: 0.5
        }}>
            <a href="https://niviz.org/" target="_blank">niViz</a>
        </div>
    </div>
}
