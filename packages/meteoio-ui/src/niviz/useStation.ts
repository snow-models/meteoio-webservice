import {useEffect, useState} from "react";
import niviz from "./niviz";


/** gives a niviz Station object from parsable data */
export function useStation(data: string, type: 'pro' | 'smet') {
    const [station, setStation] = useState(undefined)
    const [errors, setErrors] = useState<Error[]>(undefined)

    useEffect(() => {
        setStation(undefined)
        setErrors(undefined)
        if (!data || !type) {
            return
        }

        let cancelled = false
        const t = setTimeout(() => {
            if (cancelled) {
                return;
            }
            const label = `niviz parse station - ${Math.random().toString().substring(2)}`
            console.time(label)
            try {
                niviz.parse(data, type, (err, station) => {
                    console.timeEnd(label)
                    if (err) {
                        console.error('niviz parse station - error', err)
                        setErrors(ee => [...(ee ?? []), err])
                    } else {
                        if (cancelled) {
                            return
                        }
                        setStation(station)
                    }
                })
            } catch (e) {
                console.trace('niviz parse station - error:', e)
                if (cancelled) {
                    return
                }
                setErrors(ee => [...(ee ?? []), e])
            }
        }, 10)
        return () => {
            cancelled = true
            clearTimeout(t)
        }
    }, [data, type])

    return {station, errors}
}
