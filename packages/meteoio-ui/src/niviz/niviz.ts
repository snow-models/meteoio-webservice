import './niviz-bundle-latest'
import './niviz-fonts.css'

interface _SettingObject<T = unknown> {
    name?: string
    type?: string
    value?: unknown
    values?: unknown
    hint?: unknown
    default?: T
    required?: boolean
}

interface _GradientConfig {
    min: number
    max: number
    steps: number
    /** Palette name referring to a dictionary of statically defined palettes */
    palette: string

    gradient: 'linear' | 'grainshape' // | 'ordinal' // TODO
}

/** list of RGB steps, values in 0-255 */
type _Palette = [number, number, number][]

declare class ProParser {
    /** ? */
    public static code: Record<number, string>
    /** ? */
    public static codelist: Record<number, string>

    public static codes: Config
}

declare class Station {
    // ...
}

declare class Timeline {
    /** ? */
    public static parameters: string[]

    public static defaults: Config;

    draw()

    setDate(date: Date)

    setProperties(properties: {parameter: string} | Record<string, any>)

    /**
     * Synchronize this graph with another rangegraph derivate by
     * registering for all the relevant events.
     */
    synchronize(graph: Timeline)

    /**
     * Desynchronize this graph with another rangegraph derivate by
     * registering for all the relevant events.
     */
    desynchronize(graph: Timeline)
}

declare class Gradient {
    public min: number
    public max: number
    public steps: number
    public palette: _Palette

    public static palettes: Record<string, _Palette>

    /**
     * A Gradient represents a color gradient and its labels
     *
     * @class Gradient
     * @constructor
     */
    constructor(config: _GradientConfig);
}

declare class SimpleProfile {
    public static defaults: Config;
}

declare class Config {
    /**
     * A serializable JSON configuration that may be persisted in the local storage.
     * A Config object is a collection of Setting objects.
     *
     * @class Config
     * @constructor
     *
     * @param {String} [name=''] The configuration's name.
     * @param {Array} [settings] The initial list of settings.
     * @param {String} [display_name=''] The configurations display name.
     */
    constructor(name: string, settings: _SettingObject[], display_name?: string);

    /**
     * Add settings to the configuration.
     *
     * @method add
     * @chainable
     * @param {Array<Object>} settings
     */
    add(settings: _SettingObject);

    /**
     * Retrieve sepecific setting from config.
     *
     * @method get
     * @param {String} name The name of the setting to retrieve
     * @return {Object|String|Array<Object>} Return specific setting
     */
    get(name: string)

    /**
     * Set a specific setting in this config.
     *
     * @method set
     * @chainable
     * @param {String} name The name of the setting to set
     * @param {Object|String|Array<Object>} value The value of the setting
     */
    set(name: string, value)

    /**
     * Store the config in the local storage.
     *
     * @method store
     * @chainable
     */
    store()

    /**
     * Load the config from the local storage.
     *
     * @method load
     * @chainable
     */
    load()

    /**
     * Take a JSON object and merge its properties into the config.
     *
     * @method merge
     * @chainable
     * @param {Object} values
     */
    merge(values)
}

declare const niviz: {
    parse(data: string, type: 'pro' | string, callback: (error: Error | undefined, station: unknown) => void): void

    draw(type: string, station: unknown, canvas: SVGSVGElement, options: {
        parameter?: string
        miniature?: boolean
    } | Record<string, any>): any

    ProParser: typeof ProParser
    Gradient: typeof Gradient
    Timeline: typeof Timeline
    Station: typeof Station
    SimpleProfile: typeof SimpleProfile
    Config: typeof Config
    Common: any
}

export default niviz
