import {useEffect, useState} from "react";

export function usePromise<T>(f: () => Promise<T>, deps?: unknown[], swr: boolean = false): [T | undefined, {
    error: Error | undefined,
    isLoading: boolean
}] {
    const [result, setResult] = useState<T>()
    const [error, setError] = useState<Error>()
    const [loading, setLoading] = useState<boolean>(true)
    useEffect(() => {
        setLoading(true)
        if (!swr) {
            setResult(undefined)
        }
        setError(undefined);
        (async () => {
            try {
                setResult(await f?.())
            }
            catch (e) {
                setResult(undefined)
                setError(e)
            }
            setLoading(false)
        })()
    }, deps ?? [])
    return [result, {error, isLoading: loading}]
}
