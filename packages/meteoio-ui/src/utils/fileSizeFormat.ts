export const fileSizeFormat: (bytes?: number) => string = bytes => {
    if (bytes === undefined || bytes === null) {
        return ''
    }
    if (bytes === 0) {
        return '0 bytes';
    }
    const units = ['B', 'KB', 'MB', 'GB', 'TB'];
    const k = 1000;  // NOTE: using si units
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(2)) + ' ' + units[i];
};
