import {makeRandomUuid} from "./makeRandomUUID";
import {downloadBlob} from "./downloadBlob";
import {getMaybeErrorMessageString} from "../components/MaybeErrorAlert";
import {message} from 'antd'


type MessageInstance = ReturnType<typeof message.useMessage>[0]

export type Fetcher = (path: string) => Promise<Blob>

export async function downloadFile(messageApi: MessageInstance, path: string, fetcher: Fetcher, name: string) {
    const key = makeRandomUuid()
    messageApi.open({
        key,
        type: 'loading',
        duration: 60_000,
        content: 'Downloading file...'
    })
    try {
        const file = await fetcher(path)
        downloadBlob(file as Blob, name)
        messageApi.open({
            key,
            type: 'success',
            duration: 2000,
            content: 'Download succeeded.',
        })
        setTimeout(() => {
            messageApi.destroy(key)
        }, 2000)
    } catch (err) {
        messageApi.open({
            key,
            type: 'error',
            content: getMaybeErrorMessageString(err),
        })
    } finally {
        setTimeout(() => {
            messageApi.destroy(key)
        }, 10_000)
    }
}
