export function downloadBlob(blob: Blob, name: string) {
    const url = URL.createObjectURL(blob)
    const a = document?.createElement?.('a')
    a.style.display = 'none'
    a.href = url
    a.download = name
    document?.body?.appendChild?.(a)
    a.click()
    window.URL.revokeObjectURL(url)
    a.remove()
}
