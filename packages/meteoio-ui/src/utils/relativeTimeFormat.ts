export const relativeTimeFormat: (date: Date | string, maxRecentMs?: number) => string = (_date, maxRecentMs) => {
    const date: Date = new Date(_date)
    const difference = (Date.now() - date.getTime()) / 1000;
    const rtf = new Intl.RelativeTimeFormat(undefined, {style: 'short'})
    if (maxRecentMs !== undefined && difference * 1000 < maxRecentMs) {
        if (maxRecentMs >= 60_000) {
            return 'seconds ago'  //`< ${rtf.format(-maxRecentMs / 60_000, 'minutes')}`
        }
        return `< ${rtf.format(-maxRecentMs / 1000, 'second')}`
    }
    const c = 0.9;
    if (difference < -86400 * c) {
        return date.toLocaleDateString(undefined, {dateStyle: 'medium'})
    } else if (difference < -3600 * c) {
        return rtf.format(Math.floor(-difference / 3600), 'hour')
    } else if (difference < -60 * c) {
        return rtf.format(Math.floor(-difference / 60), 'minute')
    } else if (difference < 0) {
        return rtf.format(Math.floor(-difference), 'second')
    } else if (difference < 60 * c) {
        return rtf.format(Math.floor(-difference), 'second')
    } else if (difference < 3600 * c) {
        return rtf.format(Math.floor(-difference / 60), 'minute')
    } else if (difference < 86400 * c) {
        return rtf.format(Math.floor(-difference / 3600), 'hour')
    } else if (difference < 2620800 * c) {
        return rtf.format(Math.floor(-difference / 3600 / 24), 'day')
    } else if (difference < 31449600 * c) {
        return date.toLocaleDateString(undefined, {dateStyle: 'medium'})
    } else {
        return date.toLocaleDateString(undefined, {dateStyle: 'medium'})
    }
}
