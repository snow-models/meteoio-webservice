import {BrandVariants, createDarkTheme, createLightTheme, Theme} from "@fluentui/react-theme";
import {theme as antd_theme, ThemeConfig} from 'antd'

// TODO: Allow for some easy customization of the following colors.
// TODO: None of the following colors allows to set the background of html body.

export const DEFAULT_brand: BrandVariants = {
    "10": "#0e334a",
    "20": "#113c58",
    "30": "#134565",
    "40": "#05adcb", // colorBrandForegroundLinkPressed, dataset Handle
    "50": "#175277",
    "60": "#18577e",
    "70": "#195b85", // TopBar text, Home page title, subtle buttons text
    "80": "#22638c", // primary Buttons, control labels
    "90": "#2a6b93",
    "100": "#4480a5",
    "110": "#5d94b6",
    "120": "#81adc8",
    "130": "#a5c6da",
    "140": "#b9d3e3", // secondary button border (and spinner background track)
    "150": "#cde0eb",
    "160": "#e0ecf3",
}

export const brand_color_comments: Record<keyof BrandVariants, string> = {
    "10": "",
    "20": "",
    "30": "",
    "40": "Dataset handles",
    "50": "",
    "60": "",
    "70": "Main home link text, home page title, subtle buttons text",
    "80": "Primary buttons, Form field labels",
    "90": "",
    "100": "",
    "110": "",
    "120": "",
    "130": "",
    "140": "Form field borders, secondary button border, spinner background track",
    "150": "",
    "160": "",
} as const


const customizeThemeDetails: (theme: Theme) => Theme = theme => {
    return {
        ...theme,

        // Let's make the buttons and inputs less rounded
        borderRadiusMedium: '2px',

        // Let's make the inputs border more light and branded
        colorNeutralStroke1: theme.colorBrandStroke2,

        // Let's make the inputs underline more branded
        colorNeutralStrokeAccessible: theme.colorBrandStroke1,

        // Let's make the subtle buttons more branded like links
        colorNeutralForeground2: theme.colorBrandForeground2,
    }
}

export function makeTheme(brand?: BrandVariants) {
    if (brand === undefined) {
        brand = DEFAULT_brand
    }

    const lightTheme = customizeThemeDetails(createLightTheme(brand))
    const darkTheme = customizeThemeDetails(createDarkTheme(brand))

    const antd_tokens: ThemeConfig['token'] = {
        colorPrimary: brand["80"],
        borderRadius: 2,
        colorError: lightTheme.colorPaletteRedForeground1,
        colorSuccess: lightTheme.colorPaletteGreenForeground1,
    }

    const lightTheme_antd: ThemeConfig = {
        token: antd_tokens,
        algorithm: [],
        components: {
            Select: {
                colorBorder: lightTheme.colorNeutralStroke1,
            }
        }
    }

    const darkTheme_antd: ThemeConfig = {
        token: antd_tokens,
        algorithm: [antd_theme.darkAlgorithm],
        components: {
            Select: {
                colorBorder: darkTheme.colorNeutralStroke1,
            }
        }
    }

    return {lightTheme, darkTheme, lightTheme_antd, darkTheme_antd}
}
