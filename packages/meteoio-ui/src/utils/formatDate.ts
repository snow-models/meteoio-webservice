export const formatDate: (date?: Date) => string = date => {
    if (!date) {
        return ''
    }
    const YYYY = date.getFullYear()
    const Mo = date.getMonth() + 1 >= 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`
    const DD = date.getDate() >= 10 ? date.getDate() : `0${date.getDate()}`
    // const HH = date.getHours() >= 10 ? date.getHours() : `0${date.getHours()}`
    // const Mi = date.getMinutes() >= 10 ? date.getMinutes() : `0${date.getMinutes()}`
    // const SS = date.getSeconds() >= 10 ? date.getSeconds() : `0${date.getSeconds()}`
    return `${YYYY}-${Mo}-${DD}`  // `T${HH}:${Mi}:${SS}`
}
