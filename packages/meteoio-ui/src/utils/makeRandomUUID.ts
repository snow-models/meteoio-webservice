export const makeRandomUuid: () => string = () => {
    // @ts-ignore
    return crypto.randomUUID()  // NOTE: this requires a secure context (localhost or https)
}
