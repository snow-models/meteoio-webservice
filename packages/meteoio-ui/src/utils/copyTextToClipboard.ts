export const copyTextToClipboard: (text: string) => Promise<void> = async text => {
    await navigator?.clipboard?.writeText?.(text);
}
