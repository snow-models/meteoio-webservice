import * as React from 'react'
import {FilesList} from "./components/FilesList"
import {PageTitle} from "./components/PageTitle"
import {PageSectionTitle} from "./components/PageSectionTitle"
import {RowFiller, Stack} from "./layouts/Stack"
import {ThemeProvider} from "./providers/ThemeProvider"
import {DropdownField} from "./components/DropdownField"
import {KeyValueTable} from "./components/KeyValueTable"
import {FreeTextField} from "./components/FreeTextField"
import {RepositoryCard} from "./components/RepositoryCard"
import {Button} from "./components/Button"
import {ProgressBar} from "./components/ProgressBar";
import {HeaderMainAsideLayout} from "./layouts/HeaderMainAsideLayout";
import {RepositoryHeader} from "./components/RepositoryHeader";
import {Spinner} from "./components/Spinner";
import {MainContentSplit} from "./layouts/MainContentSplit";
import {TopBar} from "./layouts/TopBar";
import {FileSingleField} from "./components/FileSingleField";
import {FilesField} from "./components/FilesField";
import {SimpleProperties} from "./components/SimpleProperties";

export const UIDemoApp: React.FC = () => {
    return <>
        <ThemeProvider>
            <TopBar
                start="start"
                center="center"
                end="end"
            />
            <PageTitle>That was a TopBar</PageTitle>
            <br/>
            <br/>
            <br/>
            <br/>
            <Stack rowGap={"XL"}>
                <Stack horizontal>
                    <PageTitle>Page Title</PageTitle>
                    <RowFiller/>
                    <Stack horizontal columnGap="XS">
                        <Button appearance="subtle" label="Subtle1"/>
                        <Button appearance="subtle" label="AndThen2"/>
                    </Stack>
                </Stack>
                <DropdownField
                    label="DropdownField"
                    options={[
                        {label: 'Option 1', value: '1'},
                        {label: 'Option 2', value: '2'},
                    ]}
                />
                <KeyValueTable
                    keyLabel="Parameter"
                    value={{keyA: 'valueA', keyB: 'valueB'}}
                />
                <FreeTextField
                    label="FreeTextField"
                />
                <FileSingleField
                    label="FileSingleField"
                />
                <FilesField
                    label="FilesField"
                />
                <Stack horizontal>
                    <RowFiller/>
                    <Button label="Primary Button" appearance="primary"/>
                </Stack>
            </Stack>
            <br/>
            <br/>
            <br/>
            <br/>
            <Stack>
                <PageTitle>ProgressBar, Spinner and text</PageTitle>
                <ProgressBar label="Label" thickness="large" description="Description text for the progress bar"/>
                <span>
                    This is a paragraph of text:
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eget iaculis purus. Morbi mattis
                    ornare arcu sit amet blandit. Sed efficitur aliquam arcu eget volutpat. Donec id pellentesque diam.
                    Praesent vitae risus sollicitudin purus ultricies porta. Sed ac pharetra sapien. Duis faucibus dolor
                    sem, et sollicitudin mauris faucibus ac. Curabitur luctus lectus sit amet turpis porta, eu iaculis
                    sem sagittis.
                    Praesent auctor risus non lectus condimentum, eget auctor magna porttitor. Aenean luctus ac metus
                    aliquam bibendum. Curabitur eleifend, magna a pretium pulvinar, odio augue posuere diam, vitae
                    scelerisque nibh massa porttitor ante. Duis consectetur ex justo. Curabitur ultricies tellus sem,
                    nec hendrerit sapien ultricies et. Mauris urna ante, pretium laoreet sodales nec, congue quis
                    ligula. Cras vehicula arcu est, at pretium justo sodales eu. Proin sodales tincidunt mauris a
                    suscipit. Nunc porttitor nibh id accumsan porta. Duis eleifend egestas est. Duis ac augue mi.
                    Phasellus non consequat nunc. Integer ornare mauris ac mollis aliquet. Ut sed ullamcorper arcu.
                    Fusce imperdiet, elit eu bibendum consequat, sem ipsum vulputate purus, eget faucibus erat est vitae
                    nisl.
                </span>
                <Spinner label="This is a huge spinner" size="huge"/>
                <Spinner label="This is a tiny spinner"/>
            </Stack>
            <br/>
            <br/>
            <br/>
            <br/>
            <Stack rowGap={"XL"}>
                <PageTitle>RepositoryCard</PageTitle>
                {[...new Array(3)].map((_, i) => <RepositoryCard
                    key={i}
                    heading={`Heading dataset ${i}`}
                    access={i % 2 != 0 ? "public" : "private"}
                    description="Some secondary text"
                    metadata="metadata"
                />)}
            </Stack>
            <br/>
            <br/>
            <br/>
            <br/>
            <Stack rowGap="XL">
                <PageTitle>MainContentSplit</PageTitle>
                <MainContentSplit nav={<>
                    navigation
                </>}>
                    {[...new Array(3)].map((_, i) => <RepositoryCard
                        key={i}
                        heading={`Heading dataset ${i}`}
                        access={i % 2 != 0 ? "public" : "private"}
                        description="Some secondary text"
                        metadata="metadata"
                    />)}
                </MainContentSplit>
            </Stack>
            <br/>
            <br/>
            <br/>
            <br/>
            <Stack rowGap="XL">
                <PageTitle>HeaderMainAsideLayout</PageTitle>
                <HeaderMainAsideLayout
                    header={<RepositoryHeader
                        handle="demo/ui"
                        access="private"
                        buttons={<Button size="small" appearance="outline" label="Settings"/>}
                    />}
                    body={<>
                        <main>
                            <Stack minWidth={300}>
                                <Stack horizontal>
                                    <PageSectionTitle>Title A</PageSectionTitle>
                                    <RowFiller/>
                                    <Stack horizontal>
                                        <Button size="small" appearance="secondary" label="Download"/>
                                        <Button size="small" appearance="secondary" label="Sample"/>
                                    </Stack>
                                </Stack>
                                <FilesList
                                    size="small"
                                    files={[
                                        {name: 'test', date: new Date()},
                                        ...[...new Array(10)].map((_, i) => ({
                                            name: 'test_' + i,
                                            date: new Date('1970-01-01')
                                        }))
                                    ]}
                                    onRowClick={row => alert(JSON.stringify(row))}
                                />
                            </Stack>
                        </main>
                        <aside>
                            <Stack rowGap="L">
                                <Stack rowGap="XS">
                                    <PageSectionTitle>Title B</PageSectionTitle>
                                    <span>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Ut gravida magna diam, nec feugiat nulla viverra at.
                                        Etiam placerat erat vitae nisi ultricies accumsan.
                                        Praesent porttitor turpis nisi, a consectetur tellus ornare dictum.
                                    </span>
                                </Stack>
                                <Stack rowGap="XS">
                                    <PageSectionTitle>Title C</PageSectionTitle>
                                    <span>
                                        Lorem ipsum dolor sit amet.
                                    </span>
                                </Stack>
                            </Stack>
                        </aside>
                    </>
                    }
                />
            </Stack>
            <br/>
            <br/>
            <br/>
            <br/>
            <Stack>
                <PageTitle>SimpleProperties</PageTitle>
                <SimpleProperties
                    value={{
                        test1: 'demo',
                        test2: undefined,//42,
                        test3: new Date(),
                    }}
                    schema={{
                        test1: {
                            type: 'line',
                            required: true,
                            title: 'Test text line',
                            description: 'Description text for this test',
                        },
                        test2: {
                            type: 'int',
                            title: 'Test int',
                            description: 'Description text for this test',
                        },
                        test3: {
                            type: 'date',
                            title: 'Test date',
                            description: 'Description text for this test',
                        }
                    }}
                />
            </Stack>
        </ThemeProvider>
    </>
}
