import * as React from 'react'
import {PropsWithChildren, useContext, useMemo} from 'react'
import {FluentProvider} from "@fluentui/react-components";
import {makeTheme} from "../utils/theme";
import {App as AntdCtxProvider, ConfigProvider} from 'antd';
import {BrandContext} from "./BrandContext";


export const ThemeProvider: React.FC<PropsWithChildren<{
    dark?: boolean
    targetDocument?: Document
}>> = props => {
    const brand = useContext(BrandContext)
    const {darkTheme, darkTheme_antd, lightTheme, lightTheme_antd} = useMemo(() => makeTheme(brand), [brand])
    return <FluentProvider theme={props.dark ? darkTheme : lightTheme} targetDocument={props.targetDocument}
                           applyStylesToPortals>
        <ConfigProvider theme={props.dark ? darkTheme_antd : lightTheme_antd}>
            <AntdCtxProvider>
                {props.children}
            </AntdCtxProvider>
        </ConfigProvider>
    </FluentProvider>
}
