import {createContext} from "react";
import {BrandVariants} from "@fluentui/react-theme";

export const BrandContext = createContext<BrandVariants | undefined>(undefined)
