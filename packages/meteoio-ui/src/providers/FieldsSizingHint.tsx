import * as React from 'react'
import {createContext} from 'react'

export interface IFieldsSizingHint {
    fieldWidth?: number
}

export const FieldsSizingHintContext = createContext<IFieldsSizingHint>({})
