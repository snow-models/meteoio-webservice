import * as React from 'react'
import {useState} from 'react'
import {Stack} from "../layouts/Stack";
import {DropdownFieldOption, DropdownMultiselectField} from "./DropdownField";
import {CronFields} from 'cron-parser'

const month_options: DropdownFieldOption[] = [
    {value: '*', label: 'Any month'},
    {value: 'jan', label: 'January'},
    {value: 'feb', label: 'February'},
    {value: 'mar', label: 'March'},
    {value: 'apr', label: 'April'},
    {value: 'may', label: 'May'},
    {value: 'jun', label: 'June'},
    {value: 'jul', label: 'July'},
    {value: 'aug', label: 'August'},
    {value: 'sep', label: 'September'},
    {value: 'oct', label: 'October'},
    {value: 'nov', label: 'November'},
    {value: 'dec', label: 'December'},
]

const weekday_options: DropdownFieldOption[] = [
    {value: '*', label: 'Any week day'},
    {value: 'mon', label: 'Monday'},
    {value: 'tue', label: 'Tuesday'},
    {value: 'wed', label: 'Wednesday'},
    {value: 'thu', label: 'Thursday'},
    {value: 'fri', label: 'Friday'},
    {value: 'sat', label: 'Saturday'},
    {value: 'sun', label: 'Sunday'},
]

const month_day_options: DropdownFieldOption[] = [
    {value: '*', label: 'Any day of the month'},
]
for (let i=1; i<=31; i++){
    month_day_options.push({value: `${i}`, label: `${i}`})
}

export const OldWipCronEditor: React.FC<{

}> = props => {
    const [fields, setFields] = useState<CronFields>()

    return <Stack>
        <Stack horizontal>
            <DropdownMultiselectField
                label="Month"
                options={month_options}
            />
        </Stack>
        <Stack horizontal>
            <DropdownMultiselectField
                label="Day of the month"
                options={month_day_options}
            />
            <DropdownMultiselectField
                label="Day of week"
                options={weekday_options}
            />
        </Stack>
    </Stack>
}
