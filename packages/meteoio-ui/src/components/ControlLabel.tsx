import * as React from 'react'
import {CSSProperties, PropsWithChildren} from 'react'
import {makeStyles} from "@fluentui/react-components";
import {tokens} from "@fluentui/react-theme";


const useStyles = makeStyles({
    span: {
        fontSize: tokens.fontSizeBase200,
        fontWeight: tokens.fontWeightSemibold,
        color: tokens.colorBrandForeground1,
    }
})

export const ControlLabel: React.FC<PropsWithChildren<{ style?: CSSProperties }>> = props => {
    const styles = useStyles()

    return <span className={styles.span} style={props.style}>
        {props.children}
    </span>
}
