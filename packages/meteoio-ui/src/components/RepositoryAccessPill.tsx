import * as React from 'react'
import {Badge} from "@fluentui/react-components";
import {GlobeRegular, LockClosedRegular} from "@fluentui/react-icons";

export type RepositoryAccess =
    | 'private'
    | 'public'

export const RepositoryAccessPill: React.FC<{
    access: RepositoryAccess
}> = props => {
    return <Badge
        appearance="outline"
        color={props.access === 'public' ? 'brand' : 'informative'}
        iconPosition="after"
        size="medium"
        icon={<>
            {props.access === 'private' && <LockClosedRegular/>}
            {props.access === 'public' && <GlobeRegular/>}
        </>}
    >
        {props.access === 'private' && 'Private'}
        {props.access === 'public' && 'Public'}
    </Badge>
}
