import * as React from 'react'
import {ChangeEventHandler, DragEventHandler, useCallback, useRef, useState} from 'react'
import {Field, FieldProps} from "@fluentui/react-field";
import {ControlLabel} from "./ControlLabel";
import {makeStyles, mergeClasses, shorthands, Text, tokens} from "@fluentui/react-components";
import {AddRegular} from "@fluentui/react-icons";
import {Stack} from "../layouts/Stack";
import {FilesInDOMList} from "./FilesInDOMList";
import {fileSizeFormat} from "../utils/fileSizeFormat";

export const useStyles = makeStyles({
    area: {
        ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke1),
        borderBottomColor: tokens.colorNeutralStrokeAccessible,
        ...shorthands.borderRadius(tokens.borderRadiusSmall),
        ...shorthands.paddingInline(tokens.spacingHorizontalS),
        ...shorthands.paddingBlock(tokens.spacingVerticalSNudge),
        backgroundColor: tokens.colorNeutralBackground1,
        minHeight: '50px'
    },
    addLabel: {
        cursor: 'pointer',
        color: tokens.colorNeutralForeground4,
        '&:hover': {
            color: tokens.colorBrandForegroundLinkHover,
        }
    },
    areaWhileDragging: {
        boxShadow: '0 0 6px -1px inset ' + tokens.colorBrandStroke2,
        backgroundColor: tokens.colorBrandBackground2,
    },
    placeholderText: { // NOTE: this is for FileSingleField
        color: tokens.colorNeutralForeground4,
        '&:hover': {
            color: tokens.colorBrandForegroundLinkHover,
        }
    },
    addIcon: {
        color: tokens.colorBrandForegroundLink,
    }
})

/** Allow selection of many files. This is derived from FileSingleField, which is for one file only. */
export const FilesField: React.FC<{
    label?: React.ReactNode
    value?: File[]
    onChange?: (files: File[]) => void
    fieldProps?: Omit<FieldProps, 'label'>
    allowDuplicateFileNames?: boolean
    minHeight?: number
}> = props => {
    const [value_, setValue_] = useState<File[] | undefined>()  // NOTE: this is for testing purpose
    const files = props.value ?? value_
    const onChange = props.onChange ?? setValue_

    const inputRef = useRef<HTMLInputElement>();

    const pushFilesList = useCallback((filesIn: FileList) => {
        const files2 = files?.slice?.() ?? []
        const n = filesIn?.length ?? 0;
        for (let i = 0; i < n; i++) {
            const f = filesIn?.item?.(i)
            if (!f) {
                continue // this should never happen... but it's not an issue.
            }
            // if (files2.includes(f)) {
            //     continue // this sometimes happens because the browser recycles
            //     // the object referring to the same file if the user selects it two times.
            //     ... let's avoid uncertainty: we don't handle this case differently.
            // }
            if (!props.allowDuplicateFileNames && files2.some(x => x.name === f.name)) {
                continue
            }
            files2.push(f)
        }
        onChange?.(files2)
    }, [files, onChange, props.allowDuplicateFileNames])

    // file selection click
    const handleInputChange = useCallback<ChangeEventHandler<HTMLInputElement>>((event) => {
        const __input: HTMLInputElement = event.target
        if (__input) {
            pushFilesList(__input.files)
            // NOTE: the input field must be reset after using the list of files,
            // otherwise a following selection of the same file does not trigger any change event.
            __input.value = null
        }
    }, [pushFilesList])

    // drag and drop
    const [isDragging, setIsDragging] = useState<boolean>(false)
    const handleDragging = useCallback<DragEventHandler>(e => {
        e.stopPropagation()
        e.preventDefault()
        setIsDragging(true)
    }, [])
    const handleDrop = useCallback<DragEventHandler>(e => {
        e.stopPropagation()
        e.preventDefault()
        pushFilesList(e?.dataTransfer?.files)
        setIsDragging(false)
    }, [pushFilesList])

    const styles = useStyles()

    return <>
        <Field
            label={<ControlLabel>{props.label}</ControlLabel>}
            hint={files?.length >= 1 ? <div style={{textAlign: 'right'}}>
                {files.length} files, &nbsp;
                {fileSizeFormat(files.map(f => f.size).reduce((a, b) => a + b, 0))}
            </div> : undefined}
            {...props.fieldProps}
        >
            <div
                className={mergeClasses(styles.area, isDragging && styles.areaWhileDragging)}
                onDrop={handleDrop}
                onDragEnter={handleDragging}
                onDragOver={handleDragging}
                onDragLeave={() => setIsDragging(false)}
                onDragEnd={() => setIsDragging(false)}
                style={props.minHeight ? {minHeight: props.minHeight} : undefined}
            >
                <Stack>
                    {files?.length >= 1 && <FilesInDOMList
                        files={files}
                        onRemoveRequested={file => onChange(files.filter(f => f !== file))}
                    />}

                    <label className={styles.addLabel}
                           style={files?.length > 0 ? undefined : {minHeight: props.minHeight, display: 'flex'}}>
                        <Stack horizontal alignItems="center" columnGap="S">
                            {/*<Button*/}
                            {/*    // size="small"*/}
                            {/*    appearance="primary"*/}
                            {/*    label={<>*/}
                            {/*        <AddRegular/>*/}
                            {/*        &nbsp; Browse files...*/}
                            {/*    </>}*/}
                            {/*    onClick={() => inputRef?.current?.click?.()}*/}
                            {/*/>*/}
                            <AddRegular className={styles.addIcon}/>
                            <Text>
                                Add files...
                            </Text>
                        </Stack>
                        <input
                            style={{display: 'none'}}
                            type="file"
                            multiple
                            ref={inputRef}
                            onChange={handleInputChange}
                        />
                    </label>
                </Stack>
            </div>
        </Field>
    </>
}
