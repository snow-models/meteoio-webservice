import * as React from 'react'
import {makeStyles, Text, tokens} from '@fluentui/react-components'
import {TextProps} from "@fluentui/react-text";

const useStyles = makeStyles({
    text: {
        fontWeight: tokens.fontWeightSemibold,
        color: tokens.colorBrandForegroundLinkPressed,
    }
})

export const Handle: React.FC<{
    handle: string
    label?: React.ReactNode
    textProps?: TextProps
}> = props => {
    const styles = useStyles()

    return <>
        <Text {...props.textProps} className={styles.text}>
            {props.label ?? props.handle}
        </Text>
    </>
}
