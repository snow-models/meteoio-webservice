import * as React from 'react'
import {Spinner as FSpinner} from '@fluentui/react-components'
import {Stack} from "../layouts/Stack";
import {memo} from "react";

export const Spinner: React.FC<{
    size?: "small" | "tiny" | "extra-small" | "medium" | "large" | "extra-large" | "huge"
    label?: string
    labelPosition?: "before" | "after" | "above" | "below"
    tall?: boolean
}> = memo(props => {
    const {size, ...rest} = props
    if (props.tall) {
        return <Stack rowGap="XXXL">
            <span>&nbsp;</span>
            <FSpinner {...rest} size={size ?? 'large'}/>
            <span>&nbsp;</span>
        </Stack>
    }
    return <FSpinner {...rest} size={size ?? 'tiny'}/>
})
