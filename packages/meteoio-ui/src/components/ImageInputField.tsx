import * as React from 'react'
import {Field, makeStyles, shorthands, tokens} from "@fluentui/react-components";
import {ControlLabel} from "./ControlLabel";
import {FieldProps} from "@fluentui/react-field";
import {ImageInput} from "./ImageInput";

const useStyles = makeStyles({
    area: {
        ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke1),
        borderBottomColor: tokens.colorNeutralStrokeAccessible,
        ...shorthands.borderRadius(tokens.borderRadiusSmall),
        ...shorthands.paddingInline(tokens.spacingHorizontalS),
        ...shorthands.paddingBlock(tokens.spacingVerticalSNudge),
        backgroundColor: tokens.colorNeutralBackground1,
        // minHeight: '50px'
        width: 'fit-content',
    },
    input: {
        width: '100%'
    },
    smallCode: {
        '& > textarea, & > input': {
            fontSize: tokens.fontSizeBase100,
            lineHeight: tokens.lineHeightBase100,
            fontFamily: tokens.fontFamilyMonospace,
            wordBreak: 'break-all',
            whiteSpace: 'pre-wrap',
        }
    }
})

export const ImageInputField: React.FC<{
    label?: React.ReactNode
    fieldProps?: Omit<FieldProps, 'label'>
    value?: string
    onChange?: (value: string) => void
    maxWidth: number
    maxHeight: number
}> = props => {

    const styles = useStyles()


    return <Field
        label={<ControlLabel>{props.label}</ControlLabel>}
        {...props.fieldProps}
    >
        <div className={styles.area}>
            <ImageInput
                value={props.value}
                onChange={props.onChange}
                maxWidth={props.maxWidth}
                maxHeight={props.maxHeight}
            />
        </div>
    </Field>
    // return <InputField
    //     label={props.label}
    //     value={props.value}
    //     onChange={props.onChange}
    // />
}
