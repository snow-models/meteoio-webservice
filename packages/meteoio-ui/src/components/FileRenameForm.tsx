import * as React from 'react'
import {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import {FreeTextField} from "./FreeTextField";
import {Stack} from "../layouts/Stack";
import {AsyncActionButton} from "./AsyncActionButton";

export const FileRenameForm: React.FC<{
    initialName?: string
    label?: React.ReactNode
    onRenameRequested?: (name: string) => Promise<void>
    // onValidate?: (name: string) => boolean
}> = props => {

    const [name, setName] = useState<string>(null)
    useEffect(() => {
        setName(null)
    }, [props.initialName]);

    const isValid = useMemo<boolean>(() => {
        //props.onValidate?.(name) ?? true
        if (!name) {
            return false
        }
        if (name === '' || name === '.' || name === '..') {
            return false
        }
        if (name.startsWith(' ')) {
            return false
        }
        if (name.includes('/') || name.includes('?') || name.includes('#')) {
            return false
        }
        // ok
        return true
    }, [name])

    const [pending, setPending] = useState<boolean>(false)

    const canSubmit = name && isValid && !pending

    const submit = useCallback(async () => {
        if (!canSubmit) {
            return
        }
        try {
            setPending(true)
            await props.onRenameRequested?.(name.trim())
            setName(null)
            setPending(false)
        }
        catch (e) {
            setPending(false)
            throw e
        }
    }, [name, canSubmit])

    const inputRef = useRef<HTMLInputElement & HTMLTextAreaElement>()
    useEffect(() => {
        setInterval(() => {
            // NOTE: this assumes FolderCreationForm is only rendered in an open modal and not when closed.
            inputRef?.current?.focus?.()
        }, 100)
    }, [])

    return <>
        <FreeTextField
            inputRef={inputRef}
            readOnly={pending}
            label={props.label ?? <>New name</>}
            value={name ?? props.initialName}
            onChange={setName}
            fieldProps={{
                validationState: name ? (isValid ? 'none' : 'error') : 'none',
                validationMessage: name ? (isValid ? undefined : 'Invalid name') : undefined,
            }}
            onReturn={submit}
        />
        <br/>
        <Stack horizontal justifyContent="end">
            <AsyncActionButton
                appearance="primary"
                isAdditionallyPending={pending}
                //icon={<FolderAddRegular/>}
                label="Confirm"
                disabled={!canSubmit}
                onClick={submit}
            />
        </Stack>
    </>
}
