import {BookFilled, BookRegular, bundleIcon} from "@fluentui/react-icons";

export const RepositoryIcon = bundleIcon(BookFilled, BookRegular)
//export const RepositoryIcon = bundleIcon(BookDefaultFilled, BookDefaultRegular)
