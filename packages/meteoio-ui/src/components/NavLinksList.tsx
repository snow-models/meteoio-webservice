import * as React from 'react'
import {PropsWithChildren} from 'react'
import {makeStyles, mergeClasses, shorthands, tokens} from "@fluentui/react-components";
import {LinkProps, NavLink} from "react-router-dom";
import {ArrowRightFilled} from "@fluentui/react-icons";


const useStyles = makeStyles({
    root: {
        minWidth: '180px',
        // ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke2),
        // ...shorthands.borderRadius(tokens.borderRadiusXLarge),
        //marginTop: tokens.spacingVerticalXL,
        marginBottom: tokens.spacingVerticalXXXL,
        // ...shorthands.padding(tokens.spacingVerticalS, 0),
        overflowX: 'hidden',
        overflowY: 'hidden',

        '& > a > span.arrow': {
            // color: tokens.colorNeutralForegroundDisabled,
            color: tokens.colorBrandForegroundLink,
            // filter: 'drop-shadow(1px 0 0 currentColor)',
            // marginLeft: '1em',
            fontSize: '0.9em',
            display: 'inline-block',
            transform: 'translate(9px, 4px)',
            opacity: 0.75,
            ...shorthands.transition('transform, opacity', '0.2s', '0', 'ease'),
        },
        '& > a': {
            display: 'block',
            backgroundColor: tokens.colorNeutralBackgroundAlpha,
            ...shorthands.padding(tokens.spacingVerticalSNudge, tokens.spacingHorizontalM),
            textDecorationLine: 'none',
            color: tokens.colorNeutralForeground2Link,
        },
        '& > a:hover': {
            backgroundColor: tokens.colorNeutralBackground3Hover,
        },
        '& > a:hover > span.arrow': {
            color: tokens.colorBrandForegroundLinkHover,
            transform: 'translate(12px, 4px)',
            opacity: 1,
        },
        '& > a.active': {
            backgroundColor: tokens.colorNeutralBackground3,
            fontWeight: 500,
        }
    },
    root_topMargin: {
        marginTop: tokens.spacingVerticalXL,
    },
    root_inlineMargin: {
        marginLeft: tokens.spacingHorizontalL,
        marginRight: tokens.spacingHorizontalL,
    },
    variant_box: {
        ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke2),
        ...shorthands.borderRadius(tokens.borderRadiusXLarge),
        '& > a:not(:first-child)': {
            ...shorthands.borderTop('thin', 'solid', tokens.colorNeutralStroke2),
        },
        '& > a': {
            color: tokens.colorNeutralForeground2Link,
        },
        '& > a:hover': {
            backgroundColor: tokens.colorNeutralBackground3Hover,
        },
        '& > a.active': {
            backgroundColor: tokens.colorNeutralBackgroundAlpha,
            boxShadow: `inset 7px 0 0 -4px ${tokens.colorBrandStroke1}`
        }
    }
})

export const NavLinksList: React.FC<PropsWithChildren<{
    links: React.PropsWithChildren<LinkProps>[]
    variant?: 'main' | 'box'
    noTopMargin?: boolean
    noInlineMargin?: boolean
}>> = props => {

    const styles = useStyles()

    return <>
        <div className={mergeClasses(
            styles.root,
            !props.noTopMargin && styles.root_topMargin,
            !props.noInlineMargin && styles.root_inlineMargin,
            props.variant === 'box' && styles.variant_box,
        )}>
            {props.links.map((link, i) =>
                <NavLink key={i} {...link}>
                    {link.children}
                    <span className="arrow"><ArrowRightFilled/></span>
                </NavLink>
            )}
            {props.children}
        </div>
    </>
}

// Idea: maybe add arrow right icons on the right?