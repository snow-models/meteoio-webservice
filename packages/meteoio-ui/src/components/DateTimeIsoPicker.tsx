import React, {useMemo, useState} from "react";
import {DatePicker} from "antd";
import dayjs from "dayjs";
import {Input, makeStyles, mergeClasses, shorthands, tokens} from "@fluentui/react-components";

export const formatDateTime: (date?: Date, calenderDate?: dayjs.Dayjs, sameTime?: boolean) => string = (date, calenderDate, sameTime) => {
    if (!date || isNaN(date.getTime())) return "";
    const year = date.getFullYear().toString().padStart(4, "0");
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const useCdForSameTime = calenderDate?.isValid?.() && sameTime
    const hours =
        useCdForSameTime
            ? calenderDate.get("hours")?.toString().padStart(2, "0")
            : date.getHours().toString().padStart(2, "0");
    const minutes =
        useCdForSameTime
            ? calenderDate.get("minutes")?.toString().padStart(2, "0")
            : date.getMinutes().toString().padStart(2, "0");
    const seconds =
        useCdForSameTime
            ? calenderDate.get("seconds")?.toString().padStart(2, "0")
            : date.getSeconds().toString().padStart(2, "0");

    return `${year}-${month}-${day}T${hours}:${minutes}:${seconds}`
}

const useStyles = makeStyles({
    root: {
        // width: '100%',
        '& .ant-picker': {
            ...shorthands.border(0, 'none'),
            boxShadow: 'none',
            width: '30px',
        },
        '& .ant-picker-input > input': {
            ...shorthands.border(0, 'none'),
            boxShadow: 'none',
            ...shorthands.outline(0, 'none'),
        },
        '& .ant-picker .ant-picker-input > input': {
            display: 'none !important',
        }
    },
    valid: {},
    invalid: {
        '& *': {
            color: tokens.colorPaletteRedForeground1,
        },
    }
})

export const DateTimeIsoPicker: React.FC<{
    value: string
    onChange: (value: string) => void
    disabled?: boolean
    inputRef?: React.Ref<HTMLInputElement>
}> = ({
          value,
          onChange,
          disabled,
          inputRef,
      }) => {
    const styles = useStyles()
    const [inputValue, setInputValue] = useState<string>(value);
    const [calenderDate, setCalenderDate] = useState<string>(value);

    const isValid = useMemo<boolean>(() => {
        if (!inputValue) {
            return undefined
        }
        const dayJsValue = dayjs(inputValue)
        if (!dayJsValue?.isValid?.()) {
            return false
        }
        if (isNaN((new Date(inputValue))?.getTime?.())) {
            return false
        }
        // ok
        return true
    }, [inputValue])

    const handleDateSelect = (date: dayjs.Dayjs) => {
        const jsDate = date.toDate();
        jsDate.setHours(0, 0, 0, 0);
        const formattedDate = formatDateTime(jsDate, dayjs(calenderDate), true);
        setCalenderDate(formattedDate);
        onChange(formattedDate);
        setInputValue(formattedDate);
    };

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputValue = e.target.value;
        setInputValue(inputValue);
        onChange(inputValue)
    };

    const handleInputBlur = (e: React.ChangeEvent<HTMLInputElement>) => {
        const inputValue = e.target.value;
        const date = new Date(inputValue);
        if (inputValue && !isValid) {
            // dialogs.message.error("Invalid date input")?.then?.();
            return false;
        }
        const formattedDate = formatDateTime(date);
        setCalenderDate(formattedDate);
        if (isValid) {
            setInputValue(formattedDate);
            onChange(formattedDate);
        }
    };

    const handleIncrementHour = (hours: number) => {
        const djv = dayjs(inputValue)
        if (djv.isValid()) {
            const dateValue = djv
                ?.add(hours, "hour")
                ?.format("YYYY-MM-DDTHH:mm:ss");
            setCalenderDate(dateValue);
            setInputValue(dateValue);
            onChange(dateValue);
        }
    };

    const handleKeyPress = (e: any) => {
        if (e.key === "ArrowUp") {
            handleIncrementHour(1);
        } else if (e.key === "ArrowDown") {
            handleIncrementHour(-1);
        } else if (e.key === "Enter") {
            handleInputBlur(e);
        }
    };

    return (
        <Input
            ref={inputRef}
            className={mergeClasses(styles.root, isValid ? styles.valid : styles.invalid)}
            value={inputValue}
            placeholder="YYYY-MM-DDTHH:mm:ss"
            contentAfter={
                <DatePicker
                    placeholder=""
                    bordered={false}
                    value={calenderDate && dayjs(calenderDate).isValid() ? dayjs(calenderDate) : null}
                    onSelect={handleDateSelect}
                    allowClear={false}
                />
            }
            onChange={(e) => handleInputChange(e)}
            onBlur={(e) => handleInputBlur(e)}
            onKeyDown={(e) => handleKeyPress(e)}
            disabled={disabled}
        />
    );
};
