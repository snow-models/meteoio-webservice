import * as React from 'react'
import {PropsWithChildren} from 'react'
import {Dropdown, Field, Input, Option, SpinButton, Textarea} from "@fluentui/react-components";
import {DatePicker} from "./DatePicker";
import {ControlLabel} from "./ControlLabel";
import {FieldDescription} from "./FieldDescription";
import {DropdownFieldOption} from "./DropdownField";

// NOTE: Do not expand this too much. We want this to be simple.
//       If you need something more specific, then just make a specific component with implicit schema.

export type SimplePropertySchema = {
    title: string
    description?: string
    required?: boolean
} & (
    {
        type: 'line' | 'paragraph' | 'date'
    } | {
    type: 'enum'
    options: DropdownFieldOption[]
} | {
    type: 'int'
    min?: number
    max?: number
}
)

export type SchematicType = Record<string, unknown> | object

export type SimplePropertiesSchema<T extends SchematicType = SchematicType> = Record<keyof T, SimplePropertySchema>

export function SimpleProperties<T extends SchematicType>(props: {
    value?: Partial<T>
    onChange?: (value: Partial<T>) => void,
    schema: SimplePropertiesSchema<T>
    disabled?: boolean
}) {
    return <>
        {Object.keys(props.schema).map(key =>
            <PropertyField key={key} schema={props.schema[key]}>
                <PropertyValue
                    schema={props.schema[key]}
                    value={props.value?.[key]}
                    onChange={props.onChange
                        ? v => props.onChange({...props.value, [key]: v})
                        : undefined}
                    disabled={props.disabled}
                />
            </PropertyField>
        )}
    </>
}

export const PropertyValue: React.FC<{
    value?: unknown,
    onChange?: (value) => void,
    schema: SimplePropertySchema
    disabled?: boolean
}> = props => {
    if (props.schema.type === 'int') {
        return <>
            <SpinButton
                disabled={props.disabled}
                min={props.schema?.min}
                max={props.schema?.max}
                value={props.value as number ?? null}
                onChange={(_ev, data) => {
                    if (data.value !== undefined) {
                        props.onChange?.(data.value);
                    } else if (data.displayValue !== undefined) {
                        try {
                            const newValue = parseInt(data.displayValue);
                            if (!Number.isNaN(newValue)) {
                                props.onChange?.(newValue);
                            } else {
                                console.error(`Cannot parse "${data.displayValue}" as a number.`);
                                props.onChange?.(null)
                            }
                        } catch (e) {
                            console.trace(e)
                            props.onChange?.(null)
                        }
                    }
                }}
            />
        </>
    }
    if (props.schema.type === 'line') {
        return <>
            <Input
                disabled={props.disabled}
                value={props.value as string ?? ''}
                onChange={(ev, data) => props.onChange?.(data.value)}
            />
        </>
    }
    if (props.schema.type === 'paragraph') {
        return <>
            <Textarea
                disabled={props.disabled}
                resize="vertical"
                value={props.value as string ?? ''}
                onChange={(ev, data) => props.onChange?.(data.value)}
            />
        </>
    }
    if (props.schema.type === 'date') {
        return <>
            <DatePicker
                disabled={props.disabled}
                value={props.value as Date}
                onChange={props.onChange}
            />
        </>
    }
    if (props.schema.type === 'enum') {
        const opt = props.value !== undefined ? props.schema.options?.find?.(o => o.value === props.value) : undefined
        return <>
            <Dropdown
                disabled={props.disabled}
                value={opt?.text ?? opt?.value ?? null as string}
                selectedOptions={opt ? [opt.value] : []}
                onOptionSelect={props.onChange ? (event, data) => props.onChange(data.optionValue) : undefined}
            >
                {props.schema.options.map((opt, i) =>
                    <Option key={`${i}-${opt.value}`} value={opt.value} text={opt.text ?? (typeof opt.label === 'string' ? opt.label : opt.value)}>
                        {opt.label}
                    </Option>)}
            </Dropdown>
        </>
    }
}

export const PropertyField: React.FC<PropsWithChildren<{
    schema: SimplePropertySchema
}>> = props => {
    return <FieldDescription
        field={<Field
            label={<ControlLabel>{props.schema.title}</ControlLabel>}
            //hint={props.schema.description}
            required={props.schema.required}
        >
            {props.children}
        </Field>}
        description={props.schema.description}
    />
}
