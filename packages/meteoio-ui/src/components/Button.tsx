import * as React from 'react'
import {CSSProperties, MouseEventHandler, ReactNode} from 'react';
import {
    Button as FButton,
    CompoundButton,
    makeStyles,
    mergeClasses,
    shorthands,
    Slot,
    tokens
} from "@fluentui/react-components";

const useStyles = makeStyles({
    button: {
        // fontWeight: tokens.fontWeightMedium,
    },
    noInlinePadding: {
        minWidth: '1px',
        ...shorthands.paddingInline(0),
    },
    danger: {
        color: tokens.colorPaletteRedForeground1,
    },
    wide: {
        minWidth: '200px',
    }
})

export interface ButtonProps {
    label?: ReactNode
    secondaryLine?: string
    disabled?: boolean
    onClick?: MouseEventHandler
    href?: string
    target?: string
    appearance?: 'primary' | 'secondary' | 'outline' | 'subtle'
    size?: 'small' | 'medium' | 'large'
    wide?: boolean
    noInlinePadding?: boolean
    icon?: Slot<'span'>
    iconPosition?: 'before' | 'after'  // NOTE: implemented while not in use
    danger?: boolean
    style?: CSSProperties
}

export const Button: React.FC<ButtonProps> = props => {

    const {secondaryLine, ...commonProps} = props

    const styles = useStyles()

    if (secondaryLine) {
        return <CompoundButton
            as={props.href ? 'a' : 'button'}
            secondaryContent={secondaryLine}
            className={mergeClasses(styles.button, props.wide && styles.wide, props.noInlinePadding && styles.noInlinePadding, props.danger && styles.danger)}
            {...commonProps}
        >
            {props.label}
        </CompoundButton>
    }

    return <FButton
        as={props.href ? 'a' : 'button'}
        className={mergeClasses(styles.button, props.wide && styles.wide, props.noInlinePadding && styles.noInlinePadding, props.danger && styles.danger)}
        {...commonProps}
    >
        {props.label}
    </FButton>
}
