import React, { useState, useRef } from 'react';
import { Upload, message } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { RcFile } from 'antd/es/upload/interface';
import ImgCrop from 'antd-img-crop';

interface ImageInputProps {
    value?: string
    onChange?: (value: string) => void
    displayMode?: 'cover' | 'contain';
    maxWidth: number
    maxHeight: number
}

export const ImageInput: React.FC<ImageInputProps> = (
    { value, onChange, displayMode = 'contain', maxWidth, maxHeight }
) => {
    const [fileList, setFileList] = useState<any[]>([]);
    const uploadRef = useRef<any>(null);

    const handleBeforeUpload = async (file: RcFile) => {
        const resizedImage = await resizeImage(file, maxWidth, maxHeight, displayMode);
        const reader = new FileReader();
        reader.readAsDataURL(resizedImage);
        reader.onload = () => {
            const base64Value = reader.result as string;
            onChange(base64Value);
            setFileList([{ uid: '-1', name: file.name, status: 'done', url: base64Value }]);
        };
        reader.onerror = (error) => {
            message.error(`File upload failed: ${error}`);
        };
        return false; // Prevent default upload behavior
    };

    return (
        <div>
            <ImgCrop
                minZoom={0.5}
                beforeCrop={recodeImage}
                fillColor="transparent"
                aspect={1}
                cropperProps={{objectFit: 'contain', style: {}, zoomSpeed: 1, restrictPosition: false, mediaProps: {}}}
            >
                <Upload
                    listType="picture-card"
                    accept="image/png, image/gif, image/jpeg, image/svg+xml"
                    ref={uploadRef}
                    beforeUpload={handleBeforeUpload}
                    fileList={fileList}
                    onRemove={() => {
                        setFileList([]);
                        onChange('');
                    }}
                    showUploadList={false}
                >
                    <div onClick={() => uploadRef.current?.upload?.click?.()} style={{ cursor: 'pointer' }}>
                        {value ? (
                            <img src={value} alt="Preview" style={{ width: 100, height: 100, objectFit: 'cover', position: 'relative', top: 3 }} />
                        ) : (
                            <div style={{ width: 100, height: 100, backgroundColor: '#f0f0f0', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                                <UploadOutlined style={{ fontSize: 24 }} />
                            </div>
                        )}
                    </div>
                </Upload>
            </ImgCrop>
            {/*<Button icon={<UploadOutlined />} onClick={() => uploadRef.current?.upload?.click()}>*/}
            {/*    Upload Image*/}
            {/*</Button>*/}
        </div>
    );
};

const resizeImage = (file: RcFile, maxWidth: number, maxHeight: number, mode: 'cover' | 'contain'): Promise<File> => {
    return new Promise((resolve, reject) => {
        const img = document.createElement('img');
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        img.onload = () => {
            let width = img.width;
            let height = img.height;
            let offsetX = 0;
            let offsetY = 0;

            if (mode === 'contain') {
                if (width > height) {
                    if (width > maxWidth) {
                        height = Math.round((height *= maxWidth / width));
                        width = maxWidth;
                    }
                } else {
                    if (height > maxHeight) {
                        width = Math.round((width *= maxHeight / height));
                        height = maxHeight;
                    }
                }
                offsetX = (width - maxWidth) / 2;
                offsetY = (height - maxHeight) / 2;
            } else if (mode === 'cover') {
                const scale = Math.max(maxWidth / width, maxHeight / height);
                width = Math.round(width * scale);
                height = Math.round(height * scale);
                offsetX = (width - maxWidth) / 2;
                offsetY = (height - maxHeight) / 2;
            }

            canvas.width = maxWidth;
            canvas.height = maxHeight;
            ctx!.drawImage(img, -offsetX, -offsetY, width, height);

            canvas.toBlob((blob) => {
                if (blob) {
                    const resizedFile = new File([blob], file.name, { type: 'image/png' });
                    resolve(resizedFile);
                } else {
                    reject(new Error('Image resize failed'));
                }
            }, 'image/png');
        };
        img.onerror = reject;
        img.src = URL.createObjectURL(file);
    });
};

const recodeImage = (file: RcFile): Promise<File> => {
    return new Promise((resolve, reject) => {
        const img = document.createElement('img');
        const canvas = document.createElement('canvas');
        const ctx = canvas.getContext('2d');

        img.onload = () => {
            canvas.width = img.width;
            canvas.height = img.height;
            ctx!.drawImage(img, 0, 0, img.width, img.height);

            canvas.toBlob((blob) => {
                if (blob) {
                    const resizedFile = new File([blob], file.name, { type: 'image/png' });
                    resolve(resizedFile);
                } else {
                    reject(new Error('Image recode failed'));
                }
            }, 'image/png');
        };
        img.onerror = reject;
        img.src = URL.createObjectURL(file);
    });
};
