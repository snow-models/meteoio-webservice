import * as React from 'react'
import {useEffect, useMemo, useState} from 'react'
import {relativeTimeFormat} from "../utils/relativeTimeFormat";
import {parseDate} from "meteoio-platform-web-pages/src/_common/parseDate";

export const RelativeTimeText: React.FC<{
    date?: Date | string
}> = props => {

    const _date = useMemo<Date>(() => {
        try {
            return parseDate(props.date)
        } catch (e) {
            console.trace(e)
        }
    }, [props.date])

    const [progressive, setProgressive] = useState<number>(0)
    useEffect(() => {
        let delay = 10_000
        let tio = null;
        const h = () => {
            setProgressive(v => v + 1)
            delay *= 1.02
            tio = setTimeout(h, delay)
        }
        tio = setTimeout(h, delay)
        return () => {
            clearTimeout(tio)
        }
    }, [])

    const _str = useMemo<string>(() => _date ? relativeTimeFormat(_date, 60000) : '', [_date, progressive])

    return <time
        dateTime={_date?.toISOString?.()}
        title={_date?.toString?.()}
    >
        {_str}
    </time>
}
