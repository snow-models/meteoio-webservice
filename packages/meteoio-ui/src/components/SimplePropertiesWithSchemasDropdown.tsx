import * as React from 'react';
import {useMemo} from 'react';
import {DropdownField, DropdownFieldOption} from "./DropdownField";
import {SchematicType, SimpleProperties, SimplePropertiesSchema} from "./SimpleProperties";
import {FieldDescription} from "./FieldDescription";

export function SimplePropertiesWithSchemasDropdown<T extends SchematicType>(props: {
    schemaOptionValue?: string
    onSchemaChange?: (schemaKey?: string) => void
    value?: Partial<T>
    onChange?: (value: Partial<T>) => void
    schemas: { option: DropdownFieldOption, schema: SimplePropertiesSchema }[]
    schemaDropdownLabel?: string
    schemaDropdownDescription?: string
}) {

    const selection = useMemo(() =>
        props.schemaOptionValue ? props.schemas.find(s => s.option.value === props.schemaOptionValue) : undefined,
        [props.schemaOptionValue, props.schemas])



    return <>
        <FieldDescription
            field={<DropdownField
                label={props.schemaDropdownLabel}
                options={props.schemas.map(p => p.option)}
                value={props.schemaOptionValue}
                onChange={props.onSchemaChange}
            />}
            description={props.schemaDropdownDescription}
        />
        {selection && <SimpleProperties
            schema={selection.schema}
            value={props.value}
            onChange={props.onChange}
        />}
    </>
}
