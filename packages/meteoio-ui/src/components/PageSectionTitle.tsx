import * as React from 'react'
import {PropsWithChildren} from 'react'
import {makeStyles, Text} from "@fluentui/react-components";
import {Stack} from "../layouts/Stack";

const useStyles = makeStyles({
    text: {
        marginBottom: 0,
        marginTop: 0,
    }
})

export const PageSectionTitle: React.FC<PropsWithChildren<{
    icon?: React.ReactNode
    iconPosition?: 'start' | 'end'
}>> = props => {

    const styles = useStyles()

    return <Stack
        horizontal
        alignItems="center"
        justifyContent={props.iconPosition === 'end' ? "space-between" : undefined}
        columnGap="S"
    >
        {(props.iconPosition === 'start' || props.iconPosition === undefined) && props.icon}
        <Text block size={400} weight="semibold" className={styles.text}>
            {props.children}
        </Text>
        {(props.iconPosition === 'end' || props.iconPosition === undefined) && props.icon}
    </Stack>
}
