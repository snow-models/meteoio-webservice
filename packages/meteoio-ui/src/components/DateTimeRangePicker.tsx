import * as React from 'react'
import {ReactNode, useMemo, useRef} from 'react'
import {Stack} from "../layouts/Stack";
import {ControlLabel} from "./ControlLabel";
import {Button} from "./Button";
import {Caption2Strong, Input, Text} from "@fluentui/react-components"
import {DateTimeIsoPicker, formatDateTime} from "./DateTimeIsoPicker";

export interface Range {
    begin?: null | string
    end?: null | string
    duration_days?: null | number
}

interface RangeComputing {
    input?: Range
    cleaned: {
        begin?: string
        end?: string
        duration_days?: number
    }
    shine: {
        isDurationProvided: boolean
        isBeginAbsolute: boolean
        isEndAbsolute: boolean
        isEndCurrent: boolean
        isUnderDetermined: boolean
        isOverDetermined: boolean
    }
    absolute?: {
        begin: Date
        end: Date
        duration_days: number
    }
}

function compute(input?: Range) {

    const cleaned: RangeComputing['cleaned'] = {
        begin: input?.begin || undefined,
        end: input?.end || undefined,
        duration_days: (input?.duration_days === undefined || input?.duration_days === null)
            ? undefined : getNonNegativeInt(input.duration_days),
    }

    const isEndCurrent = cleaned.end?.toUpperCase?.()?.trim?.() === 'NOW'

    const shine: RangeComputing['shine'] = {
        isDurationProvided: cleaned.duration_days !== undefined,
        isBeginAbsolute: cleaned.begin !== undefined,
        isEndAbsolute: cleaned.end !== undefined && !isEndCurrent,
        isEndCurrent,
        isUnderDetermined: !((cleaned.begin !== undefined && cleaned.end !== undefined) || (cleaned.begin !== undefined && cleaned.duration_days !== undefined) || (cleaned.end !== undefined && cleaned.duration_days !== undefined)),
        isOverDetermined: !!cleaned.begin && !!cleaned.end && !!cleaned.duration_days,
    }

    const head = {input, cleaned, shine}

    if (!shine.isOverDetermined && !shine.isUnderDetermined) {

        const day = 3600_000 * 24

        const end: Date = shine.isEndCurrent ? new Date() : (
            shine.isEndAbsolute ? new Date(cleaned.end) :
                new Date(new Date(cleaned.begin).getTime() + cleaned.duration_days * day))

        const begin = shine.isBeginAbsolute ? new Date(cleaned.begin) :
            new Date(end.getTime() - cleaned.duration_days * day)

        const duration_days = Math.ceil((end.getTime() - begin.getTime()) / day)

        const absolute: RangeComputing['absolute'] = {begin, end, duration_days}

        return {...head, absolute}
    }
    return {...head}
}

export const DateTimeRangePicker: React.FC<{
    label?: ReactNode
    value?: Range
    onChange: (value: Range) => void  // TODO: remove Date objects from outgoing data.
}> = props => {

    // TODO?: implement a DateTimePicker and use it in place of DatePicker

    const computed = useMemo<RangeComputing>(() => compute(props.value), [props.value])

    const durationInputRef = useRef<HTMLInputElement>()
    const beginPickerRef = useRef<HTMLInputElement>()
    const endPickerRef = useRef<HTMLInputElement>()

    const handleSetDateInsteadOfNow = (value?: string) => {
        const nowReplacement = new Date()
        nowReplacement.setHours(0, 0, 0, 0)
        props.onChange?.({...props.value, end: value ?? formatDateTime(nowReplacement)})
        setTimeout(() => {
            endPickerRef?.current?.focus?.()
        }, 1)
        setTimeout(() => {
            endPickerRef?.current?.focus?.()
        }, 10)
        setTimeout(() => {
            endPickerRef?.current?.focus?.()
        }, 100)
    }

    return <>
        <Stack rowGap="None">
            <Stack horizontal columnGap="None" alignItems="end">
                <Stack rowGap="None" width="50%">
                    {props.label && <ControlLabel>{props.label}</ControlLabel>}
                </Stack>
                <Stack rowGap="None" width="50%" alignItems="end">
                    <Caption2Strong style={{opacity: 0.7}}>End</Caption2Strong>
                </Stack>
            </Stack>
            <Stack horizontal columnGap="None">
                <Stack rowGap="None" width="50%">
                    {computed.shine.isBeginAbsolute
                        ? <DateTimeIsoPicker
                            inputRef={beginPickerRef}
                            value={computed.cleaned.begin}
                            onChange={date => {
                                props.onChange({...props.value, begin: date})
                            }}
                        /> : <Input
                            ref={durationInputRef}
                            type="number"
                            value={computed.cleaned.duration_days?.toFixed?.(0) ?? ''}
                            onChange={(ev, data) => {
                                props.onChange?.({...props.value, duration_days: parseInt(data.value)})
                            }}
                            contentAfter={<Text size={200}>days</Text>}
                        />}
                    <Stack horizontal justifyContent="end">
                        {computed.shine.isBeginAbsolute
                            ? <Button
                                size="small"
                                appearance="subtle"
                                label={<Text size={100}>...or insert a duration</Text>}
                                onClick={() => {
                                    props.onChange?.({
                                        ...props.value,
                                        begin: null,
                                        duration_days: computed.absolute?.duration_days
                                    })
                                    setTimeout(() => {
                                        durationInputRef?.current?.focus?.()
                                    }, 100)
                                }}
                            />
                            : <Button
                                size="small"
                                appearance="subtle"
                                label={<Text size={100}>...or select a begin date</Text>}
                                onClick={() => {
                                    const replaceDate = new Date(computed.absolute?.begin)
                                    replaceDate.setHours(0, 0, 0, 0)
                                    props.onChange?.({
                                        ...props.value,
                                        duration_days: null,
                                        begin: formatDateTime(replaceDate)
                                    })
                                    setTimeout(() => {
                                        beginPickerRef?.current?.focus?.()
                                    }, 100)
                                }}
                            />}
                    </Stack>
                </Stack>
                {computed.shine.isEndCurrent
                    ? <Stack rowGap="None" width="50%">
                        <Input
                            // contentAfter={<CalendarMonthRegular
                            //     style={{color: tokens.colorNeutralForeground3, cursor: 'pointer'}}
                            //     onClick={() => handleSetDateInsteadOfNow()}
                            // />}
                            onChange={(ev) => handleSetDateInsteadOfNow(ev.target?.value)}
                            // readOnly
                            onSelect={(ev) => (ev.target as HTMLInputElement).setSelectionRange(0, (ev.target as HTMLInputElement)?.value?.length)}
                            value="Current Time"
                            defaultValue="Current Time"
                            placeholder="Current Time"
                            type="text"
                        />
                        <Stack horizontal justifyContent="end">
                            <Button
                                size="small"
                                appearance="subtle"
                                label={<Text size={100}>...or select an end date</Text>}
                                onClick={() => handleSetDateInsteadOfNow()}
                            />
                        </Stack>
                    </Stack>
                    : <Stack rowGap="None" width="50%">
                        <DateTimeIsoPicker
                            inputRef={endPickerRef}
                            value={props.value.end || ''}
                            // NOTE: I'm ignoring the possible combination of start date absolute and end date relative.
                            onChange={date => {
                                props.onChange?.({...props.value, end: date})
                            }}
                        />
                        <Stack horizontal justifyContent="end">
                            <Button
                                size="small"
                                appearance="subtle"
                                label={<Text size={100}>...or switch to current time</Text>}
                                onClick={() => {
                                    props.onChange?.({...props.value, end: 'NOW'})
                                }}
                            />
                        </Stack>
                    </Stack>}
            </Stack>
        </Stack>
    </>
}


function getNonNegativeInt(x: string | number): number {
    let y;
    try {
        y = parseInt(x + '')
    } catch (e) {
        y = 0
    }
    if (isNaN(y)) {
        y = 0
    }
    y = Math.max(0, y)
    return y
}
