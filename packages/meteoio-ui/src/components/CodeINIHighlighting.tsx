import * as React from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter/dist/esm/light';
import ini from 'react-syntax-highlighter/dist/esm/languages/hljs/ini';
import base_code_style from 'react-syntax-highlighter/dist/esm/styles/hljs/color-brewer';
import {makeStyles, shorthands, tokens} from "@fluentui/react-components";

SyntaxHighlighter.registerLanguage('ini', ini);

const useStyles = makeStyles({
    root: {
        ...shorthands.margin(0),
        '& .react-syntax-highlighter-line-number': {
            fontSize: tokens.fontSizeBase100,
            color: tokens.colorNeutralForegroundDisabled,
            ...shorthands.borderRight('thin', 'dashed', tokens.colorNeutralForegroundDisabled),
            marginRight: tokens.spacingHorizontalS,
        }
    }
})

const code_style = {
    ...base_code_style,
    hljs: {
        ...base_code_style.hljs,
        display: "block",
        overflowX: "auto",
        padding: "0",
        background: "transparent",
        color: tokens.colorNeutralForeground1,
        lineHeight: tokens.lineHeightBase200,
    },
    "hljs-subst": {
        color: tokens.colorNeutralForeground4
    },
    "hljs-attr": {
        color: tokens.colorBrandForeground2
    },
    "hljs-string": {
        color: tokens.colorNeutralForeground3
    },
    "hljs-comment": {
        color: tokens.colorNeutralForegroundDisabled
    },
    "hljs-number": {
        color: tokens.colorPaletteLightGreenForeground1
    },
    "hljs-section": {
        color: tokens.colorNeutralForeground1,
        fontWeight: 'bold',
        //fontSize: tokens.fontSizeBase300
    },
};

export const CodeINIHighlighting: React.FC<{
    code: string
    containerProps?: Exclude<React.HTMLAttributes<HTMLPreElement>, 'className'>
}> = props => {
    const styles = useStyles()

    return <SyntaxHighlighter
        className={styles.root}
        //wrapLongLines
        showLineNumbers
        language={'ini'}
        style={code_style}
    >{props.code}</SyntaxHighlighter>
}
