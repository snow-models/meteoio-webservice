import * as React from 'react'
import {Field, Input} from "@fluentui/react-components";
import {ControlLabel} from "./ControlLabel";


export const LoginField: React.FC<{
    type: 'email' | 'password' | 'text'
    label?: string
    value?: string
    onChange?: (value: string) => void
    onEnter?: () => void
    required?: boolean
    authFailure?: boolean
    autoComplete?: string
    disabled?: boolean
    innerRef?: React.Ref<HTMLInputElement>
}> = props => {
    return <>
        <Field
            label={props.label ? <ControlLabel>{props.label}</ControlLabel> : undefined}
            required={props.authFailure ? props.required : undefined}
            // validationState={props.authFailure ? "error" : undefined}
            // validationMessage={props.authFailure ? ' ' : undefined}
        >
            <Input
                ref={props.innerRef}
                disabled={props.disabled}
                autoComplete={props.autoComplete}
                type={props.type}
                value={props.value as string ?? ''}
                onChange={(ev, data) => props.onChange?.(data.value)}
                onKeyPress={props.onEnter ? (ev) => {
                    if (ev.key === 'Enter') {
                        props.onEnter?.()
                    }
                } : undefined}
            />
        </Field>
    </>
}
