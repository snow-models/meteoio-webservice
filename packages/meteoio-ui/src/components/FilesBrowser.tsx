import * as React from "react";
import {MouseEventHandler, ReactNode, useCallback, useMemo, useRef, useState} from "react";
import {Menu, MenuItem, MenuList, MenuPopover, Text, tokens} from "@fluentui/react-components";
import {
    ArrowClockwiseFilled,
    ArrowDownloadFilled,
    ArrowDownloadRegular,
    RenameRegular,
    RenameFilled,
    ArrowUpRegular,
    BinRecycleFilled,
    BinRecycleRegular,
    bundleIcon,
    ChevronRightRegular,
    FolderOpenRegular
} from "@fluentui/react-icons";
import {Spinner} from "./Spinner";
import {Stack} from "../layouts/Stack";
import {Button} from "./Button";
import {Breadcrumb} from "./Breadcrumb";
import {MaybeErrorAlert} from "./MaybeErrorAlert";
import {ProgressBar} from "./ProgressBar";
import {FileRow, FilesList} from "./FilesList";
import {Empty, message} from "antd";
import {downloadFile, Fetcher} from "../utils/downloadFile";
import {PositioningShorthand} from "@fluentui/react-positioning";
import {MenuArrow} from "./MenuArrow";
import {Modal} from "./Modal";
import {AsyncActionButton} from "./AsyncActionButton";
import {FileRenameForm} from "./FileRenameForm";


const DownloadIcon = bundleIcon(ArrowDownloadFilled, ArrowDownloadRegular)
// const MoveIcon = bundleIcon(FolderArrowRightFilled, FolderArrowRightRegular)
const RenameIcon = bundleIcon(RenameFilled, RenameRegular)
const DeleteIcon = bundleIcon(BinRecycleFilled, BinRecycleRegular)


export interface IFilesBrowser {
    rootLabel?: ReactNode
    path?: string
    onChDir?: (path: string) => void
    entries?: FileRow[]
    error?: unknown
    isLoading?: boolean
    isRefetching?: boolean
    refetch?: () => Promise<void>
    fileFetcher?: Fetcher
    noTopBottomBorder?: boolean
    overlayEntries?: FileRow[]
    onOverlayActionsRender?: (row: FileRow) => ReactNode
    onFileDeleteRequested?: (row: FileRow) => Promise<void>
    onFileRenameRequested?: (row: FileRow, new_name: string) => Promise<void>
}

// NOTE: some refactoring could improve this: a click callback in FileRow instead of fileFetcher and onOverlayClick.
//       ... or better: an optional click callback in FileRow.

export const FilesBrowser: React.FC<IFilesBrowser> = props => {

    const [messageApi, messageContextHolder] = message.useMessage({maxCount: 1})

    const isRoot = props.path === ''

    const rows = useMemo<FileRow[]>(() => {
        if (props.overlayEntries === undefined) {
            return props.entries ?? []
        }
        const union_paths = new Set<string>()
        const union: FileRow[] = []
        const use_row = (row: FileRow) => {
            if (!union_paths.has(row.name)) {
                union_paths.add(row.name)
                union.push(row)
            }
        }
        props.overlayEntries?.forEach?.(use_row)
        props.entries?.forEach?.(use_row)
        return union
    }, [props.entries, props.overlayEntries])

    // File popup for ops like download, move, delete, etc.
    const [filePopRow, setFilePopRow] = useState<FileRow | undefined>(undefined)
    const filePopXY = useRef<{ x: number, y: number }>({x: 0, y: 0})
    const filePopPos = useMemo<PositioningShorthand>(() => ({
        target: {
            getBoundingClientRect: () => ({
                ...filePopXY.current,
                top: filePopXY.current.y,
                left: filePopXY.current.x,
                bottom: 0,
                right: 0,
                width: 0,
                height: 0,
            })
        },
        position: 'below',
        align: 'start',
        arrowPadding: 4,
        offset: {
            mainAxis: 10,
        }
    }), [filePopXY])
    const [deletingRow, setDeletingRow] = useState<FileRow>()
    const [renamingRow, setRenamingRow] = useState<FileRow>()
    const filePopChildren = useMemo<ReactNode | undefined>(() => {
        if (filePopRow) {
            const row: FileRow = filePopRow
            console.debug({
                sdfg: props.onOverlayActionsRender,
                df89g: !!props.overlayEntries?.includes?.(row),
            })
            if (props.onOverlayActionsRender && props.overlayEntries?.includes?.(row)) {
                return props.onOverlayActionsRender?.(row)
            } else {
                return <MenuList>
                    {row.type === 'file' &&<MenuItem
                        icon={<DownloadIcon/>}
                        onClick={() => downloadFile(messageApi, props.path + '/' + row.name, props.fileFetcher, row.name).then()}
                    >
                        Download
                    </MenuItem>}
                    {/*<MenuItem*/}
                    {/*    icon={<MoveIcon/>}*/}
                    {/*    onClick={() => alert('TODO')}*/}
                    {/*>*/}
                    {/*    Move...*/}
                    {/*</MenuItem>*/}
                    {props.onFileRenameRequested && <MenuItem
                        icon={<RenameIcon/>}
                        onClick={() => setRenamingRow(row)}
                    >
                        Rename...
                    </MenuItem>}
                    {props.onFileDeleteRequested && <MenuItem
                        icon={<DeleteIcon/>}
                        onClick={() => setDeletingRow(row)}
                    >
                        Delete...
                    </MenuItem>}
                </MenuList>
            }
        }
    }, [filePopRow])

    const mouseClickHandler = useCallback<MouseEventHandler>(ev => {
        filePopXY.current.x = ev.clientX
        filePopXY.current.y = ev.clientY
    }, [])

    return <div onClick={mouseClickHandler} onContextMenu={mouseClickHandler}>
        {messageContextHolder}

        <Menu
            open={!!filePopRow && !!filePopChildren}
            closeOnScroll
            positioning={filePopPos}
            onOpenChange={(_, data) => {
                if (!data.open) {
                    setFilePopRow(undefined)
                }
            }}
        >
            <MenuPopover>
                <MenuArrow/>
                {filePopChildren}
            </MenuPopover>
        </Menu>

        <Modal
            open={!!deletingRow}
            footer={null}
            onCancel={() => setDeletingRow(undefined)}
        >
            <Stack rowGap="XL">
                <Text>
                    Are you sure to want to delete the following {deletingRow?.type}?
                </Text>
                <FilesList
                    //noTopBottomBorder
                    hideHeading
                    files={[deletingRow]}
                />
                <Stack horizontal justifyContent="end">
                    <Button
                        label="Cancel"
                        onClick={() => setDeletingRow(undefined)}
                    />
                    <AsyncActionButton
                        danger
                        icon={<DeleteIcon/>}
                        disabled={!deletingRow}
                        label={`Delete ${deletingRow?.type}`}
                        onClick={async () => {
                            await props.onFileDeleteRequested(deletingRow)
                            setDeletingRow(undefined)
                        }}
                    />
                </Stack>
            </Stack>
        </Modal>

        <Modal
            open={!!renamingRow}
            footer={null}
            onCancel={() => setRenamingRow(undefined)}
        >
            {renamingRow && <FileRenameForm
                initialName={renamingRow.name}
                onRenameRequested={async name => {
                    await props.onFileRenameRequested?.(renamingRow, name)
                    setRenamingRow(undefined)
                }}
            />}
        </Modal>

        <Stack rowGap="XXS">
            <Stack horizontal columnGap="None" alignItems="center">
                <Button
                    size="small"
                    disabled={isRoot}
                    appearance="subtle"
                    icon={<ArrowUpRegular title="Go back"/>}
                    onClick={() => {
                        props.onChDir?.(props.path.split('/').slice(0, -1).join('/'))
                    }}
                />
                <div style={{flexGrow: 1, border: `1px solid ${tokens.colorNeutralStroke2}`, overflowX: 'auto'}}>
                    <Stack horizontal columnGap="None" alignItems="center" grow>
                        <Button
                            size="small"
                            appearance="subtle"
                            noInlinePadding={isRoot}
                            icon={<FolderOpenRegular title="Go back to root"/>}
                            label={isRoot && props.rootLabel}
                            onClick={() => props.onChDir?.('')}
                        />
                        <Breadcrumb
                            items={props.path?.split?.('/')?.map?.((name, i, items) => <Button
                                size="small"
                                appearance="subtle"
                                label={name}
                                noInlinePadding
                                onClick={name ? () => {
                                    props.onChDir?.(items?.slice?.(0, i + 1)?.join?.('/') ?? '/')
                                } : undefined}
                            />)}
                            sep={<ChevronRightRegular/>}
                        />
                    </Stack>
                </div>
                <Button
                    size="small"
                    appearance="subtle"
                    disabled={props.isLoading || props.isRefetching}
                    //icon={<ArrowClockwiseRegular title="Refresh"/>}
                    onClick={() => props.refetch?.()}
                    noInlinePadding
                    label={<Text size={300}>
                        &nbsp;&nbsp;
                        <ArrowClockwiseFilled title="Refresh"/>
                        &nbsp;&nbsp;
                    </Text>}
                />
            </Stack>
            <MaybeErrorAlert error={props.error}/>
            {(props.isRefetching) ? <ProgressBar/> : <ProgressBar hide value={0}/>}
            <FilesList
                directoriesFirst
                noTopBottomBorder={props.noTopBottomBorder}
                //files={isRoot ? props.entries : [{name: '..', type: 'folder', date: new Date()}, ...(props.entries ?? [])]}
                files={rows ?? []}
                // TODO?: pass some new selectedFiles prop?
                onRowClick={row => {
                    if (row?.type === 'folder') {
                        props.onChDir?.([...props.path?.split?.('/') ?? [''], row.name].join('/'))
                    }
                    if (row?.type === 'file') {
                        setFilePopRow(row)
                    }
                }}
                onRowContextMenu={row => {
                    if (row?.type === 'file' || row?.type === 'folder') {
                        setFilePopRow(row)
                    }
                }}
            />
            {props.isLoading && <Spinner tall/>}
            {rows.length === 0 && !props.isLoading && <>
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No files here"/>
            </>}
        </Stack>
    </div>
}
