import * as React from 'react'
import {PropsWithChildren, ReactNode} from 'react'
import {Stack} from "../layouts/Stack";
import {PageSectionTitle} from "./PageSectionTitle";
import {InfoRegular} from "@fluentui/react-icons";
import {makeStyles, shorthands, Text, tokens} from "@fluentui/react-components";


const useStyles = makeStyles({
    root: {
        backgroundColor: tokens.colorBrandBackground2,
        color: tokens.colorBrandForeground2,
        ...shorthands.padding(tokens.spacingVerticalM),
        paddingTop: tokens.spacingVerticalS,
        // position: 'sticky',
        // top: '100px',
    }
})


export const InfoBox: React.FC<PropsWithChildren<{
    title?: ReactNode
}>> = props => {
    const styles = useStyles()
    return <>
        <div className={styles.root}>
            <Stack rowGap="XS">
                <PageSectionTitle icon={<Text size={400}><InfoRegular/></Text>} iconPosition="start">
                    {props.title ?? 'Info'}
                </PageSectionTitle>
                <div>
                    {props.children}
                </div>
            </Stack>
        </div>
    </>
}
