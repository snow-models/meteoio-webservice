import * as React from 'react'
import {Component, ErrorInfo, ReactNode} from "react";

interface Props {
    children?: ReactNode;
}

interface State {
    hasError: boolean;
}

export class ErrorBoundary extends Component<Props, State> {
    public state: State = {
        hasError: false
    };

    public static getDerivedStateFromError(_: Error): State {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error("Uncaught error:", error, errorInfo);
    }

    public render() {
        if (this.state.hasError) {
            return <>
                <div style={{
                    textAlign: 'center',
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    fontSize: '1.2rem',
                }}>
                    <span>Something went wrong...</span>
                    <span onClick={() => {this.setState({hasError: false})}} style={{
                        fontSize: '0.8rem',
                        cursor: 'pointer',
                        marginTop: '1em',
                        textDecoration: 'underline',
                    }}>Retry</span>
                </div>
            </>;
        }

        return this.props.children;
    }
}

