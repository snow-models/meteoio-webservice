import * as React from 'react'
import Markdown from "react-markdown";
import {memo} from "react";
import {Components} from "react-markdown/lib";
import {ExtLink} from "./ExtLink";

const COMPONENTS: Components = {
    p: ({ node, ...props }) => <p style={{marginTop: 0, marginBottom: 6}} {...props} />,
    em: ({ node, ...props }) => <em style={{ fontStyle: 'italic' }} {...props} />,
    strong: ({ node, ...props }) => <strong style={{ fontWeight: 'bold' }} {...props} />,
    // : ({ node, ...props }) => <strong style={{ fontWeight: 'bold' }} {...props} />,
    li: ({ node, ...props }) => <li style={{ marginBottom: '2px' }} {...props} />,
    ul: ({ node, ...props }) => <ul style={{ paddingLeft: '20px' }} {...props} />,
    a: ({ node, ...props }) => <ExtLink href={props.href} children={props.children}/>,
}

const ALLOWED_ELEMENTS: string[] = ['p', 'em', 'strong', 'ul', 'li', 'a']

export const SimpleFormattingMarkdownView: React.FC<{
    source: string
}> = memo(props => {
    return <div>
        <Markdown
            allowedElements={ALLOWED_ELEMENTS}
            components={COMPONENTS}
            children={props.source}
        />
    </div>
})
