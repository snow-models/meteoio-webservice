import * as React from 'react'
import {ButtonProps} from "./Button";
import {ORCIDiD_IconVector} from "./ORCIDiD_IconVector";
import {AsyncActionButton} from "./AsyncActionButton";

export const ORCIDiD_LoginButton: React.FC<Omit<ButtonProps & { onClick: () => Promise<void> }, 'icon' | 'iconPosition'>> = props => {
    const {appearance, ...rest} = props
    const appearance_ = appearance ?? 'secondary'
    return <>
        <AsyncActionButton
            appearance={appearance_}
            icon={<>
                <ORCIDiD_IconVector variant={appearance_ == 'primary' ? 'reversed' : undefined}/>
            </>}
            {...rest}
        />
    </>
}
