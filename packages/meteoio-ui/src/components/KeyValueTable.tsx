import * as React from 'react'
import {ReactNode} from 'react'
import {Input, makeStyles} from "@fluentui/react-components";
import {InputProps} from "@fluentui/react-input";
import {ControlLabel} from "./ControlLabel";
import {Button} from "./Button";
import {Stack} from "../layouts/Stack";
import {tokens} from "@fluentui/react-theme";


const useStyles = makeStyles({
    table: {
        borderCollapse: 'collapse'
    },
    row: {
        borderBottomWidth: 'thin',
        borderBottomStyle: 'solid',
        borderBottomColor: tokens.colorNeutralStroke2,
    },
    th: {
        paddingLeft: tokens.spacingHorizontalMNudge,
        paddingRight: tokens.spacingHorizontalMNudge,
        paddingBottom: tokens.spacingVerticalXS,
        textAlign: 'left',
    },
    cell: {
        paddingLeft: 0,
        paddingRight: 0,
        paddingTop: 0,
        paddingBottom: 0,
        textAlign: 'left',
    },
    input: {
        paddingLeft: tokens.spacingHorizontalS,
        paddingRight: tokens.spacingHorizontalS,
        marginBottom: '-1px',
    }
})


/** @deprecated */
export const KeyValueTable: React.FC<{
    keyLabel?: ReactNode
    valueLabel?: ReactNode
    value: Record<string, string>
    onChange?: (newMapping: Record<string, string>) => void
    inputProps?: Omit<InputProps, 'value' | 'onChange' | 'appearance'>
    onAddKey?: () => string
}> = props => {
    const styles = useStyles()

    // TODO?: make a list of properties, keep sorting unchanged in case of key changes.

    return <>
        <table className={styles.table}>
            <thead>
            <tr className={styles.row}>
                <th className={styles.th}>
                    <ControlLabel>{props.keyLabel ?? 'Key'}</ControlLabel>
                </th>
                <th className={styles.th}>
                    <ControlLabel>{props.valueLabel ?? 'Value'}</ControlLabel>
                </th>
            </tr>
            </thead>
            <tbody>
            {Object.entries(props.value ?? {}).map(([key, value]) => <tr key={key} className={styles.row}>
                <td className={styles.cell}>
                    <Stack>
                        <Input
                            {...props.inputProps}
                            appearance="underline"
                            className={styles.input}
                            value={key}  // FIXME: on change, the input is replaced => lost focus => unusable.
                            onChange={(ev, data) => {
                                const x = JSON.parse(JSON.stringify(props.value))
                                delete x[key]
                                x[data.value] = value
                                props.onChange(x)
                            }}
                        />
                    </Stack>
                </td>
                <td className={styles.cell}>
                    <Stack>
                        <Input
                            {...props.inputProps}
                            appearance="underline"
                            className={styles.input}
                            value={value}
                            onChange={(ev, data) => {
                                props.onChange({...props.value, [key]: data.value})
                            }}
                        />
                    </Stack>
                </td>
            </tr>)}
            </tbody>
            {props.onAddKey && props.onChange && <tbody>
            <tr>
                <td colSpan={2}>
                    <Button
                        label="+"
                        onClick={() => {
                            const key = props.onAddKey()
                            props.onChange({...props.value, [key]: ''})
                        }}
                    />
                </td>
            </tr>
            </tbody>}
        </table>
    </>
}
