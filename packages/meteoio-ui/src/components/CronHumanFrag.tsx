import * as React from 'react'
import {useCronStringDecoding} from "../hooks/useCronStringDecoding";

export const CronHumanFrag: React.FC<{
    cron?: string
}> = props => {
    const {humanString, error} = useCronStringDecoding(props.cron)
    return <>{humanString ?? (error ? 'Error' : '')}</>
}
