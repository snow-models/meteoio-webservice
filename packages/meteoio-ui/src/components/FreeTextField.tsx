import * as React from 'react'
import {KeyboardEventHandler, useCallback} from 'react'
import {Field, Input, makeStyles, mergeClasses, Textarea, tokens} from "@fluentui/react-components";
import {ControlLabel} from "./ControlLabel";
import {FieldProps} from "@fluentui/react-field";

const useStyles = makeStyles({
    input: {
        width: '100%'
    },
    smallCode: {
        '& > textarea, & > input': {
            fontSize: tokens.fontSizeBase100,
            lineHeight: tokens.lineHeightBase100,
            fontFamily: tokens.fontFamilyMonospace,
            wordBreak: 'break-all',
            whiteSpace: 'pre-wrap',
        }
    }
})

export const FreeTextField: React.FC<{
    label?: React.ReactNode
    value?: string
    onChange?: (value: string) => void
    multiline?: boolean
    fieldProps?: Omit<FieldProps, 'label'>
    readOnly?: boolean
    onReturn?: () => void
    inputRef?: React.Ref<HTMLInputElement & HTMLTextAreaElement>
    smallCode?: boolean
    maxLength?: number
}> = props => {

    const styles = useStyles()

    const keypressHandler = useCallback<KeyboardEventHandler>(props.onReturn ? ev => {
        if (ev.key === 'Enter') {
            props.onReturn?.()
        }
    } : undefined, [props.onReturn])

    return <Field
        label={<ControlLabel>{props.label}</ControlLabel>}
        {...props.fieldProps}
    >
        {/*<ControlLabel>{props.label}</ControlLabel><br/>*/}
        {props.multiline
            ? <Textarea
                ref={props.inputRef}
                readOnly={props.readOnly}
                className={mergeClasses(styles.input, props.smallCode && styles.smallCode)}
                value={props.value}
                onChange={(ev, data) => props.onChange?.(data.value)}
                onKeyPress={keypressHandler}
                spellCheck={props.smallCode ? 'false' : undefined}
                maxLength={props.maxLength}
                rows={5}
            />
            : <Input
                ref={props.inputRef}
                readOnly={props.readOnly}
                className={mergeClasses(styles.input, props.smallCode && styles.smallCode)}
                value={props.value}
                onChange={(ev, data) => props.onChange?.(data.value)}
                onKeyPress={keypressHandler}
                spellCheck={props.smallCode ? 'false' : undefined}
                maxLength={props.maxLength}
            />}
    </Field>
    // return <InputField
    //     label={props.label}
    //     value={props.value}
    //     onChange={props.onChange}
    // />
}
