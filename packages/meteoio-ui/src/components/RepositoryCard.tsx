import * as React from 'react'
import {Card, CardHeader, makeStyles, Text} from "@fluentui/react-components";
import {Stack} from "../layouts/Stack";
import {tokens} from "@fluentui/react-theme";
import {RepositoryAccess, RepositoryAccessPill} from "./RepositoryAccessPill";
import {RepositoryIcon} from "./RepositoryIcon";

const useStyles = makeStyles({
    head: {
        color: tokens.colorBrandForeground1,
        fontSize: tokens.fontSizeBase400,
    },
    desc: {
        color: tokens.colorNeutralForeground3,
        fontSize: tokens.fontSizeBase300,
    },
    meta: {
        color: tokens.colorNeutralForeground4,
        fontSize: tokens.fontSizeBase200,
    },
})

export const RepositoryCard: React.FC<{
    heading?: React.ReactNode
    access?: RepositoryAccess
    description?: string
    metadata?: React.ReactNode
    appearance?: 'filled' | 'filled-alternative' | 'outline' | 'subtle'
}> = props => {

    const styles = useStyles()

    return <article>
        <Card appearance={props.appearance}>
            <CardHeader
                header={<Stack horizontal alignItems="center" columnGap="S">
                    <Text size={500}>
                        <RepositoryIcon/>
                    </Text>
                    <Text weight="semibold" block className={styles.head}>
                        {props.heading}
                    </Text>
                    <span>&nbsp;</span>
                    {props.access && <RepositoryAccessPill access={props.access}/>}
                </Stack>}
                description={<Stack rowGap="None">
                    <Text className={styles.desc} block truncate>
                        {props.description}
                    </Text>

                    {props.metadata && <aside className={styles.meta}>
                        {props.metadata}
                    </aside>}
                </Stack>}
            />
        </Card>
    </article>
}
