import * as React from 'react'
import {Radio} from "antd";
// import type {ButtonProps} from "antd/es/button/button";
// import {useMemo} from "react";
// import {tokens} from "@fluentui/react-components";

//import {Button as AButton, Space} from "antd";


export interface ButtonRadioTogglesItem {
    value: string
    label: React.ReactNode
}

export const ButtonRadioToggles: React.FC<{
    value?: string
    options: ButtonRadioTogglesItem[]
    onChange: (value: string) => void
    // onBtnProps?: ButtonProps
    // offBtnProps?: ButtonProps
}> = props => {

    // const onBtnProps = useMemo<ButtonProps>(() => ({
    //     type: 'primary',
    //     disabled: true,
    //     style: {color: tokens.colorNeutralForeground1, fontWeight: tokens.fontWeightSemibold},
    //     ...(props.onBtnProps ?? {}),
    // }), [props.onBtnProps])
    //
    // const offBtnProps = useMemo<ButtonProps>(() => ({
    //     ...(props.offBtnProps ?? {}),
    // }), [props.offBtnProps])

    return <>
        <Radio.Group
            options={props.options}
            onChange={ev => props.onChange?.(ev.target?.value)}
            value={props.value}
            optionType="button"
        />
        {/*<Space.Compact>*/}
        {/*    {props.options?.map?.(opt => <AButton {...(props.value ? onBtnProps : offBtnProps)}>*/}
        {/*        {opt.label ?? opt.value}*/}
        {/*    </AButton>)}*/}
        {/*</Space.Compact>*/}
    </>
}
