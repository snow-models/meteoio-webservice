import * as React from 'react'
import {PropsWithChildren} from 'react'
import {makeStyles, Text} from "@fluentui/react-components";

const useStyles = makeStyles({
    text: {
        marginBottom: 0,
        marginTop: 0,
    }
})

export const PageTitle: React.FC<PropsWithChildren<{}>> = props => {

    const styles = useStyles()

    return <Text as="h1" size={500} weight="semibold" className={styles.text}>
        {props.children}
    </Text>
}
