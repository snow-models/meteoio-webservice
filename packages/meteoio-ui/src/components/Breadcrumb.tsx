import * as React from 'react'
import {Stack} from "../layouts/Stack";

export const Breadcrumb: React.FC<{
    items: React.ReactNode[]
    sep: React.ReactNode
}> = props => {

    return <Stack horizontal columnGap="XS" alignItems="center">
        {props.items.map((it, i) => <React.Fragment key={i}>
            {i > 0 && props.sep}
            {it}
        </React.Fragment>)}
    </Stack>
}
