import * as React from 'react'
import {Alert} from "./Alert";


export type MaybeError = Error | string | {
    message?: string
    stack?: { payload?: string, detail?: string }
} | {
    status?: number | string
    payload?: string | {
        status_code?: number | string;
        detail?: string;
        extra?: void;
    };
}

export const getMaybeErrorMessageString = (err?: MaybeError) => {
    if (!err) {
        return ''
    }
    // if (typeof err === 'object' && 'payload' in err && typeof err?.payload === 'object' && 'status_code' in err?.payload && err?.payload?.status_code === 401) {
    //     return 'Unauthorized'
    // }
    const msg_opt = [
        ...(typeof err === 'string' ? [err] : []),
        ...(err instanceof Error ? [err.message] : [
            ...(typeof err === 'object' ? [
                ...('message' in err ? [err.message] : []),
                ...('stack' in err ? [
                    ...(typeof err.stack === 'object' ? [
                        ...(err.stack.payload ? [err.stack.payload] : []),
                        ...(err.stack.detail ? [err.stack.detail] : []),
                    ] : []),
                ] : []),
                ...('payload' in err ? [
                    ...(typeof err.payload === 'string' ? [err.payload] : []),
                    ...(typeof err.payload === 'object' ? [
                        ...(err.payload?.detail ? [err.payload?.detail] : []),
                    ] : []),
                ] : []),
            ] : []),
        ]),
    ]
    return msg_opt.length > 0 ? msg_opt.join(', ') : `Unknown error`
}

export const MaybeErrorAlert: React.FC<{
    error?: MaybeError
}> = props => {

    if (props.error) {
        return <Alert level="error">
            {getMaybeErrorMessageString(props.error)}
        </Alert>
    } else {
        return null
    }
}
