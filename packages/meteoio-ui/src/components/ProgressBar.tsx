import * as React from 'react'
import {makeStyles, ProgressBar as FProgressBar, Text} from '@fluentui/react-components'
import {Stack} from "../layouts/Stack";
import {ControlLabel} from "./ControlLabel";
import {tokens} from "@fluentui/react-theme";

const useStyles = makeStyles({
    desc: {
        color: tokens.colorNeutralForeground4,
        fontSize: tokens.fontSizeBase200,
    },
})

export const ProgressBar: React.FC<{
    value?: number
    max?: number
    thickness?: "medium" | "large"
    label?: React.ReactNode
    description?: React.ReactNode
    hide?: boolean
}> = props => {

    const styles = useStyles()

    return <>
        <Stack rowGap="None">
            {props.label && <ControlLabel>
                {props.label}
            </ControlLabel>}

            <FProgressBar style={props.hide ? {opacity: 0} : undefined} {...props}/>

            {props.description && <Text className={styles.desc}>
                {props.description}
            </Text>}
        </Stack>
    </>
}
