import * as React from 'react'
import {useContext} from 'react'
import {FieldsSizingHintContext} from "../providers/FieldsSizingHint";
import {makeStyles, Text, tokens} from "@fluentui/react-components";
import {useScreenSize} from "../hooks/useScreenSize";

const useStyles = makeStyles({
    root_h: {
        display: 'grid',
        columnGap: tokens.spacingHorizontalM,
        gridTemplateColumns: '1fr 1fr'
    },
    root_v: {
        display: 'grid',
        columnGap: tokens.spacingHorizontalM,
        gridTemplateColumns: '1fr'
    },
    description: {
        color: tokens.colorNeutralForeground3,
    }
})

export const FieldDescription: React.FC<{
    field: React.ReactNode
    description: React.ReactNode
}> = props => {
    const {isMobile} = useScreenSize()
    const styles = useStyles()
    const sh = useContext(FieldsSizingHintContext)
    const twoCols = !(isMobile || sh.fieldWidth === undefined)

    return <div
        className={isMobile ? styles.root_v : styles.root_h}
        style={twoCols
            ? {gridTemplateColumns: `${sh.fieldWidth}px minmax(${sh.fieldWidth}px, 1fr)`}
            : {gridTemplateColumns: `1fr`}
        }
    >
        <div>
            {props.field}
        </div>
        <div className={styles.description}>
            {twoCols && <br />}
            <Text size={100}>
                {props.description}
            </Text>
        </div>
    </div>
}
