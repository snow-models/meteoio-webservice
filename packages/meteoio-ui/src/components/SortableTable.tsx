import * as React from 'react'
import {MouseEventHandler, useCallback, useMemo, useRef, useState} from 'react'
import {makeStyles, Menu, MenuPopover, mergeClasses, shorthands} from "@fluentui/react-components";
import {tokens} from "@fluentui/react-theme";
import {ControlLabel} from "./ControlLabel";
import {PositioningShorthand} from "@fluentui/react-positioning";
import {MenuArrow} from "./MenuArrow";

const useStyles = makeStyles({
    table: {
        width: '100%',
        borderCollapse: 'collapse',

        '& > * > tr': {
            ...shorthands.borderBottom('thin', 'solid', tokens.colorNeutralStroke2),
            // borderBottomWidth: 'thin',
            // borderBottomStyle: 'solid',
            // borderBottomColor: tokens.colorNeutralStroke2,
        },
        '& > tbody > tr': {
            ...shorthands.borderTop('thin', 'solid', tokens.colorNeutralStroke2),
        },
        '& > tbody > tr:hover': {
            backgroundColor: tokens.colorNeutralBackground1Hover,
        }
    },

    noTopBottomBorder: {
        '& > tbody > tr': {
            ...shorthands.borderTop('0', 'none'),
        },
        '& > tbody > tr:last-child': {
            ...shorthands.borderBottom('0', 'none'),
        },
    },

    sortIcon: {
        color: tokens.colorNeutralForeground3
    },

    'extra-small': {
        fontSize: '0.9em',

        '& > * > tr > td': {
            paddingLeft: tokens.spacingHorizontalXS,
            paddingRight: tokens.spacingHorizontalXS,
            paddingBottom: tokens.spacingVerticalXXS,
            paddingTop: tokens.spacingVerticalXXS,
        },
    },
    'small': {
        '& > * > tr > td': {
            paddingLeft: tokens.spacingHorizontalS,
            paddingRight: tokens.spacingHorizontalS,
            paddingBottom: tokens.spacingVerticalXS,
            paddingTop: tokens.spacingVerticalXS,
        },
    },
    'medium': {
        '& > * > tr > td': {
            paddingLeft: tokens.spacingHorizontalM,
            paddingRight: tokens.spacingHorizontalM,
            paddingBottom: tokens.spacingVerticalS,
            paddingTop: tokens.spacingVerticalS,
        },
    },
})

export interface Column<RowT extends object = Record<string, unknown>> {
    field?: string & keyof RowT
    title?: string | React.ReactNode
    onRenderCellContent?: (row: RowT, i: number) => React.ReactNode
    sortCast?: 'number'
    textAlign?: 'start' | 'end'
}

export function SortableTable<RowT extends object = Record<string, unknown>>(props: React.PropsWithRef<{
    columns: Column<RowT>[]
    rows: RowT[]
    style?: React.CSSProperties
    size?: 'extra-small' | 'small' | 'medium'
    hideHeading?: boolean
    onRowClick?: (row: RowT) => void
    onRowContextMenu?: (row: RowT) => void
    noTopBottomBorder?: boolean
    missingPlaceholder?: React.ReactNode
    majorSortKeyFn?: (row: RowT) => number  // use this to put directories before files, for example
    defaultSortKey?: keyof RowT & string
    rowKeyField?: keyof RowT
    showMenuOnRowClick?: boolean
    showMenuOnRowContextMenu?: boolean
    onRenderMenuChildren?: (row: RowT) => React.ReactNode /// NOTE: reacts on row changes, does not react on changes of this prop.
}>) {
    const styles = useStyles()

    const [sortKey, setSortKey] = useState<(keyof RowT & string) | undefined>(props.defaultSortKey)
    const [sortAsc, setSortAsc] = useState<boolean>(false)

    const rowsSorted = useMemo(() => {
        if (sortKey === undefined && props.majorSortKeyFn === undefined) {
            return props.rows
        }
        const rr = props.rows.slice()

        const __col = props.columns?.find?.(col => col.field === sortKey)
        const sortCaster = __col?.sortCast === 'number' ? x => {
            if (x === undefined) {
                return
            }
            try {
                return parseFloat(x + '')
            } catch (e) {
                console.trace(e)
            }
        } : x => x;

        rr.sort((a, b) => {
            if (props.majorSortKeyFn) {
                const ma = props.majorSortKeyFn(a)
                const mb = props.majorSortKeyFn(b)
                if (ma > mb) {
                    return 1
                }
                if (ma < mb) {
                    return -1
                }
            }
            if (sortKey === undefined) {
                return 0
            }
            const va = sortCaster(a?.[sortKey])
            const vb = sortCaster(b?.[sortKey])
            const ua = a?.[sortKey] === undefined || a?.[sortKey] === null
            const ub = b?.[sortKey] === undefined || b?.[sortKey] === null
            if (ua && ub) {
                return 0
            }
            if (ua) {
                return 1
            }
            if (ub) {
                return -1
            }
            if (typeof va === 'string' && typeof vb === 'string' && va.localeCompare) {
                return va.localeCompare(vb, undefined, {numeric: true}) * (sortAsc ? -1 : 1)
            }
            if (va > vb) {
                return sortAsc ? 1 : -1
            }
            if (va < vb) {
                return sortAsc ? -1 : 1
            }
            return 0
        })

        return rr
    }, [props.columns, props.rows, sortAsc, sortKey])

    const rootMouseClickXY = useRef<{ x: number, y: number }>({x: 0, y: 0})
    const rootMouseClickHandler = useCallback<MouseEventHandler>(ev => {
        rootMouseClickXY.current.x = ev.clientX
        rootMouseClickXY.current.y = ev.clientY
    }, [])
    const menuPositioning = useMemo<PositioningShorthand>(() => ({
        target: {
            getBoundingClientRect: () => ({
                ...rootMouseClickXY.current,
                top: rootMouseClickXY.current.y,
                left: rootMouseClickXY.current.x,
                bottom: 0,
                right: 0,
                width: 0,
                height: 0,
            })
        },
        position: 'below',
        align: 'start',
        arrowPadding: 4,
        offset: {
            mainAxis: 10,
        }
    }), [])
    const [menuRow, setMenuRow] = useState<RowT | undefined>()
    const menuChildren = useMemo(() => props.onRenderMenuChildren?.(menuRow), [menuRow])

    return <>
        {props.onRenderMenuChildren && <Menu
            open={!!menuRow && !!menuChildren}
            closeOnScroll
            positioning={menuPositioning}
            onOpenChange={(_, data) => {
                if (!data.open) {
                    setMenuRow(undefined)
                }
            }}
        >
            <MenuPopover>
                <MenuArrow/>
                {menuChildren}
            </MenuPopover>
        </Menu>}
        <table style={props.style}
               onClick={rootMouseClickHandler} onContextMenu={rootMouseClickHandler}
               className={mergeClasses(styles.table, styles[props.size ?? 'medium'], props.noTopBottomBorder && styles.noTopBottomBorder)}>
            {props.hideHeading !== true && <thead>
            <tr>
                {props.columns.map((col, i) => <th
                    key={`c${i}-${col.field}`}
                    style={{
                        cursor: col.field === undefined ? 'not-allowed' : 'pointer',
                        textAlign: col.textAlign ?? 'start',
                    }}
                    onClick={col.field === undefined ? undefined : () => {
                        if (col.field === sortKey) {
                            setSortAsc(v => !v)
                        } else {
                            setSortKey(col.field)
                            setSortAsc(false)
                        }
                    }}
                >
                    <ControlLabel>
                        &nbsp;{col.title ?? col.field ?? ''}&nbsp;
                    </ControlLabel>
                    {col.field && <span className={styles.sortIcon}>
                        &nbsp;
                        {sortKey === col.field && (sortAsc ? <>&darr;</> : <>&uarr;</>)}
                    </span>}
                </th>)}
            </tr>
            </thead>}
            <tbody>
            {rowsSorted.map((row, i) => <tr
                key={row?.[props.rowKeyField]?.toString?.() ?? `${sortKey}-${sortAsc ? 'a' : 'd'}-${i}`}>
                {props.columns.map((col, j) =>
                    <td
                        key={col.field ?? `__col_${j}`}
                        style={{
                            textAlign: col.textAlign ?? 'start',
                            cursor: props.onRowClick || props.showMenuOnRowClick ? 'pointer' : undefined,
                            backgroundColor: row === menuRow ? tokens.colorNeutralBackground3Hover : undefined,
                        }}
                        onClick={() => {
                            if (props.showMenuOnRowClick) {
                                setMenuRow(row)
                            }
                            props.onRowClick?.(row)
                        }}
                        onContextMenu={props.onRowContextMenu || props.showMenuOnRowContextMenu ? (ev) => {
                            if (props.showMenuOnRowContextMenu) {
                                setMenuRow(row)
                            }
                            props.onRowContextMenu?.(row)
                            ev.preventDefault()
                            return false
                        } : undefined}
                    >
                        {col.onRenderCellContent === undefined
                            ? row?.[col.field]?.toString?.() ?? (props.missingPlaceholder ?? '###')
                            : col.onRenderCellContent(row, i)}
                    </td>)}
            </tr>)}
            </tbody>
        </table>
    </>
}
