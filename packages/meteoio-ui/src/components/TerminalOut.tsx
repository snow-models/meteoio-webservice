import * as React from "react";
import {useEffect, useRef, useState} from "react";
import {makeStyles} from "@fluentui/react-components";
import {Terminal} from 'xterm';
import {FitAddon} from 'xterm-addon-fit';
import 'xterm/css/xterm.css';


const useStyles = makeStyles({
    root: {
        overflowX: 'hidden',
        overflowY: 'hidden',
    }
})


export const TerminalOut: React.FC<{
    text: string
}> = props => {
    const styles = useStyles()
    const rootRef = useRef<HTMLDivElement>()
    const [term, setTerm] = useState<{term: Terminal, fitAddon: FitAddon} | undefined>()

    useEffect(() => {
        if (rootRef?.current) {
            const term = new Terminal({
                convertEol: true,
                disableStdin: true,
                cursorBlink: false,
                cursorStyle: 'underline',
            });
            const fitAddon = new FitAddon();
            term.loadAddon(fitAddon);
            term.open(rootRef.current)
            term.attachCustomKeyEventHandler(() => false)
            setTerm({term, fitAddon})

            const resize = () => {fitAddon.fit()}
            resize()
            window?.addEventListener?.('resize', resize)

            return () => {
                window?.removeEventListener?.('resize', resize)
                setTerm(undefined)
                term.dispose()
            }
        }
    }, [rootRef?.current])

    useEffect(() => {
        if (term) {
            term.term.clear()
            term.term.write(props.text)
            term.term.scrollToBottom()
        }
    }, [term, props.text])

    return <>
        <div className={styles.root} ref={rootRef}>
        </div>
    </>
}
