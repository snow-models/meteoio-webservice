import * as React from 'react'
import {tokens} from "@fluentui/react-components";

export const MenuArrow: React.FC = () => {
    return <div style={{
        position: "relative",
        width: 10,
        height: 0,
    }}>
        <div style={{
            position: "relative",
            left: 10,
            top: -11,
            width: 10,
            height: 10,
            zIndex: -1,
            transform: 'rotate(45deg)',
            //boxShadow: '0 0 5px 5px #000',
            backgroundColor: tokens.colorNeutralBackground1,
            borderTop: `1px solid ${tokens.colorNeutralStroke2}`,
            borderLeft: `1px solid ${tokens.colorNeutralStroke2}`,
        }}>
            &nbsp;
        </div>
    </div>
}
