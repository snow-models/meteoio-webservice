import * as React from 'react'
import {PropsWithChildren, useEffect, useRef, useState} from 'react'

export const ScreenHeightLimitedContainer: React.FC<PropsWithChildren<{
    freeSpaceBottom?: number
}>> = props => {

    const [offsetTop, setOffsetTop] = useState<number>(0)

    const rootRef = useRef<HTMLDivElement>()

    useEffect(() => {
        const resizeHandler = () => {
            setOffsetTop(rootRef?.current?.offsetTop ?? 0)
        }
        resizeHandler()
        window?.addEventListener('resize', resizeHandler)
        return () => {
            window?.removeEventListener('resize', resizeHandler)
        }
    }, [rootRef?.current])

    return <>
        <div
            ref={rootRef}
            style={{
                height: `calc(100vh - ${offsetTop}px - ${props.freeSpaceBottom ?? 20}px)`,
                overflowY: 'scroll',
                width: '100%'
            }}
        >
            {props.children}
        </div>
    </>
}
