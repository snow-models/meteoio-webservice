import * as React from 'react'
import {ReactNode, useCallback, useMemo, useState} from 'react'
import {Tree, TreeDataNode} from 'antd'
import {FileRow} from "./FilesList";
import {EventDataNode} from "rc-tree/lib/interface";


const updateTreeData = (list: TreeDataNode[], key: React.Key, children: TreeDataNode[]): TreeDataNode[] =>
    list.map((node) => {
        if (node.key === key) {
            return {
                ...node,
                children,
            };
        }
        if (node.children) {
            return {
                ...node,
                children: updateTreeData(node.children, key, children),
            };
        }
        return node;
    });


export const FoldersTree: React.FC<{
    rootLabel?: ReactNode
    listCallback?: (path: string) => Promise<FileRow[]>
    onNodeClick?: (node: TreeDataNode) => void
    currentPath?: string
}> = props => {

    const root = useMemo<TreeDataNode>(() => ({
        key: '',
        children: null,
        title: props.rootLabel,
    }), [])

    const [treeData, setTreeData] = useState<TreeDataNode[]>(() => [root])

    const onLoadData = useCallback(async (treeNode: EventDataNode<TreeDataNode>) => {
        // if (treeNode.children) {
        //     return;
        // }
        const path: string = treeNode.key as string
        const entries = await props.listCallback?.(path) ?? []
        setTreeData((origin) =>
            updateTreeData(origin, treeNode.key, entries.map(row => ({
                key: `${path}/${row.name}`,
                title: row.name,
                isLeaf: row.type !== 'folder',
                style: {whiteSpace: 'nowrap'},
            }))),
        );
    }, []);

    return <>
        <Tree.DirectoryTree
            rootStyle={{overflowX:'hidden'}}
            switcherIcon={false}
            autoExpandParent
            selectedKeys={[]}
            expandedKeys={path2parents(props.currentPath)}
            // expandedKeys={props.currentPath? [props.currentPath] : undefined}
            // defaultExpandedKeys={props.currentPath? [props.currentPath] : undefined}
            // showLine={true}//{showLeafIcon: true}}
            treeData={treeData}
            loadData={onLoadData}
            onClick={(e, node) => {
                props.onNodeClick?.(node)
                onLoadData(node).then()
            }}
            // onExpand={(expandedKeys, info) => {
            //     onLoadData(info.node).then()
            // }}
        />
    </>
}

const path2parents: (path: string) => string[] = path => {
    const _path = path.startsWith('/') ? path : `/${path}`
    const parts = _path.split('/')
    return parts.map((p, index) => '' + parts.slice(0, index + 1).join('/'))
}
