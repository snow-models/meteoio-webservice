import * as React from 'react'
import {
    createTableColumn,
    DataGrid,
    DataGridBody,
    DataGridCell,
    DataGridHeader,
    DataGridHeaderCell,
    DataGridRow,
    makeStyles,
    TableCellLayout,
    useFluent,
    useScrollbarWidth
} from "@fluentui/react-components";

import {DocumentRegular, FolderRegular,} from "@fluentui/react-icons";
import {relativeTimeFormat} from "../utils/relativeTimeFormat";
import {TableColumnSizingOptions} from "@fluentui/react-table";
import {ControlLabel} from "./ControlLabel";
import {tokens} from "@fluentui/react-theme";

export interface FileRow {
    name?: string
    date?: Date
    type?: 'file' | 'folder'
}

const useStyles = makeStyles({
    dateCell: {
        color: tokens.colorNeutralForeground4,
        textAlign: 'right',
        float: 'right',
        display: 'block',
    },
})

const columns = [
    createTableColumn<FileRow>({
        columnId: 'name',
        compare(a, b) {
            return a.name > b.name ? 1 : (a.name < b.name ? -1 : 0)
        },
        renderHeaderCell: () => <ControlLabel>File</ControlLabel>,
        renderCell: (item) =>
            <TableCellLayout
                media={item.type === 'file'
                    ? <DocumentRegular/> : (item.type === 'folder'
                        ? <FolderRegular/> : undefined)}
            >
                {item.name}
            </TableCellLayout>
    }),
    createTableColumn<FileRow>({
        columnId: 'date',
        compare(a, b) {
            return a.date?.getTime?.() > b.date?.getTime?.() ? 1 : (a.date?.getTime?.() < b.date?.getTime?.() ? -1 : 0)
        },
        renderHeaderCell: () => <ControlLabel>Date</ControlLabel>,
        renderCell: (item) => <DateCell item={item}/>,
    }),
]


export const DateCell: React.FC<{
    item: FileRow
}> = props => {
    const styles = useStyles()
    return <TableCellLayout className={styles.dateCell}>
        {props.item.date && <time
            dateTime={props.item.date.toISOString?.()}
            title={props.item.date.toString?.()}
        >
            {relativeTimeFormat(props.item.date)}
        </time>}
    </TableCellLayout>
}


const columnSizingOptions: TableColumnSizingOptions = {
    name: {
        idealWidth: 100,
    },
    date: {
        defaultWidth: 150,
        minWidth: 120,
    },
}

export const OverengineeredFilesList: React.FC<{
    files?: FileRow[]
    hideDates?: boolean
    selectionMode?: 'single' | 'multiselect'
    onSelectionChange?: (selectedItems: Set<string>) => void
    selectedItems?: Iterable<string>
    subtleSelection?: boolean
    size?: 'extra-small' | 'small' | 'medium'
    height?: number
    hideHeading?: boolean
}> = props => {
    const {targetDocument} = useFluent();
    const scrollbarWidth = useScrollbarWidth({targetDocument});

    return <div style={props.height ? {marginRight: scrollbarWidth} : undefined}>
        <DataGrid
            sortable
            getRowId={(item) => item.name}
            columns={props.hideDates ? [columns[0]] : columns}
            items={props.files ?? []}
            focusMode={"cell"}
            selectionAppearance={"neutral"}
            subtleSelection={props.subtleSelection}
            size={props.size ?? 'small'}
            selectionMode={props.selectionMode}
            selectedItems={props.selectedItems}
            onSelectionChange={props.onSelectionChange ? (e, data) => {
                props.onSelectionChange(data.selectedItems as Set<string>) // NOTE: getRowId gives string.
            } : undefined}
            columnSizingOptions={columnSizingOptions}
            resizableColumns
        >
            {props.hideHeading !== true &&
            <DataGridHeader style={props.height ? {paddingRight: scrollbarWidth} : undefined}>
                <DataGridRow selectionCell={props.selectionMode ? {"aria-label": "Select all files"} : undefined}>
                    {({renderHeaderCell, columnId}) => (
                        <DataGridHeaderCell
                            style={columnId === 'date' ? {flexDirection: 'row-reverse'} : undefined}>{renderHeaderCell()}</DataGridHeaderCell>
                    )}
                </DataGridRow>
            </DataGridHeader>}
            <DataGridBody<FileRow>
                style={props.height ? {height: props.height, overflowY: 'scroll', overflowX: 'hidden'} : undefined}>
                {({item, rowId}, style) => (
                    <DataGridRow<FileRow>
                        key={rowId}
                        selectionCell={props.selectionMode ? {"aria-label": "Select file"} : undefined}
                        style={style}
                    >
                        {({renderCell, columnId}) => (
                            <DataGridCell
                                align={columnId === 'date' ? 'right' : undefined}>{renderCell(item)}</DataGridCell>
                        )}
                    </DataGridRow>
                )}
            </DataGridBody>
        </DataGrid>
    </div>
}
