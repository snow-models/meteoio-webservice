import * as React from 'react'
import {DatePicker as FDatePicker, IDatePicker} from '@fluentui/react-datepicker-compat'
import {makeStyles} from "@fluentui/react-components";
import {formatDate} from "../utils/formatDate";

// NOTE: this module is mostly for compatibility fixes of react-datepicker-compat.

const useStyles = makeStyles({
    root: {
        '& > .fui-DatePicker': {
            width: '100%',
        },
        '& .fui-DatePicker__popupSurface': {
            zIndex: 2,
        },
    },
})

export const DatePicker: React.FC<{
    value?: Date
    onChange?: (date: Date) => void
    componentRef?: React.RefObject<IDatePicker>
    disabled?: boolean
}> = props => {
    const styles = useStyles()
    return <div className={styles.root}>
        <FDatePicker
            disabled={props.disabled}
            allowTextInput
            componentRef={props.componentRef}
            formatDate={formatDate}
            value={props.value ?? null}
            onSelectDate={props.onChange}
            inlinePopup  // NOTE: we don't like this, but the portal created by react-datepicker-compat is not styled.
        />
        {/* NOTE: the popupSurface is created here as sibling: this is why we need the div. */}
    </div>
}
