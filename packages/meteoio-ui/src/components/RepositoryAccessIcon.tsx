import * as React from "react";
import {GlobeRegular, LockClosedRegular} from "@fluentui/react-icons";
import {RepositoryAccess} from "./RepositoryAccessPill";

export const RepositoryAccessIcon: React.FC<{
    access?: RepositoryAccess
}> = props => {
    return <>
        {props.access === 'private' && <LockClosedRegular/>}
        {props.access === 'public' && <GlobeRegular/>}
    </>
}
