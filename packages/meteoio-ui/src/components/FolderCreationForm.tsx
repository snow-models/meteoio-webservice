import * as React from 'react'
import {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import {FreeTextField} from "./FreeTextField";
import {Stack} from "../layouts/Stack";
import {AsyncActionButton} from "./AsyncActionButton";

export const FolderCreationForm: React.FC<{
    label?: React.ReactNode
    onCreateRequested?: (name: string) => Promise<void>
    // onValidate?: (name: string) => boolean
}> = props => {

    const [name, setName] = useState<string>('')

    const isValid = useMemo<boolean>(() => {
        //props.onValidate?.(name) ?? true
        if (name === '' || name === '.' || name === '..') {
            return false
        }
        if (name.startsWith(' ')) {
            return false
        }
        if (name.includes('/') || name.includes('?') || name.includes('#')) {
            return false
        }
        // ok
        return true
    }, [name])

    const [pending, setPending] = useState<boolean>(false)

    const canSubmit = name && isValid && !pending

    const submit = useCallback(async () => {
        if (!canSubmit) {
            return
        }
        try {
            setPending(true)
            await props.onCreateRequested?.(name.trim())
            setName('')
            setPending(false)
        } catch (e) {
            setPending(false)
            throw e
        }
    }, [name, canSubmit])

    const inputRef = useRef<HTMLInputElement & HTMLTextAreaElement>()
    useEffect(() => {
        setInterval(() => {
            // NOTE: this assumes FolderCreationForm is only rendered in an open modal and not when closed.
            inputRef?.current?.focus?.()
        }, 100)
    }, [])

    return <>
        <FreeTextField
            inputRef={inputRef}
            readOnly={pending}
            label={props.label ?? <>Folder name</>}
            value={name}
            onChange={setName}
            fieldProps={{
                validationState: name ? (isValid ? 'none' : 'error') : 'none',
                validationMessage: name ? (isValid ? undefined : 'Invalid name') : undefined,
            }}
            onReturn={submit}
        />
        <br/>
        <Stack horizontal justifyContent="end">
            <AsyncActionButton
                appearance="primary"
                isAdditionallyPending={pending}
                //icon={<FolderAddRegular/>}
                label="Confirm"
                disabled={!canSubmit}
                onClick={submit}
            />
        </Stack>
    </>
}
