import * as React from 'react'
import {useMemo} from 'react'
import {Breadcrumb} from "./Breadcrumb";
import {Handle} from "./Handle";
import {makeStyles, Text, tokens} from "@fluentui/react-components";
import {TextProps} from "@fluentui/react-text";

const useStyles = makeStyles({
    sep: {
        color: tokens.colorNeutralForeground3,
    }
})

export const HandleBreadcrumb: React.FC<{
    handle: string
    textProps?: TextProps
}> = props => {
    const styles = useStyles()

    const path = useMemo<string[]>(() => props.handle.split('/'), [props.handle])

    return <Breadcrumb
        items={path.map((h, i) =>
            <Handle textProps={props.textProps} handle={path.slice(0, i + 1).join('/')} label={h}/>)}
        sep={<Text {...props.textProps} className={styles.sep}>/</Text>}
    />
}
