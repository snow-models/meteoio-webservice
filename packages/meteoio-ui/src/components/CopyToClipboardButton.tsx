import * as React from 'react'
import {useEffect, useState} from 'react'
import {Button, ButtonProps} from './Button'
import {bundleIcon, CheckmarkRegular, CopyFilled, CopyRegular} from '@fluentui/react-icons'
import {copyTextToClipboard} from "../utils/copyTextToClipboard";
import {tokens} from "@fluentui/react-components";

const CopyIcon = bundleIcon(CopyFilled, CopyRegular)

export const CopyToClipboardButton: React.FC<{
    data: string
    buttonProps?: ButtonProps
}> = props => {

    const [copied, setCopied] = useState<boolean>(false)
    useEffect(() => {
        setCopied(false)
    }, [props.data])

    return <>
        <Button
            appearance={copied ? 'subtle' : 'secondary'}
            size="small"
            noInlinePadding
            icon={copied ? <CheckmarkRegular color={tokens.colorPaletteGreenForeground1}/> :
                <CopyIcon fontSize="0.8em"/>}
            onClick={async () => {
                await copyTextToClipboard(props.data)
                setCopied(true)
                setTimeout(() => {
                    setCopied(false)
                }, 3000)
            }}
            {...props.buttonProps}
        />
    </>
}
