import * as React from 'react'
import {makeStyles, tokens} from "@fluentui/react-components";


const useStyles = makeStyles({
    root: {
        // ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke2),
        // ...shorthands.borderRadius(tokens.borderRadiusXLarge),
        //marginTop: tokens.spacingVerticalXL,
        //marginBottom: tokens.spacingVerticalXXXL,
        marginLeft: tokens.spacingHorizontalL,
        marginRight: tokens.spacingHorizontalL,
        // ...shorthands.padding(tokens.spacingVerticalS, 0),
    },
})

/** Makes a box that has the same spacings of NavLinksList  */
export const NavRelatedBox: React.FC<React.PropsWithChildren<{
    verticalMargin: number
}>> = props => {
    const styles = useStyles()

    return <>
        <div
            className={styles.root}
            style={{marginTop: props.verticalMargin, marginBottom: props.verticalMargin}}
        >
            {props.children}
        </div>
    </>
}
