import * as React from 'react'
import {PageSectionTitle} from "./PageSectionTitle";
import {HandleBreadcrumb} from "./HandleBreadcrumb";
import {RowFiller, Stack} from "../layouts/Stack";
import {RepositoryAccess, RepositoryAccessPill} from "./RepositoryAccessPill";
import {tokens} from "@fluentui/react-components";
import {RepositoryIcon} from "./RepositoryIcon";

export const RepositoryHeader: React.FC<{
    access?: RepositoryAccess
    handle: string
    buttons?: React.ReactNode
}> = props => {

    // NOTE: tabs may be implemented in HeaderMainAsideLayout.

    return <Stack>
        <Stack horizontal>
            <PageSectionTitle>
                <Stack horizontal alignItems="center">
                    <RepositoryIcon fontSize={tokens.fontSizeBase600}/>
                    <HandleBreadcrumb handle={props.handle} textProps={{size: 500}}/>
                    {props.access && <RepositoryAccessPill access={props.access}/>}
                </Stack>
            </PageSectionTitle>
            <RowFiller/>
            <Stack horizontal>
                {props.buttons}

            </Stack>
        </Stack>
    </Stack>
}
