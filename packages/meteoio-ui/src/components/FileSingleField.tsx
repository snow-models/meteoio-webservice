import * as React from 'react'
import {DragEventHandler, useCallback, useMemo, useRef, useState} from 'react'
import {Field, FieldProps} from "@fluentui/react-field";
import {ControlLabel} from "./ControlLabel";
import {mergeClasses, Text} from "@fluentui/react-components";
import {DocumentRegular} from "@fluentui/react-icons";
import {Stack} from "../layouts/Stack";
import {FilesInDOMList} from "./FilesInDOMList";
import {useStyles} from "./FilesField";


/** Allow selection of 1 file. FilesField is derived from this for multiple files */
export const FileSingleField: React.FC<{
    label?: React.ReactNode
    value?: File
    onChange?: (file: File) => void
    fieldProps?: Omit<FieldProps, 'label'>
}> = props => {
    const [value_, setValue_] = useState<File | undefined>()  // NOTE: this is for testing purpose
    const file = props.value ?? value_
    const onChange = props.onChange ?? setValue_

    const inputRef = useRef<HTMLInputElement>();

    // file selection click
    const handleInputChange = useCallback(() => {
        const f = inputRef?.current?.files?.item?.(0) ?? undefined
        if (f?.name) {
            onChange?.(f)
        } else {
            onChange?.(undefined)
        }
    }, [inputRef, onChange])

    // drag and drop
    const [isDragging, setIsDragging] = useState<boolean>(false)
    const handleDragging = useCallback<DragEventHandler>(e => {
        e.stopPropagation()
        e.preventDefault()
        setIsDragging(true)
    }, [])
    const handleDrop = useCallback<DragEventHandler>(e => {
        e.stopPropagation()
        e.preventDefault()
        onChange?.(e?.dataTransfer?.files?.item?.(0))
        setIsDragging(false)
    }, [onChange])

    const styles = useStyles()

    const files = useMemo<File[] | undefined>(() => file ? [file] : undefined, [file])

    return <>
        <Field
            label={<ControlLabel>{props.label}</ControlLabel>}
            {...props.fieldProps}
        >
            <label
                className={mergeClasses(styles.area, isDragging && styles.areaWhileDragging)}
                onDrop={handleDrop}
                onDragEnter={handleDragging}
                onDragOver={handleDragging}
                onDragLeave={() => setIsDragging(false)}
                onDragEnd={() => setIsDragging(false)}
            >
                {file === undefined
                    ? <Stack horizontal alignItems="center" columnGap="S">
                        <DocumentRegular/>
                        <Text className={styles.placeholderText}>
                            Select a file...
                        </Text>
                    </Stack>
                    : <FilesInDOMList files={files}/>}

                <input
                    style={{display: 'none'}}
                    type="file"
                    ref={inputRef}
                    onChange={handleInputChange}
                />
            </label>
        </Field>
    </>
}
