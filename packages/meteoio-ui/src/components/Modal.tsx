import * as React from 'react'
import {Modal as AModal, ModalProps} from 'antd'
import {ThemeProvider} from "../providers/ThemeProvider";
import {bundleIcon, DismissFilled, DismissRegular} from "@fluentui/react-icons";

const CloseIcon = bundleIcon(DismissFilled, DismissRegular)

export const Modal: React.FC<ModalProps> = props => {
    const {children, ...rest} = props
    return <>
        <AModal closeIcon={<CloseIcon fontSize={22}/>} {...rest}>
            <ThemeProvider>
                {children}
            </ThemeProvider>
        </AModal>
    </>
}
