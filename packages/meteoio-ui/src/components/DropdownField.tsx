import * as React from 'react'
import {useMemo} from 'react'
import {Dropdown, Label, makeStyles, Option} from "@fluentui/react-components";
import {ControlLabel} from "./ControlLabel";
import {Spinner} from "./Spinner";

const useStyles = makeStyles({
    input: {
        width: '100%'
    }
})

export type DropdownFieldOption = {
    value: string
} & (
    {
        label: string
        text?: string
    } | {
        label: React.ReactNode
        text: string
    }
)

export const DropdownField: React.FC<{
    label?: string
    value?: string
    options: DropdownFieldOption[]
    onChange?: (value: string) => void
    loading?: boolean
}> = props => {

    const styles = useStyles()

    const sel = useMemo(() => props.value ? [props.value] : [], [props.value])
    const opt = useMemo(() =>
        props.options?.find?.(opt => opt.value === props.value), [props.options, props.value])

    return <Label>
        <ControlLabel>{props.label}</ControlLabel><br/>
        <Dropdown
            className={styles.input}
            value={(opt?.text ?? opt?.label ?? props.value) as string}
            selectedOptions={sel}
            onOptionSelect={props.onChange ? (event, data) => props.onChange(data.optionValue) : undefined}
            expandIcon={props.loading ? <Spinner size="tiny"/> : undefined}
        >
            {props.options.map((opt, i) =>
                <Option key={`${i}-${opt.value}`} value={opt.value} text={opt.text ?? (typeof opt.label === 'string' ? opt.label : opt.value)}>
                    {opt.label}
                </Option>)}
        </Dropdown>
    </Label>
}

export const DropdownMultiselectField: React.FC<{
    label?: string
    value?: string[]
    options: DropdownFieldOption[]
    onChange?: (value: string[]) => void
}> = props => {

    const styles = useStyles()

    return <Label>
        <ControlLabel>{props.label}</ControlLabel><br/>
        <Dropdown
            multiselect
            className={styles.input}
            selectedOptions={props.value ?? []}
            onOptionSelect={props.onChange ? (event, data) => props.onChange(data.selectedOptions) : undefined}
        >
            {props.options.map((opt, i) =>
                <Option key={`${i}-${opt.value}`} value={opt.value} text={opt.text ?? (typeof opt.label === 'string' ? opt.label : opt.value)}>
                    {opt.label}
                </Option>)}
        </Dropdown>
    </Label>
}
