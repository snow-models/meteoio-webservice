import * as React from 'react'
import {useMemo, useState} from 'react'
import {makeStyles, mergeClasses, Text, tokens} from "@fluentui/react-components";
import {DeleteFilled, DocumentRegular} from "@fluentui/react-icons";
import {fileSizeFormat} from "../utils/fileSizeFormat";
import {Button} from "./Button";

const useStyles = makeStyles({
    grid: {
        display: 'grid',
        alignItems: 'center',
        gridTemplateColumns: 'auto 1fr auto',
        columnGap: tokens.spacingHorizontalS,
        rowGap: tokens.spacingVerticalXS,
    },
    gridWithPointer: {
        '& > *': {
            cursor: 'pointer'
        }
    },
    fileIcon: {
        color: tokens.colorBrandForeground2,
    },
    fileNameText: {},
    fileSizeText: {
        color: tokens.colorNeutralForegroundDisabled,
        fontSize: 'small',
        textAlign: 'right'
    },
    removeIcon: {
        color: tokens.colorPaletteRedForeground1,
    }
})

const fileKeyMap: WeakMap<File, string> = new WeakMap<File, string>()

export const FilesInDOMList: React.FC<{
    files?: File[]
    onRemoveRequested?: (file: File) => void
}> = props => {
    const styles = useStyles()
    useMemo(() => {
        // NOTE: populating a weak-key map to associate ids to each File object
        props.files?.forEach?.(f => {
            if (!fileKeyMap.has(f)) {
                fileKeyMap.set(f, Math.random().toString())
            }
        })
    }, [props.files])

    return <>
        <div className={mergeClasses(styles.grid, props.onRemoveRequested && styles.gridWithPointer)}>
            {props.files?.map?.((file, i) => <FileFrag
                key={fileKeyMap.get(file) ?? i}
                file={file}
                onRemoveRequested={props.onRemoveRequested ? () => props.onRemoveRequested(file) : undefined}
            />)}
        </div>
    </>
}


export const FileFrag: React.FC<{
    file: File
    onRemoveRequested?: () => void
}> = props => {
    const styles = useStyles()
    const file = props.file
    const [isDeleting, setIsDeleting] = useState<boolean>(false)

    const preDelHandler = props.onRemoveRequested ? () => setIsDeleting(v => !v) : undefined

    return <>
        {isDeleting
            ? <Button
                noInlinePadding
                size="small"
                appearance="subtle"
                onClick={props.onRemoveRequested}
                label={<DeleteFilled className={styles.removeIcon}/>}
            />
            : <DocumentRegular
                className={styles.fileIcon} onClick={preDelHandler}/>}

        <Text
            className={styles.fileNameText}
            wrap={false}
            block truncate
            onClick={preDelHandler}
        >
            {file?.name}
        </Text>

        <Text
            className={styles.fileSizeText}
            wrap={false}
            block
            onClick={preDelHandler}
        >
            {file?.size !== undefined && fileSizeFormat(file.size)}
        </Text>
    </>
}
