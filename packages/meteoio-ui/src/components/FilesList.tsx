import * as React from 'react'
import {Column, SortableTable} from "./SortableTable";
import {RelativeTimeText} from "./RelativeTimeText";
import {DocumentBulletListArrowLeftRegular, DocumentRegular, FolderFilled, FolderRegular} from "@fluentui/react-icons";
import {Stack} from "../layouts/Stack";
import {makeStyles, Text} from "@fluentui/react-components";
import {tokens} from "@fluentui/react-theme";
import {fileSizeFormat} from "../utils/fileSizeFormat";
import {FluentIconsProps} from "@fluentui/react-icons/lib/utils/FluentIconsProps.types";

export interface FileRow {
    name?: string
    date?: Date | string
    type?: 'file' | 'folder'
    size?: number
    isLink?: boolean
}

const useStyles = makeStyles({
    date: {
        color: tokens.colorNeutralForeground4,
    },
    size: {
        color: tokens.colorNeutralForeground4,
    }
})

const DateLabel: React.FC<{
    row: FileRow
}> = props => {
    const styles = useStyles()
    return <Text className={styles.date}>
        <RelativeTimeText date={props.row.date}/>
    </Text>
}

export const SizeLabel: React.FC<{
    row: FileRow
}> = props => {
    const styles = useStyles()
    return <Text className={styles.size}>
        {fileSizeFormat(props.row.size)}
    </Text>
}

const FILE_ICON_PROPS: FluentIconsProps = {
    fontSize: tokens.fontSizeBase500,
    opacity: 0.66,
}

export const FileNameLabel: React.FC<{
    row: FileRow
}> = props => <Stack horizontal alignItems="center" columnGap="S">
    {props.row.type === 'folder'
        ? (props.row.size === 0 ? <FolderRegular {...FILE_ICON_PROPS}/> : <FolderFilled {...FILE_ICON_PROPS}/>)
        : (props.row.isLink ? <DocumentBulletListArrowLeftRegular {...FILE_ICON_PROPS}/> :
            <DocumentRegular {...FILE_ICON_PROPS}/>)}
    <Text block truncate wrap={false}
          style={{overflow: 'hidden', maxWidth: 'calc(98vw - 120px)'}}>{props.row.name}</Text>
</Stack>

const columns: Column<FileRow>[] = [
    {
        field: 'name',
        title: 'Name',
        onRenderCellContent: row => <FileNameLabel row={row}/>
    },
    {
        field: 'date',
        title: 'Date',
        textAlign: 'end',
        onRenderCellContent: row => <DateLabel row={row}/>,
    },
    {
        field: 'size',
        title: 'Size',
        textAlign: 'end',
        sortCast: 'number',
        onRenderCellContent: row => <SizeLabel row={row}/>
    },
]

export const FilesList: React.FC<{
    directoriesFirst?: boolean
    files?: FileRow[]
    size?: 'extra-small' | 'small' | 'medium'
    hideHeading?: boolean
    onRowClick?: (row: FileRow) => void
    onRowContextMenu?: (row: FileRow) => void
    noTopBottomBorder?: boolean
}> = props => {
    return <>
        <SortableTable
            defaultSortKey='name'
            majorSortKeyFn={props.directoriesFirst ? f => f.type === 'folder' ? -1 : 1 : undefined}
            noTopBottomBorder={props.noTopBottomBorder}
            hideHeading={props.hideHeading}
            size={props.size}
            columns={columns}
            rows={props.files ?? []}
            onRowClick={props.onRowClick}
            onRowContextMenu={props.onRowContextMenu}
        />
    </>
}
