import * as React from 'react'
import {MouseEventHandler, useCallback, useState} from 'react'
import {Button, ButtonProps} from "./Button";
import {Spinner} from "./Spinner";
import {Modal} from 'antd'
import {getMaybeErrorMessageString} from "./MaybeErrorAlert";

export const AsyncActionButton: React.FC<ButtonProps & {
    onClick: (ev: MouseEvent) => Promise<void>,
    isAdditionallyPending?: boolean
}> = props => {
    const {onClick, icon, disabled, ...rest} = props
    const [_pending, setPending] = useState<boolean>(false)
    const pending = _pending || props.isAdditionallyPending
    const [modal, contextHolder] = Modal.useModal();

    const wrappedOnClick = useCallback<MouseEventHandler>(async (ev) => {
        setPending(true)
        try {
            await onClick(ev)
        } catch (e) {
            console.trace(e)
            modal.error({
                title: props.label,
                content: getMaybeErrorMessageString(e),
            })
        } finally {
            setPending(false)
        }
    }, [onClick])

    return <>
        <Button
            {...rest}
            icon={pending ? <Spinner size="tiny"/> : icon}
            disabled={disabled || pending}
            onClick={wrappedOnClick}
        />
        {contextHolder}
    </>
}
