import * as React from 'react'
import '../niviz/niviz-bundle-latest'
import {NiVizSmet} from "../niviz/NiVizSmet";
import {ErrorBoundary} from "./ErrorBoundary";


export const NiViz: React.FC<{
    // filename: string
    data: string
}> = props => {
    return <ErrorBoundary>
        <NiVizSmet data={props.data}/>
    </ErrorBoundary>
}