import * as React from 'react'
import {ControlLabel} from "./ControlLabel";
import {Stack} from "../layouts/Stack";
import {makeStyles, shorthands, Text, tokens} from "@fluentui/react-components";

const useStyles = makeStyles({
    text: {
        ...shorthands.border('thin', 'solid', tokens.colorNeutralStroke2),
        ...shorthands.borderRadius(tokens.borderRadiusMedium),
        fontSize: tokens.fontSizeBase100,
        lineHeight: tokens.lineHeightBase100,
        color: tokens.colorNeutralForeground3,
        ...shorthands.padding('0.2em', '0.9em'),
        wordBreak: 'break-all',
        whiteSpace: 'pre-wrap',
    }
})

export const FreeCodeDisplayLikeField: React.FC<{
    label?: React.ReactNode
    value: string
}> = props => {
    const styles = useStyles()
    return <>
        <Stack rowGap="None">
            {props.label && <ControlLabel>{props.label}</ControlLabel>}
            <Text block font="monospace" wrap className={styles.text}>
                {props.value}
            </Text>
        </Stack>
    </>
}
