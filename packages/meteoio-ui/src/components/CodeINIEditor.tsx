import React, {CSSProperties, useEffect, useRef} from "react";
import {basicSetup, EditorView} from "codemirror"
import {keymap} from "@codemirror/view"
import {EditorState} from "@codemirror/state";
import {HighlightStyle, StreamLanguage, StreamParser, syntaxHighlighting} from "@codemirror/language"
import {indentLess, insertTab} from "@codemirror/commands"
import {tags} from '@lezer/highlight';
import {tokens} from "@fluentui/react-components";

const propertiesParser: StreamParser<{
    inValue: boolean,
    inMultiline: boolean,    // Is the current line a multiline value
}> = {
    // Inspired from:
    //      @codemirror/legacy-modes/mode/properties
    //      @codemirror/legacy-modes/mode/python

    name: "meteoio-ini",

    token: function (stream, state) {
        if (state.inValue) {
            if(stream.match(/^\\\s*$/, true)) {
                state.inMultiline = true
                stream.eatSpace()
                return "escape"
            }
            if (stream.match(/^[#;]/, true)) {
                stream.skipToEnd();
                state.inMultiline = false
                state.inValue = false
                return "comment";
            }
            if (!state.inMultiline) {
                // Handle Number Literals
                if (stream.match(/^([+\-])?\d+(\.\d*)?(e([+\-])?\d+)?\s*([#;]+.*)?$/i, false)) {
                    // NOTE: above check ensures there's nothing relevant after the number, allowing comments
                    stream.match(/^([+\-])?\d+(\.\d*)?(e([+\-])?\d+)?\s*/i, true)
                    state.inMultiline = false
                    state.inValue = false
                    return "number"
                }
            }
            stream.match(/[^#;\\]+/i, true)  // NOTE: previous matches must guarantee this consumes the stream.
            if (stream.eol()) {
                state.inMultiline = false
                state.inValue = false
            }
            else{
                // next iteration will consume in-value comment or multiline escape char
            }
            return "quote"
        }
        else {
            if (stream.match(/^[#;]/, true)) {
                stream.skipToEnd();
                return "comment";
            }
            if (stream.match(/^\[[^\]]+]\s*([#;].*)?$/, false)) {
                stream.match(/^\[[^\]]+]\s*/, true)
                return "header";
            }
            if (stream.match(/^[^=#;\\]+\s*=\s*\S+/, false)) {
                stream.match(/^[^=#;\\]+\s*/, true)
                return "def"
            }
            if (stream.match('=', true)) {
                stream.eatSpace()
                if(!stream.eol()) {
                    state.inValue = true
                }
                return "operator"
            }
        }
        stream.next()
        stream.eatWhile(/[^=#;]/)
        return "invalid";
    },

    startState: function () {
        return {
            inValue: false,
            inMultiline: false,
        };
    }

};

const themedSyntaxHighlight = syntaxHighlighting(HighlightStyle.define([
    {tag: tags.heading, color: tokens.colorNeutralForeground1, fontWeight: 'bold'},
    {tag: tags.comment, color: tokens.colorNeutralForegroundDisabled},
    {tag: tags.name, color: tokens.colorBrandForeground2}, // first word of keys
    {tag: tags.operator, color: tokens.colorBrandForegroundLinkPressed}, // = between key and value
    {tag: tags.quote, color: tokens.colorNeutralForeground3},  // all values
    {tag: tags.number, color: tokens.colorStatusSuccessForeground1, fontWeight: 'bold'},
    {tag: tags.escape, color: tokens.colorPaletteLilacBorderActive, fontWeight: 'bold'},
    {tag: tags.invalid, color: tokens.colorStatusWarningForeground1},
]))

export const CodeINIEditor: React.FC<{
    readOnly?: boolean
    initialValue: string
    onChange?: (value: string) => void
    maxHeight?: CSSProperties['maxHeight']
    outlined?: boolean
}> = ({initialValue, onChange, readOnly, maxHeight, outlined}) => {
    const editorRef = useRef<HTMLDivElement>(null);
    const viewRef = useRef<EditorView>(null);

    useEffect(() => {
        if (!editorRef.current) return;

        const state = EditorState.create({
            doc: initialValue,
            extensions: [
                basicSetup,
                EditorView.baseTheme({
                    '&': {
                        maxHeight,
                        outline: outlined ? `1px solid ${tokens.colorNeutralStroke2}` : '0 none',
                    },
                    '&.cm-focused': {
                        // outline: `1px solid ${tokens.colorBrandStroke1}`,
                        outline: outlined ? `1px solid ${tokens.colorBrandStroke2Hover}` : '0 none',
                    },
                    '& .cm-gutters': {
                        backgroundColor: tokens.colorNeutralBackground1,
                        borderRight: `1px dashed ${tokens.colorNeutralStroke2}`,
                        color: tokens.colorNeutralForegroundDisabled,
                    },
                    '& .cm-gutterElement': {
                        fontSize: '0.8em',
                        // lineHeight: '1.1rem',
                    },
                    '& .cm-activeLine, & .cm-activeLineGutter': {
                        backgroundColor: '#70707009',
                    },
                    // '&.cm-focused .cm-selectionLayer .cm-selectionBackground': {  // '&.cm-focused > .cm-scroller > .cm-selectionLayer .cm-selectionBackground': {
                    //    backgroundColor: tokens.colorBrandBackgroundSelected,  // or --colorBrandBackground4Static // both are too saturated. The default is good.
                    // },
                }),
                themedSyntaxHighlight,
                ...(readOnly ? [
                    EditorState.readOnly.of(true),
                    EditorView.editable.of(false),
                ] : []),
                EditorView.lineWrapping,
                keymap.of([
                    {key: "Tab", run: insertTab, shift: indentLess},
                ]),
                StreamLanguage.define(propertiesParser),
                // keymap.of([...foldKeymap, { key: "Mod-z", run: undo }, { key: "Mod-y", run: redo }]),
                EditorView.updateListener.of((update) => {
                    if (update.docChanged) {
                        const newValue = update.state.doc.toString();
                        onChange?.(newValue);
                    }
                }),
            ],
        });

        viewRef.current = new EditorView({
            state,
            parent: editorRef.current,
        });

        return () => {
            if (viewRef.current) {
                viewRef.current.destroy();
            }
        };
    }, [onChange, readOnly, maxHeight, outlined]);

    return <div ref={editorRef}></div>;
};

export default CodeINIEditor;
