import * as React from 'react'
import {PropsWithChildren} from 'react'
import {makeStyles, mergeClasses, shorthands, Text, tokens} from "@fluentui/react-components";
import {Stack} from "../layouts/Stack";
import {CheckmarkCircleRegular, ErrorCircleRegular, InfoRegular, TriangleRegular,} from '@fluentui/react-icons'

const useStyles = makeStyles({
    root: {
        ...shorthands.padding(tokens.spacingVerticalS, tokens.spacingHorizontalM),
        ...shorthands.borderRadius(tokens.borderRadiusNone),
        color: tokens.colorNeutralForeground1,
        '& > .icon': {
            fontSize: '1.4em'
        }
    },
    root_small: {
        ...shorthands.padding(tokens.spacingVerticalXS, tokens.spacingHorizontalM),
    },
    bordered: {
        ...shorthands.borderStyle('solid'),
        ...shorthands.borderWidth('thin'),
    },
    low_info: {
        // backgroundColor: tokens.colorNeutralBackground4,
        // boxShadow: `inset 0 950px 400px -1000px ${tokens.colorNeutralBackground2}`,
        // ...shorthands.borderColor(tokens.colorNeutralStroke2),
        color: tokens.colorNeutralForeground4,
        '& > .icon': {color: tokens.colorNeutralForeground4},
    },
    info: {
        backgroundColor: tokens.colorNeutralBackground4,
        boxShadow: `inset 0 950px 400px -1000px ${tokens.colorNeutralBackground2}`,
        ...shorthands.borderColor(tokens.colorNeutralStroke2),
        '& > .icon': {color: tokens.colorNeutralForeground3},
    },
    success: {
        backgroundColor: tokens.colorPaletteGreenBackground1,
        boxShadow: `inset 0 950px 400px -1000px ${tokens.colorPaletteGreenBackground2}`,
        ...shorthands.borderColor(tokens.colorPaletteGreenBorder1),
        '& > .icon': {color: tokens.colorPaletteGreenForeground2},
    },
    warning: {
        backgroundColor: tokens.colorPaletteYellowBackground2,
        //boxShadow: `inset 0 950px 400px -1000px ${tokens.colorPaletteYellowBackground2}`,
        ...shorthands.borderColor(tokens.colorPaletteYellowBorder2),
        '& > .icon': {color: tokens.colorPaletteYellowForeground2},
    },
    error: {
        backgroundColor: tokens.colorPaletteRedBackground1,
        boxShadow: `inset 0 950px 400px -1000px ${tokens.colorPaletteRedBackground2}`,
        ...shorthands.borderColor(tokens.colorPaletteRedBorder1),
        '& > .icon': {color: tokens.colorPaletteRedForeground2},
    },
    severe: {
        backgroundColor: tokens.colorPaletteDarkOrangeBackground1,
        boxShadow: `inset 0 950px 400px -1000px ${tokens.colorPaletteDarkOrangeBackground2}`,
        ...shorthands.borderColor(tokens.colorPaletteDarkOrangeBorder1),
        '& > .icon': {color: tokens.colorPaletteDarkOrangeForeground2},
    }
})

export const Alert: React.FC<PropsWithChildren<{
    level?: 'low_info' | 'info' | 'success' | 'warning' | 'error' | 'severe'
    bordered?: boolean
    end?: React.ReactNode
    size?: 'small'
}>> = props => {

    const styles = useStyles()

    return <Stack
        horizontal
        className={mergeClasses(styles.root, styles[props.level], props.bordered && styles.bordered, props.size === 'small' && styles.root_small)}
        columnGap="M"
        alignItems="flex-start"
    >
        <Text className="icon" size={props.size === 'small' ? 200 : 300}>
            {props.level === 'low_info' && <InfoRegular/>}
            {props.level === 'info' && <InfoRegular/>}
            {props.level === 'success' && <CheckmarkCircleRegular/>}
            {props.level === 'warning' && <InfoRegular/>}
            {props.level === 'error' && <ErrorCircleRegular/>}
            {props.level === 'severe' && <TriangleRegular/>}
        </Text>
        <Stack
            horizontal
            columnGap="M"
            alignItems="flex-start"
            justifyContent="space-between"
            wrap
            grow
        >
            <Text block truncate wrap size={props.size === 'small' ? 200 : 300}>
                {props.children}
            </Text>
            {props.end}
        </Stack>
    </Stack>
}
