import * as React from 'react'
import {useMemo} from 'react'
import {bundleIcon, MoreHorizontalFilled, MoreHorizontalRegular,} from "@fluentui/react-icons";
import {
    Button,
    makeStyles,
    Menu,
    MenuItem,
    MenuList,
    MenuPopover,
    MenuTrigger,
    Overflow,
    OverflowItem,
    Tab,
    TabList,
    TabProps,
    tokens,
    useIsOverflowItemVisible,
    useOverflowMenu
} from "@fluentui/react-components";

// Adapted from https://react.fluentui.dev/?path=/docs/components-tablist--default#with-overflow

const MoreHorizontal = bundleIcon(MoreHorizontalFilled, MoreHorizontalRegular);

export type ITab = TabProps & { value: string }

/**
 * A menu item for an overflow menu that only displays when the tab is not visible
 */
const OverflowMenuItem: React.FC<{
    tab: ITab
    onClick: React.MouseEventHandler<HTMLDivElement>
}> = props => {
    const {tab, onClick} = props;
    const isVisible = useIsOverflowItemVisible(tab.id);

    if (isVisible) {
        return null;
    }

    return <MenuItem key={tab.value} icon={tab.icon} onClick={onClick}>
        <div>{tab.children}</div>
    </MenuItem>;
};


const useOverflowMenuStyles = makeStyles({
    menu: {
        backgroundColor: tokens.colorNeutralBackground1,
    },
    menuButton: {
        alignSelf: "center",
    },
});

/**
 * A menu for selecting tabs that have overflowed and are not visible.
 */
const OverflowMenu: React.FC<{
    tabs: ITab[]
    onTabSelect?: (tabValue: string) => void
}> = props => {

    const {ref, isOverflowing, overflowCount} =
        useOverflowMenu<HTMLButtonElement>();

    const hasIcons = useMemo<boolean>(() => props.tabs?.some?.(tab => !!tab.icon), [props.tabs])

    const styles = useOverflowMenuStyles();

    if (!isOverflowing) {
        return null;
    }

    return (
        <Menu hasIcons={hasIcons}>
            <MenuTrigger disableButtonEnhancement>
                <Button
                    appearance="transparent"
                    className={styles.menuButton}
                    ref={ref}
                    icon={<MoreHorizontal/>}
                    aria-label={`${overflowCount} more tabs`}
                    role="tab"
                />
            </MenuTrigger>
            <MenuPopover>
                <MenuList className={styles.menu}>
                    {props.tabs.map((tab) => (
                        <OverflowMenuItem
                            key={tab.value}
                            tab={tab}
                            onClick={() => props.onTabSelect?.(tab.value)}
                        />
                    ))}
                </MenuList>
            </MenuPopover>
        </Menu>
    );
};

export const TabListWithOverflow: React.FC<{
    selectedTabValue?: string
    onTabSelect?: (tabValue: string) => void
    tabs: ITab[]
}> = props => {
    return <>
        <Overflow minimumVisible={2}>
            <TabList
                selectedValue={props.selectedTabValue}
                onTabSelect={(_, d) => props.onTabSelect(d.value?.toString?.())}
            >
                {props.tabs.map((tab) => {
                    return (
                        <OverflowItem
                            key={tab.value}
                            id={tab.value}
                            priority={tab.value === props.selectedTabValue ? 2 : 1}
                        >
                            <Tab {...tab}>
                                {tab.children}
                            </Tab>
                        </OverflowItem>
                    );
                })}
                <OverflowMenu tabs={props.tabs} onTabSelect={props.onTabSelect}/>
            </TabList>
        </Overflow>
    </>
}
