import * as React from 'react'
import {useState} from 'react'
import {Cron, CronError, PeriodType} from "react-js-cron";
import {Input, makeStyles, Text} from "@fluentui/react-components";
import 'react-js-cron/dist/styles.css'
import {MaybeErrorAlert} from "./MaybeErrorAlert";
import {Stack} from "../layouts/Stack";
import {ControlLabel} from "./ControlLabel";
import {Button} from "./Button";
import {useCronStringDecoding} from "../hooks/useCronStringDecoding";
import {useDelayedValue} from "../hooks/useDelayedValue";


const useStyles = makeStyles({
    root: {
        minWidth: '200px',
    },
    codeInput: {
        fontFamily: 'monospace'
    }
})

const cronAllowedPeriods: PeriodType[] = ['year', 'month', 'week', 'day', 'hour', 'minute']

export const CronEditor: React.FC<{
    label?: React.ReactNode
    readOnly?: boolean
    value?: string
    onChange?: (value: string) => void
}> = props => {
    const [isEditingCode, setIsEditingCode] = useState<boolean>(false)
    const [compCronError, setCompCronError] = useState<CronError | undefined>()
    const styles = useStyles()
    const _delayed_cron_string = useDelayedValue(props.value, 400)
    const cronDecoding = useCronStringDecoding(_delayed_cron_string)

    return <>
        <Stack rowGap={isEditingCode ? 'XS' : 'None'} className={styles.root}>
            <Stack horizontal alignItems="baseline" justifyContent="space-between">
                <ControlLabel>{props.label}</ControlLabel>
                <span>
                    <Button
                        disabled={!isEditingCode}
                        size="small"
                        appearance="subtle"
                        label={<>&nbsp;easy&nbsp;</>}
                        noInlinePadding
                        onClick={() => setIsEditingCode(false)}
                    />
                    &nbsp;
                    <Button
                        disabled={isEditingCode}
                        size="small"
                        appearance="subtle"
                        label={<>&nbsp;advanced&nbsp;</>}
                        noInlinePadding
                        onClick={() => {
                            setIsEditingCode(true)
                            setCompCronError(undefined)
                        }}
                    />
                </span>
            </Stack>
            {isEditingCode && <Stack rowGap="None">
                <Input
                        readOnly={props.readOnly}
                    className={styles.codeInput}
                    value={props.value}
                    onChange={(ev, data) => props.onChange(data.value)}
                />
                <Text size={200} font="monospace">&nbsp;&nbsp;m&nbsp;h&nbsp;d&nbsp;mon&nbsp;w</Text>
            </Stack>}
            {!isEditingCode && <Cron
                readOnly={props.readOnly}
                displayError
                shortcuts={false}
                mode="multiple"
                clockFormat="12-hour-clock"
                allowedPeriods={cronAllowedPeriods}
                value={props.value ?? '0 0 * * *'}
                setValue={props.onChange}
                onError={setCompCronError}
                clearButtonProps={{
                    type: 'default',
                    size: 'small',
                }}
            />}
            <MaybeErrorAlert error={cronDecoding?.error ?? compCronError?.description ?? compCronError?.type}/>
        </Stack>
    </>
}