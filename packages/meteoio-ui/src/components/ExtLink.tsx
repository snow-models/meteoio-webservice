import * as React from 'react'
import {HTMLAttributeReferrerPolicy, PropsWithChildren} from "react";
import {OpenRegular} from "@fluentui/react-icons";
import {tokens} from "@fluentui/react-components";

export const ExtLink: React.FC<PropsWithChildren<{
    href?: string
    referrerPolicy?: HTMLAttributeReferrerPolicy
}>> = props => {
    return <a href={props.href} target="_blank" referrerPolicy={props.referrerPolicy}>
        {props.children} <OpenRegular style={{color: tokens.colorBrandForegroundLink, opacity: 0.75}}/>
    </a>
}
