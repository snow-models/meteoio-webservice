import * as React from 'react'
import {makeStyles} from "@fluentui/react-components";

const useStyles = makeStyles({
    root: {
        position: 'sticky'
    }
})

export const Sticky: React.FC<React.PropsWithChildren<{
    top?: number
}>> = props => {
    const styles = useStyles()
    return <div className={styles.root} style={{top: props.top ?? 0}}>
        {props.children}
    </div>
}
