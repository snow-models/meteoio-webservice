import {useMemo} from "react";
import cronstrue from "cronstrue";

export function useCronStringDecoding(cron: string) {
    return useMemo<{ humanString?: string, error?: Error }>(() => {
        if (!cron) {
            return {}
        }
        try {
            return {humanString: cronstrue.toString(cron)}
        } catch (error) {
            return {error}
        }
    }, [cron])
}
