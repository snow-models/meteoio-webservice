import * as React from 'react'
import {useCallback} from 'react'
import {App} from 'antd'
import {ModalFuncProps} from "antd/es/modal/interface";
import {ThemeProvider} from "../providers/ThemeProvider";
import {bundleIcon, DismissFilled, DismissRegular} from "@fluentui/react-icons";
import {useScreenSize} from "./useScreenSize";

const CloseIcon = bundleIcon(DismissFilled, DismissRegular)

/** NOTE: you need ThemeProvider */
export function useDialogs() {
    const {modal, message} = App.useApp()
    const {isMobile} = useScreenSize()

    const confirm = useCallback<(question: string, props?: Omit<ModalFuncProps, 'content' | 'icon' | 'title'>) => Promise<boolean>>((question, props) => {
        return new Promise<boolean>(resolve => {
            modal.confirm({
                autoFocusButton: 'cancel',
                centered: true,
                type: 'confirm',
                icon: null,
                title: question,
                onOk() {
                    resolve(true)
                },
                onCancel() {
                    resolve(false)
                },
                ...(props ?? {}),
            })
        })
    }, [modal])

    const popup = useCallback<(content: React.ReactNode) => { destroy: () => void }>(content => {
        const width = isMobile ? 250 : 400
        return modal.info({
            // TODO?: dark theme has the wrong background color here
            content: <ThemeProvider>
                <div style={{width}}>{content}</div>
            </ThemeProvider>,
            modalRender(node) {
                return <ThemeProvider>{node}</ThemeProvider>
            },
            centered: true,
            closable: true,
            footer: null,
            maskClosable: true,
            closeIcon: <CloseIcon fontSize={22}/>,
            icon: null,
            width: width + 24 * 2,
        })
    }, [modal, isMobile])

    return {
        confirm,
        popup,
        message, // NOTE: the duration parameter is in seconds (default: 3s).
    }
}
