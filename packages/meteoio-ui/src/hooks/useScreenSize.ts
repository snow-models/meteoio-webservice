import {useCallback, useEffect, useState} from "react";

const IS_MOBILE_FUNC = () => window.innerWidth < 768
const IS_LARGE_FUNC = () => window.innerWidth > 1100

export const useScreenSize = () => {
    const [isMobile, setIsMobile] = useState<boolean>(IS_MOBILE_FUNC);
    const [isLarge, setIsLarge] = useState<boolean>(IS_LARGE_FUNC);

    const handleResize = useCallback(() => {
        setIsMobile(IS_MOBILE_FUNC);
        setIsLarge(IS_LARGE_FUNC);
    }, [])

    useEffect(() => {
        window.addEventListener('resize', handleResize);
        return () => {
            window.removeEventListener('resize', handleResize);
        }

    }, []);

    return {
        isMobile,
        isLarge,
    };
}
