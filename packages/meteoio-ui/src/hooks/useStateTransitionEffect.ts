import {useEffect, useState} from "react";

export function useStateTransitionCallback<T>(state: T, callback: (prevState: T, newState: T) => void, deps: unknown[]) {
    const [prevState, setPrevState] = useState<T>(undefined)
    useEffect(() => {
        if (state !== prevState) {
            callback(prevState, state)
            setPrevState(state)
        }
    }, [state, ...deps])
}
