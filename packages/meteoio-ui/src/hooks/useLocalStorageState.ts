import {useCallback, useState} from "react";

export type StoreCallback<T> = (value: T) => void;

/**
 * Convenient typed react wrapper for localStorage
 *
 * Equivalent to useState, but slower, keyed and locally stored.
 *
 * @param key where the state shall be stored
 * @param fallback value when nothing is found in the local storage
 * @return [state, store]
 */
export default function useLocalStorageState<T>(key: string, fallback: T = undefined): [T, StoreCallback<T>] {
    const [state, setState] = useState<T>(() => {
        try {
            const str = localStorage.getItem(key)
            if (str !== null) {
                return JSON.parse(str) as T
            }
        } catch (e) {
            console.error(e);
        }
        return fallback
    });

    const store = useCallback<StoreCallback<T>>((value: T) => {
        setState(value)
        localStorage.setItem(key, JSON.stringify(value))
    }, [key])

    return [state, store]
}
