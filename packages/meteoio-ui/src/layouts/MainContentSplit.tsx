import * as React from 'react'
import {ReactNode} from 'react'
import {makeStyles, mergeClasses, shorthands, tokens} from "@fluentui/react-components";
import {useScreenSize} from "../hooks/useScreenSize";

const useStyles = makeStyles({
    root_h: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
    },
    root_v: {
        width: '100%',
    },
    main: {
        minWidth: '300px',
        flexGrow: 1,
        paddingLeft: tokens.spacingHorizontalXXXL,
        paddingRight: tokens.spacingHorizontalL,
    },
    nav: {
        width: '250px',
        flexShrink: 0,
    },
    nav_as_footer: {
        marginTop: tokens.spacingVerticalXXXL,
        ...shorthands.borderTop('medium', 'solid', tokens.colorNeutralStroke3),
        ...shorthands.padding(tokens.spacingVerticalXXXL, tokens.spacingHorizontalS, tokens.spacingVerticalS),
    },
    nav_vertical_line: {
        ...shorthands.borderRight('thin', 'solid', tokens.colorNeutralStroke2),
    },
    main_mobile: {
        // paddingLeft: 0,
        // paddingRight: 0,
        // NOTE: why did I previously put zero padding?
        paddingLeft: tokens.spacingHorizontalS,
        paddingRight: tokens.spacingHorizontalS,
    }
})

export const MainContentSplit: React.FC<React.PropsWithChildren<{
    nav?: ReactNode
    keepNavAbove?: boolean
    verticalLine?: boolean
}>> = props => {

    const {isMobile} = useScreenSize()

    const styles = useStyles()

    const navIsAbove = !isMobile || props.keepNavAbove

    const nav = props.nav
        ? <nav className={mergeClasses(
            isMobile ? (navIsAbove ? undefined : styles.nav_as_footer) : styles.nav,
            props.verticalLine && !isMobile && styles.nav_vertical_line,
        )}>{props.nav}</nav>
        : null

    return <div className={isMobile ? styles.root_v : styles.root_h}>
        {navIsAbove && nav}
        <main className={mergeClasses(styles.main, isMobile && styles.main_mobile)}>{props.children}</main>
        {!navIsAbove && nav}
    </div>
}
