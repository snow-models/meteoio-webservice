import * as React from 'react'
import {useScreenSize} from "../hooks/useScreenSize";
import {makeStyles, mergeClasses, shorthands} from "@fluentui/react-components";
import {tokens} from "@fluentui/react-theme";


const useStyles = makeStyles({
    root: {
        // display: 'flex',
        // flexDirection: 'column',
        // rowGap: tokens.spacingVerticalXXXL,

        '& > header': {
            // position: 'sticky',
            // top: '-25px',
            // zIndex: 2,
            paddingLeft: tokens.spacingHorizontalL,
            paddingRight: tokens.spacingHorizontalL,
            // paddingTop: tokens.spacingVerticalL,
            backgroundColor: tokens.colorNeutralBackground3,
            ...shorthands.borderBottom('thin', 'solid', tokens.colorNeutralStroke2),
        },
        '& > header > nav': {
            // marginTop: tokens.spacingVerticalL,
            backgroundColor: tokens.colorNeutralBackground3,
        }
    },
    header_v_spaced: {
        paddingTop: tokens.spacingVerticalL,
        '& > nav': {
            marginTop: tokens.spacingVerticalL,
        },
    },
    body: {
        marginTop: tokens.spacingVerticalXXXL,
    },
    horiz: {
        display: 'grid',
        gridTemplateColumns: 'auto 1fr auto',
        gridTemplateAreas: '"aside main right"',
        // ...shorthands.marginInline('auto'),
        width: '100%',
        // maxWidth: '1200px',
        justifyContent: 'center',
        // flexDirection: 'row',
        // flexWrap: 'wrap',
        rowGap: tokens.spacingVerticalXXXL,
        // paddingLeft: tokens.spacingHorizontalL,
        // paddingRight: tokens.spacingHorizontalL,
        boxSizing: 'border-box',

        '& > *': {
            maxWidth: '100vw',
            gridArea: 'main',
        },
        '& > main': {
            gridArea: 'main',
            //maxWidth: '66%',
            minWidth: '300px',
            paddingLeft: tokens.spacingHorizontalL,
            paddingRight: tokens.spacingHorizontalL,
        },
        '& > aside': {
            gridArea: 'aside',
            width: '300px',
            paddingLeft: tokens.spacingHorizontalXXXL,
            paddingRight: tokens.spacingHorizontalL,

            '&.right': {
                gridArea: 'right',
            }
        },
    },
    vert: {
        display: 'flex',
        flexDirection: 'column',
        rowGap: tokens.spacingVerticalXXXL,
        // paddingLeft: tokens.spacingHorizontalM,
        // paddingRight: tokens.spacingHorizontalM,

        '& > main': {
            paddingLeft: tokens.spacingHorizontalM,
            paddingRight: tokens.spacingHorizontalM,
        },
        '& > aside': {
            paddingLeft: tokens.spacingHorizontalM,
            paddingRight: tokens.spacingHorizontalM,
        },
    },
    horiz_medium: {
        // columnGap: tokens.spacingHorizontalXL,
        '& > aside': {
            paddingRight: tokens.spacingHorizontalXL,
        }
    },
    horiz_large: {
        // columnGap: tokens.spacingHorizontalXXXL,
        '& > aside': {
            paddingRight: tokens.spacingHorizontalXXXL,
        }
    },
})

export const HeaderMainAsideLayout: React.FC<{
    header?: React.ReactNode
    nav?: React.ReactNode
    alert?: React.ReactNode

    /** expects to find a <main> and an optional <aside> */
    body: React.ReactNode
}> = props => {
    const {isMobile, isLarge} = useScreenSize()
    const styles = useStyles()

    const head = (props.header || props.nav) ? <header className={props.header ? styles.header_v_spaced : undefined}>
        {props.header}
        <nav>
            {props.nav}
        </nav>
    </header> : null

    return <div className={styles.root}>
        {head}
        {props.alert}
        <div
            className={mergeClasses(styles.body, isMobile ? styles.vert : mergeClasses(styles.horiz, isLarge ? styles.horiz_large : styles.horiz_medium))}>
            {props.body}
        </div>
    </div>
}

// TODO: cleanup the usage of this components (design deviations led to anti-patterns)
