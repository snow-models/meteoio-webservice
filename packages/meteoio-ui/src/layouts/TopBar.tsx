import * as React from 'react'
import {ReactNode, useCallback, useEffect, useRef, useState} from 'react'
import {makeStyles, mergeClasses, shorthands, Text, tokens} from "@fluentui/react-components";
import {ArrowUpRegular} from "@fluentui/react-icons";
import {ThemeProvider} from "../providers/ThemeProvider";
import {createPortal} from "react-dom";
import {Stack} from "./Stack";
import {Button} from "../components/Button";
import {useScreenSize} from "../hooks/useScreenSize";

const useStyles = makeStyles({
    root: {
        display: 'flex',
        columnGap: tokens.spacingHorizontalL,
        flexDirection: 'row',
        // flexWrap: 'wrap-reverse',  // TODO?: maybe use this to avoid cutting the repo title?
        justifyContent: 'space-between',
        // ...shorthands.borderBottom('thin', 'solid', tokens.colorNeutralStroke3),
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        zIndex: 2,
        ...shorthands.transition([['box-shadow', '300ms']]),
    },
    nb1: {
        backgroundColor: tokens.colorNeutralBackground1,
    },
    nb3: {
        backgroundColor: tokens.colorNeutralBackground3,
    },
    rootWithShadow: {
        boxShadow: '0 0 15px -4px ' + tokens.colorBrandShadowAmbient,
    }
})

const FALLBACK_HEIGHT = 40


/** Top page header with dark theme, fixed position and above all layering with portal, without z-index mess. */
export const TopBar: React.FC<{
    start?: ReactNode
    center?: ReactNode
    end?: ReactNode
    dark?: boolean
    showScrollToTop?: boolean
    bg?: 'nb1' | 'nb3'
}> = props => {

    const {isMobile} = useScreenSize()
    const styles = useStyles()
    const [height, setHeight] = useState<number>(FALLBACK_HEIGHT)
    const headerRef = useRef<HTMLElement>()
    const [isAtTop, setIsAtTop] = useState<boolean>(true)

    const handleSizing = useCallback(() => {
        if (headerRef?.current) {
            setHeight(headerRef?.current?.getBoundingClientRect?.()?.height ?? FALLBACK_HEIGHT)
        }
    }, [headerRef])

    const handleScroll = useCallback(() => {
        setIsAtTop(window.scrollY <= 0)
    }, [])

    useEffect(() => {
        window.addEventListener('resize', handleSizing)
        window.addEventListener('scroll', handleScroll)
        handleSizing()
        setTimeout(handleSizing, 200)
    }, [])

    return <>
        <div style={{height}}>
        </div>
        {createPortal(
            <ThemeProvider dark={props.dark}>
                <header
                    className={mergeClasses(styles.root, !isAtTop && styles.rootWithShadow, styles[props.bg ?? 'nb1'])}
                    ref={headerRef}>
                    {props.start}
                    {props.showScrollToTop && !isAtTop && <>
                        <Button
                            noInlinePadding
                            appearance="subtle"
                            label={<Stack horizontal alignItems="center">
                                <Text size={400}><ArrowUpRegular/></Text>
                                {!isMobile && <Text size={200}>Scroll to top</Text>}
                            </Stack>}
                            onClick={() => document?.body?.scrollTo({
                                top: 0,
                                behavior: 'smooth',
                            })}
                        />
                    </>}
                    {props.center}
                    {props.end}
                </header>
            </ThemeProvider>,
            document?.body
        )}
    </>
}
