import * as React from 'react'
import {PropsWithChildren} from 'react'
import {HorizontalSpacingTokens, makeStyles, tokens} from "@fluentui/react-components";
import {Property} from "csstype";

const useStyles = makeStyles({
    vertical_root: {
        display: 'flex',
        flexDirection: 'column',
        flexShrink: 1,
        '& > *': {
            boxSizing: 'border-box',
            width: '100%',
            flexShrink: 1,
            marginLeft: 0,
            marginRight: 0,
        },
        width: '100%',
        boxSizing: 'border-box'
    },

    horizontal_root: {
        display: 'flex',
        flexDirection: 'row',
        flexShrink: 1,
    },

    rowFiller: {
        flexGrow: 999,
    },
})


export type GapSize =
    | 'None'
    | 'XXS'
    | 'XS'
    | 'S'
    | 'M'
    | 'L'
    | 'XL'
    | 'XXXL'


const GapSizeToHorizontalToken: Record<GapSize, HorizontalSpacingTokens[keyof HorizontalSpacingTokens]> = {
    None: tokens.spacingHorizontalNone,
    XXS: tokens.spacingHorizontalXXS,
    XS: tokens.spacingHorizontalXS,
    S: tokens.spacingHorizontalS,
    M: tokens.spacingHorizontalM,
    L: tokens.spacingHorizontalL,
    XL: tokens.spacingHorizontalXL,
    XXXL: tokens.spacingHorizontalXXXL,
}


const GapSizeToVerticalToken: Record<GapSize, HorizontalSpacingTokens[keyof HorizontalSpacingTokens]> = {
    None: tokens.spacingVerticalNone,
    XXS: tokens.spacingVerticalXXS,
    XS: tokens.spacingVerticalXS,
    S: tokens.spacingVerticalS,
    M: tokens.spacingVerticalM,
    L: tokens.spacingVerticalL,
    XL: tokens.spacingVerticalXL,
    XXXL: tokens.spacingVerticalXXXL,
}


export const Stack: React.FC<PropsWithChildren<{
    grow?: boolean
    horizontal?: boolean
    rowGap?: GapSize
    columnGap?: GapSize
    height?: number
    width?: number | string
    minWidth?: number | string
    maxWidth?: number | string
    wrap?: boolean
    justifyContent?: Property.JustifyContent
    alignItems?: Property.AlignItems
    className?: string
    center?: boolean
    overflow?: Property.Overflow
}>> = props => {

    const styles = useStyles()

    return <div
        className={`${props.horizontal ? styles.horizontal_root : styles.vertical_root}${props.className ? ' ' + props.className : ''}`}
        style={{
            rowGap: GapSizeToVerticalToken[props.rowGap ?? 'M'],
            columnGap: GapSizeToHorizontalToken[props.columnGap ?? 'M'],
            flexGrow: props.grow ? 1 : undefined,
            width: props.width,
            minWidth: props.minWidth,
            maxWidth: props.maxWidth,
            flexWrap: props.wrap ? 'wrap' : undefined,
            justifyContent: props.justifyContent,
            alignItems: props.alignItems,
            margin: props.center ? `0 auto` : undefined,
            overflow: props.overflow,
            height: props.height,
        }}>
        {props.children}
    </div>
}

export const RowFiller: React.FC = () => {
    const styles = useStyles()

    return <div className={styles.rowFiller}>&nbsp;</div>
}
